package com.logicpd.papapill.computervision.detection;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.computervision.BinCamera;
import com.logicpd.papapill.computervision.ImageUtility;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/*
 * This  class is to test images derived from a seperate coded solution for improved edge detection
 * this is the first step in integrating those test.  If this proves to be worthwhile, and improve
 * the experience, this code will be converted into functional android based code.  The pros,
 * theoritical improvement of around 5% on detection accuracy. cons, could be longer in processing
 * time.
 * Brady Hustad - Logic PD - 2019-01 - Initial Creation
 */
@RunWith(AndroidJUnit4.class)
public class NewEdgeTest {

    //Class Variables
    private static boolean setUpRun = false;
    private Detector mDetector;

    //Rules
    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    //****************************************************************************
    //Setup and Teardowns
    //****************************************************************************
    @Before
    public void init() {
        mDetector = new Detector();
    }

    /*
     * This test is for the Abilify pill.  The user should expect a picture of small white almost
     * rectangular pills with 1-3 red dots centered on the pills.
     */
    @Test
    public void AbilifyEdgeTest() {
        Bitmap pic = ImageUtility.retrieveImage("testImages/Abilify");
        assertThat(pic, instanceOf(Bitmap.class));

        Bitmap edge = ImageUtility.retrieveImage("testImages/Abilify_Edge1");
        assertThat(edge, instanceOf(Bitmap.class));

        DetectionData dd = mDetector.processImage(convertBitmap(pic), 1, true, convertBitmap(edge), true);

        ImageUtility.storeImage(dd.getSelectedPill(), "/testImages/results/Abilify");

        File dir = Environment.getExternalStorageDirectory();
        String fileToGet = dir.getPath() + "/Pictures/testImages/results/Abilify.jpg";
        File testFile = new File(fileToGet);

        assertTrue(testFile.exists());
        assertTrue(dd.getSelectedPills().size() > 0);
    }

    /*
     * This test is for acetaminophen 500mg capsules.  The user should expect to see banded
     * blue, white and red capsules.  Same red dots.
     */
    @Test
    public void Acetaminophen_500mgEdgeTest() {
        Bitmap pic = ImageUtility.retrieveImage("testImages/Acetaminophen_500mg");
        assertThat(pic, instanceOf(Bitmap.class));

        Bitmap edge = ImageUtility.retrieveImage("testImages/Acetaminophen_500mg_Edge1");
        assertThat(edge, instanceOf(Bitmap.class));

        DetectionData dd = mDetector.processImage(convertBitmap(pic), 1, true, convertBitmap(edge), true);

        ImageUtility.storeImage(dd.getSelectedPill(), "/testImages/results/Acetaminophen_500mg");

        File dir = Environment.getExternalStorageDirectory();
        String fileToGet = dir.getPath() + "/Pictures/testImages/results/Acetaminophen_500mg.jpg";
        File testFile = new File(fileToGet);

        assertTrue(testFile.exists());
        assertTrue(dd.getSelectedPills().size() > 0);
    }

    /*
     * This test is for Acyclovir.  The user should expect pink pentagon pills.
     * Same red dots.
     */
    @Test
    public void AcyclovirEdgeTest() {
        Bitmap pic = ImageUtility.retrieveImage("testImages/Acyclovir");
        assertThat(pic, instanceOf(Bitmap.class));

        Bitmap edge = ImageUtility.retrieveImage("testImages/Acyclovir_Edge1");
        assertThat(edge, instanceOf(Bitmap.class));

        DetectionData dd = mDetector.processImage(convertBitmap(pic), 1, true, convertBitmap(edge), true);

        ImageUtility.storeImage(dd.getSelectedPill(), "/testImages/results/Acyclovir");

        File dir = Environment.getExternalStorageDirectory();
        String fileToGet = dir.getPath() + "/Pictures/testImages/results/Acyclovir.jpg";
        File testFile = new File(fileToGet);

        assertTrue(testFile.exists());
        assertTrue(dd.getSelectedPills().size() > 0);
    }

    //***************************************************************************
    // Helper Methods
    //***************************************************************************
    private byte[] convertBitmap(Bitmap bmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        return stream.toByteArray();
    }


}
