package com.logicpd.papapill.espresso;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.fragments.my_medication.SelectPauseMedicationFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.utils.PreferenceUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

import org.apache.log4j.chainsaw.Main;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertFalse;

@RunWith(AndroidJUnit4.class)
public class JIRA191_PauseMedication extends BaseJiraTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    /*
     * populate database with mock contents
     */
    public void initialize() {
        super.initialize();
    }

    /*
     * Execute from Home to PIN entry fragment
     */
    @Test
    public void testPrePauseMedication() {
        // mock data for bin dispense
        initialize();

        int userId = userIds[0];

        // create dispense times
        int[] dispenseTimeIds = new int[3];
        long ONE_HOUR = 60 * 60 * 1000;
        Date now = new Date();
        String hourPrior = buildTimeString(now.getTime() - ONE_HOUR);
        dispenseTimeIds[0] = createDispenseTime(userId, "morning", hourPrior);
        String hourLater = buildTimeString(now.getTime() + ONE_HOUR);
        dispenseTimeIds[1] = createDispenseTime(userId, "lunch", hourLater);
        String fivehourLater = buildTimeString(now.getTime() + ONE_HOUR*2);
        dispenseTimeIds[2] = createDispenseTime(userId, "dinner", fivehourLater);

        // create schedule
        int[] scheduleIds = new int[6];
        Date morningProcessDate = new Date(now.getTime() - ONE_HOUR);
        scheduleIds[0] = createSchedule(userId, medIds[0], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);
        scheduleIds[1] = createSchedule(userId, medIds[2], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);

        Date lunchProcessDate = new Date(now.getTime() + ONE_HOUR);
        scheduleIds[2] = createSchedule(userId, medIds[0], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);
        scheduleIds[3] = createSchedule(userId, medIds[2], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);

        Date dinnerProcessDate = new Date(now.getTime() + ONE_HOUR*2);
        scheduleIds[4] = createSchedule(userId, medIds[0], dispenseTimeIds[2], ScheduleRecurrenceEnum.DAILY, null, null, dinnerProcessDate);
        scheduleIds[5] = createSchedule(userId, medIds[2], dispenseTimeIds[2], ScheduleRecurrenceEnum.DAILY, null, null, dinnerProcessDate);

        SystemClock.sleep(1000);

        // Home fragment -> ManageMed   "GET/VIEW MEDICATIONS"
        Button btnMed = mActivityRule.getActivity().findViewById(R.id.button_my_medication);
        assertFalse(btnMed == null);
        onView(withId(R.id.button_my_medication)).perform(click());

        // ManageMed -> Pause Medication    "PAUSE MEDICATION"     MyMedicationFragment
        onView(withId(R.id.button_pause_med)).perform(click());

        // Pause Medication -> User 'abby'                         SelectUserForGetMedsFragment
        onView(withText("abby")).perform(click());

        // TODO push 'enter' button
        // Enter system key, enter -> medication list
       // onView(withId(R.id.edittext_system_key)).perform(typeText("1234"), pressImeActionButton());

        cleanup();
    }

    /*
     * Execute through the critical Paused Medication wireframes after PIN screen.
     * NOTE: no need to extend MainActivity for proxy pattern; Alan has 'addFragment()' already !
     * We can directly jump into any particular fragment as starting point.
     */
    @Test
    public void testHappyPath() {
        // mock data for bin dispense
        initialize();

        // load 1st wireframe fragment of this workflow
        int userId = userIds[0];
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        user = model.getUserByID(userId);
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);

        // SelectPauseMedicationFragment
        MainActivity main = mActivityRule.getActivity();
        Fragment fragment = new SelectPauseMedicationFragment();
        main.addFragment(fragment, bundle, SelectPauseMedicationFragment.class.getName());

        // select button - select all
        onView(withId(R.id.button_select)).perform(click());

        // select button - next
        onView(withId(R.id.button_next)).perform(click());

        // MedicationsPausedFragment, select button - done
        onView(withId(R.id.button_done)).perform(click());

        cleanup();
    }
}
