package com.logicpd.papapill.espresso;

import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.fragments.my_medication.AlarmReceivedFragment;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.wireframes.BaseWireframe;

import org.bouncycastle.jce.provider.symmetric.ARC4;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

/*
 * Use idleResource detection and wait() for correct view timing.
 * https://stackoverflow.com/questions/50628219/is-it-possible-to-use-espressos-idlingresource-to-wait-until-a-certain-view-app
 */
public class JIRA275_MultiUsersDispense extends BaseJiraTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    /*
     * populate database with mock contents
     */
    @Override
    public void initialize() {
        super.initialize();
    }

    /*
     * wait for view to idle after display
     */
    private void waitUntilViewIsDisplayed(Matcher<View> matcher) {
        IdlingResource idlingResource = new ViewIdlingResource(matcher, isDisplayed());
        try {
            IdlingRegistry.getInstance().register(idlingResource);
            // First call to onView is to trigger the idler.
            onView(withId(0)).check(doesNotExist());
        } finally {
            IdlingRegistry.getInstance().unregister(idlingResource);
        }
    }
    /*
     * Create schedule instances for 2 users that occur at the same time.
     * 1. Alarm will present both selections.
     * 2. User will be able to select the preferred instance.
     * 3. System will process as scheduled for full dispense.
     * 4. Upon completion, Alarm will present subsequent user.
     * 5. User will be able to select subsequent instance for 2nd (final) dispense.
     */
    @Test
    public void testHappyPathA() {
        // mock data for bin dispense
        initialize();
        addHappyPathData();

        // create bundle
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        user = model.getUserByID(userIds[0]);
        List<JoinScheduleDispense> list = model.getNonPausedJoinScheduleDispenseByUserId(user.getId());
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        bundle.putSerializable("scheduleItem", list.get(0));

        // load fragment
        MainActivity main = mActivityRule.getActivity();
        Fragment fragment = new AlarmReceivedFragment();
        main.addFragment(fragment, bundle, AlarmReceivedFragment.class.getName());


        // click button User A              AlarmReceivedFragment
       // waitUntilViewIsDisplayed(withId(R.id.button_user_a));
        onView(withId(R.id.button_user_a)).perform(click());

        clickCommonScreens();


        // click button User B              AlarmReceivedFragment
       // onView(withId(R.id.button_user_b)).perform(click());

        //clickCommonScreens();

        cleanup();
    }

    @Test
    public void testHappyPathB() {
        // mock data for bin dispense
        initialize();
        addHappyPathData();

        // create bundle
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        user = model.getUserByID(userIds[1]);
        List<JoinScheduleDispense> list = model.getNonPausedJoinScheduleDispenseByUserId(user.getId());
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        bundle.putSerializable("scheduleItem", list.get(0));

        // load fragment
        MainActivity main = mActivityRule.getActivity();
        Fragment fragment = new AlarmReceivedFragment();
        main.addFragment(fragment, bundle, AlarmReceivedFragment.class.getName());

        // click button User B              // AlarmReceivedFragment
       // waitUntilViewIsDisplayed(withId(R.id.button_user_b));
        onView(withId(R.id.button_user_b)).perform(click());

        clickCommonScreens();


        // click button User A              AlarmReceivedFragment
       // waitUntilViewIsDisplayed(withId(R.id.button_user_a));
//        onView(withId(R.id.button_user_a)).perform(click());

//        clickCommonScreens();

        cleanup();
    }

    private void clickCommonScreens() {


        // click button PROCEED (DEV)       DispenseMedsFragment
        //waitUntilViewIsDisplayed(withId(R.id.button_developer));
        onView(withId(R.id.button_developer)).perform(click());

        // click button PROCEED (DEV)       DispensingMedsFragment
        onView(withId(R.id.button_developer)).perform(click());

        // click button OK                  TakeMedsFragment
        onView(withId(R.id.button_ok)).perform(click());

        // click button OK                  ReturnCupFragment
        onView(withId(R.id.button_ok)).perform(click());

        // click button OK                  ScheduledMedicationTakenFragment
        onView(withId(R.id.button_ok)).perform(click());

        SystemClock.sleep(2000);
    }

    private void addHappyPathData() {

        int[] dispenseTimeIds = new int[2];
        long ONE_HOUR = 60 * 60 * 1000;
        Date now = new Date();
        int[] scheduleIds = new int[2];

        medIds[4] = insertMedication(userIds[1], "Epi Pen", "1mg", 4, 5,"britany", MedScheduleTypeEnum.BOTH, MedPausedEnum.NOT_PAUSED);

        for(int i=0; i<2; i++) {
            int userId = userIds[i];
            String hourNow = buildTimeString(now.getTime());
            dispenseTimeIds[i] = createDispenseTime(userId, "lunch", hourNow);

            // create schedule
            Date lunchProcessDate = new Date(now.getTime());
            int medId = (0==i)?medIds[0] : medIds[4];
            scheduleIds[i] = createSchedule(userId, medId, dispenseTimeIds[i], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);
        }
        SystemClock.sleep(1000);
    }

    private void addOneUserData() {

        int[] dispenseTimeIds = new int[2];
        long ONE_HOUR = 60 * 60 * 1000;
        Date now = new Date();
        int[] scheduleIds = new int[2];

        int userId = userIds[0];
        String hourNow = buildTimeString(now.getTime());
        dispenseTimeIds[0] = createDispenseTime(userId, "lunch", hourNow);

        // create schedule
        Date lunchProcessDate = new Date(now.getTime());
        scheduleIds[0] = createSchedule(userId, medIds[0], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);
        SystemClock.sleep(1000);
    }

    @Test
    public void testOneUserA() {
        // mock data for bin dispense
        initialize();
        addOneUserData();

        // create bundle
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        user = model.getUserByID(userIds[0]);
        List<JoinScheduleDispense> list = model.getNonPausedJoinScheduleDispenseByUserId(user.getId());
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        bundle.putSerializable("scheduleItem", list.get(0));

        // load fragment
        MainActivity main = mActivityRule.getActivity();
        Fragment fragment = new AlarmReceivedFragment();
        main.addFragment(fragment, bundle, AlarmReceivedFragment.class.getName());

        // click button User A             // AlarmReceivedFragment
        onView(withId(R.id.button_user_a)).perform(click());

        clickCommonScreens();

        cleanup();
    }
}
