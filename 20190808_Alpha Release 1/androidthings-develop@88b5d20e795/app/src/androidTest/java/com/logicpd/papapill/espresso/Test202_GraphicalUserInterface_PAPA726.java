package com.logicpd.papapill.espresso;



import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.repositories.SystemRepository;
import com.logicpd.papapill.utils.PreferenceUtils;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/*
1.	Power on the Papapill device.
2.	Verify that after boot, the LCD screen displays a home menu.
 */
@RunWith(AndroidJUnit4.class)
public class Test202_GraphicalUserInterface_PAPA726
{
        @Rule
        public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
                MainActivity.class);

        @BeforeClass
        static public void Initialize()
        {
            /*
            To show fragment_home page, below parameters must be met
            !! NOTE !! database maybe in an invalid state for use.
            1. preference NOT 1st time run
            2. db systemkey != null
          */
            Context appContext = InstrumentationRegistry.getTargetContext();
            PreferenceUtils.setFirstTimeRun(false);

            //DatabaseHelper db = new DatabaseHelper(appContext);
            new SystemRepository().insert(AppConstants.DEFAULT_SYSTEM_KEY);
        }

        @Test
        public void MedicationButtonDisplayed()
        {
            /*
               Is there a home menu in here ?
             */
            Button btnMed = mActivityRule.getActivity().findViewById(R.id.button_my_medication);
            assertFalse(btnMed == null);

            Button btnUser = mActivityRule.getActivity().findViewById(R.id.button_user_settings);
            assertFalse(btnUser == null);

            Button btnSys = mActivityRule.getActivity().findViewById(R.id.button_system_manager);
            assertFalse(btnSys == null);

            Button btnHelp = mActivityRule.getActivity().findViewById(R.id.button_help);
            assertFalse(btnHelp == null);
        }
}
