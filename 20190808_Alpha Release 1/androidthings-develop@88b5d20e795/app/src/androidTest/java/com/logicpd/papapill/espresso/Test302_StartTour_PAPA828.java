package com.logicpd.papapill.espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.LinearLayout;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.utils.PreferenceUtils;

import static android.support.test.espresso.Espresso.onView;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertFalse;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/*
 1.	See Test 301 for setup
 2.	Select the Start Tour option
 3.	Verify the device overview tour start page is displayed
 */
@RunWith(AndroidJUnit4.class)
public class Test302_StartTour_PAPA828
{
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @BeforeClass
    static public void Initialize()
    {
        /*
            To show welcome screen (fragment_power_on_welcome.xml)
            !! NOTE !! wipe out database to set initial state.

            1. preference 1st time run
            2. db systemkey == null
          */
        Context appContext = InstrumentationRegistry.getTargetContext();
        PreferenceUtils.setFirstTimeRun(true);

        AppDatabase.deleteAll();
        //db.deleteSystemKeys();
    }

    @AfterClass
    static public void Cleanup()
    {
        Context appContext = InstrumentationRegistry.getTargetContext();
        PreferenceUtils.setFirstTimeRun(true);
    }

    @Test
    public void SelectStartTourPage()
    {
            //1. perform a click selection of start tour.
            //2. verify device overview tour starts
        onView(withText("START TOUR")).perform(click());
         // Might want to use idleResource if necessary
         // https://github.com/yeuchi/AdvancedAndroid_TeaTime-TESP.00-StartingCode/blob/master/app/src/androidTest/java/com/example/android/teatime/IdlingResourceMenuActivityTest.java
        LinearLayout layout = mActivityRule.getActivity().findViewById(R.id.fragment_power_on_welcome);
        assertFalse(null==layout);
    }
}
