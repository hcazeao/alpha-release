package com.logicpd.papapill.espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.utils.PreferenceUtils;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertFalse;

/*
 1.	See Test 304 for setup
 2.	Verify after the training modules are complete, the first time setup wizard is displayed
 3.	Click continue.
 4.	Verify a User Agreement is presented to the user.
 5.	Select the Checkbox acknowledging the agreement.
 6.	Select the accept button.
 7.	Verify the screen progresses.
 */
@RunWith(AndroidJUnit4.class)
public class Test305_FirstTimeSetup_PAPA832_833 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @BeforeClass
    static public void Initialize()
    {
        /*
            To show welcome screen (fragment_power_on_user_agreement.xml)
            !! NOTE !! wipe out database to set initial state.

            1. preference 1st time run
            2. db systemkey == null
          */
        Context appContext = InstrumentationRegistry.getTargetContext();
        PreferenceUtils.setFirstTimeRun(true);

        //DatabaseHelper db = new DatabaseHelper(appContext);
        AppDatabase.deleteAll();
        //db.deleteSystemKeys();
    }

    @Test
    public void SelectSetup()
    {
        onView(withText("START SETUP")).perform(click());
        /*
         * Might want to use idleResource if necessary
         * https://github.com/yeuchi/AdvancedAndroid_TeaTime-TESP.00-StartingCode/blob/master/app/src/androidTest/java/com/example/android/teatime/IdlingResourceMenuActivityTest.java
         */
        LinearLayout layout = mActivityRule.getActivity().findViewById(R.id.fragment_power_on_user_agreement);
        assertFalse(null==layout);

        TextView textView = mActivityRule.getActivity().findViewById(R.id.txtUserAgreement);
        assertFalse(null==textView);

    }
}
