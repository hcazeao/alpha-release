package com.logicpd.papapill.espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.LinearLayout;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.utils.PreferenceUtils;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertFalse;

import android.widget.TextView;

import org.hamcrest.Matcher;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
/*
 1.	Setup a WPA2 Personal Wi-Fi Router
 2.	See Test 307 for setup
 3.	Verify the new network is displayed in the list of available networks.
 4.	Connect to the network and enter the password for the WPA2 Personal Router.
 5.	Verify an IP address is assigned to the device.
 */
@RunWith(AndroidJUnit4.class)
public class Test308_NetworkConnection_PAPA835_788 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @BeforeClass
    static public void Initialize()
    {
        /*
         * wipe out database to intialize setup
         */
        Context appContext = InstrumentationRegistry.getTargetContext();
        PreferenceUtils.setFirstTimeRun(true);

        AppDatabase.deleteAll();
    }

    @Test
    public void Select2WiFiConfiguration()
    {
        /*
            Setup Wizard -> User agreement -> Connect to wireless network -> select a network
         */

        // -> User agreement [fragment_power_on_welcome] [R.id.button_start_setup]
        onView(withText("START SETUP")).perform(click());
        LinearLayout layoutAgreement = mActivityRule.getActivity().findViewById(R.id.fragment_power_on_user_agreement);
        assertFalse(null==layoutAgreement);

        // -> Connect to wireless network [fragment_power_on_user_agreement] [R.id.button_agree]
        onView(withText("I AGREE")).perform(click());
        LinearLayout layoutSysKey = mActivityRule.getActivity().findViewById(R.id.fragment_system_key);
        assertFalse(null==layoutSysKey);

        /*
         * work in progres:

        onView(withId(R.id.edittext_system_key)).perform(setTextInTextView("1234"));
        LinearLayout layoutWireless = mActivityRule.getActivity().findViewById(R.id.fragment_system_settings_wireless);
        assertFalse(null==layoutWireless);

        // -> Select a network [R.id.button_wifi]
        onView(withText("WI-FI")).perform(click());

        // enter system key

        onView(withId(R.id.button_ok)).perform(click());

        // -> Select network
        LinearLayout layoutwifi = mActivityRule.getActivity().findViewById(R.id.fragment_system_settings_select_wifi);
        assertFalse(null==layoutwifi);

        // find network from list
        onView(withText("Superior-Dev")).perform(click());
        onView(withText("CONNECT")).perform(click());

        // select, enter credentials
        onView(withId(R.id.textPassword)).perform(setTextInTextView("Builder_5424_"));
*/
        // check ip address
    }

    public static ViewAction setTextInTextView(final String value){
        return new ViewAction() {
            @SuppressWarnings("unchecked")
            @Override
            public Matcher<View> getConstraints() {
                return allOf(isDisplayed(), isAssignableFrom(TextView.class));
//                                            ^^^^^^^^^^^^^^^^^^^
// To check that the found view is TextView or it's subclass like EditText
// so it will work for TextView and it's descendants
            }

            @Override
            public void perform(UiController uiController, View view) {
                ((TextView) view).setText(value);
            }

            @Override
            public String getDescription() {
                return "replace text";
            }
        };
    }
}
