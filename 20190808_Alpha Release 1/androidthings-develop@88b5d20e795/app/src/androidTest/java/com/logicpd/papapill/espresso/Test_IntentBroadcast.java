package com.logicpd.papapill.espresso;

import android.app.Fragment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.app.FragmentManager;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.IntentTypeEnum;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.receivers.IntentBroadcast;
import com.logicpd.papapill.utils.PreferenceUtils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_IntentBroadcast extends Fragment implements IntentMsgInterface
{
    private String mCommand;
    private String mState;
    private String mMsg;
    private String mVersion;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void testEnd2End()
    {
        MainActivity main = mActivityRule.getActivity();
        main.addFragment(this, null, this.getClass().getSimpleName());

        mCommand = this.getClass().getSimpleName();
        mState = "state";
        mMsg = IntentMsgEnum.success.toString();
        mVersion = "version";
        IntentBroadcast.send(IntentTypeEnum.msg, mCommand, mState, mMsg, mVersion);
    }

    public boolean handleIntent(String command,
                                String state,
                                String msg,
                                String version) {
        assertEquals(mCommand, command);
        assertEquals(mState, state);
        assertEquals(mMsg, msg);
        assertEquals(mVersion, version);
        return true;
    }
}
