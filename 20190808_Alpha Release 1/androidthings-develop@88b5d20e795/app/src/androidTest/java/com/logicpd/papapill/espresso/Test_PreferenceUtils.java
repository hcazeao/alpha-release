package com.logicpd.papapill.espresso;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.utils.PreferenceUtils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_PreferenceUtils
{
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void testSetCameraIsCalibrated()
    {
        PreferenceUtils.setCameraIsCalibrated(true);
        boolean isCalibrated = PreferenceUtils.getIsCameraCalibrated();
        assertEquals(isCalibrated, true);
    }

    @Test
    public void testSetCameraIsNotCalibrated()
    {
        PreferenceUtils.setCameraIsCalibrated(false);
        boolean isCalibrated = PreferenceUtils.getIsCameraCalibrated();
        assertEquals(isCalibrated, false);
    }

    @Test
    public void testSetFirstTimeRun()
    {
        PreferenceUtils.setFirstTimeRun(true);
        boolean isFirstTimeRun = PreferenceUtils.getFirstTimeRun();
        assertEquals(isFirstTimeRun, true);
    }

    @Test
    public void testSetNotFirstTimeRun()
    {
        PreferenceUtils.setFirstTimeRun(false);
        boolean isFirstTimeRun = PreferenceUtils.getFirstTimeRun();
        assertEquals(isFirstTimeRun, false);
    }

    @Test
    public void testSetSSID()
    {
        PreferenceUtils.setSSID("SSID");
        String ssid = PreferenceUtils.getSSID();
        assertEquals(ssid, "SSID");
    }

    @Test
    public void testSetClearSSID()
    {
        PreferenceUtils.setSSID(null);
        String ssid = PreferenceUtils.getSSID();
        assertEquals(ssid, null);
    }

    @Test
    public void testSetCarouselPosInterval()
    {
        PreferenceUtils.setCarouselPosInterval(2000);
        int interval = PreferenceUtils.getCarouselPosInterval();
        assertEquals(interval, 2000);
    }
}
