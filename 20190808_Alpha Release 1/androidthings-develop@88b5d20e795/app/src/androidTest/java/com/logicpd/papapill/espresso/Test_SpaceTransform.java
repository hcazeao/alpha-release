package com.logicpd.papapill.espresso;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.computervision.ImageUtility;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.models.CoordinateData;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_SpaceTransform {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    /*
     *
     */
    @Test
    public void testTransform() {

        mActivityRule.getActivity().stopMedicationScheduleService();
        /*
         * here is the loop, copied from Class Imageutility to perform dot creation.
         */
        //AppConfig dConfig = AppConfig.getInstance();
        double XPixels = 1048;
        double YPixels = 768;
        double height = 0;

        double rho0=85.732;
        double rotation=0.01308996938995747;
        double m=-0.00067934;
        double b=0.1552735;
        double convergentTheta=-0.017453292519943295;
        double convergentRho=85.232;
        double XPixel=1024;
        double YPixel=768;
        double convergentPixelX=498;
        double convergentPixelY=384;
        //double fill=0.0;
       // double mImageUpperLeftXmm=-162.54522175356954;
       // double mImageUpperLeftYmm=-58.137520494537455;


        double mImageConvergeXmm = -1 * Math.cos(convergentTheta) * convergentRho; // X Convergence point in millimeters, negative sign to put small numbers at left
        double mImageConvergeYmm = Math.sin(-1 * convergentTheta) * convergentRho; // Y Convergence point in millimeters, negative sign to put small numbers at top

        // fill is a value in percent full ranging between 0-100
        double fill = Math.abs(height);
        if (fill > 100) {
            fill = 100;
        }

        double mImageUpperLeftXmm = mImageConvergeXmm - (m * fill + b) * convergentPixelX; // upper left X position in millimeters
        double mImageUpperLeftYmm = mImageConvergeYmm - (m * fill + b) * convergentPixelY; // upper left Y position in millimeters

        double Xabs, Yabs, Xrel, Yrel;
        double PixelX, PixelY;

        /*
         * create points
         */
        List<CoordinateData> ListRhoTheta = new ArrayList<CoordinateData>();
        List<Point> ListXy = new ArrayList<Point>();

        for (double lrho = -30; lrho <= 30; lrho += 10){
            for (double ltheta = -9; ltheta <= 9; ltheta += 3) {
                double rtheta = Math.toRadians(ltheta);

                CoordinateData rhoTheta = new CoordinateData();
                rhoTheta.radius =lrho;
                rhoTheta.degrees =ltheta;
                ListRhoTheta.add(rhoTheta);

                //Do rotation
                //Get absolute x,y of real world (mm in both directions) with the 0,0 base at convergence
                Xabs = -1 * Math.cos(rtheta) * (lrho + rho0); // Absolute X position in millimeters, small numbers left
                Yabs = Math.sin(-1 * rtheta) * (lrho + rho0); // Absolute Y position in millimeters, small numbers top

                double Xbase = Xabs - mImageConvergeXmm;
                double Ybase = Yabs - mImageConvergeYmm;

                double rotTheta = rotation;
                double Xrot = (Math.cos(rotTheta) * Xbase - Math.sin(rotTheta) * Ybase) + mImageConvergeXmm;
                double Yrot = (Math.sin(rotTheta) * Xbase + Math.cos(rotTheta) * Ybase) + mImageConvergeYmm;

                Xrel = Xrot - mImageUpperLeftXmm; // Relative X position in millimeters, 0 at UL
                Yrel = Yrot - mImageUpperLeftYmm; // Relative Y position in millimeters, 0 at UL

                PixelX = Xrel / (m * fill + b); // Convert millimeters back to pixels
                PixelY = Yrel / (m * fill + b); // Convert millimeters back to pixels


                // Typecast PixelX and PixelY back to int and use to draw dots on the screen where
                // we expect the calibration intersections to be.
                // Verify that pixels are within image extents before plotting.
                // Overlay this with the original image for a given height.  If this looks good,
                // we can move on to the next step to determine
                // rho and theta based on the pixel X, Y.
                if (PixelX >= 0 && PixelY >=0 && PixelX <= XPixels && PixelY <= YPixels) {
                    Point cl = new Point(PixelX, PixelY);
                    ListXy.add(cl);
                    //Imgproc.circle(matUndistort, cl, 3, new Scalar(0, 0, 255), 3, 8, 0);
                }
            }
        }

        /*
         * inverse mapping and compare value
         */

        Log.d("test", "ListXy.size:"+ListXy.size());
        int i = 0;
        for(Point xy : ListXy){
            CoordinateData convPoint = ImageUtility.convertPoint(xy.x,
                                                                xy.y,
                                                                    height);

            CoordinateData rhoTheta = ListRhoTheta.get(i++);
            double diff = Math.abs(convPoint.radius- rhoTheta.radius);
            boolean isBigError = (diff > 1.1)?true:false;
            assertEquals(isBigError, false);


            double diffDegree = Math.abs(convPoint.degrees- rhoTheta.degrees);
            boolean isBigDegree = (diffDegree > 1.1)?true:false;
            assertEquals(isBigDegree, false);

            Log.d("TEST", i+"x:"+xy.x+ "y:"+xy.y+" convPoint radius:"+convPoint.radius+" rhoTheta radius:"+rhoTheta.radius + "convPoint degree:"+convPoint.degrees+" rhoTheta degree:"+rhoTheta.degrees);
        }
    }

}
