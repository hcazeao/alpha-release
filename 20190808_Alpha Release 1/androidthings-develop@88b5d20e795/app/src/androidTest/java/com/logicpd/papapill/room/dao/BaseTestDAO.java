package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.Rule;

import java.util.ArrayList;
import java.util.List;

public class BaseTestDAO {
    protected AppDatabase db = null;
    protected Long[] userIds;

    public void initialize()
    {
        db = AppDatabase.getDatabase(App.getContext());
        db.userDao().deleteAll();
        createUserEntities();
    }

    protected void createUserEntities()
    {
        // insert
        List<UserEntity> listUsers = new ArrayList<UserEntity>();
        for(int i=0; i<10; i++) {
            UserEntity user = new UserEntity();
            user.setUserName("user" + i);
            user.setPatientName("patient"+i);
            user.setPin("pin+i");
            listUsers.add(user);
        }
        userIds = db.userDao().insertAll(listUsers);
    }
}
