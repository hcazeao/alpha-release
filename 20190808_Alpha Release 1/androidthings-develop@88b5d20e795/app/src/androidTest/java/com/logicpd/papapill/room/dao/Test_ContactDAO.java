package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_ContactDAO extends BaseTestDAO {

    public void cleanup()
    {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.contactDao().deleteAll();
        db.userDao().deleteAll();
    }

    @Test
    public void testCRUDSingleRow()
    {
        if(null==db)
            initialize();

        // create
        ContactEntity contactInsert = new ContactEntity();
        int userId = (int)(long)userIds[0];
        contactInsert.setUserId(userId);
        contactInsert.setName("name");
        contactInsert.setTextNumber("TextNumber");
        contactInsert.setVoiceNumber("VoiceNumber");
        contactInsert.setEmail("Email");
        contactInsert.setCategory("Category");
        contactInsert.setSelected(true);
        contactInsert.setTextSelected(false);
        contactInsert.setVoiceSelected(true);
        contactInsert.setEmailSelected(false);
        contactInsert.setRelationship(2);

        // delete
        db.contactDao().deleteAll();

        // insert
        Long id = db.contactDao().insert(contactInsert);

        // read
        ContactEntity contactRead = db.contactDao().get((int)(long)id);
        assertEquals(contactRead.getUserId(), userId);
        assertEquals(contactRead.getName(), "name");
        assertEquals(contactRead.getTextNumber(), "TextNumber");
        assertEquals(contactRead.getVoiceNumber(), "VoiceNumber");
        assertEquals(contactRead.getEmail(), "Email");
        assertEquals(contactRead.getCategory(), "Category");
        assertEquals(contactRead.isSelected(), true);
        assertEquals(contactRead.isTextSelected(), false);
        assertEquals(contactRead.isVoiceSelected(), true);
        assertEquals(contactRead.isEmailSelected(), false);
        assertEquals(contactRead.getRelationship(), 2);

        // update
        ContactEntity contactUpdate = contactInsert;
        contactUpdate.setId((int)(long)id);
        contactUpdate.setName("updatedContact");
        db.contactDao().update(contactUpdate);

        // read
        contactRead = db.contactDao().get((int)(long)id);
        assertEquals(contactRead.getName(), "updatedContact");

        // delete by id
        db.contactDao().delete((int)(long)id);

        // verify deleted !
        ContactEntity deleted = db.contactDao().get((int)(long)id);
        assertEquals(deleted, null);
        cleanup();
    }

    @Test
    public void testCRUDManyRows()
    {
        if(null==db)
            initialize();

        // delete
        db.contactDao().deleteAll();

        // insert
        List<ContactEntity> list = createManyRows();
        db.contactDao().insertAll(list);

        // read
        List<ContactEntity> listRead = db.contactDao().getAll();
        int i = 0;
        for(ContactEntity contactRead : listRead) {
            int userId = (int)(long)userIds[i];
            assertEquals(contactRead.getUserId(), userId);
            assertEquals(contactRead.getName(), "name"+i);
            assertEquals(contactRead.getTextNumber(), "TextNumber"+i);
            assertEquals(contactRead.getVoiceNumber(), "VoiceNumber"+i);
            assertEquals(contactRead.getEmail(), "Email"+i);
            assertEquals(contactRead.getCategory(), "Category"+i);
            assertEquals(contactRead.isSelected(), true);
            assertEquals(contactRead.isTextSelected(), false);
            assertEquals(contactRead.isVoiceSelected(), true);
            assertEquals(contactRead.isEmailSelected(), false);
            assertEquals(contactRead.getRelationship(), 2+i);
            i++;
        }
        cleanup();
    }

    @Test
    public void testCRUDByUserId()
    {
        if(null==db)
            initialize();

        // delete
        db.contactDao().deleteAll();

        // insert
        List<ContactEntity> list = createManyRows();
        db.contactDao().insertAll(list);

        /*
         * select by Userid
         */

        // expect to NOT find anything
        List<ContactEntity> nonExist = db.contactDao().getByUserId(-5);
        assertEquals(nonExist.size(), 0);

        // expect to find 1 row
        int id = (int)(long)userIds[0];
        List<ContactEntity> listRead = db.contactDao().getByUserId(id);
        assertEquals(listRead.size(), 1);
        assertEquals(listRead.get(0).getUserId(), id);

        // check content
        ContactEntity contactRead = listRead.get(0);
        assertEquals(contactRead.getUserId(), id);
        assertEquals(contactRead.getName(), "name0");
        assertEquals(contactRead.getTextNumber(), "TextNumber0");
        assertEquals(contactRead.getVoiceNumber(), "VoiceNumber0");
        assertEquals(contactRead.getEmail(), "Email0");
        assertEquals(contactRead.getCategory(), "Category0");
        assertEquals(contactRead.isSelected(), true);
        assertEquals(contactRead.isTextSelected(), false);
        assertEquals(contactRead.isVoiceSelected(), true);
        assertEquals(contactRead.isEmailSelected(), false);
        assertEquals(contactRead.getRelationship(), 2);

        /*
         * delete by user id
         */
        db.contactDao().deleteByUserId(2);
        List<ContactEntity> deletedContact = db.contactDao().getByUserId(0);
        assertEquals(deletedContact.size(), 0);
        cleanup();
    }

    /*@Test
    public void testCascade()
    {
        if(null==db)
            initialize();

        // delete
        db.userDao().deleteAll();
        db.contactDao().deleteAll();

        // insert
        UserEntity user = new UserEntity();
        user.setUserName("user");
        Long userId = db.userDao().insert(user);

        ContactEntity contact = new ContactEntity();
        contact.setUserId((int)(long)userId);
        Long contactId = db.contactDao().insert(contact);

        // cascade delete automatically removes dependencies
        db.userDao().delete((int)(long)userId);
        ContactEntity nonExist = db.contactDao().get((int)(long)contactId);
        assertEquals(nonExist, null);
    }*/

    private List<ContactEntity> createManyRows()
    {
        // create many BinEntities
        List<ContactEntity> list = new ArrayList<ContactEntity>();
        for(int i=0; i<10; i++)
        {
            ContactEntity contactInsert = new ContactEntity();
            int id = (int)(long)userIds[i];
            contactInsert.setUserId(id);
            contactInsert.setName("name"+i);
            contactInsert.setTextNumber("TextNumber"+i);
            contactInsert.setVoiceNumber("VoiceNumber"+i);
            contactInsert.setEmail("Email"+i);
            contactInsert.setCategory("Category"+i);
            contactInsert.setSelected(true);
            contactInsert.setTextSelected(false);
            contactInsert.setVoiceSelected(true);
            contactInsert.setEmailSelected(false);
            contactInsert.setRelationship(2+i);
            list.add(contactInsert);
        }
        return list;
    }

}
