package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_JoinScheduleDispenseTime extends BaseTestDAO{

    private Long[] scheduleIds;
    private Long[] dispenseIds;
    private Long[] medicationIds;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    public void cleanup() {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.userDao().deleteAll();
        db.scheduleDao().deleteAll();
        db.medicationDao().deleteAll();
        db.dispenseTimeDao().deleteAll();
    }

    @Test
    public void testGetJoinById() {
        if(null==db)
            initialize();

        setupDB();

        int scheduleId = (int)(long)scheduleIds[0];
        JoinScheduleDispense read = db.scheduleDao().getJoinById(scheduleId);

        int userId = (int)(long)userIds[0];
        assertEquals(read.getScheduleId(), scheduleId);

        int dispenseId = (int)(long)dispenseIds[0];
        assertEquals(read.getDispenseTimeId(), dispenseId);

        assertEquals(read.getUserId(), userId);
        assertEquals(read.getDispenseAmount(), 5);
        int medId = (int)(long)medicationIds[0];
        assertEquals(read.getMedicationId(), medId);
        assertEquals(read.getRecurrence(), 6);
        assertEquals(read.getScheduleDate(), "date0");
        assertEquals(read.getScheduleDay(), "day0");
        assertEquals(read.getDispenseName(), "name0");
        assertEquals(read.getDispenseTime(), "02:10:10");
        assertEquals(read.isActive(), true);
        cleanup();
    }

    @Test
    public void testGetJoin() {
        if(null==db)
            initialize();

        setupDB();

        List<JoinScheduleDispense> listRead = db.scheduleDao().getAllJoin();
        int i=0;
        for(JoinScheduleDispense read : listRead) {
            int userId = (int)(long)userIds[i];
            assertEquals(read.getUserId(), userId);
            int medId = (int)(long)medicationIds[i];
            assertEquals(read.getMedicationId(), medId);
            assertEquals(read.getDispenseAmount(), 5+i);
            assertEquals(read.getRecurrence(), 6+i);
            assertEquals(read.getScheduleDate(), "date"+i);
            assertEquals(read.getScheduleDay(), "day"+i);
            assertEquals(read.getDispenseName(), "name"+i);
            assertEquals(read.getDispenseTime(), "02:10:1"+i);
            assertEquals(read.isActive(), true);
            i++;
        }
        cleanup();
    }

    @Test
    public void testGetJoinByUserIdNullDayDate() {
        if(null==db)
            initialize();

        setupDB();

        // delete
        db.scheduleDao().deleteAll();
        db.dispenseTimeDao().deleteAll();

        // insert
        List<DispenseTimeEntity> listDispenseTime = createManyRows4DispenseTime();
        dispenseIds = db.dispenseTimeDao().insertAll(listDispenseTime);

        List<ScheduleEntity> listSchedule = createManyRows4Schedule(dispenseIds);

        listSchedule.get(1).setScheduleDay(null);
        listSchedule.get(1).setScheduleDate(null);
        db.scheduleDao().insertAll(listSchedule);

        int userId = (int)(long)userIds[1];
        List<JoinScheduleDispense> listRead = db.scheduleDao().getJoinByUserIdNullDayDate(userId);
        assertEquals(listRead.size(), 1);
        cleanup();
    }

    @Test
    public void testGetJoinByUserIdScheduleDay() {
        if(null==db)
            initialize();

        setupDB();

        int userId = (int)(long)userIds[3];
        int medId = (int)(long)medicationIds[3];

        List<JoinScheduleDispense> listRead = db.scheduleDao().getJoinByUserIdScheduleDay(userId, "day3");
        JoinScheduleDispense read = listRead.get(0);
        assertEquals(read.getUserId(), userId);
        assertEquals(read.getMedicationId(), medId);
        assertEquals(read.getDispenseAmount(), 8);
        assertEquals(read.getRecurrence(), 9);
        assertEquals(read.getScheduleDate(), "date3");
        assertEquals(read.getScheduleDay(), "day3");
        assertEquals(read.getDispenseName(), "name3");
        assertEquals(read.getDispenseTime(), "02:10:13");
        assertEquals(read.isActive(), true);
        cleanup();
    }

    @Test
    public void testGetJoinByUserIdScheduleDate() {
        if(null==db)
            initialize();

        setupDB();

        int userId = (int)(long)userIds[3];
        List<JoinScheduleDispense> listRead = db.scheduleDao().getJoinByUserIdScheduleDate(userId, "date3");
        JoinScheduleDispense read = listRead.get(0);
        assertEquals(read.getUserId(), userId);
        cleanup();
    }

    @Test
    public void testGetJoinByDispenseTimeId() {
        if(null==db)
            initialize();

        setupDB();

        int dispenseTimeId = (int)(long)dispenseIds[0];
        JoinScheduleDispense read = db.scheduleDao().getJoinByDispenseTimeId(dispenseTimeId);
        assertEquals(read.getDispenseTimeId(), dispenseTimeId);
        cleanup();
    }

    @Test
    public void testGetJoinByUserIdMedicationId() {
        if(null==db)
            initialize();

        setupDB();

        int userId = (int)(long)userIds[2];
        int medId = (int)(long)medicationIds[2];
        List<JoinScheduleDispense> listRead  = db.scheduleDao().getJoinByUserIdMedicationId(userId, medId);
        JoinScheduleDispense read = listRead.get(0);
        assertEquals(read.getUserId(), userId);
        assertEquals(read.getMedicationId(), medId);
        cleanup();
    }

    @Test
    public void testCascade() {
        if(null==db)
            initialize();

        setupDB();

        int scheduleId = (int)(long)scheduleIds[0];
        int dispenseId = (int)(long)dispenseIds[0];

        // cascade delete automatically removes dependency row
        db.dispenseTimeDao().delete(dispenseId);
        ScheduleEntity nonExist = db.scheduleDao().get(scheduleId);
        assertEquals(nonExist, null);
        cleanup();
    }

    @Test
    public void testGetNonPausedJoinByUserId() {
        if(null==db)
            initialize();

        setupDB();

        int userId = (int)(long)userIds[2];
        int medId = (int)(long)medicationIds[2];

        // medication is paused
        List<JoinScheduleDispense> listRead  = db.scheduleDao().getNonPausedJoinByUserId(userId);
        assertEquals(listRead.size(), 0);

        // update pause value
        MedicationEntity medication = db.medicationDao().get(medId);
        medication.setPaused(false);
        db.medicationDao().update(medication);

        // medication not paused
        listRead  = db.scheduleDao().getNonPausedJoinByUserId(userId);
        assertEquals(listRead.size(), 1);
        cleanup();
    }

    private AppDatabase setupDB()
    {
        // delete
        db.dispenseTimeDao().deleteAll();
        db.scheduleDao().deleteAll();

        // insert
        List<DispenseTimeEntity> listDispenseTime = createManyRows4DispenseTime();
        dispenseIds = db.dispenseTimeDao().insertAll(listDispenseTime);

        List<ScheduleEntity> listSchedule = createManyRows4Schedule(dispenseIds);
        scheduleIds = db.scheduleDao().insertAll(listSchedule);

        return db;
    }

    private List<DispenseTimeEntity> createManyRows4DispenseTime()
    {
        // create many BinEntities
        List<DispenseTimeEntity> list = new ArrayList<DispenseTimeEntity>();
        for(int i=0; i<10; i++)
        {
            DispenseTimeEntity dispenseTimeInsert = new DispenseTimeEntity();
            int userId = (int)(long)userIds[i];
            dispenseTimeInsert.setUserId(userId);
            dispenseTimeInsert.setDispenseName("name"+i);
            dispenseTimeInsert.setDispenseTime("02:10:1"+i);
            dispenseTimeInsert.setDispenseAmount(2+i);
            dispenseTimeInsert.setActive(true);
            list.add(dispenseTimeInsert);
        }
        return list;
    }

    private List<ScheduleEntity> createManyRows4Schedule(Long[] ids)
    {
        List<ScheduleEntity> list = new ArrayList<ScheduleEntity>();
        for(int i=0; i<10; i++) {
            int userId = (int)(long)userIds[i];
            int medId = (int)(long)medicationIds[i];
            int dispenseId = (int)(long)dispenseIds[i];

            ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medId, dispenseId);
            scheduleEntity.setUserId(userId);
            scheduleEntity.setMedicationId(medId);
            scheduleEntity.setDispenseTimeId(dispenseId);
            scheduleEntity.setDispenseAmount(5+i);
            scheduleEntity.setRecurrence(6+i);
            scheduleEntity.setScheduleDate("date"+i);
            scheduleEntity.setScheduleDay("day"+i);
            list.add(scheduleEntity);
        }
        return list;
    }

    private void createMedicationRows()
    {
        List<MedicationEntity> list = new ArrayList<MedicationEntity>();
        for(int i=0; i<10; i++)
        {
            int userId = (int)(long)userIds[i];
            MedicationEntity medicationInsert = new MedicationEntity(userId);
            medicationInsert.setMedicationName("name"+i);
            medicationInsert.setMedicationNickname("nickname"+i);
            medicationInsert.setStrengthValue(2+i);
            medicationInsert.setStrengthMeasurement("measure"+i);
            medicationInsert.setDosageInstructions("instruction"+i);
            medicationInsert.setTimeBetweenDoses(3+i);
            medicationInsert.setMaxUnitsPerDay(4+i);
            medicationInsert.setMaxNumberPerDose(5+i);
            medicationInsert.setMedicationQuantity(6+i);
            medicationInsert.setUseByDate("date"+i);
            medicationInsert.setFillDate("fillDate"+i);
            medicationInsert.setMedicationLocation(7+i);
            medicationInsert.setMedicationLocationName("location"+i);
            medicationInsert.setPaused(true);
            medicationInsert.setMedicationScheduleType(8+i);
            medicationInsert.setPatientName("Patient"+i);
            list.add(medicationInsert);
        }
        medicationIds = db.medicationDao().insertAll(list);
    }

    @Override
    public void initialize()
    {
        super.initialize();
        createMedicationRows();
    }
}
