package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.JoinContactSettingUser;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_SettingDAO extends BaseTestDAO {
    private Long[] contactIds;
    private Long[] settingIds;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    public void cleanup() {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.userDao().deleteAll();
        db.contactDao().deleteAll();
        db.settingDao().deleteAll();
    }

    @Test
    public void testJoinAll() {
        if(null==db)
            initialize();

        int userId = (int)(long)userIds[0];
        int contactId = (int)(long)contactIds[0];

        NotificationSettingEntity settingEntity = new NotificationSettingEntity(userId, contactId);
        settingEntity.setUserId(userId);
        settingEntity.setId(2);
        settingEntity.setContactId(contactId);
        settingEntity.setSettingName("name");
        settingEntity.setTextSelected(true);
        settingEntity.setVoiceSelected(false);
        settingEntity.setEmailSelected(true);

        // delete
        db.settingDao().deleteAll();

        // insert
        Long id = db.settingDao().insert(settingEntity);

        // Join Read
        List<JoinContactSettingUser> listJoin = db.settingDao().getAllJoin();
        assertEquals(listJoin.size(), 1);

        JoinContactSettingUser join = listJoin.get(0);
        assertEquals(join.getUserId(), userId);
        assertEquals(join.getContactId(), contactId);
        cleanup();
    }

    @Test
    public void testCRUDSingleRow() {
        if(null==db)
            initialize();

        int userId = (int)(long)userIds[0];
        int contactId = (int)(long)contactIds[0];

        NotificationSettingEntity settingEntity = new NotificationSettingEntity(userId, contactId);
        settingEntity.setUserId(userId);
        settingEntity.setId(2);
        settingEntity.setContactId(contactId);
        settingEntity.setSettingName("name");
        settingEntity.setTextSelected(true);
        settingEntity.setVoiceSelected(false);
        settingEntity.setEmailSelected(true);

        // delete
        db.settingDao().deleteAll();

        // insert
        Long id = db.settingDao().insert(settingEntity);

        // read
        NotificationSettingEntity settingRead = db.settingDao().get((int)(long)id);
        assertEquals(settingRead.getUserId(), userId);
        assertEquals(settingRead.getId(), 2);
        assertEquals(settingRead.getContactId(), contactId);
        assertEquals(settingRead.getSettingName(), "name");
        assertEquals(settingRead.isTextSelected(), true);
        assertEquals(settingRead.isVoiceSelected(), false);
        assertEquals(settingRead.isEmailSelected(), true);

        // delete single
        db.settingDao().delete((int)(long)id);
        NotificationSettingEntity nonExist = db.settingDao().get(0);
        assertEquals(nonExist, null);
        cleanup();
    }

    @Test
    public void testCRUDManyRows() {
        if(null==db)
            initialize();

        setupDB();

        // read
        List<NotificationSettingEntity> listRead = db.settingDao().getAll();
        int i=0;
        for(NotificationSettingEntity settingRead : listRead)
        {
            int userId = (int)(long)userIds[i];
            assertEquals(settingRead.getUserId(), userId);
            assertEquals(settingRead.getId(), 2+i);
            int contactId = (int)(long)contactIds[i];
            assertEquals(settingRead.getContactId(), contactId);
            assertEquals(settingRead.getSettingName(), "name"+i);
            assertEquals(settingRead.isTextSelected(), true);
            assertEquals(settingRead.isVoiceSelected(), false);
            assertEquals(settingRead.isEmailSelected(), true);
            i++;
        }
        cleanup();
    }

    @Test
    public void testGetByUserIdContactId()
    {
        if(null==db)
            initialize();

        setupDB();

        // read
        int userId = (int)(long)userIds[2];
        int contactId = (int)(long)contactIds[2];
        List<NotificationSettingEntity> listRead = db.settingDao().getByUserIdContactId(userId, contactId);
        assertEquals(listRead.size(), 1);

        NotificationSettingEntity settingEntity = listRead.get(0);
        assertEquals(settingEntity.getUserId(), userId);
        cleanup();
    }

    @Test
    public void testGetByUserId()
    {
        if(null==db)
            initialize();

        setupDB();

        // read
        int userId = (int)(long)userIds[2];
        List<NotificationSettingEntity> listRead = db.settingDao().getByUserId(userId);
        assertEquals(listRead.size(), 1);

        NotificationSettingEntity settingEntity = listRead.get(0);
        assertEquals(settingEntity.getUserId(), userId);
        cleanup();
    }

    @Test
    public void testDeleteByContactId()
    {
        if(null==db)
            initialize();

        setupDB();

        int contactId = (int)(long)contactIds[2];

        // delete 1 by contact id
        db.settingDao().deleteByContactId(contactId);

        // read
        int userId = (int)(long)userIds[2];
        List<NotificationSettingEntity> nonExistList = db.settingDao().getByUserIdContactId(userId, contactId);
        assertEquals(nonExistList.size(), 0);
        cleanup();
    }

    @Test
    public void testDeleteByUserIdContactId()
    {
        if(null==db)
            initialize();

        setupDB();

        // delete
        int userId = (int)(long)userIds[2];
        int contactId = (int)(long)contactIds[2];
        db.settingDao().deleteByUserIdContactId(userId, contactId);

        // read
        List<NotificationSettingEntity> nonExistList = db.settingDao().getByUserIdContactId(userId, contactId);
        assertEquals(nonExistList.size(), 0);
        cleanup();
    }

    @Test
    public void testCascade()
    {
        if(null==db)
            initialize();

        // delete
        db.userDao().deleteAll();
        db.contactDao().deleteAll();
        db.settingDao().deleteAll();

        // insert
        UserEntity user = new UserEntity();
        user.setUserName("user");
        user.setPin("pin");
        user.setPatientName("patient");
        Long userId = db.userDao().insert(user);

        // remove userId in beta, should retrieve from join of contact - notification setting - user
        ContactEntity contact = new ContactEntity();
        contact.setUserId((int)(long)userId);
        Long contactId = db.contactDao().insert(contact);

        NotificationSettingEntity setting = new NotificationSettingEntity((int)(long)userId, (int)(long)contactId);
        Long settingId = db.settingDao().insert(setting);

        // cascade delete automatically removes dependencies
        db.userDao().delete((int)(long)userId);
        NotificationSettingEntity nonExist = db.settingDao().get((int)(long)settingId);
        assertEquals(nonExist, null);
        cleanup();
    }

    private AppDatabase setupDB()
    {
        // delete
        db.settingDao().deleteAll();

        // insert
        List<NotificationSettingEntity> list = createManyRows();
        settingIds = db.settingDao().insertAll(list);
        return db;
    }

    private List<NotificationSettingEntity> createManyRows()
    {
        List<NotificationSettingEntity> list = new ArrayList<NotificationSettingEntity>();
        for(int i=0; i<10; i++) {
            int userId = (int)(long)userIds[i];
            int contactId = (int)(long)contactIds[i];

            NotificationSettingEntity settingEntity = new NotificationSettingEntity(userId, contactId);
            settingEntity.setUserId(userId);
            settingEntity.setId(2+i);
            settingEntity.setContactId(contactId);
            settingEntity.setSettingName("name"+i);
            settingEntity.setTextSelected(true);
            settingEntity.setVoiceSelected(false);
            settingEntity.setEmailSelected(true);
            list.add(settingEntity);
        }
        return list;
    }

    @Override
    public void initialize()
    {
        super.initialize();
        createManyContacts();
    }

    private void createManyContacts()
    {
        // create many BinEntities
        List<ContactEntity> list = new ArrayList<ContactEntity>();
        for(int i=0; i<10; i++)
        {
            // remove userId in beta, should retrieve from join of contact - notification setting - user
            ContactEntity contactInsert = new ContactEntity();
            int id = (int)(long)userIds[i];
            contactInsert.setUserId(id);
            contactInsert.setName("name"+i);
            contactInsert.setTextNumber("TextNumber"+i);
            contactInsert.setVoiceNumber("VoiceNumber"+i);
            contactInsert.setEmail("Email"+i);
            contactInsert.setCategory("Category"+i);
            contactInsert.setSelected(true);
            contactInsert.setTextSelected(false);
            contactInsert.setVoiceSelected(true);
            contactInsert.setEmailSelected(false);
            contactInsert.setRelationship(2+i);
            list.add(contactInsert);
        }
        contactIds = db.contactDao().insertAll(list);
    }
}