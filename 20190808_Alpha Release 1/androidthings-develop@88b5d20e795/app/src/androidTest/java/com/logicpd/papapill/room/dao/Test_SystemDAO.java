package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.dao.SystemDao;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.SystemEntity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_SystemDAO {

    private Long[] ids;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    public void cleanup() {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.systemDao().deleteAll();
    }

    @Test
    public void testCRUDSingleRow() {
        SystemEntity systemEntity = new SystemEntity();
        systemEntity.setSystemKey("key");
        systemEntity.setSerialNumber("number");

        AppDatabase db = AppDatabase.getDatabase(App.getContext());

        // delete
        db.systemDao().deleteAll();

        // insert
        Long id = db.systemDao().insert(systemEntity);

        // read
        SystemEntity read = db.systemDao().get((int)(long)id);
        assertEquals(read.getSystemKey(), "key");
        assertEquals(read.getSerialNumber(), "number");

        // delete single
        db.systemDao().delete((int)(long)id);
        SystemEntity nonExist = db.systemDao().get((int)(long)id);
        assertEquals(nonExist, null);
        cleanup();
    }


    @Test
    public void testCRUDManyRows() {
        AppDatabase db = setupDB();

        // read
        List<SystemEntity> listRead = db.systemDao().getAll();
        int i=0;
        for(SystemEntity read : listRead)
        {
            assertEquals(read.getSystemKey(), "key"+i);
            assertEquals(read.getSerialNumber(), "number"+i);
            i++;
        }
        cleanup();
    }

    @Test
    public void testUpdateSerialNumberById()
    {
        AppDatabase db = setupDB();
        int id = (int)(long)ids[0];
        int retVal = db.systemDao().updateSerialNumberById(id, "updateSerial");
        assertEquals(retVal, 1);

        SystemEntity read = db.systemDao().get(id);
        assertEquals(read.getSerialNumber(), "updateSerial");
        cleanup();
    }

    @Test
    public void testUpdateSystemKeyById()
    {
        AppDatabase db = setupDB();
        int id = (int)(long)ids[0];
        int retVal = db.systemDao().updateSystemKeyById(id, "updateKey");
        assertEquals(retVal, 1);

        SystemEntity read = db.systemDao().get(id);
        assertEquals(read.getSystemKey(), "updateKey");
        cleanup();
    }

    private AppDatabase setupDB()
    {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());

        // delete
        db.systemDao().deleteAll();

        // insert
        List<SystemEntity> list = createManyRows();
        ids = db.systemDao().insertAll(list);
        return db;
    }

    private List<SystemEntity> createManyRows()
    {
        List<SystemEntity> list = new ArrayList<SystemEntity>();
        for(int i=0; i<10; i++) {
            SystemEntity systemEntity = new SystemEntity();
            systemEntity.setSystemKey("key"+i);
            systemEntity.setSerialNumber("number"+i);
            list.add(systemEntity);
        }
        return list;
    }
}
