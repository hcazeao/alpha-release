package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.UserRepository;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_binDAO {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    public void cleanup()
    {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.binDao().deleteAll();
    }

    @Test
    public void testCRUDSingleRow()
    {
        // create
        BinEntity binInsert = new BinEntity();
        binInsert.setPillCount(2);
        binInsert.setUpperLeftX(3);
        binInsert.setUpperLeftY(4);
        binInsert.setLowerRightX(5);
        binInsert.setLowerRightY(6);
        binInsert.setPillTemplate("insertBin");
        binInsert.setRunAnalysis(7);
        binInsert.setFillLevel(8);

        AppDatabase db = AppDatabase.getDatabase(App.getContext());

        // delete
        db.binDao().deleteAll();

        // insert
        Long id = db.binDao().insert(binInsert);
        //assertEquals(retVal, 1);    // successful insert for 1 row

        // read
        BinEntity binRead = db.binDao().get((int)(long) id);
        assertEquals(binRead.getPillCount(), 2);
        assertEquals(binRead.getUpperLeftX(), 3);
        assertEquals(binRead.getUpperLeftY(), 4);
        assertEquals(binRead.getLowerRightX(), 5);
        assertEquals(binRead.getLowerRightY(), 6);
        assertEquals(binRead.getPillTemplate(), "insertBin");
        assertEquals(binRead.getRunAnalysis(), 7);
        assertEquals(binRead.getFillLevel(), 8);

        // update
        BinEntity binUpdate = binInsert;
        binUpdate.setPillTemplate("updatedBin");
        db.binDao().update(binUpdate);

        // read -- check update
        binRead = db.binDao().get((int)(long)id);
        assertEquals(binRead.getPillTemplate(), "updatedBin");
        cleanup();
    }

    @Test
    public void testCRUDManyRows()
    {
        // create many BinEntities
        List<BinEntity> list = new ArrayList<BinEntity>();
        for(int i=0; i<10; i++)
        {
            BinEntity binInsert = new BinEntity();
            binInsert.setId(i);
            binInsert.setPillCount(2+i);
            binInsert.setUpperLeftX(3+i);
            binInsert.setUpperLeftY(4+i);
            binInsert.setLowerRightX(5+i);
            binInsert.setLowerRightY(6+i);
            binInsert.setPillTemplate("insertBin"+i);
            binInsert.setRunAnalysis(7+i);
            binInsert.setFillLevel(8+i);
            list.add(binInsert);
        }

        AppDatabase db = AppDatabase.getDatabase(App.getContext());

        // delete
        db.binDao().deleteAll();

        // insert
        db.binDao().insertAll(list);

        // read
        List<BinEntity> listRead = db.binDao().getAll();
        int i = 0;
        for(BinEntity binRead : listRead) {
            assertEquals(binRead.getPillCount(), 2+i);
            assertEquals(binRead.getUpperLeftX(), 3+i);
            assertEquals(binRead.getUpperLeftY(), 4+i);
            assertEquals(binRead.getLowerRightX(), 5+i);
            assertEquals(binRead.getLowerRightY(), 6+i);
            assertEquals(binRead.getPillTemplate(), "insertBin"+i);
            assertEquals(binRead.getRunAnalysis(), 7+i);
            assertEquals(binRead.getFillLevel(), 8+i);
            i++;
        }
        cleanup();
    }
}
