package com.logicpd.papapill.room.repositories;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.interfaces.OnAsyncEventListener;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.Result;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_BinRepository implements OnAsyncEventListener {

    @Override
    public void OnPreExecute() {

    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @AfterClass
    static public void cleanup()
    {
        new BinRepository().syncOp(CRUDEnum.DELETE_ALL, null);
    }

    @Test
    public void testDelete()
    {
        // start with a clean slate
        cleanup();

        // create some entries
        List<IBaseEntity> list = create10();
        list.remove(0);

        // insert 1 of 9
        int id = (int)(long)new BinRepository().syncOp(CRUDEnum.INSERT, list);

        // async delete !!
        new BinRepository().delete(id);

/*
 * Need to add a wait() or idle.
 * List<BinEntity> nonExist = (List<BinEntity>)new BinRepository().syncOp(CRUDEnum.QUERY_ALL, null);
 *       assertEquals(nonExist.size(), 0);
 */
    }

    @Override
    public void OnPostExecute(Object result) {
        // read and check there is none

    }


    @Test
    public void testRead()
    {
        // start with a clean slate
        new BinRepository().syncOp(CRUDEnum.DELETE_ALL, null);

        // create some entries
        List<IBaseEntity> list = create10();
        list.remove(0);

        // inser 1 of 9
        new BinRepository().syncOp(CRUDEnum.INSERT, list);

        // read - should be 1
        BinEntity binRead = (BinEntity)new BinRepository().read(1);
        assertEquals(binRead.getId(), 1);
    }

    @Test
    public void testSyncOp()
    {
        // start with a clean slate
        new BinRepository().syncOp(CRUDEnum.DELETE_ALL, null);

        // create some entries
        List<IBaseEntity> list = create10();

        // test INSERT_ALL
        new BinRepository().syncOp(CRUDEnum.INSERT_ALL, list);

        // test QUERY_ALL
        List<BinEntity> listRead = (List<BinEntity>)(List<?>)new BinRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(listRead.size(), 10);

        // test UPDATE
        BinEntity bin = listRead.get(3);
        bin.setPillTemplate("templateTest");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(bin);
        new BinRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        BinEntity binRead = (BinEntity) new BinRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(binRead.getPillTemplate(), "templateTest");
    }

    private List<IBaseEntity> create10()
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++) {
            BinEntity entity = new BinEntity();
            entity.setId(i);
            list.add(entity);
        }
        return list;
    }
}
