package com.logicpd.papapill.room.repositories;

import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)

public class Test_ContactRepository extends BaseTestRepository {
    private Long[] contactIds;

    @AfterClass
    static public void cleanup()
    {
        new UserRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new ContactRepository().syncOp(CRUDEnum.DELETE_ALL, null);
    }

    @Test
    public void testSyncOp() {

        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Users();
        create10Contacts();

        // test QUERY_ALL
        List<ContactEntity> list = (List<ContactEntity>) new ContactRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(list.size(), 10);

        // test UPDATE
        ContactEntity entity = list.get(3);
        entity.setName("updateName");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(entity);
        new ContactRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        ContactEntity contactRead = (ContactEntity) new ContactRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(contactRead.getName(), "updateName");
    }

    private void create10Contacts()
    {
        // create many BinEntities
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++) {
            ContactEntity contactInsert = new ContactEntity();
            int id = (int)(long)userIds[i];
            contactInsert.setUserId(id);
            contactInsert.setName("name"+i);
            contactInsert.setTextNumber("TextNumber"+i);
            contactInsert.setVoiceNumber("VoiceNumber"+i);
            contactInsert.setEmail("Email"+i);
            contactInsert.setCategory("Category"+i);
            contactInsert.setSelected(true);
            contactInsert.setTextSelected(false);
            contactInsert.setVoiceSelected(true);
            contactInsert.setEmailSelected(false);
            contactInsert.setRelationship(2+i);
            list.add(contactInsert);
        }
        contactIds = (Long[])new ContactRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }
}
