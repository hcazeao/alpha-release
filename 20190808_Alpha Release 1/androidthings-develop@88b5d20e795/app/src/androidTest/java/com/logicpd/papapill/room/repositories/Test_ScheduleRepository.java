package com.logicpd.papapill.room.repositories;

import android.support.test.runner.AndroidJUnit4;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)

public class Test_ScheduleRepository extends BaseTestRepository {
    private Long[] dispenseIds;
    private Long[] scheduleIds;
    private Long[] medicationIds;

    @AfterClass
    static public void cleanup()
    {
        new ScheduleRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new MedicationRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new DispenseTimeRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new UserRepository().syncOp(CRUDEnum.DELETE_ALL, null);
    }

    @Test
    public void testSyncOps()
    {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Users();
        create10DispenseTimes();
        createMedicationRows();
        create10Schedules();

        // test QUERY_ALL
        List<ScheduleEntity> list = (List<ScheduleEntity>)new ScheduleRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(list.size(), 10);

        // test UPDATE
        ScheduleEntity schedule = list.get(3);
        schedule.setScheduleDay("updateDay");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(schedule);
        new ScheduleRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        ScheduleEntity scheduleRead = (ScheduleEntity)new ScheduleRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(scheduleRead.getScheduleDay(), "updateDay");
    }

    @Test
    public void testGetNonPausedScheduledItemsForDay()
    {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Users();
        create10DispenseTimes();
        createMedicationRows();
        create10Schedules();

        List<JoinScheduleDispense> list = new ScheduleRepository().getNonPausedJoinByUserIdDay((int)(long)userIds[0], "day0");
        assertEquals(list.size(), 0);

        list = new ScheduleRepository().getNonPausedJoinByUserIdDay((int)(long)userIds[1], "day1");
        assertEquals(list.size(), 1);
    }

    @Test
    public void testGetNonPausedScheduledItemsForDate()
    {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Users();
        create10DispenseTimes();
        createMedicationRows();
        create10Schedules();

        List<JoinScheduleDispense> list = new ScheduleRepository().getNonPausedJoinByUserIdScheduleDate((int)(long)userIds[0], "date0");
        assertEquals(list.size(), 0);

        list = new ScheduleRepository().getNonPausedJoinByUserIdScheduleDate((int)(long)userIds[1], "date1");
        assertEquals(list.size(), 1);
    }


    @Test
    public void testGetNonPausedDailyScheduledItems()
    {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Users();
        create10DispenseTimes();
        createMedicationRows();
        create10Schedules();

        int userId = (int)(long)userIds[1];

        List<JoinScheduleDispense> list = new ScheduleRepository().getNonPausedJoinByUserIdNullDayDate((int)(long)userIds[1]);
        assertEquals(list.size(), 0);

        List<ScheduleEntity> schedules = (List<ScheduleEntity>)(List<?>)new ScheduleRepository().readAll();
        ScheduleEntity schedule = schedules.get(1);
        schedule.setScheduleDay(null);
        schedule.setScheduleDate(null);
        new ScheduleRepository().update(schedule);

        // confirm I do have a med that is not paused
        List<MedicationEntity> meds = (List<MedicationEntity>)new MedicationRepository().getByUserId(userId);
        assertEquals(meds.size(), 1);
        assertEquals(meds.get(0).isPaused(), false);
        assertEquals(meds.get(0).getUserId(), userId);

        list = new ScheduleRepository().getNonPausedJoinByUserIdNullDayDate(userId);
        assertEquals(list.size(), 1);
    }


    private void create10DispenseTimes()
    {
        // create many BinEntities
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++)
        {
            DispenseTimeEntity dispenseTimeInsert = new DispenseTimeEntity();
            int userId = (int)(long)userIds[i];
            dispenseTimeInsert.setUserId(userId);
            dispenseTimeInsert.setDispenseName("name"+i);
            dispenseTimeInsert.setDispenseTime("02:10:1"+i);
            dispenseTimeInsert.setDispenseAmount(2+i);
            dispenseTimeInsert.setActive(true);
            list.add(dispenseTimeInsert);
        }
        dispenseIds = (Long[])new DispenseTimeRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }

    private void createMedicationRows()
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++)
        {
            int userId = (int)(long)userIds[i];
            MedicationEntity medicationInsert = new MedicationEntity(userId);
            medicationInsert.setMedicationName("name"+i);
            medicationInsert.setMedicationNickname("nickname"+i);
            medicationInsert.setStrengthValue(2+i);
            medicationInsert.setStrengthMeasurement("measure"+i);
            medicationInsert.setDosageInstructions("instruction"+i);
            medicationInsert.setTimeBetweenDoses(3+i);
            medicationInsert.setMaxUnitsPerDay(4+i);
            medicationInsert.setMaxNumberPerDose(5+i);
            medicationInsert.setMedicationQuantity(6+i);
            medicationInsert.setUseByDate("date"+i);
            medicationInsert.setFillDate("fillDate"+i);
            medicationInsert.setMedicationLocation(7+i);
            medicationInsert.setMedicationLocationName("location"+i);
            MedPausedEnum isPaused = MedPausedEnum.values()[i%2];
            medicationInsert.setPaused(isPaused.value);
            MedScheduleTypeEnum type = MedScheduleTypeEnum.values()[i%3];
            medicationInsert.setMedicationScheduleType(type.value);
            medicationInsert.setPatientName("Patient"+i);
            list.add(medicationInsert);
        }
        medicationIds = (Long[])new MedicationRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }

    private void create10Schedules()
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++) {
            int userId = (int)(long)userIds[i];
            int medId = (int)(long)medicationIds[i];
            int dispenseId = (int)(long)dispenseIds[i];

            ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medId, dispenseId);
            scheduleEntity.setUserId(userId);
            scheduleEntity.setMedicationId(medId);
            scheduleEntity.setDispenseTimeId(dispenseId);
            scheduleEntity.setDispenseAmount(5+i);
            scheduleEntity.setRecurrence(6+i);
            scheduleEntity.setScheduleDate("date"+i);
            scheduleEntity.setScheduleDay("day"+i);
            list.add(scheduleEntity);
        }
        scheduleIds = (Long[])new ScheduleRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }
}
