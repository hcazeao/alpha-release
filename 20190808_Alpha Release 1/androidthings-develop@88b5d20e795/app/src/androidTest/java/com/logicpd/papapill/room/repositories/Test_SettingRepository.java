package com.logicpd.papapill.room.repositories;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinContactSettingUser;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)

/*
 * More additional join tests to be added in beta when we correct scheme for contact-notification setting-user
 */
public class Test_SettingRepository extends BaseTestRepository{

    private Long[] contactIds;
    private Long[] settingIds;

    @AfterClass
    static public void cleanup()
    {
        new UserRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new NotificationSettingRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new ContactRepository().syncOp(CRUDEnum.DELETE_ALL, null);
    }

    @Test
    public void testJoin()
    {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        insert10();

        // test JOIN QUERY
        List<JoinContactSettingUser> listJoin = (List<JoinContactSettingUser> )new NotificationSettingRepository().syncOp(CRUDEnum.QUERY_JOIN_ALL, null);
        assertEquals(listJoin.size(), 10);
    }

    @Test
    public void testSyncOp()
    {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        insert10();

        // test QUERY_ALL
        List<NotificationSettingEntity> listSettings = (List<NotificationSettingEntity> )new NotificationSettingRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(listSettings.size(), 10);

        // test UPDATE
        NotificationSettingEntity setting = listSettings.get(3);
        setting.setSettingName("updateSetting");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(setting);
        new NotificationSettingRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        NotificationSettingEntity settingRead = (NotificationSettingEntity) new NotificationSettingRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(settingRead.getSettingName(), "updateSetting");
    }

    private void insert10()
    {
        create10Users();
        create10Contacts();
        create10Settings();
    }

    private void create10Settings()
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++) {
            int userId = (int)(long)userIds[i];
            int contactId = (int)(long)contactIds[i];

            NotificationSettingEntity settingEntity = new NotificationSettingEntity(userId, contactId);
            settingEntity.setUserId(userId);
            settingEntity.setId(2+i);
            settingEntity.setContactId(contactId);
            settingEntity.setSettingName("name"+i);
            settingEntity.setTextSelected(true);
            settingEntity.setVoiceSelected(false);
            settingEntity.setEmailSelected(true);
            list.add(settingEntity);
        }
        settingIds = (Long[])new NotificationSettingRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }

    private void create10Contacts()
    {
        // create many BinEntities
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++)
        {
            ContactEntity contactInsert = new ContactEntity();
            int id = (int)(long)userIds[i];
            contactInsert.setUserId(id);
            contactInsert.setName("name"+i);
            contactInsert.setTextNumber("TextNumber"+i);
            contactInsert.setVoiceNumber("VoiceNumber"+i);
            contactInsert.setEmail("Email"+i);
            contactInsert.setCategory("Category"+i);
            contactInsert.setSelected(true);
            contactInsert.setTextSelected(false);
            contactInsert.setVoiceSelected(true);
            contactInsert.setEmailSelected(false);
            contactInsert.setRelationship(2+i);
            list.add(contactInsert);
        }
        contactIds = (Long[])new ContactRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }
}
