package com.logicpd.papapill.room.repositories;

import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_UserRepository extends BaseTestRepository {

    @Test
    public void testSyncOp() {
        // start with a clean slate
        new UserRepository().syncOp(CRUDEnum.DELETE_ALL, null);

        // test INSERT_ALL
        create10Users();

        // test QUERY_ALL
        List<UserEntity> list = (List<UserEntity>) new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(list.size(), 10);

        // test UPDATE
        UserEntity entity = list.get(3);
        entity.setUserName("updateUsername");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(entity);
        new UserRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        UserEntity userRead = (UserEntity) new UserRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(userRead.getUserName(), "updateUsername");
    }
}
