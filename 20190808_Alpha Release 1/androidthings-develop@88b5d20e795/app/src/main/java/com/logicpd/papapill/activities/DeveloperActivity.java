package com.logicpd.papapill.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.gpio.LockManager;
import com.logicpd.papapill.device.gpio.TinyGResetManager;
import com.logicpd.papapill.device.i2c.CupLight;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.device.tinyg.CommandManager;
import com.logicpd.papapill.device.tinyg.CoordinateManager;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.receivers.IntentBroadcast;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.gpio.LightringManager;
import com.logicpd.papapill.device.models.PositionData;
import com.logicpd.papapill.device.tinyg.CommandBuilder;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.device.i2c.Sensor;

/**
 * NOTE: This activity is a single screen "dashboard" with button functionality
 * that is meant to assist in testing. This will not be in the final app.
 */
public class DeveloperActivity extends Activity {

    public static final String TAG = "DeveloperActivity";

    private TinyGDriver tg = TinyGDriver.getInstance();

    private Sensor drawer = new Sensor(BoardDefaults.I2C_PORT,
                                        BoardDefaults.I2C_MUX_DRAWER_CHANNEL,
                                        AppConfig.getInstance().getDrawerThresholdHigh());
    private Sensor door = new Sensor(BoardDefaults.I2C_PORT,
                                        BoardDefaults.I2C_MUX_DOOR_CHANNEL,
                                        AppConfig.getInstance().getDoorThresholdHigh());
    private Sensor cup = new Sensor(BoardDefaults.I2C_PORT,
                                        BoardDefaults.I2C_MUX_CUP_CHANNEL,
                                        AppConfig.getInstance().getCupThresholdHigh());

    private static CupLight cupLight = new CupLight(BoardDefaults.I2C_PORT, BoardDefaults.I2C_MUX_CUP_CHANNEL);

    PositionData positionData = new PositionData();

    Handler handler = new Handler();

    // Some global variables (put into a settings file later):
    private int binNumber = 5;

    private boolean light = false;
    private boolean cuplight = false;

    private int distanceCarousel = 120;
    private int distanceRadial = 37;    // Alpha Camera to dip tube rho distance
    private int distanceZ = 46;         // Alpha 25% bin depth without touching
    private int feedrateCarousel = 500;
    private int feedrateRadial = 1000;
    private int feedrateZ = 1000;
    protected BroadcastReceiver mReceiver;

    private void initializeActivity() {
        TextView numboxBinTextView = findViewById(R.id.numbox_bin);
        numboxBinTextView.setText(Integer.toString(binNumber));

        TextView cdTextView = findViewById(R.id.carousel_distance);
        cdTextView.setText(Integer.toString(distanceCarousel));
        TextView cfTextView = findViewById(R.id.carousel_feedrate);
        cfTextView.setText(Integer.toString(feedrateCarousel));

        TextView rdTextView = findViewById(R.id.radial_distance);
        rdTextView.setText(Integer.toString(distanceRadial));
        TextView rfTextView = findViewById(R.id.radial_feedrate);
        rfTextView.setText(Integer.toString(feedrateRadial));

        TextView zdTextView = findViewById(R.id.z_distance);
        zdTextView.setText(Integer.toString(distanceZ));
        TextView zfTextView = findViewById(R.id.z_feedrate);
        zfTextView.setText(Integer.toString(feedrateZ));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

        AppConfig appConfig = AppConfig.getInstance();
        appConfig.loadFile(this.getApplicationContext());

        // Release the TinyG from reset
        TinyGResetManager.getInstance().resetTinyG();
        Log.d(TAG, "TinyG out of reset");

        // Initialize activity.
        initializeActivity();
        Log.d(TAG, "Activity Initialized");

        // Initialize the Tinyg driver.
        if(!tg.initialize(true)) {
            Log.e(TAG, "Failed to initialize Tinyg driver.");
        }

        // Button Listeners
        final Button buttonStart = findViewById(R.id.button_start_dispense);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.doPrepDispense(binNumber, 666);
                } catch (Exception e) {}
            }
        });

        final Button buttonCancel = findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.write(CommandBuilder.CMD_APPLY_CANCEL_MOVE);
                } catch (Exception e) {}
            }
        });

        final Button buttonHomeCarousel = findViewById(R.id.button_home_carousel);
        buttonHomeCarousel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    double targetPosition = CoordinateManager.getHomeBinOffset();
                    String params = String.format("%f 0.3", targetPosition);
                    CommandManager.getInstance().callCommand(
                            DeviceCommand.COMMAND_ROTATE_CAROUSEL, params, null);
                } catch (Exception e) {}
            }
        });

        final Button buttonHomeRadial = findViewById(R.id.button_home_radial);
        buttonHomeRadial.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_HOME, "x", null);
                } catch (Exception e) {}
            }
        });

        final Button buttonHomeZ = findViewById(R.id.button_home_z);
        buttonHomeZ.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_HOME, "z", null);
                } catch (Exception e) {}
            }
        });

        final Button buttonVacuumOn = findViewById(R.id.button_vacuum_on);
        buttonVacuumOn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.write(CommandBuilder.CMD_COOLANT_ON);
                } catch (Exception e) {}
            }
        });

        final Button buttonVacuumOff = findViewById(R.id.button_vacuum_off);
        buttonVacuumOff.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.write(CommandBuilder.CMD_COOLANT_OFF);
                } catch (Exception e) {}
            }
        });

        final Button buttonLightToggle = findViewById(R.id.button_light);
        buttonLightToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    light = !light;
                    LightringManager.getInstance().configureOutput(light);
                } catch (Exception e) {}
            }
        });

        final Button buttonCupLightToggle = findViewById(R.id.button_cup_light);
        buttonCupLightToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    cuplight = !cuplight;
                    if (cuplight) {
                        cupLight.setCupLightOn();
                    } else {
                        cupLight.setCupLightOff();
                    }
                } catch (Exception e) {}
            }
        });

        final Button buttonReadDrawerProximity = findViewById(R.id.button_read_drawer_proximity);
        buttonReadDrawerProximity.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    TextView editText = findViewById(R.id.result_text_drawer_proximity);
                    String hex = Integer.toHexString(drawer.getSensorValue());
                    editText.setText("0x" + hex);
                } catch (Exception e) {
                    Log.e("I2C", "Exception when reading I2C.");
                }
            }
        });

        final Button buttonReadDoorProximity = findViewById(R.id.button_read_door_proximity);
        buttonReadDoorProximity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    TextView editText = findViewById(R.id.result_text_drawer_proximity);
                    String hex = Integer.toHexString(door.getSensorValue());
                    editText.setText("0x" + hex);
                } catch (Exception e) {
                    Log.e("I2C", "Exception when reading I2C");
                }
            }
        });

        final Button buttonReadCupProximity = findViewById(R.id.button_read_cup_proximity);
        buttonReadCupProximity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    TextView editText = findViewById(R.id.result_text_drawer_proximity);
                    String hex = Integer.toHexString(cup.getSensorValue());
                    editText.setText("0x" + hex);
                } catch (Exception e) {
                    Log.e("I2C", "Exception when reading I2C");
                }
            }
        });

        final Button buttonAbsolutePositionSensor = findViewById(R.id.button_read_abs_pos_sensor);
        buttonAbsolutePositionSensor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.commandManager.callCommand(DeviceCommand
                            .COMMAND_READ_POSITION, "", positionData);
                    while (!CommandManager.getInstance().isCommandDone(
                            DeviceCommand.COMMAND_READ_POSITION))
                    {
                        // Probably should have a software timer safety valve to break out in case
                        // there is a bug in the command.
                    }

                    TextView editText = findViewById(R.id.result_text_abs_pos_sensor);
                    String hex = Double.toString(positionData.getAbsoluteDegrees(true));
                    editText.setText(hex + " deg");
                } catch (Exception e) {
                    Log.e("AbsSensor", "Exception when reading UART.");
                }
            }
        });

        final Button buttonLockDoor = findViewById(R.id.button_door_lock);
        buttonLockDoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LockManager.getInstance().LockDoor();
                } catch (Exception e) {
                    Log.e("buttonLockDoor", "Exception occurred", e);
                }
            }
        });

        final Button buttonUnlockDoor = findViewById(R.id.button_door_unlock);
        buttonUnlockDoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LockManager.getInstance().UnlockDoor();
                } catch (Exception e) {
                    Log.e("buttonUnlockDoor", "Exception occurred", e);
                }
            }
        });

        final Button buttonLockDrawer = findViewById(R.id.button_drawer_lock);
        buttonLockDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LockManager.getInstance().LockDrawer();
                } catch (Exception e) {
                    Log.e("buttonLockDrawer", "Exception occurred", e);
                }
            }
        });

        final Button buttonUnlockDrawer = findViewById(R.id.button_drawer_unlock);
        buttonUnlockDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LockManager.getInstance().UnlockDrawer();
                } catch (Exception e) {
                    Log.e("buttonUnlockDrawer", "Exception occurred", e);
                }
            }
        });

        final Button buttonConfigure = findViewById(R.id.button_config);
        buttonConfigure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    tg.softwareReset();
                }
                catch (Exception e) {}
            }
        });

        final Button buttonDemo = findViewById(R.id.button_demo);
        buttonDemo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RunDemoLoop();
            }
        });

        final Button buttonCarouselUp = findViewById(R.id.button_carouselup);
        buttonCarouselUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    String cmdParams = String.format("a %s %s", distanceCarousel, feedrateCarousel);
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
                }
                catch (Exception e) {}
            }
        });

        final Button buttonCarouselDown = findViewById(R.id.button_carouseldown);
        buttonCarouselDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    String cmdParams = String.format("a %s %s", -distanceCarousel, feedrateCarousel);
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
                }
                catch (Exception e) {}
            }
        });

        final Button buttonRadialUp = findViewById(R.id.button_radialup);
        buttonRadialUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    String cmdParams = String.format("x %s %s", distanceRadial, feedrateRadial);
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
                }
                catch (Exception e) {}
            }
        });

        final Button buttonRadialDown = findViewById(R.id.button_radialdown);
        buttonRadialDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    String cmdParams = String.format("x %s %s", -distanceRadial, feedrateRadial);
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
                }
                catch (Exception e) {}
            }
        });

        final Button buttonZUp = findViewById(R.id.button_zup);
        buttonZUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    String cmdParams = String.format("z %s %s", distanceZ, feedrateZ);
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
                }
                catch (Exception e) {}
            }
        });

        final Button buttonZDown = findViewById(R.id.button_zdown);
        buttonZDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    String cmdParams = String.format("z %s %s", -distanceZ, feedrateZ);
                    tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
                }
                catch (Exception e) {}
            }
        });

        // Text button listeners
        final Button buttonBinUp = findViewById(R.id.button_bin_up);
        buttonBinUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (binNumber >= 14) {  // Only 14 bins are available out of 15 total
                    return;
                }
                TextView editText = findViewById(R.id.numbox_bin);
                binNumber = Integer.parseInt(editText.getText().toString()) + 1;
                editText.setText(Integer.toString(binNumber));
            }
        });

        final Button buttonBinDown = findViewById(R.id.button_bin_down);
        buttonBinDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (binNumber <= 1) {
                    return;
                }
                TextView editText = findViewById(R.id.numbox_bin);
                binNumber = Integer.parseInt(editText.getText().toString()) - 1;
                editText.setText(Integer.toString(binNumber));
            }
        });

        final Button buttonCarouselDistanceUp = findViewById(R.id.button_carousel_distance_up);
        buttonCarouselDistanceUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.carousel_distance);
                distanceCarousel = Integer.parseInt(editText.getText().toString()) + 1;
                editText.setText(Integer.toString(distanceCarousel));
            }
        });
        final Button buttonCarouselDistanceDown = findViewById(R.id.button_carousel_distance_down);
        buttonCarouselDistanceDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.carousel_distance);
                distanceCarousel = Integer.parseInt(editText.getText().toString()) - 1;
                editText.setText(Integer.toString(distanceCarousel));
            }
        });
        final Button buttonCarouselFeedRateUp = findViewById(R.id.button_carousel_feedrate_up);
        buttonCarouselFeedRateUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.carousel_feedrate);
                feedrateCarousel = Integer.parseInt(editText.getText().toString()) + 100;
                editText.setText(Integer.toString(feedrateCarousel));
            }
        });
        final Button buttonCarouselFeedRateDown = findViewById(R.id.button_carousel_feedrate_down);
        buttonCarouselFeedRateDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.carousel_feedrate);
                feedrateCarousel = Integer.parseInt(editText.getText().toString()) - 100;
                editText.setText(Integer.toString(feedrateCarousel));
            }
        });

        final Button buttonRadialDistanceUp = findViewById(R.id.button_radial_distance_up);
        buttonRadialDistanceUp.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
               TextView editText = findViewById(R.id.radial_distance);
               distanceRadial = Integer.parseInt(editText.getText().toString()) + 1;
               editText.setText(Integer.toString(distanceRadial));
           }
        });
        final Button buttonRadialDistanceDown = findViewById(R.id.button_radial_distance_down);
        buttonRadialDistanceDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.radial_distance);
                distanceRadial = Integer.parseInt(editText.getText().toString()) - 1;
                editText.setText(Integer.toString(distanceRadial));
            }
        });
        final Button buttonRadialFeedRateUp = findViewById(R.id.button_radial_feedrate_up);
        buttonRadialFeedRateUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.radial_feedrate);
                feedrateRadial = Integer.parseInt(editText.getText().toString()) + 100;
                editText.setText(Integer.toString(feedrateRadial));
            }
        });
        final Button buttonRadialFeedRateDown = findViewById(R.id.button_radial_feedrate_down);
        buttonRadialFeedRateDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.radial_feedrate);
                feedrateRadial = Integer.parseInt(editText.getText().toString()) - 100;
                editText.setText(Integer.toString(feedrateRadial));
            }
        });

        final Button buttonZDistanceUp = findViewById(R.id.button_z_distance_up);
        buttonZDistanceUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.z_distance);
                distanceZ = Integer.parseInt(editText.getText().toString()) + 1;
                editText.setText(Integer.toString(distanceZ));
            }
        });
        final Button buttonZDistanceDown = findViewById(R.id.button_z_distance_down);
        buttonZDistanceDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.z_distance);
                distanceZ = Integer.parseInt(editText.getText().toString()) - 1;
                editText.setText(Integer.toString(distanceZ));
            }
        });
        final Button buttonZFeedRateUp = findViewById(R.id.button_z_feedrate_up);
        buttonZFeedRateUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.z_feedrate);
                feedrateZ = Integer.parseInt(editText.getText().toString()) + 100;
                editText.setText(Integer.toString(feedrateZ));
            }
        });
        final Button buttonZFeedRateDown = findViewById(R.id.button_z_feedrate_down);
        buttonZFeedRateDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView editText = findViewById(R.id.z_feedrate);
                feedrateZ = Integer.parseInt(editText.getText().toString()) - 100;
                editText.setText(Integer.toString(feedrateZ));
            }
        });
    }

    protected void createReceiver()
    {
        if(null==mReceiver) {
            /*
             * need to extend this for use here
             */
            mReceiver = new IntentBroadcast(null);
        }
    }

    // Sandbox testing grounds.
    private void RunDemoLoop() {
        try {
            //tg.doFullDispense(null, 666);
        } catch (Exception e) {
            Log.d(TAG, "Demo loop failed");
        }
    }

    /*class readPressureSensorTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "Some string result to pass to onPostExecute";

            int pressure = tg.pressureSensor.getPressure();
            result = Integer.toString(pressure);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            TextView editText = findViewById(R.id.result_text_pressure);
            editText.setText(result);
        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Activity Destroyed");
    }
}
