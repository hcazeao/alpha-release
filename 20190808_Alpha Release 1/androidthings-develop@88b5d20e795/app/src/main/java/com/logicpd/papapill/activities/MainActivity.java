package com.logicpd.papapill.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.arch.persistence.room.Room;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.logicpd.papapill.App;
import com.logicpd.papapill.BuildConfig;
import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.AppModel;
import com.logicpd.papapill.data.DaySchedule;
import com.logicpd.papapill.device.gpio.SpeakerManager;
import com.logicpd.papapill.device.gpio.TinyGResetManager;
import com.logicpd.papapill.enums.BuildFlavorsEnum;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.CarouselPosIntentEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.fragments.help.DeveloperFragment;
import com.logicpd.papapill.fragments.help.DeveloperTestFragment;
import com.logicpd.papapill.fragments.my_medication.CannotRetrieveMedFragment;
import com.logicpd.papapill.fragments.my_medication.DispenseMedsFragment;
import com.logicpd.papapill.fragments.my_medication.GetAsNeededMedicationFragment;
import com.logicpd.papapill.fragments.my_medication.GetScheduledMedEarlyFragment;
import com.logicpd.papapill.fragments.my_medication.ManualRetrievalFragment;
import com.logicpd.papapill.fragments.my_medication.MyMedicationFragment;
import com.logicpd.papapill.fragments.my_medication.TakeMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectMedLocationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.AddUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.SelectDeleteUserFragment;
import com.logicpd.papapill.interfaces.OnServiceListener;
import com.logicpd.papapill.receivers.IntentBroadcast;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.my_medication.AlarmReceivedFragment;
import com.logicpd.papapill.fragments.power_on.WelcomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.misc.CloudManager;
import com.logicpd.papapill.receivers.ConnectionReceiver;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.repositories.SystemRepository;
import com.logicpd.papapill.services.CarouselPosService;
import com.logicpd.papapill.services.MedicationScheduleService;
import com.logicpd.papapill.utils.FragmentUtils;
import com.logicpd.papapill.utils.PreferenceUtils;
import com.logicpd.papapill.utils.ReflectionUtil;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.AddNewUser;
import com.logicpd.papapill.wireframes.workflows.DeleteUser;
import com.logicpd.papapill.wireframes.workflows.EditUser;
import com.logicpd.papapill.wireframes.workflows.GetAsNeededMedication;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.util.List;

import static com.logicpd.papapill.App.getContext;

/**
 * The main activity - controls all of the associated fragments
 * @author alankilloren
 *
 * TODO 12April - start thinking about Model-views-XXX
 *  1. Imagine MainActivity as the 'main' controller
 *  2. AppModel is the 'main' model
 *  3. Fragments are the 'views' (sub-controllers too -- all mumble jumble)
 *  4. Each wireframe feature deserves its own 'model'
 */
public class MainActivity extends Activity implements OnServiceListener, OnButtonClickListener, ConnectionReceiver.ConnectionReceiverListener {
    public static final String TAG = "MainActivity";

    private final int REQUEST_PERMISSION_CAMERA = 1;
    private AppModel mAppModel;
    private FrameLayout rootLayout;
    private AppDatabase appDb;
    private CloudManager cloudManager;
    private DaySchedule UserASchedule, UserBSchedule;
    private DispenseEventReceiver dispenseEventReceiver;
    private AudioManager audioManager;
    private List<ScheduleEntity> scheduleQueue;
    protected BroadcastReceiver mReceiver;

    /* OpenCV requires an initialization for the Android/Jsva version of their toolset at the
     * activity level.  This is that initialization.
     */
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    //mOpenCvCameraView.enableView();
                }
                break;

                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //setTheme(R.style.Sunrise);//TODO testing
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAppModel = new AppModel(this);

        // Enable the speaker amplifier
        SpeakerManager.getInstance().enableSpeaker();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            int volume = PreferenceUtils.getAudioVolume();
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);//TODO setting default volume at 75
        }

        // create room database
        appDb = Room.databaseBuilder(this, AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();

        rootLayout = findViewById(R.id.layout_root);

        //System is yelling that I need to code for permissions to the camera.
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                showExplanation("Permission Needed", "The system requires access to the camera", Manifest.permission.CAMERA, REQUEST_PERMISSION_CAMERA);
            } else {
                requestPermission(Manifest.permission.CAMERA, REQUEST_PERMISSION_CAMERA);
            }
        }

        // first-time run
        if (PreferenceUtils.getFirstTimeRun()) {
            //TODO following lines are for developer:
            BaseWireframe model = mAppModel.getBaseModel();
            if (model.getSystemKey() == null || model.getSystemKey().isEmpty()) {
                new SystemRepository().insert(AppConstants.DEFAULT_SYSTEM_KEY);
                PreferenceUtils.setFirstTimeRun(false);
            }

            FragmentUtils.showFragment(this, new WelcomeFragment(), rootLayout, null, "Home");
        } else {
            FragmentUtils.showFragment(this, new HomeFragment(), rootLayout, null, "Home");
            //getScheduleData();
        }

        AppConfig appConfig = AppConfig.getInstance();
        appConfig.loadFile(this.getApplicationContext());

        checkConnection();//check to see if we are connected to network

        // Initialize the Tinyg driver. Reset first due to uart startup issues
        TinyGResetManager.getInstance().resetTinyG();
        if (appConfig.isTinyGAvailable) {
            if (!TinyGDriver.getInstance().initialize(false)) {
                Log.e(TAG, "Failed to initialize TinyG driver.");
            }
        }

        // Start the medication scheduler service when booting the app.
        mAppModel.gotoStateIdle();
    }

    public void enterFeature(String featureName) {
        mAppModel.enterFeature(featureName);
    }

    public void exitFeature(String simpleName) {
        mAppModel.exitFeature(simpleName);
    }

    public void exitFeature() {
        mAppModel.exitFeature(null);
    }

    public BaseWireframe getFeatureModel() {
        return mAppModel.getFeatureModel(null);
    }

    public BaseWireframe getFeatureModel(String featureName) {
        return mAppModel.getFeatureModel(featureName);
    }

    /**
     * Start the medication schedule service
     */
    public void startMedicationScheduleService() {
        Log.d(TAG, "startMedicationScheduleService");

        StopIfRunningService(MedicationScheduleService.class.getName());

        Intent intent = new Intent(this, MedicationScheduleService.class);
        startService(intent);
    }

    /*
     * Start carousel position service
     * unit is idle
     */
    public void startCarouselPosService(CarouselPosCmdEnum command,
                                        int binId)
    {
        if(AppConfig.getInstance().isTinyGAvailable) {
            StopIfRunningService(CarouselPosService.class.getName());

            // start service
            Log.d(TAG, "startCarouselPosService()");
            Intent intent = new Intent(this, CarouselPosService.class);
            intent.putExtra(CarouselPosIntentEnum.command.toString(), command);
            intent.putExtra(CarouselPosIntentEnum.binId.toString(), binId);
            startService(intent);
        }
    }

    private void StopIfRunningService(String serviceName)
    {
        // stop service if already running
        final ActivityManager activityManager = (ActivityManager)getContext().getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            String runningServiceName = runningServiceInfo.service.getClassName();

            Log.d(TAG, String.format("running: %s myService: %s", runningServiceName, serviceName));
            if (runningServiceName.equals(serviceName)) {
                if (runningServiceName.equals(CarouselPosService.class.getName())) {
                    stopCarouselPosService();
                }
                if (runningServiceName.equals(MedicationScheduleService.class.getName())) {
                    stopMedicationScheduleService();
                }
            }
        }
    }

    /*
     * Stop carousel position service
     * unit is about to perform: dispense, load-meds, or manual-access
     */
    public void stopCarouselPosService()
    {
        if(AppConfig.getInstance().isTinyGAvailable) {
            Log.d(TAG, "stopCarouselPosService()");
            Intent intent = new Intent(this, CarouselPosService.class);
            stopService(intent);
            TinyGDriver.getInstance().commandManager.clearAllCommands();
        }
    }

    /**
     * Stop medication scheduler service
     * (Should be stopped when unit is about to dispense, load meds, manual access)
     */
    public void stopMedicationScheduleService() {
        Log.d(TAG, "stopMedicationScheduleService()");
        Intent intent = new Intent(this, MedicationScheduleService.class);
        stopService(intent);
    }

    @Override
    public void onButtonClicked(Bundle bundle) {
        processFragment(bundle);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();//exit
        } else {
            getFragmentManager().popBackStack();//go back to previous fragment
        }
    }

    /*
     * Backdoor to add a fragment -- for testing right now
     */
    public void addFragment(Fragment fragment,
                            Bundle bundle,
                            String fragmentName)
    {
        FragmentUtils.showFragment(this, fragment, rootLayout, bundle, fragmentName);
    }


    /**
     * Handles displaying of fragments via onButtonClicked callback
     *
     * @param bundle passed in Bundle object from fragment
     */
    private void processFragment(Bundle bundle) {
        String fragmentName;

        if (bundle != null) {
            fragmentName = bundle.getString("fragmentName");

            if (bundle.containsKey("updateSchedule")) {
                if (bundle.getBoolean("updateSchedule")) {
                    //getScheduleData();
                }
            }

            //removes a fragment from the back stack
            if (bundle.containsKey("removeFragment")) {
                if (bundle.getBoolean("removeFragment")) {
                    FragmentUtils.removeFragment(this);
                }
            }

            //removes all fragments
            if (bundle.containsKey("removeAllFragments")) {
                if (bundle.getBoolean("removeAllFragments")) {
                    FragmentUtils.removeAllFragments(this);
                }
            }

            //removes all fragments up to the specified fragment
            if (bundle.containsKey("removeAllFragmentsUpToCurrent")) {
                FragmentUtils.removeAllFragmentsUpToCurrent(this, bundle.getString("removeAllFragmentsUpToCurrent"));
            }

            //displays a fragment and adds to back stack
            if (fragmentName != null) {

                setApplicationState(fragmentName);
                inflateFragment(fragmentName, bundle);

                //This is to maintain the data integrity in the system
                checkDataIntegrity(fragmentName);
            }
        }
    }

    /*
     * Application state changes while entering fragment
     *
     * For state change 'idle' -> 'feature'
     * see: MyMedicationFragment, ManageUsersFragment, ManageMedsFragment
     */
    private void setApplicationState(String fragmentName) {
        switch(fragmentName) {
            case "Home":
            case HomeFragment.TAG:
            case MyMedicationFragment.TAG:
            case ManageUsersFragment.TAG:
            case ManageMedsFragment.TAG:
                mAppModel.gotoStateIdle();
                break;

            case DeveloperFragment.TAG:
            case DeveloperTestFragment.TAG:
                stopMedicationScheduleService(); // fall through
            case SelectMedLocationFragment.TAG:
                stopCarouselPosService();
                break;


            case DispenseMedsFragment.TAG:
                mAppModel.gotoStateMotion4Meds();
                break;

            // leaving state: Motion4Meds -> feature
            case CannotRetrieveMedFragment.TAG:
            case ManualRetrievalFragment.TAG:
            case TakeMedsFragment.TAG:
                mAppModel.gotoStateFeature();
        }
    }

    /*
     * Inflate a fragment and put on top of view stack
     */
    private void inflateFragment(String fragmentName,
                                 Bundle bundle) {
        switch(fragmentName) {
            case "Back":
                onBackPressed();
                break;

            case "Exit":
                finish();
                break;

            case "Home":
            case HomeFragment.TAG:
                FragmentUtils.removeAllFragments(this);
                fragmentName = HomeFragment.class.getSimpleName();
                // intentional fall through

            default:
                Class<?> clazz = ReflectionUtil.getClassPath(fragmentName);
                try {
                    Log.i(TAG, "Showing Fragment: " + fragmentName);
                    FragmentUtils.showFragment(this, (Fragment) clazz.newInstance(), rootLayout, bundle, fragmentName);
                }
                catch (Exception ex){
                    Log.e(TAG, "failed to find fragment package:"+ex);
                }
        }
    }

    private void checkConnection() {
        boolean isConnected = ConnectionReceiver.isConnected();
        if (!isConnected) {
            //TODO show a No Internet Alert or Dialog?
            Log.e(AppConstants.TAG, "No network connection!");
        } else {
            Log.d(AppConstants.TAG, "Connected to network");

            //we're connected to the internet, so initialize Cloud communications
            cloudManager = CloudManager.getInstance(this);
            cloudManager.setOnCloudCallbackListener(new CloudManager.CloudCallbackListener() {
                @Override
                public void onCloudCallback(String msg, Object object) {
                    Log.d(AppConstants.TAG, "MainActivity.OnCloudCallbackListener - Message received from IoT hub: " + msg);

                    //TODO handle callbacks from the cloud for this activity here
                }
            });
            cloudManager.connect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cloudManager != null && cloudManager.isConnected()) {
            cloudManager.close();
        }
        if (dispenseEventReceiver != null) {
            unregisterReceiver(dispenseEventReceiver);
        }
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }

    protected void createReceiver()
    {
        if(null==mReceiver) {
            mReceiver = new IntentBroadcast(getFragmentManager());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        createReceiver();
        registerReceiver(mReceiver, new IntentFilter(IntentBroadcast.INTENT_MESSAGE));

        if (dispenseEventReceiver == null) {
            dispenseEventReceiver = new DispenseEventReceiver();
        }
        registerReceiver(dispenseEventReceiver, new IntentFilter(AppConstants.DISPENSE_EVENT_INTENT));

        App.getInstance().setConnectionListener(this);
        BuildFlavorsEnum flavor = BuildFlavorsEnum.valueOf(BuildConfig.FLAVOR.toUpperCase());
        switch(flavor) {
            case BREADBOARD:
            case VISIONONLY:
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
                break;
        }
        FirebaseApp.initializeApp(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            Log.e(AppConstants.TAG, "No network connection!");
            //TODO show a No Internet Alert or Dialog?
        } else {
            Log.d(AppConstants.TAG, "Connected to network");
        }
    }

    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    /**
     * This receiver kicks off when a pill is ready to be dispensed. It handles intents sent
     * by the Medication Schedule Service.
     */
    public class DispenseEventReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Received Dispense Event Intent");
            if (intent == null) {
                return;
            }

            String action = intent.getAction();
            if (action != null && action.equals(AppConstants.DISPENSE_EVENT_INTENT)) {
                int scheduleId = intent.getIntExtra(AppConstants.DISPENSE_EVENT_SCHEDULE, -1);
                if (scheduleId > -1) {

                    BaseWireframe model = mAppModel.getBaseModel();
                    JoinScheduleDispense scheduleItem = model.getScheduleItem(scheduleId);

                    if(null != scheduleItem) {
                        // We are about to display the AlarmReceivedFragment, stop the medication
                        // scheduler service...
                        stopMedicationScheduleService();

                        // Sound alarm and show AlarmReceivedFragment
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("scheduleItem", scheduleItem);
                        String fragmentName = "AlarmReceiverFragment";
                        FragmentUtils.showFragment(MainActivity.this, new AlarmReceivedFragment(), rootLayout, bundle, fragmentName);
                    }
                    else {
                        Log.e(TAG, "ScheduleItem is null with scheduleId:"+scheduleId);
                    }
                }
            }
        }
    }

    //This method is the center place where whe can do any data integrity check and correction
    //Right now Medication data integrity check and correction implemented
    //Later a point any other data also can be validated and processed here.
    private void checkDataIntegrity(String fragmentName){
        if (fragmentName == "Home"){
            BaseWireframe model = mAppModel.getBaseModel();

            //This will check the medication data integrity and clean the database
            List<MedicationEntity> medications = model.getMedications();
            if(medications != null){
                for (MedicationEntity medication : medications) {
                    //Medication location will Bin Location(1-14), Drawer Location (97), Refridgerator Location (98), Other Location (99)
                    //If Medication Location = 0 means there is no location assigned
                    if (medication.getMedicationLocation() == 0){
                        //Delete medication, if there is no location assigned
                        model.removeMedication(medication);
                    } else{
                        if(medication.getMedicationScheduleType() != MedScheduleTypeEnum.AS_NEEDED.value){
                            List<ScheduleEntity> schedules = model.getScheduleItemsByMedication(medication);
                            if (schedules == null || schedules.size() == 0) {
                                //Delete medication, if there is no schedule assigned
                                model.removeMedication(medication);
                            }
                        }
                    }
                }
            }
        }
    }
}