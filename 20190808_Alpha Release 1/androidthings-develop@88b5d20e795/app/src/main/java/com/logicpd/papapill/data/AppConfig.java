package com.logicpd.papapill.data;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.logicpd.papapill.BuildConfig;
import com.logicpd.papapill.PapapillException;
import com.logicpd.papapill.utils.PreferenceUtils;

import org.apache.qpid.proton.amqp.messaging.ApplicationProperties;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/*
 * This is a data object to hold the configuration data as loaded from the config.json file.
 * Brady Hustad - Logic PD - 2018-08-30 - Initial Creation
 * Brady Hustad - Logic PD - 2018-12-12 - Moved to App wide location and renamed to fit all needs.
 */
public class AppConfig {

    public static final String TAG = AppConfig.class.getSimpleName();

    // Device information
    private String serialNumber;
    private String iotHubDeviceEndpoint;
    private String iotHubSharedAccessKey;

    // Open CV camera calibration parameters
    private double[] cameraMatrix; //{1157.761879014254, 0, 820, 0, 1156.661159728501, 616, 0, 0, 1};
    private double[] distortionCoeffs; //{-0.4072881805734203, 0.1463792551013924, 0, 0, 0};

    // Pill detection algorithm parameters
    private double rho0; // radius of bin center arc in millimeters
    private double mmm; // slope of line used to calculate mm/pixel given fill percentage 0-100
    private double mmb; // y-intercept of line used to calculate mm/pixel given fill percentage 0-100
    private int imageWidth; // the width of the image to capture from the camera, used for all calculation on image
    private int imageHeight; // the height of the image to capture from the camera, used for all calculation on image
    private double convergentTheta; // angle in degrees of Convergent point that moves minimally with fill
    private double convergentRho; // radius in millimeters of Convergent point that moves minimally with fill
    private int convergentPixelX; // X coordinate in pixels of Convergent point that moves minimally with fill
    private int convergentPixelY; // Y coordinate in pixels of Convergent point that moves minimally with fill
    private double rotation;

    // Variables for the deadzone of the picking head.
    // - Rho Limit is the positive and negative on the Rho axis for picking from the 0 point.
    // - Theta is the intercept of y = x/12 + b (intercept) bigger number more space, littler less
    double deadzoneRhoMinLimit;
    double deadzoneRhoMaxLimit;
    double deadzoneThetaIntercept;

    // Max number of results (pill locations) algorithm should return
    int detectionsReturned;

    // Dimensions of the bin used for template generation
    private double binThetaMax;
    private double binThetaMin;
    private double binRhoMax;
    private double binDepth; // The depth of the bin in mm.

    // Outputs from template generation
    private double templateSlope;
    private int[][] templateUpperLeftXY;
    private String templateFilename; // Which template image (0-100) to use for the algorithm

    // Peripheral / motor controller parameters
    public boolean isCameraAvailable = false;
    public boolean isTinyGAvailable = false;

    private double absPositionOffset;

    public double camera2TinyGRhoOffset;
    public double camera2TinyGThetaOffset;

    // Carousel calibration parameters
    private double bin1Location;
    private double bin2location;
    private double bin3location;
    private double bin4location;
    private double bin5location;
    private double bin6location;
    private double bin7location;
    private double bin8location;
    private double bin9location;
    private double bin10location;
    private double bin11location;
    private double bin12location;
    private double bin13location;
    private double bin14location;

    // Proximity Sensor Thresholds
    private int doorThresholdHigh;
    private int doorThresholdLow;
    private int drawerThresholdHigh;
    private int drawerThresholdLow;
    private int cupThresholdHigh;
    private int cupThresholdLow;

    /******************************************************
     * Singleton Setup
     ******************************************************/
    private static class InstanceHolder {
        private static final AppConfig mConfig = new AppConfig();
    }

    public static AppConfig getInstance() {
        return InstanceHolder.mConfig;
    }

    /******************************************************
     * Constructors
     ******************************************************/
    // Assumes the file is the generic file from the SD card.
    private AppConfig() {
    }

    private AppConfig(String jsonFile) {
        try {
            parseJson(jsonFile);
            Log.d(TAG, "parseJson()");
        } catch(PapapillException pe) {
            Log.e(TAG, pe.getMessage());
        }
    }

    public boolean loadFile(Context context)
    {
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            File file = new File(sdcard, "config.json");

            BufferedReader br;

            if (file.exists()) {
                Log.d(TAG, "Using config.json found on SD card.");
                br = new BufferedReader(new FileReader(file));
            }
            else {
                Log.d(TAG, "No config.json found on SD card. Using default.");
                br = new BufferedReader(new InputStreamReader(context.getAssets()
                        .open("config.json")));
            }

            StringBuilder text = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
                Log.d(TAG, "text:"+line);
            }
            br.close();

            parseJson(text.toString());
            return true;
        }
        catch (Exception ex)
        {
            Log.e(TAG, "loadFile failed");
            return false;
        }
    }

    /******************************************************
     * Getters and Setters
     ******************************************************/

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getIotHubDeviceEndpoint() {
        return this.iotHubDeviceEndpoint;
    }

    public void setIotHubDeviceEndpoint(String iotHubDeviceEndpoint) {
        this.iotHubDeviceEndpoint = iotHubDeviceEndpoint;
    }

    public String getIotHubSharedAccessKey() {
        return this.iotHubSharedAccessKey;
    }

    public void setIotHubSharedAccessKey(String iotHubSharedAccessKey) {
        this.iotHubSharedAccessKey = iotHubSharedAccessKey;
    }

    public Mat getMatCameraMatrix() {
        Mat cameraMatrix = Mat.eye(3, 3, CvType.CV_64F);
        double[] matrix = this.getCameraMatrix();
        if ( matrix == null) {
            matrix = new double[9];
            PreferenceUtils.setCameraIsCalibrated(false);
        }

        cameraMatrix.put(0, 0, matrix);
        return cameraMatrix;
    }

    public Mat getMatDistortionCoefficients() {
        Mat distortionCoefficients = Mat.zeros(8, 1, CvType.CV_64F);
        double[] coeffs = getDistortionCoeffs();

        if (null==coeffs) {
            coeffs = new double[5];
            PreferenceUtils.setCameraIsCalibrated(false);
        }
        distortionCoefficients.put(0, 0, coeffs);
        return distortionCoefficients;
    }

    public double[] getCameraMatrix() {
        return cameraMatrix;
    }

    private void setCameraMatrix(double[] cameraMatrix) {
        this.cameraMatrix = cameraMatrix;
    }

    public double[] getDistortionCoeffs() {
        return distortionCoeffs;
    }

    private void setDistortionCoeffs(double[] distortionCoeffs) {
        this.distortionCoeffs = distortionCoeffs;
    }

    public double getRho0() {
        return rho0;
    }

    private void setRho0(double rho0) {
        this.rho0 = rho0;
    }

    public double getMmm() {
        return mmm;
    }

    private void setMmm(double mmm) {
        this.mmm = mmm;
    }

    public double getMmb() {
        return mmb;
    }

    private void setMmb(double mmb) {
        this.mmb = mmb;
    }

    public int getImageWidth() { return imageWidth; }

    private void setImageWidth(int imageWidth) { this.imageWidth = imageWidth; }

    public int getImageHeight() { return  imageHeight; }

    private void setImageHeight(int imageHeight) {this.imageHeight = imageHeight; }

    public double getConvergentTheta() {
        return convergentTheta;
    }

    private void setConvergentTheta(double convergentTheta) {
        this.convergentTheta = convergentTheta;
    }

    public double getConvergentRho() {
        return convergentRho;
    }

    private void setConvergentRho(double convergentRho) {
        this.convergentRho = convergentRho;
    }

    public int getConvergentPixelX() {
        return convergentPixelX;
    }

    private void setConvergentPixelX(int convergentPixelX) {
        this.convergentPixelX = convergentPixelX;
    }

    public int getConvergentPixelY() {
        return convergentPixelY;
    }

    private void setConvergentPixelY(int convergentPixelY) {
        this.convergentPixelY = convergentPixelY;
    }

    public double getRotation() {
        return rotation;
    }

    private void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public double getBinThetaMax() {
        return binThetaMax;
    }

    private void setBinThetaMax(double binThetaMax) {
        this.binThetaMax = binThetaMax;
    }

    public double getBinThetaMin() {
        return binThetaMin;
    }

    private void setBinThetaMin(double binThetaMin) {
        this.binThetaMin = binThetaMin;
    }

    public double getBinRhoMax() {
        return binRhoMax;
    }

    private void setBinRhoMax(double binRhoMax) {
        this.binRhoMax = binRhoMax;
    }

    public double getBinDepth() {
        return binDepth;
    }

    private void setBinDepth(double binDepth) {
        this.binDepth = binDepth;
    }

    public double getAbsPositionOffset() { return absPositionOffset; }

    private void setAbsPositionOffset(double absPositionOffset) {
        this.absPositionOffset = absPositionOffset;
    }

    public double getTemplateSlope() { return templateSlope; }

    private void setTemplateSlope(double templateSlope) { this.templateSlope = templateSlope; }

    public int[][] getTemplateUpperLeftXY() { return templateUpperLeftXY; }

    private void setTemplateUpperLeftXY(int[][] templateUpperLeftXY) { this.templateUpperLeftXY = templateUpperLeftXY; }

    public String getTemplateFilename() {
        return templateFilename;
    }

    private void setTemplateFilename(String templateFilename) {
        this.templateFilename = templateFilename;
    }

    public double getDeadzoneRhoMinLimit() {
        return deadzoneRhoMinLimit;
    }

    public void setDeadzoneRhoMinLimit(double deadzoneRhoMinLimit) {
        this.deadzoneRhoMinLimit = deadzoneRhoMinLimit;
    }

    public double getDeadzoneRhoMaxLimit() { return deadzoneRhoMaxLimit; }

    public void setDeadzoneRhoMaxLimit(double deadzoneRhoMaxLimit) {
        this.deadzoneRhoMaxLimit = deadzoneRhoMaxLimit;
    }

    public double getDeadzoneThetaIntercept() {
        return deadzoneThetaIntercept;
    }

    public void setDeadzoneThetaIntercept(double deadzoneThetaIntercept) {
        this.deadzoneThetaIntercept = deadzoneThetaIntercept;
    }

    public int getDetectionsReturned() {
        return detectionsReturned;
    }

    public void setDetectionsReturned(int detectionsReturned) {
        this.detectionsReturned = detectionsReturned;
    }

    public void setCameraAvailable(boolean cameraIsAvailable) {
        isCameraAvailable = cameraIsAvailable;
    }

    public boolean getCameraAvailable() {
        return isCameraAvailable;
    }

    public void setTinyGAvailable(boolean tinyGIsAvailable) {
        isTinyGAvailable = tinyGIsAvailable;
    }

    public boolean getTinyGAvailable() {
        return isTinyGAvailable;
    }

    public double getCamera2TinyGRhoOffset() {
        return camera2TinyGRhoOffset;
    }

    public void setCamera2TinyGRhoOffset(double rho) {
        camera2TinyGRhoOffset = rho;
    }

    public double getCamera2TinyGThetaOffset() {
        return camera2TinyGThetaOffset;
    }

    public void setCamera2TinyGThetaOffset(double theta) {
        camera2TinyGThetaOffset = theta;
    }

    public void setBin1location(double location) { bin1Location = location; }
    public double getBin1location() { return bin1Location; }
    public void setBin2location(double location) { bin2location = location; }
    public double getBin2location() { return bin2location; }
    public void setBin3location(double location) { bin3location = location; }
    public double getBin3location() { return bin3location; }
    public void setBin4location(double location) { bin4location = location; }
    public double getBin4location() { return bin4location; }
    public void setBin5location(double location) { bin5location = location; }
    public double getBin5location() { return bin5location; }
    public void setBin6location(double location) { bin6location = location; }
    public double getBin6location() { return bin6location; }
    public void setBin7location(double location) { bin7location = location; }
    public double getBin7location() { return bin7location; }
    public void setBin8location(double location) { bin8location = location; }
    public double getBin8location() { return bin8location; }
    public void setBin9location(double location) { bin9location = location; }
    public double getBin9location() { return bin9location; }
    public void setBin10location(double location) { bin10location = location; }
    public double getBin10location() { return bin10location; }
    public void setBin11location(double location) { bin11location = location; }
    public double getBin11location() { return bin11location; }
    public void setBin12location(double location) { bin12location = location; }
    public double getBin12location() { return bin12location; }
    public void setBin13location(double location) { bin13location = location; }
    public double getBin13location() { return bin13location; }
    public void setBin14location(double location) { bin14location = location; }
    public double getBin14location() { return bin14location; }

    public void setDoorThresholdHigh(int threshold) { doorThresholdHigh = threshold; }
    public int getDoorThresholdHigh() { return doorThresholdHigh; }
    public void setDoorThresholdLow(int threshold) { doorThresholdLow = threshold; }
    public int getDoorThresholdLow() { return doorThresholdLow; }
    public void setDrawerThresholdHigh(int threshold) { drawerThresholdHigh = threshold; }
    public int getDrawerThresholdHigh() { return drawerThresholdHigh; }
    public void setDrawerThresholdLow(int threshold) { drawerThresholdLow = threshold; }
    public int getDrawerThresholdLow() { return drawerThresholdLow; }
    public void setCupThresholdHigh(int threshold) { cupThresholdHigh = threshold; }
    public int getCupThresholdHigh() { return cupThresholdHigh; }
    public void setCupThresholdLow(int threshold) { cupThresholdLow = threshold; }
    public int getCupThresholdLow() { return cupThresholdLow; }

    /******************************************************
     * Private Methods
     ******************************************************/

    /*
     * This parses the config.json file into the various fields.
     */
    public void parseJson(String jsonFile) throws PapapillException {
        try {
            JSONObject config = new JSONObject(jsonFile);

            setSerialNumber(config.getString("serialNumber"));
            setIotHubDeviceEndpoint(config.getString("iotHubDeviceEndpoint"));
            setIotHubSharedAccessKey(config.getString("iotHubSharedAccessKey"));

            String cameraMatrix = config.getString("cameraMatrix");
            List<String> cm = Arrays.asList(cameraMatrix.split("\\s*,\\s*"));
            double[] cmArray = new double[cm.size()];
            for (int i = 0; i < cm.size(); i++) {
                cmArray[i] = Double.parseDouble(cm.get(i));
            }
            setCameraMatrix(cmArray);

            String distortionCoeffs = config.getString("distortionCoefficients");
            List<String> dc = Arrays.asList(distortionCoeffs.split("\\s*,\\s*"));
            double[] dcArray = new double[dc.size()];
            for (int i = 0; i < dc.size(); i++) {
                dcArray[i] = Double.parseDouble(dc.get(i));
            }
            setDistortionCoeffs(dcArray);

            setRho0(config.getDouble("rho0"));
            setMmm(config.getDouble("mmm"));
            setMmb(config.getDouble("mmb"));
            setImageWidth(config.getInt("imageWidth"));
            setImageHeight(config.getInt("imageHeight"));
            setConvergentTheta(Math.toRadians(config.getDouble("convergentTheta")));
            setConvergentRho(getRho0() + config.getDouble("convergentRho"));
            setConvergentPixelX(config.getInt("convergentPixelX"));
            setConvergentPixelY(config.getInt("convergentPixelY"));
            setRotation(Math.toRadians(config.getDouble("rotation")));

            setDeadzoneRhoMinLimit(config.getDouble("deadzoneRhoMinLimit"));
            setDeadzoneRhoMaxLimit(config.getDouble("deadzoneRhoMaxLimit"));
            setDeadzoneThetaIntercept(config.getDouble("deadzoneThetaIntercept"));

            setDetectionsReturned(config.getInt("detectionsReturned"));

            setBinThetaMax(config.getDouble("binThetaMax"));
            setBinThetaMin(config.getDouble("binThetaMin"));
            setBinRhoMax(config.getDouble("binRhoMax"));
            setBinDepth(config.getDouble("binDepth"));

            setAbsPositionOffset(config.getDouble("absPositionOffset"));

            setTemplateSlope(config.getDouble("templateSlope"));

            String templateULXY = config.getString("templateUpperLeftXY");
            List<String> ulxy = Arrays.asList(templateULXY.split("\\s*,\\s*"));
            //Log.d(TAG, "ulxy content:" + templateULXY);
            //Log.d(TAG, "ulxy size" + ulxy.size());
            int[][] ulxyArray = new int[ulxy.size() / 3][3];
            int ptrThree = 0;
            int ptrArray = 0;
            for (int i = 0; i < ulxy.size(); i++) {
                if (ptrThree >= 3) {
                    ptrThree = 0;
                    ptrArray++;
                }
                ulxyArray[ptrArray][ptrThree] = Integer.parseInt(ulxy.get(i));
                //Log.d(TAG, "ulArray[" + ptrArray + "][" + ptrThree + "] = " + ulxyArray[ptrArray][ptrThree]);
                ptrThree++;
            }
            setTemplateUpperLeftXY(ulxyArray);

            setTemplateFilename(config.getString("templateFilename"));

            setCameraAvailable(config.getBoolean("isCameraAvailable"));
            setTinyGAvailable(config.getBoolean("isTinyGAvailable"));

            String offsetRho = config.getString("camera2TinyGRhoOffset");
            setCamera2TinyGRhoOffset(Double.parseDouble(offsetRho));

            String offsetTheta = config.getString("camera2TinyGThetaOffset");
            setCamera2TinyGThetaOffset(Double.parseDouble(offsetTheta));

            String bin1location = config.getString("bin1location");
            setBin1location(Double.parseDouble(bin1location));
            String bin2location = config.getString("bin2location");
            setBin2location(Double.parseDouble(bin2location));
            String bin3location = config.getString("bin3location");
            setBin3location(Double.parseDouble(bin3location));
            String bin4location = config.getString("bin4location");
            setBin4location(Double.parseDouble(bin4location));
            String bin5location = config.getString("bin5location");
            setBin5location(Double.parseDouble(bin5location));
            String bin6location = config.getString("bin6location");
            setBin6location(Double.parseDouble(bin6location));
            String bin7location = config.getString("bin7location");
            setBin7location(Double.parseDouble(bin7location));
            String bin8location = config.getString("bin8location");
            setBin8location(Double.parseDouble(bin8location));
            String bin9location = config.getString("bin9location");
            setBin9location(Double.parseDouble(bin9location));
            String bin10location = config.getString("bin10location");
            setBin10location(Double.parseDouble(bin10location));
            String bin11location = config.getString("bin11location");
            setBin11location(Double.parseDouble(bin11location));
            String bin12location = config.getString("bin12location");
            setBin12location(Double.parseDouble(bin12location));
            String bin13location = config.getString("bin13location");
            setBin13location(Double.parseDouble(bin13location));
            String bin14location = config.getString("bin14location");
            setBin14location(Double.parseDouble(bin14location));

            setDoorThresholdHigh(config.getInt("doorThresholdHigh"));
            setDoorThresholdLow(config.getInt("doorThresholdLow"));
            setDrawerThresholdHigh(config.getInt("drawerThresholdHigh"));
            setDrawerThresholdLow(config.getInt("drawerThresholdLow"));
            setCupThresholdHigh(config.getInt("cupThresholdHigh"));
            setCupThresholdLow(config.getInt("cupThresholdLow"));
        }
        catch (JSONException je)
        {
            Log.e(TAG, je.getMessage());
            throw new PapapillException("Error in building config information: " + je.getMessage());
        }
    }
}
