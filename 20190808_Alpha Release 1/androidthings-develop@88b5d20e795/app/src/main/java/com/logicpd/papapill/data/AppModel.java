package com.logicpd.papapill.data;

import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.interfaces.OnServiceListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.IWireframe;

import org.bouncycastle.jce.provider.symmetric.ARC4;

import java.util.ArrayList;
import java.util.List;

/*
 * Application Model
 * 1. provides data access (Serialize/Deserialize/data-marshalling)
 *    API accessible anywhere within this activity and lifecycle.
 * 2. provide a state machine to orchestrate actions for different services (threads).
 * 3. does NOT provide view model NOR view states.
 */
public class AppModel {

    // TODO : Use composition instead of inheritance for db access
    public List<BaseWireframe> wireframes; // current feature model as described in wireframe document
    private OnServiceListener mServiceListener;
    private final int BASE_MODEL_COUNT = 1;

    public AppModel(OnServiceListener listener) {
        // container to hold current feature(s) model(s)
        wireframes = new ArrayList<BaseWireframe>();
        insertBaseModel();
        mServiceListener = listener;
    }

    public BaseWireframe getBaseModel() {
        return wireframes.get(0);
    }

    private void insertBaseModel() {
        BaseWireframe baseModel = new BaseWireframe(null);
        wireframes.add(baseModel);
    }

    /*
     * reset to IDLE, clear all sub-models
     */
    public void reset() {
        wireframes.clear();
        insertBaseModel();
        gotoStateIdle();
    }
    /*
     * Enter into a feature
     * 1. Create a wireframe model and insert into list
     * NOTE: we can nest features
     * 2. Elevate state [IDLE->FEATURE, FEATURE->FEATURE]
     */
    public boolean enterFeature(String featureName) {
        BaseWireframe parent = getFeatureModel(null);
        BaseWireframe model = (BaseWireframe)BundleFactory.createModel(featureName, parent);
        if(null==model){
            return false;
        }
        wireframes.add(model);
        gotoStateFeature();
        return true;
    }

    /*
     * 1. Retrieve current feature model
     * 2. or a specific
     * 3. create new if not exist
     */
    public BaseWireframe getFeatureModel(String featurename) {

        int size = wireframes.size();
        if(0==size) {
            return null;
        }

        if(null!=featurename) {
            for(int i=0; i<size; i++){
                BaseWireframe model = wireframes.get(i);
                if(model.getClass().getSimpleName().equals(featurename))
                    return model;
            }

            // couldn't find feature, so start it...
            enterFeature(featurename);
            size++;
        }

        // default: get last feature in list
        return wireframes.get(--size);
    }

    /*
     * Exit a feature
     * 1. Remove a wireframe model from list
     * NOTE: we can nested features
     * 2. Reduce state [FEATURE->IDLE, FEATURE->FEATURE]
     */
    public boolean exitFeature(String simpleName) {
        int size = wireframes.size();
        if(0==size) {
            reset();
            return false;
        }

        if(isDeleteOK(simpleName)){
            wireframes.remove(--size);
        }

        if(size<=BASE_MODEL_COUNT) {
            gotoStateIdle();
        }
        return true;
    }

    /*
     * Check name match or not base model
     */
    private boolean isDeleteOK(String simpleName) {
        int size = wireframes.size();
        if(size <= 1)
            return false;

        BaseWireframe lastModel = wireframes.get(size-1);
        if(lastModel.getClass().getSimpleName().equals(simpleName)) {
            return true;
        }
        return false;
    }

    /*
     * IDLE
     * 1. Displaying HOME or TIER 1 fragments [MyMeds, System, Help, etc]
     * 2. Free from Dispense or Load Meds or Manual Retrieval or intensive computations (Detector)
     * 3. Execute MedicationScheduleService and CarouselPosService in this state.
     */
    public void gotoStateIdle() {
        wireframes.clear();
        insertBaseModel();

        // turn on MedicationScheduleService
        mServiceListener.startMedicationScheduleService();

        // turn on CarouselPosService (if not already)
        mServiceListener.startCarouselPosService(CarouselPosCmdEnum.home,0);
    }

    /*
     * FEATURE
     * 1. Displaying feature fragment described in wireframe document (example: add user)
     * 2. Free from Dispense or Load Meds or Manual Retrieval or intensive computations (Detector)
     * 3. Execute CarouselPosService in this state.
     */
    public void gotoStateFeature() {

        // turn off MedicationScheduleService
        mServiceListener.stopMedicationScheduleService();

        // CarouselPosService stays on TODO need to know previous bin#
        // mServiceListener.startCarouselPosService(CarouselPosCmdEnum.home,0);
    }

    /*
     * TRANSITION
     * 1. Issued command to end motion in CarouselPosService.
     * 2. Destroy CarouselPosService after stopping.
     */
    public void gotoStatePreTransition() {
        // turn off CarouselPosService
        //mServiceListener.stopCarouselPosService();
    }

    /*
     * Motion 4 Medication
     * 1. Dispense or Load Meds or Manual Retrieval or intensive computations (Detector)
     * 2. Make sure MedicationScheduleService and CarouselPosService are destroyed.
     */
    public void gotoStateMotion4Meds() {

        // turn off CarouselPosService -- again for insurance
         mServiceListener.stopCarouselPosService();
         mServiceListener.stopMedicationScheduleService();

        // start Commands
    }

    /*
     * TRANSITION
     * 1. Issued command to end motion but not done.
     * 2. Make sure MedicationScheduleService and CarouselPosService are destroyed.
     */
    public void gotoStatePostTransition() {
        // turn off CarouselPosService
        // turn off Commands
    }

    /*
     * ERROR
     * 1. Don't run any commands.
     * 2. Make sure MedicationScheduleService and CarouselPosService are destroyed.
     */
    public void gotoStateError() {

         mServiceListener.stopCarouselPosService();
         mServiceListener.stopMedicationScheduleService();
    }
}
