package com.logicpd.papapill.data;

import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.ScheduleEntity;

import java.util.ArrayList;
import java.util.List;

public class DaySchedule {

    private String day;
    private String date;
    private List<JoinScheduleDispense> scheduleItemList;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<JoinScheduleDispense> getScheduleItemList() {
        return scheduleItemList;
    }

    public List<ScheduleEntity> getScheduleList() {
        List<ScheduleEntity> list = new ArrayList<ScheduleEntity>();
        for(JoinScheduleDispense entity : scheduleItemList)
        {
            list.add(entity.getScheduleEntity());
        }
        return list;
    }

    public List<ScheduleEntity> getScheduleListWithValidDispenseAmounts() {
        List<ScheduleEntity> list = new ArrayList<ScheduleEntity>();
        for(JoinScheduleDispense entity : scheduleItemList)
        {
            if (entity.getDispenseAmount() > 0) {
                list.add(entity.getScheduleEntity());
            }
        }
        return list;
    }

    public void setScheduleItemList(List<JoinScheduleDispense> scheduleItemList) {
        this.scheduleItemList = scheduleItemList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
