package com.logicpd.papapill.data;

import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.ScheduleEntity;

import java.io.Serializable;
import java.util.List;

public class ScheduleList implements Serializable {

    private List<JoinScheduleDispense> schedules;

    public ScheduleList(List<JoinScheduleDispense> schedules)
    {
        this.schedules = schedules;
    }

    public List<JoinScheduleDispense> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<JoinScheduleDispense> schedules) {
        this.schedules = schedules;
    }

    public int count()
    {
        return (null==schedules)? 0 : schedules.size();
    }

    public JoinScheduleDispense getScheduleDispense(int index)
    {
        return (null==schedules)?null : schedules.get(index);
    }
}
