package com.logicpd.papapill.data.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.room.entities.MedicationEntity;

import java.util.List;

/**
 * MedicationsCheckAdapter
 *
 * @author alankilloren
 */
public class MedicationsCheckAdapter extends RecyclerView.Adapter<MedicationsCheckAdapter.DataObjectHolder> {
    private MedicationsCheckAdapter.MyClickListener myClickListener;
    private CheckSelectedListener checkSelectedListener;
    private List<MedicationEntity> medications;

    public MedicationsCheckAdapter(Context context, List<MedicationEntity> resultList) {
        this.medications = resultList;
    }


    @NonNull
    @Override
    public MedicationsCheckAdapter.DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medication_item, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicationsCheckAdapter.DataObjectHolder holder, int position) {
        MedicationEntity medication = medications.get(position);
        String s = medication.getMedicationName() + " "
                + " " + medication.getStrengthMeasurement();
        holder.tvMedicationName.setText(s);

        if (medications.get(position).isPaused()) {
            holder.checkSelected.setChecked(true);
        } else {
            holder.checkSelected.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return medications.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvMedicationName;
        CheckBox checkSelected;

        DataObjectHolder(View itemView) {
            super(itemView);
            tvMedicationName = itemView.findViewById(R.id.textview_medicationName);
            checkSelected = itemView.findViewById(R.id.checkbox_medication_item);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
            notifyDataSetChanged();
        }
    }

    public void selectAll(boolean b) {
        for (MedicationEntity medication : medications) {
            if (b) {
                medication.setPaused(true);
            } else {
                medication.setPaused(false);
            }
        }
        notifyDataSetChanged();
        checkSelectedListener.onCheckSelected();
    }

    public void toggleChecked(int position) {
        if (medications.get(position).isPaused()) {
            medications.get(position).setPaused(false);
        } else {
            medications.get(position).setPaused(true);
        }
        notifyDataSetChanged();
        checkSelectedListener.onCheckSelected();
    }

    public void setOnItemClickListener(MedicationsCheckAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public void setCheckSelectedListener(MedicationsCheckAdapter.CheckSelectedListener checkSelectedListener) {
        this.checkSelectedListener = checkSelectedListener;
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }

    public List<MedicationEntity> getListFromAdapter() {
        return medications;
    }

    /**
     * This interface is used to enable/disable 'Next' button depending on whether something is selected or not
     */
    public interface CheckSelectedListener {
        void onCheckSelected();
    }
}
