package com.logicpd.papapill.device.gpio;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;
import com.logicpd.papapill.device.enums.CommandState;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.IntentTypeEnum;
import com.logicpd.papapill.receivers.IntentBroadcast;

import java.io.IOException;

public class LockManager {
    public String TAG;

    private Handler mHandler;

    private Gpio mGpioDoorLock;
    private Gpio mGpioDoorUnlock;
    private Gpio mGpioDrawerLock;
    private Gpio mGpioDrawerUnlock;

    private static final int PULSES_LOCK_DOOR = 100;
    private static final int PULSES_UNLOCK_DOOR = 100;
    private static final int PULSES_LOCK_DRAWER = 80;
    private static final int PULSES_UNLOCK_DRAWER = 80;

    private static final String GPIO_DOOR_LOCK = "BCM13";
    private static final String GPIO_DOOR_UNLOCK = "BCM19";
    private static final String GPIO_DRAWER_LOCK = "BCM5";
    private static final String GPIO_DRAWER_UNLOCK = "BCM6";

    private LockManager() {
        TAG = this.getClass().getName();
        this.initialize();
    }

    private static class LazyHolder {
        private static final LockManager INSTANCE = new LockManager();
    }

    public static LockManager getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * Open connection to GPIO peripheral manager.
     */
    private void initialize() {
        try {

            HandlerThread mPwmThread = new HandlerThread("lockDriver");
            mPwmThread.start();
            mHandler = new Handler(mPwmThread.getLooper());

            mGpioDoorLock = PeripheralManager.getInstance().openGpio(GPIO_DOOR_LOCK);
            mGpioDoorUnlock = PeripheralManager.getInstance().openGpio(GPIO_DOOR_UNLOCK);
            mGpioDrawerLock = PeripheralManager.getInstance().openGpio(GPIO_DRAWER_LOCK);
            mGpioDrawerUnlock = PeripheralManager.getInstance().openGpio(GPIO_DRAWER_UNLOCK);
            Log.i(TAG, "Opened GPIOs for Lock Driver");

            mGpioDoorLock.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            mGpioDoorUnlock.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            mGpioDrawerLock.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            mGpioDrawerUnlock.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            Log.i(TAG, "GPIOs initially set to output low.");

        } catch (IOException | RuntimeException e) {
            Log.e(TAG,"Failed to Open GPIOs for Lock Driver");
        }
    }

    /**
     * Close connection to GPIO peripheral manager.
     */
    private void close() {
        // Remove pending Runnable from the handler.
        mHandler.removeCallbacksAndMessages(null);
        if (mGpioDoorLock != null) {
            try {
                mGpioDoorLock.close();
                mGpioDoorLock = null;
            } catch (IOException e) {
                Log.w(TAG, "Unable to close GPIO", e);
            }
        }
    }

    public void LockDoor() {
        runLockDriver(mGpioDoorLock, PULSES_LOCK_DOOR);
    }

    public void UnlockDoor() {
        runLockDriver(mGpioDoorUnlock, PULSES_UNLOCK_DOOR);
    }

    public void LockDrawer() {
        runLockDriver(mGpioDrawerLock, PULSES_LOCK_DRAWER);
    }

    public void UnlockDrawer() {
        runLockDriver(mGpioDrawerUnlock, PULSES_UNLOCK_DRAWER);
    }

    void runLockDriver(final Gpio gpio, final int pulses) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Starting runnable for: " + gpio.getName());

                try {
                    // Generate a series of pulses with frequency ~488 Hz, duty cycle 30%.
                    // The number of pulses changes depending on type/direction and is
                    // determined experimentally by adjusting the number and checking
                    // whether the lock motor turned all the way.
                    for (int p = 0; p < pulses; p++) {
                        gpio.setValue(true);
                        Thread.sleep(0, 600000);
                        gpio.setValue(false);
                        Thread.sleep(1, 400000);
                    }

                    IntentMsgEnum intentMsg = IntentMsgEnum.lockDriverError;
                    switch(gpio.getName()) {
                        case GPIO_DOOR_LOCK:
                            intentMsg = IntentMsgEnum.doorLocked;
                            break;
                        case GPIO_DOOR_UNLOCK:
                            intentMsg = IntentMsgEnum.doorUnlocked;
                            break;
                        case GPIO_DRAWER_LOCK:
                            intentMsg = IntentMsgEnum.drawerLocked;
                            break;
                        case GPIO_DRAWER_UNLOCK:
                            intentMsg = IntentMsgEnum.drawerUnlocked;
                            break;
                    }

                    // Can send intent here if needed indicating to listeners that we've finished.
                    IntentBroadcast.send(IntentTypeEnum.msg,
                            TAG,
                            CommandState.COMPLETE_STATE.toString(),
                            intentMsg.toString(),
                            "");

                    Log.d(TAG, "Finished runLockDriver.");
                } catch (Exception e) {
                    Log.e(TAG, "Error in runLockDriver runnable", e);
                }
            }
        });
    }
}
