package com.logicpd.papapill.device.tinyg.commands;

import android.util.Log;

import com.logicpd.papapill.device.enums.CommandState;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.device.tinyg.CommandManager;

public final class CommandMotorInit extends BaseCommand {

    private static final DeviceCommand identifier = DeviceCommand.COMMAND_MOTOR_INIT;

    private boolean configureTinyG;
    private int idx;

    public CommandMotorInit() {
        super(identifier);
    }

    @Override
    public void execute() {
        switch(operation) {
            case OP_STATE_START:
                // Separate the parameters via whitespace regex.
                String[] paramArray = getParamArray();
                // This command is expected to have 3 parameters. Ensure we have
                // that amount before continuing (we'll ignore any additional parameters).
                if (paramArray.length < 1) {
                    Log.e(name, "Command failed due to too few or bad parameters.");
                    return;
                } else {
                    // Check if caller wishes for us to configure the TinyG first.
                    configureTinyG = Boolean.parseBoolean(paramArray[0]);

                    if (configureTinyG) {
                        // Initialize our index variable to 0 so we start at the beginning of the
                        // TinyG settings array.
                        idx = 0;

                        Log.d(name, "Configuring TinyG settings.");

                        // Set our state to begin sending.
                        setState(CommandState.INITIALIZE_SENDING_NEXT);
                    } else {

                        Log.d(name, "Skip configuring TinyG settings.");

                        // Skip configuring the TinyG and go straight to homing.
                        CommandManager.getInstance().callCommand(
                                DeviceCommand.COMMAND_HOME_ALL, null, null);

                        setState(CommandState.INITIALIZE_WAITING_FOR_HOME);
                    }
                }
                break;

            case OP_STATE_RUN:
                switch(state) {
                    case INITIALIZE_SENDING_NEXT:

                        // Form the parameter string for the set config command (name and value).
                        String cmdParams = BoardDefaults.tinygSettings[idx].name + " "
                                + BoardDefaults.tinygSettings[idx].value;

                        // Call sub command with our parameters to set the configurable setting.
                        CommandManager.getInstance().callCommand(
                                DeviceCommand.COMMAND_SET_CONFIG, cmdParams, null);

                        // Advance state variable.
                        setState(CommandState.INITIALIZE_WAITING_FOR_RESPONSE);
                        break;

                    case INITIALIZE_WAITING_FOR_RESPONSE:
                        // Wait until the set config command we called earlier indicates that it
                        // has finished.
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_SET_CONFIG)) {

                            // The set config sub command returned successfully. We are done with
                            // this command. Increment index to move onto the next one.
                            idx++;

                            // If we've reached the end of our settings array, mark the command as
                            // complete as we are done.
                            if (idx >= BoardDefaults.tinygSettings.length) {

                                CommandManager.getInstance().callCommand(
                                        DeviceCommand.COMMAND_HOME_ALL, null, null);

                                setState(CommandState.INITIALIZE_WAITING_FOR_HOME);
                                break;
                            }

                            // Otherwise, send us back to set the next config.
                            setState(CommandState.INITIALIZE_SENDING_NEXT);
                        }
                        break;

                    case INITIALIZE_WAITING_FOR_HOME:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_HOME_ALL)) {

                            // We're done.
                            setState(CommandState.COMPLETE_STATE);
                        }
                        break;
                }
                break;

            default:
                break;
        }
    }
}
