package com.logicpd.papapill.enums;

/*
 * See AppModel for detail of the following states and utilization
 */
public enum AppStatesEnum {
    IDLE(0),
    FEATURE(1),
    MOTION(2),
    TRANSITION(3),
    ERROR(4);

    public final int value;

    private AppStatesEnum(int value) {this.value = value;}
}
