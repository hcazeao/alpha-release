package com.logicpd.papapill.enums;

public enum BuildFlavorsEnum {
    BREADBOARD {
        public String toString() {return "breadboard";}
    },
    VISIONONLY {
        public String toString() {return "visiononly";}
    },
    TOUCHSCREEN {
        public String toString() {return "touchscreen";}
    },
    BUILDSERVER {
        public String toString() {return "buildserver";}
    }
}
