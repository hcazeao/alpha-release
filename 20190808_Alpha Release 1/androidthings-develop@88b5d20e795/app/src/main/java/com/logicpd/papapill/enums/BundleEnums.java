package com.logicpd.papapill.enums;

public enum BundleEnums {
    // data objects
    user {
        public String toString() {return "user";}
    },

    medication {
        public String toString() {return "medication";}
    },

    // attributes
    isFromSchedule {
        public String toString() {return "isFromSchedule";}
    },

    isFromRefill {
        public String toString() {return "isFromRefill";}
    },

    // wireframe workflow -- destinations
    wireframe {
        public String toString() {return "wireframe";}           // wireframe workflow of this bundle
    },

    model {
        public String toString() {return "model";}              // model to hold any content
    },

    homeFragment {
        public String toString() {return "homeFragment";}       // starting fragment
    },

    fragmentName {
        public String toString() {return "fragmentName";}       // fragment we are launching
    },

    nextFragmentName {
        public String toString() {return "nextFragment";}       // fragment we are goto from launching
    },

    endFragment {
        public String toString() {return "endFragmentName";}    // last fragment in this workflow; return to homeFragment when done
    }
}
