package com.logicpd.papapill.enums;

public enum FeelingEnum {
    NONE(0) {
        public String toString() { return "Ok"; }
    },
    SAD(1) {
        public String toString() { return "SAD"; }
    },
    OK(2) {
        public String toString() { return "OK"; }
    },
    GOOD(3) {
        public String toString() { return "GOOD"; }
    };

    public final int value;

    private FeelingEnum(int value) {
        this.value = value;
    }
}
