package com.logicpd.papapill.enums;

public enum MedLocaEnum {
    BIN {
        public String toString() {return "BIN";}
    },
    DRAWER {
        public String toString() {return "DRAWER";}
    },
    REFRIGERATOR {
        public String toString() {return "REFRIGERATOR";}
    },
    OTHER {
        public String toString() {return "OTHER";}
    },
}
