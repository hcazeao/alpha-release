package com.logicpd.papapill.enums;

public enum MedPausedEnum {
    IS_PAUSED(true), NOT_PAUSED(false);
    public final boolean value;

    private MedPausedEnum(boolean value) {this.value = value;}
}
