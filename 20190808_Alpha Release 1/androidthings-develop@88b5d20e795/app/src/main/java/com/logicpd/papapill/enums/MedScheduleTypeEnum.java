package com.logicpd.papapill.enums;

public enum MedScheduleTypeEnum {
    BOTH(0), AS_NEEDED(1), SCHEDULED(2);
    public final int value;

    private MedScheduleTypeEnum(int value) {
        this.value = value;
    }
}

