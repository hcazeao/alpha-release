package com.logicpd.papapill.enums;

// TODO in Beta: We should have an internal table/list of all notification types and store/reference
// them in the database by Id instead of by strings (which we are doing now in strings.xml).
public enum NotificationTypeEnum {
    TIME_TO_TAKE_MEDS {
        public String toString() {return "Time to take medication";}
    },
    MISSED_DOSE_30_MINUTES {
        public String toString() {return "Missed dose 30 minutes";}
    },
    MISSED_DOSE_1_HOUR {
        public String toString() {return "Missed dose 1 hour";}
    },
    MISSED_DOSE_2_HOURS {
        public String toString() {return "Missed dose 2 hours";}
    },
    MISSED_DOSE_4_HOURS {
        public String toString() {return "Missed dose 4 hours";}
    },
    CONSECUTIVE_MISSED_DOSES {
        public String toString() {return "Consecutive missed doses";}
    },
    MISSED_DOSES_OVER_LAST_24_HOURS {
        public String toString() {return "Missed doses over last 24-hours";}
    },
    AS_NEEDED_MEDICATION_TAKEN {
        public String toString() {return "As Needed Medication Taken";}
    },
    SAD_FACE {
        public String toString() {return "Sad face";}
    },
    EARLY_MEDICATION_RETRIEVAL {
        public String toString() {return "Early medication retrieval (vacation mode)";}
    },
    THREE_CONSECUTIVE_PIN_ERRORS {
        public String toString() {return "3 consecutive PIN number errors";}
    },
    SYSTEM_PIN_RESET {
        public String toString() {return "System PIN number reset";}
    },
    USER_ADDED_OR_EDITED {
        public String toString() {return "User Added/Edited";}
    },
    USER_PIN_CHANGE {
        public String toString() {return "User PIN change";}
    },
    USER_PIN_RESET {
        public String toString() {return "User PIN number reset";}
    },
    LOW_PILL_COUNT {
        public String toString() {return "Low pill count";}
    },
    SYSTEM_SETTING_CHANGE {
        public String toString() {return "Change to System Settings";}
    },
    NEW_MEDICATION_ADDED {
        public String toString() {return "New Medication Added";}
    },
    MEDICATION_REFILLED {
        public String toString() {return "Medication Refilled";}
    },
    MEDICATION_SCHEDULE_CHANGED {
        public String toString() {return "Medication Schedule Changed";}
    },
    TILT_WARNING {
        public String toString() {return "Tilt Warning";}
    },
    POWER_LOSS {
        public String toString() {return "Power Loss";}
    },
    MEDICATION_EXPIRED {
        public String toString() {return "Medication expired";}
    },
    MEDICATION_EXPIRING_IN_7_DAYS {
        public String toString() {return "Medication expiring within 7 days";}
    },
}
