package com.logicpd.papapill.enums;

public enum SelectMedLocationUIState {
        UNDECIDED,
        SELECTED_BIN,
        SELECTED_DRAWER,
        SELECTED_FRIDGE,
        SELECTED_OTHER
}
