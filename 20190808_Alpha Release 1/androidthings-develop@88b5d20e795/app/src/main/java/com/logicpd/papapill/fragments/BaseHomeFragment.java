package com.logicpd.papapill.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.utils.TextUtils;

public class BaseHomeFragment extends Fragment implements View.OnClickListener {
    protected LinearLayout backButton, homeButton;
    protected OnButtonClickListener mListener;

    protected void setupViews(View view) {
        try {
            backButton = view.findViewById(R.id.button_back);
            backButton.setOnClickListener(this);
            homeButton = view.findViewById(R.id.button_home);
            homeButton.setOnClickListener(this);
        }
        catch (Exception ex) {
            Log.v("BaseHomeFragment", "setupViews() failed to find back or home buttons");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a = null;

        if (context instanceof Activity) {
            a = (Activity) context;
        }

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnButtonClickListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnButtonClickListener");
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        if (v == backButton) {
            bundle.putString("fragmentName", "Back");
        }
        if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
        }
        mListener.onButtonClicked(bundle);
    }

    public Boolean baseHomeOnClick(View v) {
        Bundle bundle = new Bundle();
        Boolean returnVal = false;

        if (v == backButton) {
            bundle.putString("fragmentName", "Back");
            returnVal = true;
        }

        if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
            returnVal = true;
        }

        mListener.onButtonClicked(bundle);
        return returnVal;
    }
}
