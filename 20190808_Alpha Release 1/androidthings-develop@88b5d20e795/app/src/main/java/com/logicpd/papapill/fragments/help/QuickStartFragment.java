package com.logicpd.papapill.fragments.help;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;

/**
 * Fragment for Help...Quick Start
 *
 * @author alankilloren
 */
public class QuickStartFragment extends BaseHomeFragment {

    public static final String TAG = "QuickStartFragment";

    private LinearLayout contentLayout;

    public QuickStartFragment() {
        // Required empty public constructor
    }

    public static QuickStartFragment newInstance() {
        return new QuickStartFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_help_quick_start, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }
}
