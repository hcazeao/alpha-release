package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.device.tinyg.commands.CommandHomeAll;
import com.logicpd.papapill.device.tinyg.commands.CommandRotateCarousel;
import com.logicpd.papapill.enums.CannotRetrieveMedUIStates;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.DispenseEventResultEnum;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.repositories.DispenseEventRepository;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.workflows.DispenseStrategy;


public class CannotRetrieveMedFragment extends BaseDispenseMedsFragment implements IntentMsgInterface
{
    public static final String TAG = "CannotRetrieveMedFragment";
    private Button btnRetry;
    private Button btnManualRetrieval;
    private View mView;
    private Bundle bundle;
    private boolean isTinyGReady = false;
    private CannotRetrieveMedUIStates mState;

    public static CannotRetrieveMedFragment newInstance()
    {
        return new CannotRetrieveMedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mState = CannotRetrieveMedUIStates.DISABLED;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cannot_retrieve_med, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        setupViews(view);
    }

    private boolean gotoEnableState(String command, String msg)
    {
        if(command.contains(CommandHomeAll.class.getSimpleName()) &&
                msg.contains(IntentMsgEnum.success.toString())) {
            Log.d(TAG, "handleIntent: CommandHomeAll success");
            Bundle bundle =createBundle(this.getClass().getSimpleName(), true);
            mListener.onButtonClicked(bundle);
            ((MainActivity)this.getActivity()).startCarouselPosService(CarouselPosCmdEnum.home,0);
            return true;
        }
        return false;
    }

    /*
     * Done moving bin for user access.
     * - launch next fragment -> ManualRetrievalFragment
     */
    private boolean handleManualDispense(String command, String msg)
    {
        /*
         * TODO: This logic is flawed.. If we have another medication to retrieve, we don't want user to takeMeds !
         */
        if(isTinyGReady &&
            command.contains(CommandRotateCarousel.class.getSimpleName()) &&
            msg.contains(IntentMsgEnum.success.toString())) {
            Log.d(TAG, "handleIntent: Manual Dispense is finished; commandRotateCarousel success");

            // restart carouselPosService -- keep bin at medication position
            int location = dispenseEventsModel.getCurrent().getMedicationEntity().getMedicationLocation();
            ((MainActivity)this.getActivity()).startCarouselPosService(CarouselPosCmdEnum.bin,location);
            Log.d(TAG, "location:"+location);

            Bundle bundle = createBundle(ManualRetrievalFragment.TAG);
            mListener.onButtonClicked(bundle);
            return true;
        }
        return false;
    }

    /*
     * handle action message from intent (send from command)
     */
    public boolean handleIntent(String command,
                                String state,
                                String msg,
                                String version)
    {
/*        Log.d(TAG, String.format("handleAction() command: %s state: %s msg: %s",
                command,
                state,
                msg));*/
        switch(mState)
        {
            case DISABLED:
                // 1. reset done - ready
                return gotoEnableState(command, msg);

            case ENABLED:
                break;

            case MANUAL_ACCESS:
                // 2. reset done, user manual move done
                return handleManualDispense(command, msg);

            case RETRY:
                break;
        }

        // 2. error again
        return false;
    }

    protected void setupViews(View view) {
        bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            if (dispenseEventsModel != null) {
                currentDispenseEvent = dispenseEventsModel.getCurrent();
                if (currentDispenseEvent != null) {
                    TextView textView = view.findViewById(R.id.textview_username);
                    textView.setText(dispenseEventsModel.user.getUserName());
                }
            }
        }
        btnRetry = view.findViewById(R.id.button_retry);
        btnRetry.setOnClickListener(this);
        TextUtils.disableButton(btnRetry);

        btnManualRetrieval = view.findViewById(R.id.button_dispense_manual_retrieval);
        btnManualRetrieval.setOnClickListener(this);
        TextUtils.disableButton(btnManualRetrieval);

        // enable button if reset is done
        if (bundle.containsKey("isEnabled")) {
            mState = CannotRetrieveMedUIStates.ENABLED;
            boolean isEnabled = bundle.getBoolean("isEnabled");
            if(isEnabled)
                updateReady();
        }
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "v:"+v.toString());

        Bundle bundle = this.getArguments();
        ((MainActivity)this.getActivity()).stopCarouselPosService();

        if (v == btnRetry)
        {
            MedicationEntity medication = currentDispenseEvent.getMedicationEntity();
            String name = DispenseStrategy.getFragmentName(medication.getMedicationLocation());
            bundle.putString("fragmentName", name);
            bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
            mListener.onButtonClicked(bundle);
        }

        else if(v == btnManualRetrieval)
        {
            /*
             * Need fragment and sensor GPIO for this.
             * - wait for sensor (door opens)
             * - wait for sensor (door close)
             */
            if (AppConfig.getInstance().isTinyGAvailable) {
                TinyGDriver.getInstance().doMoveBin4User(currentDispenseEvent.getMedicationLocation());
                mState = CannotRetrieveMedUIStates.MANUAL_ACCESS;
            }
        }
    }

    /*
     * event callback -> successful reset
     * - enable ready for retry or manual dispense
     */
    private void updateReady()
    {
        Log.d(TAG, "update");
        TextView textView = mView.findViewById(R.id.textViewSubTitleWait);
        textView.setVisibility(View.INVISIBLE);

        TextUtils.enableButton(btnManualRetrieval);
        TextUtils.enableButton(btnRetry);
        isTinyGReady = true;
    }

    private Bundle createBundle(String fragmentName,
                                boolean enabled)
    {
        Bundle bundle = this.getArguments();
        bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
        bundle.putString("fragmentName", fragmentName);
        bundle.putBoolean("isEnabled", enabled);
        return bundle;
    }
}
