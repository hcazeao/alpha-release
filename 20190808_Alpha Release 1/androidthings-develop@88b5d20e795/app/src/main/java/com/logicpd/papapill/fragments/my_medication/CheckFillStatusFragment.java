package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.FillStatusAdapter;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class CheckFillStatusFragment extends BaseHomeFragment {

    public static final String TAG = "CheckFillStatusFragment";

    private Button btnDone;
    private UserEntity user;
    private RecyclerView recyclerView;
    private TextView tvEmpty;

    public CheckFillStatusFragment() {
        // Required empty public constructor
    }

    public static CheckFillStatusFragment newInstance() {
        return new CheckFillStatusFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_fill_status, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }

        List<MedicationEntity> medicationList = model.getMedicationsForUser(user);
        //sort by bin #
        Collections.sort(medicationList, new Comparator<MedicationEntity>() {
            @Override
            public int compare(MedicationEntity o1, MedicationEntity o2) {
                return o1.getMedicationLocation() - o2.getMedicationLocation();
            }
        });
        FillStatusAdapter adapter = new FillStatusAdapter(getActivity(), medicationList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        if (medicationList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.recyclerview_medication_list);
        tvEmpty = view.findViewById(R.id.textview_empty);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "MyMedicationFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}