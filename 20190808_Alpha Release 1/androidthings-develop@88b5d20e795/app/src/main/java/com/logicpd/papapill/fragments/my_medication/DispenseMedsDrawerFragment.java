package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.gpio.LockManager;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.utils.TimerUtil;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

/**
 * DispenseMedsDrawerFragment
 *
 * @author alankilloren
 */
public class DispenseMedsDrawerFragment extends BaseDispenseMedsFragment implements OnTimerListener {

    public static final String TAG = "DispenseMedsDrawerFragment";

    private TextView tvInfo;
    private Button btnDone;
    private TextView txtUserName;

    private TimerUtil mTimer;

    Sensor drawer;

    public DispenseMedsDrawerFragment() {
        super();
        // Required empty public constructor
    }

    public static DispenseMedsDrawerFragment newInstance() {
        return new DispenseMedsDrawerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_dispense_drawer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            currentDispenseEvent = dispenseEventsModel.getCurrent();

            if (dispenseEventsModel != null &&
                    currentDispenseEvent != null) {

                MedicationEntity medication = dispenseEventsModel.getCurrent().getMedicationEntity();
                int dispenseAmount = dispenseEventsModel.getCurrent().getDispenseAmount();

                String s = "1. OPEN DEVICE DRAWER AND REMOVE " + medication.getMedicationName() + " " + medication.getStrengthMeasurement()
                        + "\n2. TAKE " + dispenseAmount + " PILLS OF " + medication.getMedicationName() + " " + medication.getStrengthMeasurement()
                        + "\n3. PLACE MEDICATION BACK IN DRAWER"
                        + "\n4. CLOSE DRAWER TIGHTLY";
                tvInfo.setText(s);
                txtUserName.setText(dispenseEventsModel.user.getUserName());

                // Change the state of the current dispense event and update db.
                dispenseEventsModel.gotoDispenseState();
            }

            // Unlock the drawer lock
            LockManager.getInstance().UnlockDrawer();

            // Set mux to read from drawer sensor.
            drawer = new Sensor(BoardDefaults.I2C_PORT,
                                BoardDefaults.I2C_MUX_DRAWER_CHANNEL,
                                AppConfig.getInstance().getDrawerThresholdHigh());

            // Start the timer to poll drawer sensor to check if drawer is opened
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    protected void setupViews(View view) {
        tvInfo = view.findViewById(R.id.textview_info);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        TextUtils.disableButton(btnDone);
        txtUserName = view.findViewById(R.id.textview_username);
    }

    @Override
    public void onTimerFinished() {
        // Read the drawer proximity sensor.
        int drawerProximity = drawer.getSensorValue();
        Log.d(TAG, "Drawer Proximity Read: " + drawerProximity);

        // Check if drawer is open.
        if (drawerProximity != PROXIMITY_ERROR && drawerProximity < AppConfig.getInstance().getDrawerThresholdLow()) {
            Log.d(TAG, "Drawer is open, wait for it to close again.");

            // Turn the drawer lock immediately, this will cause it to snap into locked mode
            // when pushed in by the user.
            LockManager.getInstance().LockDrawer();

            TextUtils.enableButton(btnDone);

        } else {
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    public void onClick(View v) {

        if (v == btnDone) {

            int drawerProximity = drawer.getSensorValue();
            Log.d(TAG, "User clicked done. Drawer Proximity Read: " + drawerProximity);

            if (drawerProximity != PROXIMITY_ERROR && drawerProximity > AppConfig.getInstance().getDrawerThresholdHigh()) {

                //Log.d(TAG, "Drawer is closed. Locking drawer and proceeding to next screen");
                //LockManager.getInstance().LockDrawer();

                // assume user will take all pills
                handleDispensed(0);
            }
        }
    }
}