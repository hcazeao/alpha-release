package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.enums.DispenseEventResultEnum;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.utils.PreferenceUtils;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.utils.TimerUtil;

import java.util.Date;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class DispenseMedsFragment extends BaseHomeFragment implements IntentMsgInterface, OnTimerListener {

    public static final String TAG = "DispenseMedsFragment";

    private Button btnDev;
    private TextView tvTitle, tvMedication;
    private DispenseEventsModel dispenseEventsModel;
    private int mDispenseNumber;
    private TimerUtil mTimer;

    public DispenseMedsFragment() {
        // Required empty public constructor
    }

    public static DispenseMedsFragment newInstance() {
        return new DispenseMedsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_dispense_meds, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            // Get the data model passed by AlarmedReceived / DispensingMeds fragments.
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");

            if (dispenseEventsModel != null &&
                    dispenseEventsModel.getCurrent() != null){

                String titleText = String.format("DISPENSING MEDICATION FOR %s", dispenseEventsModel.user.getUserName());
                tvTitle.setText(titleText);
                String medicationText = dispenseEventsModel.getMedicationText();
                tvMedication.setText(medicationText);

                // Change the state of the current dispense event and update db.
                dispenseEventsModel.gotoDispenseState();

                Log.d(TAG, "Size of dispenseEventItems = " + dispenseEventsModel.dispenseEventItems.size());

                // Now start physically dispensing the pill.
                runTinyG(dispenseEventsModel.getCurrent().getMedicationLocation());
            }
        }
    }

    /**
     * Calls TinyG to perform dispensing routine.
     * @param location
     */
    private void runTinyG(int location) {
        // Check to see if this build has TinyG available. If so, perform dispense
        if (AppConfig.getInstance().isTinyGAvailable) {

            Log.v(TAG, "runTinyG()");
            // check if tinyG is ready ?
            TinyGDriver tinyG =  TinyGDriver.getInstance();
            if(!tinyG.isStateMachineIdle()) {
                Log.d(TAG, "TinyG is not ready, wait 500ms");
                // start a timer event
                if(null==mTimer) {
                    mTimer = new TimerUtil(this);
                }
                mTimer.start();
            }
            else {
                Log.v(TAG, "Calling full dispense from real UI: " + location);
                mDispenseNumber = PreferenceUtils.getDispenseCount();
                Log.v(TAG, String.format("Request: location:%s dispenseNumber:%s", location, mDispenseNumber));
                tinyG.doPrepDispense(location, mDispenseNumber);
            }
        }
    }

    /*
     * Time is up, try to run TinyG again
     */
    public void onTimerFinished() {
        Log.v(TAG, "onTimerFinished() 500ms");
        runTinyG(dispenseEventsModel.getCurrent().getMedicationLocation());
    }

    /*
     * handle action message from intent (send from command)
     */
    public boolean handleIntent(String command,
                                String state,
                                String message,
                                String version) {   // idempotent
        //Log.d(TAG, String.format("handleAction() command: %s state: %s msg: %s", command, state, msg));
        Bundle bundle;
        IntentMsgEnum msg = IntentMsgEnum.valueOf(message);

        // 1. camera is ready !
        switch (msg) {
            case success:
                // we are waiting for the 'exact' request issued
                if(null==version || 0==version.length() ||
                        mDispenseNumber != Integer.parseInt(version)) {
                    Log.v(TAG, String.format("Failed: command:%s state:%s dispenseNumber:%s version:%s", command, state, mDispenseNumber, version));
                    return false;
                }
                //Log.v(TAG, String.format("Matched: command:%s state:%s dispenseNumber:%s version:%s", command, state, mDispenseNumber, version));
                bundle = createBundle(DispensingMedsFragment.TAG);
                mListener.onButtonClicked(bundle);
                return true;

            case limitError:
                //Log.d(TAG, "Error occured");
                // TinyG is automatically reset by ResponseParser
                Log.v(TAG, "handleIntent() limitError");
                // intentional fall through

            case paramError:
                bundle = createBundle(CannotRetrieveMedFragment.TAG);
                mListener.onButtonClicked(bundle);
                return true;
        }

        // unhandled condition
        return false;
    }

    private Bundle createBundle(String fragmentName)
    {
        Bundle bundle = this.getArguments();
        bundle.putInt("count", mDispenseNumber);
        bundle.putString("fragmentName", fragmentName);
        return bundle;
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvMedication = view.findViewById(R.id.textview_medicationName);
        tvTitle = view.findViewById(R.id.textview_title);
        btnDev = view.findViewById(R.id.button_developer);
        btnDev.setOnClickListener(this);
        backButton.setVisibility(View.GONE);

        // on breadboard, don't let user go to the next screen manually
        if(AppConfig.getInstance().isTinyGAvailable)
        {
            TextUtils.disableButton(btnDev);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle;
        if (v == btnDev &&
                !AppConfig.getInstance().isTinyGAvailable) {
            bundle = createBundle(DispensingMedsFragment.TAG);
            mListener.onButtonClicked(bundle);
        }
    }
}