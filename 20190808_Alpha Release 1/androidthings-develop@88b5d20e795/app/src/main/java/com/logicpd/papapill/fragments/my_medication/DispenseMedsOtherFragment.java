package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

/**
 * DispenseMedsOtherFragment
 *
 * @author alankilloren
 */
public class DispenseMedsOtherFragment extends BaseDispenseMedsFragment {

    public static final String TAG = "DispenseMedsOtherFragment";
    private TextView tvInfo, tvTitle;
    private Button btnDone;
    private TextView txtUserName;

    public DispenseMedsOtherFragment() {
        super();
        // Required empty public constructor
    }

    public static DispenseMedsOtherFragment newInstance() {
        return new DispenseMedsOtherFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_dispense_other, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            currentDispenseEvent = dispenseEventsModel.getCurrent();

            if (dispenseEventsModel != null &&
                    dispenseEventsModel.getCurrent() != null) {

                MedicationEntity medication = dispenseEventsModel.getCurrent().getMedicationEntity();
                String sInfo = "PLEASE TAKE\n" + dispenseEventsModel.getMedicationText()
                        + "\n\nPLACE MEDICATION BACK IN " + medication.getMedicationLocationName() + " ONCE YOU HAVE TAKEN YOUR DOSE";
                tvInfo.setText(sInfo);
                String sTitle = "RETRIEVE MEDICATION FROM " + medication.getMedicationLocationName();
                tvTitle.setText(sTitle);
                txtUserName.setText(dispenseEventsModel.user.getUserName());

                // Change the state of the current dispense event and update db.
                dispenseEventsModel.gotoDispenseState();
            }
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    protected void setupViews(View view) {
        tvInfo = view.findViewById(R.id.textview_info);
        tvTitle = view.findViewById(R.id.textview_title);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        txtUserName = view.findViewById(R.id.textview_username);
    }

    @Override
    public void onClick(View v) {

        if (v == btnDone) {
            handleDispensed(0);
        }
    }
}