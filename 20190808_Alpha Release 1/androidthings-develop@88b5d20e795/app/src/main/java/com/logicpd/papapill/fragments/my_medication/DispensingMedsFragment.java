package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.computervision.BinCamera;
import com.logicpd.papapill.computervision.detection.DetectorException;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.DispenseEventResultEnum;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.wireframes.BundleFactory;

import java.nio.ByteBuffer;
import java.util.Date;

public class DispensingMedsFragment extends BaseDispenseMedsFragment implements IntentMsgInterface {
    public static final String TAG="DispensingMedsFragment";
    protected LinearLayout contentLayout;
    protected Button btnDev, btnPhoto;
    protected TextView tvTitle, tvMedication, tvMedInfo, txtUserName;

    private BinCamera mCamera = null;
    private Handler mCameraHandler;
    private HandlerThread mCameraThread;
    private TextureView mTextureView;
    private boolean surfaceInitialized = false;

    private int updateInstance = 0;
    private int CAMERA_READY_INDEX;
    private int mDispenseNumber;

    public DispensingMedsFragment() {
    }

    public static DispensingMedsFragment newInstance() {
        return new DispensingMedsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        CAMERA_READY_INDEX = this.getResources().getInteger(R.integer.cameraReadyCount);

        // Inflate the layout for this fragment
        createCameraThread();
        return inflater.inflate(R.layout.fragment_my_meds_dispensing_meds, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        initCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView()");
        if(null!=mCamera) {
            mCamera.shutDown();
        }

        if(null!=mCameraThread) {
            mCameraThread.quit();
            mCameraHandler.removeCallbacksAndMessages(null);
            mCameraHandler = null;
            mCameraThread = null;
        }
    }

    private void createCameraThread()
    {
        //Setup Camera Thread
        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            mCameraThread = new HandlerThread("CameraBackground");
            mCameraThread.start();
            mCameraHandler = new Handler(mCameraThread.getLooper());
        }
    }

    private void initCamera()
    {
        //check to see if this build has camera enabled
        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            try {
                mCamera = new BinCamera();
                mCamera.initializeCamera(getActivity(), mCameraHandler, mOnImageAvailableListener, mTextureView);
            } catch (DetectorException de) {
                Log.e(TAG, "Error in Camera Initialization", de);
            }
        }
    }

    private void takePhoto()
    {
        if(null!=mCamera) {
            mCamera.takePicture();
            Log.d(TAG, "takePhoto() done");
            return;
        }
        Log.e(TAG, "takePhoto() camera not available");
    }

    protected void setupViews(View view) {
        contentLayout = view.findViewById(R.id.layout_content);
        tvMedication = view.findViewById(R.id.textview_medicationName);
        tvTitle = view.findViewById(R.id.textview_title);
        tvMedInfo = view.findViewById(R.id.textview_med_info);
        mTextureView = view.findViewById(R.id.camera_preview);
        txtUserName = view.findViewById(R.id.textview_username);

        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            mTextureView.setSurfaceTextureListener(surfaceTextureListener);
            mTextureView.getLayoutParams().height = this.getResources().getInteger(R.integer.imageHeight);
            mTextureView.getLayoutParams().width = this.getResources().getInteger(R.integer.imageWidth);
        }

        btnDev = view.findViewById(R.id.button_developer);
        btnDev.setOnClickListener(this);
        if (com.logicpd.papapill.data.AppConfig.getInstance().isTinyGAvailable) {
            btnDev.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {

        if (v == btnDev) {
            handleDispensed(0);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            // Get the data model passed by AlarmedReceived / DispensingMeds fragments.
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            currentDispenseEvent = dispenseEventsModel.getCurrent();
            mDispenseNumber = bundle.getInt("count");

            if (dispenseEventsModel != null && currentDispenseEvent != null) {

                String titleText = String.format("DISPENSING MEDICATION FOR %s",
                        dispenseEventsModel.user.getUserName());

                txtUserName.setText(dispenseEventsModel.user.getUserName());
                String medicationText = dispenseEventsModel.getMedicationText();

                tvTitle.setText(titleText);
                tvMedication.setText(medicationText);
            }
        }
        Log.d("TAG", "onViewCreated()");
    }

    /*
     * handle action message from intent (send from command)
     */
    @Override
    public boolean handleIntent(String command,
                                String state,
                                String message,
                                String version) {   // idempotent
        Log.d(TAG, String.format("handleAction() command: %s state: %s msg: %s", command, state, message));
        IntentMsgEnum msg = IntentMsgEnum.valueOf(message);

        // 1. reset done - ready
        switch (msg) {
            case dispensed:
                int pillsRemain = dispenseEventsModel.decrementPillsRemaining();
                return handleDispensed(pillsRemain);

            case limitError:
                Log.d(TAG, "Limit error occurred");
                Bundle bundle = createBundle(CannotRetrieveMedFragment.TAG);
                bundle.putBoolean("isEnabled", true);
                mListener.onButtonClicked(bundle);
                return true;
            case visionError:
                Log.d(TAG, "Vision error occurred");
                TinyGDriver.getInstance().reset();
                Bundle bundle1 = createBundle(CannotRetrieveMedFragment.TAG);
                mListener.onButtonClicked(bundle1);
                return true;
            case paramError:
                Bundle bundle2 = createBundle(CannotRetrieveMedFragment.TAG);
                mListener.onButtonClicked(bundle2);
                return true;
        }

        // unhandled condition
        return false;
    }

    /*
     * The listener for when the camera has taken a picture.  Once the picture is taken, it will send
     * the image to the processor to identify meds and their locations.
     */
    private ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image image = reader.acquireLatestImage();
            Log.d("TAG", "acquireLatestImage()");

            //get the bytes
            ByteBuffer imageBuf = image.getPlanes()[0].getBuffer();
            final byte[] imageBytes = new byte[imageBuf.remaining()];
            imageBuf.get(imageBytes);
            image.close();
            Log.d("TAG", "got the bytes()");

            //turn on video
            if (surfaceInitialized) {
                mCamera.startVideo(mTextureView);
                Log.d(TAG, "Surface Initialized, Video Should Start!");
            } else {
                Log.d(TAG, "Surface Is Not Initialized!!");
            }

            TinyGDriver.getInstance().doFullDispense(imageBytes, mDispenseNumber);
        }
    };

    private TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {

        /*The surface texture is available, so this is where we will create and open the camera, as
        well as create the request to start the camera preview.
         */
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            mCamera.setSurface(surface);
            mCamera.startVideo(mTextureView);
            surfaceInitialized = true;
            Log.d(TAG, "onSurfaceTextureAvailable");

        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            Log.d(TAG, "onSurfaceTextureSizeChanged");
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            Log.d(TAG, "onSurfaceTextureDestroyed()");
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            Log.d(TAG, "onSurfaceTextureUpdated");

            if(CAMERA_READY_INDEX == updateInstance)
            {
                takePhoto();
            }

            updateInstance++;
        }
    };

    @Override
    protected void gotoTakeMedsFragment() {
        Bundle bundle = createBundle(TakeMedsFragment.TAG);
        mListener.onButtonClicked(bundle);
    }
}
