package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.MedicationsAdapter;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * GetAsNeededMedicationFragment
 *
 * @author alankilloren
 */
public class GetAsNeededMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "GetAsNeededMedicationFragment";

    private TextView tvTitle, tvEmpty;
    private List<MedicationEntity> medicationList;
    private UserEntity user;
    private RecyclerView recyclerView;
    private Button btnNext;

    public GetAsNeededMedicationFragment() {
        // Required empty public constructor
    }

    public static GetAsNeededMedicationFragment newInstance() {
        return new GetAsNeededMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        String s = "GET AS-NEEDED MEDICATION";
        tvTitle.setText(s);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }

        medicationList = model.getAsNeededMedicationsForUser(user, MedPausedEnum.NOT_PAUSED);
        //sort by bin #
        Collections.sort(medicationList, new Comparator<MedicationEntity>() {
            @Override
            public int compare(MedicationEntity o1, MedicationEntity o2) {
                return o1.getMedicationLocation() - o2.getMedicationLocation();
            }
        });
        MedicationsAdapter adapter = new MedicationsAdapter(getActivity(), medicationList, false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new MedicationsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                MedicationEntity medication = medicationList.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("user", user);
                bundle.putSerializable("medication", medication);
                bundle.putString("fragmentName", "GetAsNeededDispenseAmountFragment");
                mListener.onButtonClicked(bundle);
            }
        });
        if (medicationList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            tvEmpty.setText("NO AS-NEEDED MEDICATIONS FOUND");
            recyclerView.setVisibility(View.GONE);
            btnNext.setVisibility(View.VISIBLE);
            btnNext.setText("OK");
        } else {
            tvEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.GONE);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        tvTitle = view.findViewById(R.id.textview_title);
        recyclerView = view.findViewById(R.id.recyclerview_list);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        btnNext.setVisibility(View.GONE);
        tvEmpty = view.findViewById(R.id.textview_empty);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        if (v == backButton) {
            bundle.putString("fragmentName", "MyMedicationFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnNext) {
            backButton.performClick();
        }
    }
}