package com.logicpd.papapill.fragments.my_medication;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import java.util.List;

import com.logicpd.papapill.databinding.FragmentMyMedsGetMedEarlyBinding;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

public class GetScheduledMedEarlyFragment extends BaseHomeFragment {

    public static final String TAG = "GetScheduledMedEarlyFragment";

    private UserEntity user;
    private FragmentMyMedsGetMedEarlyBinding mBinding;
    public boolean enabledSingleDose, enabledVacationSupply;

    public GetScheduledMedEarlyFragment() {
        // Required empty public constructor
    }

    public static GetScheduledMedEarlyFragment newInstance() {
        return new GetScheduledMedEarlyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_meds_get_med_early, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }
        enabledSingleDose = hasScheduleData();
        enabledVacationSupply = false;
        mBinding.invalidateAll();
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        // Special case to handle back -- need to jump over USER PIN entry
        if (v == backButton) {
            bundle.putString("removeAllFragmentsUpToCurrent", "MyMedicationFragment");
            mListener.onButtonClicked(bundle);
            return;
        }

        // Let BaseHomeFragment handle home.
        super.onClick(v);
    }

    /*
     * Single dose
     */
    public void onClickSingleDose() {
        Bundle bundle = new Bundle();
        BaseWireframe model = ((MainActivity)this.getActivity()).getFeatureModel();
        model.addMapItem("user", user);
        bundle.putSerializable("user", user);   // redundancy because not fully switched to model
        bundle.putString("fragmentName", "GetSingleDoseEarlyFragment");
        mListener.onButtonClicked(bundle);
    }

    /*
     * Vacation supply feature
     * (currently disabled until definition is available
     */
    public void onClickVacationSupply() {
        Bundle bundle = new Bundle();
        bundle.putString("fragmentName", "Home");
        mListener.onButtonClicked(bundle);
    }

    /*
     * Find next valid dose on the same day ??
     * NOTE: according to Jarrod, user can access as much as desired (example: vacation).
     * Given it is a 'single' dose, we assume user will loop N times.
     */
    private boolean hasScheduleData() {
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        List<MedicationEntity> list = model.getScheduledMedicationsForUser(user, MedPausedEnum.NOT_PAUSED);
        return (null==list || list.size()==0)?false:true;
    }
}