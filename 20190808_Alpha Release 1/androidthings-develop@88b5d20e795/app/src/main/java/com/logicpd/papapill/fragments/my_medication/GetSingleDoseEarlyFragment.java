package com.logicpd.papapill.fragments.my_medication;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.databinding.FragmentMyMedsGetSingleDoseEarlyBinding;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.utils.SimpleTimeConverter;
import com.logicpd.papapill.room.utils.TimeUtil;
import com.logicpd.papapill.room.utils.TimestampConverter;
import com.logicpd.papapill.services.ScheduleProcessor;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.DispenseStrategy;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GetSingleDoseEarlyFragment extends BaseHomeFragment {

    public static final String TAG = "GetSingleDoseEarlyFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private JoinScheduleDispense scheduleItem;
    private int dispenseAmount = 0;
    public FragmentMyMedsGetSingleDoseEarlyBinding mBinding;
    public String doseText;
    public String resumeText;
    private BaseWireframe mModel;
    private List<JoinScheduleDispense> mSchedules;
    private ScheduleProcessor mScheduleProcessor;

    public GetSingleDoseEarlyFragment() {
        // Required empty public constructor
    }

    public static GetSingleDoseEarlyFragment newInstance() {
        return new GetSingleDoseEarlyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScheduleProcessor = new ScheduleProcessor();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_meds_get_single_dose_early, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mModel = (BaseWireframe) ((MainActivity)this.getActivity()).getFeatureModel();
        user = (UserEntity) mModel.getMapItem("user");

        doseText = getContext().getString(R.string.u_chosen_dispense_early);
        if (getNextScheduledItems()) {

            DispenseTimeEntity dispenseTime = new DispenseTimeRepository().read(scheduleItem.getDispenseTimeId());
            doseText = getContext().getString(R.string.u_chosen) +
                        " " + dispenseTime.getDispenseTime() +
                        " " + dispenseTime.getDispenseName() +
                        " " + getContext().getString(R.string.dose_early);

            /*
             * Find next schedule today; else, add a date for future schedule event
             */
            Pair<String, DispenseTimeEntity> normal = getNormalScheduledItems();
            resumeText = getContext().getString(R.string.normal_scheduled_dosing_will_resume) +
                    " " + ((DispenseTimeEntity)(normal.second)).getDispenseTime() +
                    " " + ((DispenseTimeEntity)(normal.second)).getDispenseName() +
                    " ("+ ((String)normal.first) + ")";
        }
        mBinding.invalidateAll();
    }

    /*
     * Cancel early dose
     */
    public void onClickCancel()
    {
        Bundle bundle = new Bundle();
        bundle.putString("fragmentName", SingleDoseEarly.SingleDoseEarlyEnum.MyMedication.toString());
        mListener.onButtonClicked(bundle);
    }

    public void onClickDispense()
    {
        // update database with dispense events
        createDispenseEvents();
    }

    /*
     * Next schedule item for single dose early
     */
    protected boolean getNextScheduledItems() {

        /*
         * Get Non Paused schedules sorted by nextProcessDate -- top is next !
         * - Non Paused
         * - No missed dose(s)
         */
        mSchedules = new ScheduleRepository().getJoinNextScheduledItems(user.getId(), new Date());
        if(null==mSchedules || mSchedules.size()==0) {
            return false;
        }

        scheduleItem = mSchedules.get(0);
        mModel.addMapItem("scheduleItem", scheduleItem);
        return true;
    }

    /*
     * Next -> Next scheduled item to put user back on normal schedule
     */
    protected Pair<String, DispenseTimeEntity> getNormalScheduledItems() {

        JoinScheduleDispense schedule = null;
        Date nextProcessDate = scheduleItem.getNextProcessDate();
        List<JoinScheduleDispense> list = new ScheduleRepository().getJoinNextScheduledItems(user.getId(), nextProcessDate);

        schedule = (null!=list && list.size()>0) ?
                    list.get(0):
                    scheduleItem;   // There is only 1 schedule time

        DispenseTimeEntity dispenseTimeEntity;
        Calendar updatedCalendar;

        dispenseTimeEntity = schedule.getDispenseTimeEntity();
        updatedCalendar = mScheduleProcessor.getNextProcessDate(schedule, DispenseEventTypeEnum.SCHEDULED);

        // find date
        Date date = updatedCalendar.getTime();
        String dateString = TimeUtil.date2TimeString(date);
        return new Pair<String, DispenseTimeEntity>(dateString, dispenseTimeEntity);
    }

    /*
     * Use MedicationScheduleService functionality to update DispenseEvents and invoke dispense.
     */
    private void createDispenseEvents() {
        /*
         * TODO: MedicationScheduleService should be driven by Application state not event.
         */
        ((MainActivity)this.getActivity()).stopMedicationScheduleService();

        Date now = new Date();
        if(null!=mSchedules && mSchedules.size()>0) {

            /*
             * 1 dose is defined as ALL scheduled instances with the same scheduled time
             * - presume earlier scheduled medications are already processed.
             *   hence, the 1st entry is next scheduled dose.
             *
             *   ... but to filter missed dose, must be later than now
             */
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(now);

            String doseTime = mSchedules.get(0).getDispenseTime();
            final int NOTHING2PROCESS = -1;
            int scheduleId = NOTHING2PROCESS;
            for(JoinScheduleDispense schedule : mSchedules) {

                // these are the ones of interest -- create dispenseEvent
                if(schedule.getDispenseTime().equals(doseTime)) {

                    Calendar previousNextProcessDate = mScheduleProcessor.updateScheduleTable(schedule, DispenseEventTypeEnum.EARLY_DISPENSE);

                    // create a dispenseEvent -- push to db
                    scheduleId= schedule.getScheduleId();
                    mScheduleProcessor.createDispenseEvent(scheduleId, schedule.getDispenseAmount(), currentDate, previousNextProcessDate, DispenseEventTypeEnum.EARLY_DISPENSE);
                }
            }
            // send intent for cloud notification and alarms
            mScheduleProcessor.sendIntent(scheduleId);
        }
    }
}