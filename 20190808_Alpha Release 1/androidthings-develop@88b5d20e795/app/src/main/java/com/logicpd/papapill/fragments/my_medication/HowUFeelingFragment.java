package com.logicpd.papapill.fragments.my_medication;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.data.Notification;
import com.logicpd.papapill.enums.FeelingEnum;
import com.logicpd.papapill.enums.NotificationTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentManageMedsFeelingBinding;
import com.logicpd.papapill.misc.CloudManager;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

public class HowUFeelingFragment extends BaseDispenseMedsFragment {
    private FragmentManageMedsFeelingBinding mBinding;
    public static final String TAG = "HowUFeelingFragment";

    private TextView txtUserName;
    private ImageButton btn_sad, btn_ok, btn_happy;
    private Button btn_next;
    private FeelingEnum feeling = FeelingEnum.NONE;

    public HowUFeelingFragment() {
        // Required empty public constructor
    }

    public static HowUFeelingFragment newInstance() {
        return new HowUFeelingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_meds_feeling, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = null;

        btn_sad = view.findViewById(R.id.imageButton_sad);
        btn_ok = view.findViewById(R.id.imageButton_ok);
        btn_happy = view.findViewById(R.id.imageButton_happy);
        btn_next = view.findViewById(R.id.button_next);

        btn_sad.setEnabled(true);
        btn_ok.setEnabled(true);
        btn_happy.setEnabled(true);

        txtUserName = view.findViewById(R.id.textview_username);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            currentDispenseEvent = dispenseEventsModel.getCurrent();
            txtUserName.setText(dispenseEventsModel.user.getUserName());
        }

        mBinding.invalidateAll();
    }

    public void onClickSadOrIll() {
        btn_sad.setEnabled(false);
        btn_ok.setEnabled(true);
        btn_happy.setEnabled(true);
        feeling = FeelingEnum.SAD;
    }

    public void onClickOk() {
        btn_sad.setEnabled(true);
        btn_ok.setEnabled(false);
        btn_happy.setEnabled(true);
        feeling = FeelingEnum.OK;
    }

    public void onClickGood() {
        btn_sad.setEnabled(true);
        btn_ok.setEnabled(true);
        btn_happy.setEnabled(false);
        feeling = FeelingEnum.GOOD;
    }

    public void onClickNext() {
        dispenseEventsModel.setFeeling(feeling);

        // Notify when user is feeling sad :(
        if (feeling == FeelingEnum.SAD) {
            Notification notification = new Notification();
            notification.setUser(dispenseEventsModel.user);
            String message = String.format("%s is feeling %s", dispenseEventsModel.user.getUserName(), feeling.toString());
            notification.setSubject(message); //TODO Need to change according to the workflow
            notification.setContent(message); //TODO Need to change according to the workflow
            CloudManager.getInstance(getActivity()).sendNotification(notification, NotificationTypeEnum.SAD_FACE);
        }

        gotoTakeMedsFragment();
    }
}
