package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.gpio.LockManager;
import com.logicpd.papapill.device.i2c.CupLight;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.utils.TimerUtil;
import com.logicpd.papapill.wireframes.BundleFactory;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

public class ManualRetrievalFragment extends BaseDispenseMedsFragment implements IntentMsgInterface, OnTimerListener {

    public static final String TAG = "ManualRetrievalFragment";

    private TextView txtUserName;
    private TextView tvInfo;

    private Sensor door;
    private boolean isDoorOpen = false;
    private TimerUtil mTimer;

    public ManualRetrievalFragment() {
        // Required empty public constructor
    }

    public static ManualRetrievalFragment newInstance() {
        return new ManualRetrievalFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manual_retrieval, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            if (dispenseEventsModel != null) {
                currentDispenseEvent = dispenseEventsModel.getCurrent();
                txtUserName.setText(dispenseEventsModel.user.getUserName());
            }

            // Bin is ready, unlock the door.
            LockManager.getInstance().UnlockDoor();

            // Set the mux to the door sensor to begin polling for proximity
            door = new Sensor(BoardDefaults.I2C_PORT,
                                BoardDefaults.I2C_MUX_DOOR_CHANNEL,
                                AppConfig.getInstance().getDoorThresholdHigh());

            tvInfo.setText(String.format("1. SLIDE DOOR OPEN AND REMOVE BIN #%d", currentDispenseEvent.getMedicationLocation()));

            // Start a timer to poll door sensor to check if door is open.
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        txtUserName = view.findViewById(R.id.textview_username);
        tvInfo = view.findViewById(R.id.textview_info);
    }

    @Override
    public void onTimerFinished() {
        // Read the door proximity sensor.
        int doorProximity = door.getSensorValue();
        Log.d(TAG, "Door Proximity Read: " + doorProximity);

        if (isDoorOpen) {
            // Door is open, look for close.
            if (doorProximity != PROXIMITY_ERROR && doorProximity > AppConfig.getInstance().getDoorThresholdHigh()) {
                Log.d(TAG, "Door was closed!");

                // User closed the door, now lock it.
                LockManager.getInstance().LockDoor();

            } else {
                mTimer.start(PROXIMITY_READ_TIMER_MS);
            }
        } else {
            // Door is closed, look for open.
            if (doorProximity != PROXIMITY_ERROR && doorProximity < AppConfig.getInstance().getDoorThresholdLow()) {
                Log.d(TAG, "Door was opened!");

                tvInfo.setText(String.format("2. RETRIEVE %d PILLS OUT OF BIN #%d\n3. PLACE BIN BACK IN DEVICE AND CLOSE DOOR", currentDispenseEvent.getPillsRemaining(), currentDispenseEvent.getMedicationLocation()));

                isDoorOpen = true;
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    public boolean handleIntent(String command,
                                String state,
                                String message,
                                String version) {   // idempotent
        Log.d(TAG, String.format("handleAction() command: %s state: %s msg: %s", command, state, message));
        IntentMsgEnum msg = IntentMsgEnum.valueOf(message);

        // 1. reset done - ready
        switch (msg) {
            case doorLocked:
                return handleDispensed(0);
        }

        // unhandled condition
        return false;
    }

    @Override
    public void onClick(View v) {

    }
}