package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.ScheduleList;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class MedicationScheduleSummaryFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationScheduleSummaryFragment";

    private LinearLayout contentLayout;
    private TextView tvSummary, tvTitle;
    private Button btnDone, btnAnother;

    private UserEntity user;
    private MedicationEntity medication;
    private ScheduleList scheduleList;

    public MedicationScheduleSummaryFragment() {
        // Required empty public constructor
    }

    public static MedicationScheduleSummaryFragment newInstance() {
        return new MedicationScheduleSummaryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_med_schedule_summary, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");

            if (medication != null) {
                tvTitle.setText(medication.getMedicationName() + " "
                        + medication.getStrengthValue()
                        + " " + medication.getStrengthMeasurement()
                        + " SCHEDULE");

                getScheduleData();
            }

            scheduleList = (ScheduleList) bundle.getSerializable("schedules");

            if(null != scheduleList) {
                if(scheduleList.count()>0) {
                    JoinScheduleDispense schedule = scheduleList.getScheduleDispense(0);
                    if(null!=schedule) {
                        tvTitle.setText(schedule.getDispenseTime() + " MEDICATIONS");
                    }
                }

                getMedicationData();
            }
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);

        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        btnAnother = view.findViewById(R.id.button_another_schedule);
        btnAnother.setOnClickListener(this);

        tvSummary = view.findViewById(R.id.textview_med_sched_summary);
        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        String name = v.toString();
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "MyMedicationFragment");
        }

        if(v == btnAnother) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", SelectReviewMedSchedFragment.class.getSimpleName());
        }
        mListener.onButtonClicked(bundle);
    }

    private void getMedicationData()
    {
        StringBuilder sb = new StringBuilder();
        List<JoinScheduleDispense> scheduleItems = scheduleList.getSchedules();
        Map<String, MedicationEntity> map = new HashMap<String, MedicationEntity>();

        if (scheduleItems.size() > 0) {
            for(JoinScheduleDispense schedule : scheduleItems) {
                String medId = Integer.toString(schedule.getMedicationId());
                if(!map.containsKey(medId))
                {
                    MedicationEntity med = new MedicationRepository().read(schedule.getMedicationId());
                    map.put(medId, med);
                    sb.append("MEDICATION NAME: " + med.getMedicationName() + "\n");
                    sb.append("DIRECTION FOR USE: " + med.getDosageInstructions()+ "\n");
                }
            }
        } else {
            sb.append("NO MEDICATION.");
        }
        tvSummary.setText(sb.toString());
    }

    private void getScheduleData() {
        StringBuilder sb = new StringBuilder();
        //sb.append("DOSAGE TIME / INFO\n");

        List<JoinScheduleDispense> scheduleItems =new ScheduleRepository().getJoinByUserIdMedicationId(user.getId(), medication.getId());

        //sort list by time
        Collections.sort(scheduleItems, new Comparator<JoinScheduleDispense>() {
            DateFormat f = new SimpleDateFormat("h:mm a", Locale.getDefault());

            @Override
            public int compare(JoinScheduleDispense o1, JoinScheduleDispense o2) {
                try {
                    return f.parse(o1.getDispenseTime()).compareTo(f.parse(o2.getDispenseTime()));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });

        if (scheduleItems.size() > 0) {
            sb.append("DISPENSE TIME:\n");
            // loop through and form a summary of the scheduled med
            for (JoinScheduleDispense scheduleItem : scheduleItems) {
                DispenseTimeEntity dispenseTime = new DispenseTimeRepository().read(scheduleItem.getDispenseTimeId());

                switch (ScheduleRecurrenceEnum.values()[scheduleItem.getRecurrence()])
                {
                    case DAILY:
                //display sections for different schedule types (1=daily, 2=weekly, 3=monthly)
                    sb.append(dispenseTime.getDispenseName())
                            .append(" DAILY @ ")
                            .append(dispenseTime.getDispenseTime())
                            .append("\n");
                    break;

                    case WEEKLY:
                    sb.append(dispenseTime.getDispenseName())
                            .append(" ")
                            .append(scheduleItem.getScheduleDay())
                            .append(" @ ")
                            .append(dispenseTime.getDispenseTime())
                            .append("\n");
                    break;

                    case MONTHLY:
                    sb.append(dispenseTime.getDispenseName())
                            .append(" ")
                            .append(scheduleItem.getScheduleDate())
                            .append(" @ ")
                            .append(dispenseTime.getDispenseTime())
                            .append("\n");
                    break;
                }
            }

            sb.append("\nDIRECTION FOR USE: " + medication.getDosageInstructions());

        } else {
            sb.append("THIS IS AN AS-NEEDED MEDICATION.");
        }
        tvSummary.setText(sb.toString());
    }
}