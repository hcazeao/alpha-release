package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * MedicationsPausedFragment
 *
 * @author alankilloren
 */
public class MedicationsPausedFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationsPausedFragment";

    private Button btnPause, btnDone;
    private UserEntity user;

    public MedicationsPausedFragment() {
        // Required empty public constructor
    }

    public static MedicationsPausedFragment newInstance() {
        return new MedicationsPausedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medications_paused, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnPause = view.findViewById(R.id.button_pause);
        btnPause.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnPause) {
            bundle.putSerializable("user", user);
            bundle.putString("removeAllFragmentsUpToCurrent", "SelectPauseMedicationFragment");
        }
        if (v == btnDone) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "MyMedicationFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}