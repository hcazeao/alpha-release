package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.i2c.CupLight;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.utils.TimerUtil;
import com.logicpd.papapill.wireframes.BundleFactory;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class ReturnCupFragment extends BaseHomeFragment implements OnTimerListener {

    public static final String TAG = "ReturnCupFragment";

    private TextView txtUserName;

    private DispenseEventsModel dispenseEventsModel;
    private JoinDispenseEventScheduleMedication currentDispenseEvent;

    private Sensor cup;
    private static CupLight cupLight = new CupLight(BoardDefaults.I2C_PORT, BoardDefaults.I2C_MUX_CUP_CHANNEL);

    private TimerUtil mTimer;

    public ReturnCupFragment() {
        // Required empty public constructor
    }

    public static ReturnCupFragment newInstance() {
        return new ReturnCupFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_return_cup, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            if (dispenseEventsModel != null) {
                currentDispenseEvent = dispenseEventsModel.getCurrent();
                txtUserName.setText(dispenseEventsModel.user.getUserName());
            }

            // Set mux to read from cup sensor.
            cup = new Sensor(BoardDefaults.I2C_PORT,
                                BoardDefaults.I2C_MUX_CUP_CHANNEL,
                                AppConfig.getInstance().getCupThresholdHigh());

            // Start a timer to poll cup sensor to check if cup has been returned.
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        txtUserName = view.findViewById(R.id.textview_username);
    }

    @Override
    public void onTimerFinished() {
        // Read the cup proximity sensor.
        int cupProximity = cup.getSensorValue();
        Log.d(TAG, "Cup Proximity Read: " + cupProximity);

        // Check if cup is returned. If so, advance to the next fragment, otherwise, check again
        // after another timer event.
        if (cupProximity != PROXIMITY_ERROR && cupProximity > AppConfig.getInstance().getCupThresholdHigh()) {
            Log.d(TAG, "Cup has been returned, going to next screen.");

            // Turn off the cup light.
            cupLight.setCupLightOff();

            Bundle bundle = this.getArguments();
            bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
            String fragmentName = ScheduledMedicationTakenFragment.class.getSimpleName();
            /*
             * Duplicate code in BaseDispenseMedsFragment -- should go away when we switch to models
             */
            if(null!=currentDispenseEvent) {
                int i = currentDispenseEvent.getDispenseType();
                DispenseEventTypeEnum type = DispenseEventTypeEnum.values()[i];
                switch (type) {
                    case EARLY_DISPENSE:
                        fragmentName = MedicationTakenEarlyFragment.class.getSimpleName();
                        break;

                    case AS_NEEDED:
                        fragmentName = MedicationTakenFragment.class.getSimpleName();
                        break;

                    default:
                    case SCHEDULED:
                        // already set above
                }
            }
            bundle.putString("fragmentName", fragmentName);
            mListener.onButtonClicked(bundle);

        } else {
            // Cup has not been returned, check again after the next timer elapses.
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    public void onClick(View v) {

    }
}