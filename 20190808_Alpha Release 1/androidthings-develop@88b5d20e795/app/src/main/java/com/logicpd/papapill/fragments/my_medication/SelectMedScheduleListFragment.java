package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.MedicationsAdapter;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectMedScheduleListFragment extends BaseHomeFragment {

    public static final String TAG = "SelectMedScheduleListFragment";

    LinearLayout contentLayout;
    TextView tvTitle, tvEmpty;
    MedicationsAdapter adapter;
    List<MedicationEntity> medicationList;
    MedicationEntity selectedMedication = null;

    UserEntity user;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView recyclerView;
    Button btnNext;

    public SelectMedScheduleListFragment() {
        // Required empty public constructor
    }

    public static SelectMedScheduleListFragment newInstance() {
        return new SelectMedScheduleListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        tvTitle.setText("SELECT A MED SCHEDULE TO VIEW");
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }

        medicationList = model.getScheduledMedicationsForUser(user, MedPausedEnum.NOT_PAUSED);
        adapter = new MedicationsAdapter(getActivity(), medicationList, true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new MedicationsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                selectedMedication = medicationList.get(position);
                TextUtils.enableButton(btnNext);
            }
        });

        if (medicationList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        btnNext.setVisibility(View.VISIBLE);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);

        tvTitle = view.findViewById(R.id.textview_title);
        recyclerView = view.findViewById(R.id.recyclerview_list);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        TextUtils.disableButton(btnNext);
        tvEmpty = view.findViewById(R.id.textview_empty);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext &&
                null!= selectedMedication) {
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", selectedMedication);
            bundle.putString("fragmentName", "MedicationScheduleSummaryFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}