package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.MedicationsCheckAdapter;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.util.List;

/**
 * SelectPauseMedicationFragment
 *
 * @author alankilloren
 */
public class SelectPauseMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "SelectPauseMedicationFragment";

    private TextView tvTitle, tvEmpty;
    private MedicationsCheckAdapter adapter;
    private UserEntity user;
    private RecyclerView recyclerView;
    private Button btnSelect, btnNext;
    private boolean isAllSelected = false;

    public SelectPauseMedicationFragment() {
        // Required empty public constructor
    }

    public static SelectPauseMedicationFragment newInstance() {
        return new SelectPauseMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_pause_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        String s = "SELECT MEDICATIONS TO PAUSE";
        tvTitle.setText(s);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }

        final List<MedicationEntity> medicationList = model.getMedicationsForUser(user);
        adapter = new MedicationsCheckAdapter(getActivity(), medicationList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new MedicationsCheckAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                adapter.toggleChecked(position);
            }
        });

        adapter.setCheckSelectedListener(new MedicationsCheckAdapter.CheckSelectedListener() {
            @Override
            public void onCheckSelected() {
                //check to see if any selections are paused
                List<MedicationEntity> medications = adapter.getListFromAdapter();
                checkSelected(medications);
            }
        });

        if (medicationList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            btnSelect.setVisibility(View.GONE);
            btnNext.setText("OK");
        } else {
            tvEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        checkSelected(medicationList);
    }

    /**
     * This method checks the current list and enables/disables the 'Next' button accordingly
     *
     * @param medicationList Passed in current list from adapter
     */
    private void checkSelected(List<MedicationEntity> medicationList) {
        boolean b = false;
        for (MedicationEntity medication : medicationList) {
            if (medication.isPaused()) {
                b = true;
                break;
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvTitle = view.findViewById(R.id.textview_title);
        recyclerView = view.findViewById(R.id.recyclerview_medication_list);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        btnSelect = view.findViewById(R.id.button_select);
        btnSelect.setOnClickListener(this);
        tvEmpty = view.findViewById(R.id.textview_empty);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        if (v == backButton) {
            bundle.putString("fragmentName", "MyMedicationFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnSelect) {
            if (!isAllSelected) {
                adapter.selectAll(true);
                btnSelect.setText("DESELECT ALL");
                isAllSelected = true;
            } else {
                adapter.selectAll(false);
                btnSelect.setText("SELECT ALL");
                isAllSelected = false;
            }
        }
        if (v == btnNext) {
            // update db with current list
            List<MedicationEntity> medications = adapter.getListFromAdapter();
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

            for (MedicationEntity medication : medications) {
                model.updateMedication(medication);
            }
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "MedicationsPausedFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}