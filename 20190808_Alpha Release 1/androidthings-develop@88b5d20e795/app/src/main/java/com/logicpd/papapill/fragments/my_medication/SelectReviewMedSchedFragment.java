package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectDispensingTimesFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectReviewMedSchedFragment extends BaseHomeFragment {

    public static final String TAG = "SelectReviewMedSchedFragment";

    private Button btnMedication, btnDispenseTime;
    private UserEntity user;

    public SelectReviewMedSchedFragment() {
        // Required empty public constructor
    }

    public static SelectReviewMedSchedFragment newInstance() {
        return new SelectReviewMedSchedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_select_review_med_schedule, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnMedication = view.findViewById(R.id.button_by_medication);
        btnMedication.setOnClickListener(this);
        btnDispenseTime = view.findViewById(R.id.button_by_dispense_time);
        btnDispenseTime.setOnClickListener(this);

        //TextUtils.disableButton(btnMonth);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        if (v == backButton) {
            bundle.putString("fragmentName", "MyMedicationFragment");
        }
        else if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
        }
        else if (v == btnMedication) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "SelectMedScheduleListFragment");
        }
        else if (v == btnDispenseTime) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", SelectScheduleListFragment.class.getSimpleName());
        }
        mListener.onButtonClicked(bundle);
    }
}