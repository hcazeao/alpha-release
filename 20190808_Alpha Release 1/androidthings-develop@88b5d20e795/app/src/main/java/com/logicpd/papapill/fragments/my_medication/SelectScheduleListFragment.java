package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.ScheduleList;
import com.logicpd.papapill.data.adapters.DispenseTimesAdapter;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectDispensingTimesFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SelectScheduleListFragment extends BaseHomeFragment {

    public static final String TAG = "SelectScheduleListFragment";

    private UserEntity user;
    private TextView tvTitle, tvEmpty;
    private Button btnNext;
    private DispenseTimesAdapter adapter;
    private List<DispenseTimeEntity> dispenseTimeList;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private Map<String, List<JoinScheduleDispense>> map;
    private List<JoinScheduleDispense> selectedSchedules;

    public SelectScheduleListFragment() {
        // Required empty public constructor
    }

    public static SelectScheduleListFragment newInstance() {
        return new SelectScheduleListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }

        // query for dispenseTime list
        List<JoinScheduleDispense> list = model.getNonPausedJoinScheduleDispenseByUserId(user.getId());
        map = new HashMap<String, List<JoinScheduleDispense>>();
        dispenseTimeList = new ArrayList<DispenseTimeEntity>();
        for(JoinScheduleDispense join : list)
        {
            String time = join.getDispenseTime();
            DispenseTimeEntity dispenseTime = join.getDispenseTimeEntity();

            // no repeating dispense times
            if(!map.containsKey(time))
            {
                dispenseTime.setActive(false);
                dispenseTimeList.add(dispenseTime);
                List<JoinScheduleDispense> schedules = new ArrayList<JoinScheduleDispense>();
                schedules.add(join);
                map.put(time, schedules);
            }
            else
            {
                List<JoinScheduleDispense> schedules = map.get(time);
                schedules.add(join);
            }
        }

        //sort list by time
        Collections.sort(dispenseTimeList, new Comparator<DispenseTimeEntity>() {
            DateFormat f = new SimpleDateFormat("h:mm a", Locale.getDefault());

            @Override
            public int compare(DispenseTimeEntity o1, DispenseTimeEntity o2) {
                try {
                    return f.parse(o1.getDispenseTime()).compareTo(f.parse(o2.getDispenseTime()));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        adapter = new DispenseTimesAdapter(getActivity(), dispenseTimeList, true, true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new DispenseTimesAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                adapter.toggleChecked(position);

                DispenseTimeEntity dispenseTime = dispenseTimeList.get(position);
                selectedSchedules = map.get(dispenseTime.getDispenseTime());
                TextUtils.enableButton(btnNext);
            }
        });

        if (dispenseTimeList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        TextView textView = view.findViewById(R.id.textview_title);
        textView.setText("SELECT A SCHEDULE TO VIEW");

        tvTitle = view.findViewById(R.id.textview_title);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        TextUtils.disableButton(btnNext);
        recyclerView = view.findViewById(R.id.recyclerview_list);
        tvEmpty = view.findViewById(R.id.textview_empty);
        tvEmpty.setText("NO SCHEDULE FOUND FOR THIS USER");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext &&
                null!=selectedSchedules) {

            bundle.putSerializable("user", user);
            bundle.putSerializable("schedules", new ScheduleList(selectedSchedules));
            bundle.putString("fragmentName", MedicationScheduleSummaryFragment.class.getSimpleName());
            mListener.onButtonClicked(bundle);
        }
    }
}
