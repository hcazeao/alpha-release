package com.logicpd.papapill.fragments.system_manager;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

/**
 * Fragment for System Manager
 *
 * @author alankilloren
 */
public class SystemManagerFragment extends BaseHomeFragment {
    public static final String TAG = "SystemManagerFragment";
    private Button btnManageUsers, btnManageMedications, btnSystemSettings;

    public SystemManagerFragment() {
        // Required empty public constructor
    }

    public static SystemManagerFragment newInstance() {
        return new SystemManagerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_manager, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

        if (model.getUsers().size() == 0) {
            TextUtils.disableButton(btnManageMedications);
        }
    }

    protected void setupViews(View view) {
       super.setupViews(view);
        btnManageUsers = view.findViewById(R.id.button_manage_users);
        btnManageUsers.setOnClickListener(this);
        btnManageMedications = view.findViewById(R.id.button_manage_medications);
        btnSystemSettings = view.findViewById(R.id.button_system_settings);
        btnManageMedications.setOnClickListener(this);
        btnSystemSettings.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();

        if (v == btnManageUsers) {
            bundle.putString("fragmentName", "ManageUsersFragment");
        }
        if (v == btnSystemSettings) {
            bundle.putString("fragmentName", "SystemSettingsFragment");
        }
        if (v == btnManageMedications) {
            bundle.putString("fragmentName", "ManageMedsFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}