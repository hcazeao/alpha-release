package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.wireframes.BundleFactory;

/**
 * CannotDeleteDispenseTimeFragment
 *
 * @author alankilloren
 */
public class CannotDeleteDispenseTimeFragment extends BaseHomeFragment {

    public static final String TAG = "CannotDeleteDispenseTimeFragment";

    private Button btnDone;
    private TextView tvInfo;

    public CannotDeleteDispenseTimeFragment() {
        // Required empty public constructor
    }

    public static CannotDeleteDispenseTimeFragment newInstance() {
        return new CannotDeleteDispenseTimeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_cannot_delete_dispense_time, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            DispenseTimeEntity dispenseTime = (DispenseTimeEntity) bundle.getSerializable("dispensetime");
            if (dispenseTime != null) {
                String s = dispenseTime.getDispenseName() + " " + dispenseTime.getDispenseTime()
                        + " DISPENSING TIME IS CURRENTLY BEING USED";
                tvInfo.setText(s);
            }
        }
        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvInfo = view.findViewById(R.id.textview_info);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnDone) {
            backButton.performClick();
        }
    }
}