package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;
import com.logicpd.papapill.wireframes.workflows.RemoveMedication;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class CannotRefillMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "CannotRefillMedicationFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private Button btnCancel, btnRemove;
    private TextView msg;

    public CannotRefillMedicationFragment() {
        // Required empty public constructor
    }

    public static CannotRefillMedicationFragment newInstance() {
        return new CannotRefillMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_cannot_refill_med, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        String str = bundle.getString("message");
        if(null!=str)
            msg.setText(str);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnRemove = view.findViewById(R.id.button_remove_med);
        btnRemove.setOnClickListener(this);
        msg = view.findViewById(R.id.textview_medication_summary);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        if (v == btnCancel) {
            bundle.putString("removeAllFragmentsUpToCurrent", "ManageMedsFragment");
        }
        else if (v == btnRemove) {
            RefillMedication model = (RefillMedication)((MainActivity) getActivity()).getFeatureModel();
            ((MainActivity) getActivity()).enterFeature(RemoveMedication.TAG);

            bundle.putSerializable("user", model.getUser());
            bundle.putSerializable("medication", model.getMedication());
            bundle.putString("fragmentName", RemoveMedication.RemoveMedicationEnum.ConfirmRemoveMedication.toString());
        }
        mListener.onButtonClicked(bundle);
    }
}