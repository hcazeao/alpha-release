package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

public class ConfirmMedMaxUnitMinTimeFragment extends BaseHomeFragment {
    public static final String TAG = "ConfirmMedMaxUnitMinTimeFragment";
    private TextView tvMedSumary, title;
    private Button btnIncorrect, btnCorrect;
    private BaseWireframe mModel;
    private MedicationEntity medication;

    public ConfirmMedMaxUnitMinTimeFragment() {

    }

    public static ConfirmMedMaxUnitMinTimeFragment newInstance() {
        return new ConfirmMedMaxUnitMinTimeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_confirm_refill_med, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        mModel = ((MainActivity) getActivity()).getFeatureModel();
        if(null==mModel){
            Bundle bundle = this.getArguments();
            medication = (MedicationEntity) bundle.getSerializable("medication");
        }
        else {
            medication = mModel.getMedication();
        }

        title.setText(getString(R.string.confirm_max_units_min_time));
        String str = String.format("%s %d %s %d %s %d",
                        getString(R.string.max_units_per_dose), medication.getMaxNumberPerDose(),
                        getString(R.string.max_units_per_day), medication.getMaxUnitsPerDay(),
                        getString(R.string.min_hours_between_doses), medication.getTimeBetweenDoses());
        tvMedSumary.setText(str);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        title = view.findViewById(R.id.txt_title);
        btnIncorrect = view.findViewById(R.id.button_incorrect);
        btnIncorrect.setOnClickListener(this);
        btnCorrect = view.findViewById(R.id.button_correct);
        btnCorrect.setOnClickListener(this);
        tvMedSumary = view.findViewById(R.id.textview_medication_summary);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        if (v == btnIncorrect) {
            bundle.putString("message", getString(R.string.medication_max_units_or_minimum_time_not_match));
            bundle.putString("fragmentName", RefillMedication.RefillMedicationEnum.CannotRefillMedication.toString());
        }
        if (v == btnCorrect) {
            // heading to a common fragment that does not have model yet
            bundle.putSerializable("user", mModel.getUser());
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", mModel.getNextFragmentName(TAG));
        }
        mListener.onButtonClicked(bundle);
    }
}
