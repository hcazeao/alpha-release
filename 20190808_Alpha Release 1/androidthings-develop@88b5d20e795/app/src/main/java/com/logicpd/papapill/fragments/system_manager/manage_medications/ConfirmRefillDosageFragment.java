package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

public class ConfirmRefillDosageFragment extends BaseHomeFragment {
    public static final String TAG = "ConfirmRefillDosageFragment";
    private TextView tvMedSumary, title;
    private Button btnIncorrect, btnCorrect;
    private RefillMedication mModel;

    public ConfirmRefillDosageFragment() {

    }

    public static ConfirmRefillDosageFragment newInstance() {
        return new ConfirmRefillDosageFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_confirm_refill_med, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        mModel = (RefillMedication) ((MainActivity) getActivity()).getFeatureModel();
        MedicationEntity medication = mModel.getMedication();

        title.setText(this.getString(R.string.confirm_med_dosage_and_frequency));
        /*
         * WARNING: Scheduled medication does not have the following information !!!
         */
        String str = String.format("%s %d \n%s %d",
                this.getString(R.string.units_per_dose),
                medication.getMaxNumberPerDose(),
                this.getString(R.string.doses_per_day),
                medication.getMaxUnitsPerDay());
        tvMedSumary.setText(str);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        title = view.findViewById(R.id.txt_title);
        btnIncorrect = view.findViewById(R.id.button_incorrect);
        btnIncorrect.setOnClickListener(this);
        btnCorrect = view.findViewById(R.id.button_correct);
        btnCorrect.setOnClickListener(this);
        tvMedSumary = view.findViewById(R.id.textview_medication_summary);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        if (v == btnIncorrect) {
            bundle.putString("message", getString(R.string.medication_dosage_or_frequency_does_not_match));
            bundle.putString("fragmentName", RefillMedication.RefillMedicationEnum.CannotRefillMedication.toString());
        }
        if (v == btnCorrect) {
            bundle.putBoolean("isFromRefill", true);
            bundle.putString("fragmentName", RefillMedication.RefillMedicationEnum.MedicationQuantityMessage.toString());
        }
        mListener.onButtonClicked(bundle);
    }
}
