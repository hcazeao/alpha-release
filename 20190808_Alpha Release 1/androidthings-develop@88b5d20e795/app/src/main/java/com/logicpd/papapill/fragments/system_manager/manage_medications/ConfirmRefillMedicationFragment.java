package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class ConfirmRefillMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "ConfirmRefillMedicationFragment";

    private TextView tvMedSumary;
    private Button btnIncorrect, btnCorrect;
    private MedicationEntity medication;

    public ConfirmRefillMedicationFragment() {
        // Required empty public constructor
    }

    public static ConfirmRefillMedicationFragment newInstance() {
        return new ConfirmRefillMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_confirm_refill_med, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        RefillMedication model = (RefillMedication)((MainActivity)getActivity()).getFeatureModel();
        medication = model.getMedication();


        tvMedSumary.setText(getString(R.string.current_med_in_bin) + " " + medication.getMedicationLocation() + " " +
                            getString(R.string.is_next_line)+ " " +
                            medication.getMedicationName() + " " + medication.getStrengthValue() + " " + medication.getStrengthMeasurement() + " " +
                            getString(R.string.the_refill_also) + " " +
                            medication.getMedicationName() + " " + medication.getStrengthValue() + " " + medication.getStrengthMeasurement() + "?");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnIncorrect = view.findViewById(R.id.button_incorrect);
        btnIncorrect.setOnClickListener(this);
        btnCorrect = view.findViewById(R.id.button_correct);
        btnCorrect.setOnClickListener(this);
        tvMedSumary = view.findViewById(R.id.textview_medication_summary);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        if (v == btnIncorrect) {
            bundle.putString("fragmentName", RefillMedication.RefillMedicationEnum.CannotRefillMedication.toString());
        }
        if (v == btnCorrect) {
            MedScheduleTypeEnum type =  MedScheduleTypeEnum.values()[medication.getMedicationScheduleType()];
            String fragmentName;

            switch (type) {
                case SCHEDULED:
                case BOTH:
                    fragmentName = RefillMedication.RefillMedicationEnum.ConfirmRefillDosage.toString();
                    break;

                default:
                case AS_NEEDED:
                    fragmentName = RefillMedication.RefillMedicationEnum.MedicationQuantityMessage.toString();
                    break;
            }
            bundle.putBoolean("isFromRefill", true);
            bundle.putString("fragmentName", fragmentName);
        }
        mListener.onButtonClicked(bundle);
    }
}