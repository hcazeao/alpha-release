package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.ChangeMedSchedule;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class DirectionsQuantityPerDoseFragment extends BaseHomeFragment {

    public static final String TAG = "DirectionsQuantityPerDoseFragment";

    private LinearLayout contentLayout;
    private UserEntity user;
    private Button btnNext;
    private MedicationEntity medication;
    private boolean isFromSchedule, isEditMode;

    public DirectionsQuantityPerDoseFragment() {
        // Required empty public constructor
    }

    public static DirectionsQuantityPerDoseFragment newInstance() {
        return new DirectionsQuantityPerDoseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_directions_qty_dose, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
            if (bundle.containsKey("isEditMode")) {
                isEditMode = bundle.getBoolean("isEditMode");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);

        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            //save the current medication so we can get the ID for scheduling
            if (!isEditMode) {
                BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
                model.addMedication(medication);
                // do I need to medId ?
            }
            bundle.putSerializable("medication", medication);
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putBoolean("isEditMode", isEditMode);
            bundle.putString("fragmentName", ChangeMedSchedule.ChangeMedScheduleEnum.EnterDailyQuantityPerDose.toString());
        }
        mListener.onButtonClicked(bundle);
    }
}