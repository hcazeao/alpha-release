package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.ChangeMedSchedule;

/**
 * DispenseTimeDeletedFragment
 *
 * @author alankilloren
 */
public class DispenseTimeDeletedFragment extends BaseHomeFragment {

    public static final String TAG = "DispenseTimeDeletedFragment";

    private Button btnDone;
    private boolean isFromSchedule;

    public DispenseTimeDeletedFragment() {
        // Required empty public constructor
    }

    public static DispenseTimeDeletedFragment newInstance() {
        return new DispenseTimeDeletedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_dispense_time_deleted, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            String fragmentName = (isWorkflowChangeMedSchedule()) ?
                                "SelectDispensingTimesFragment":
                                ManageMedsFragment.class.getSimpleName();

            bundle.putString("removeAllFragmentsUpToCurrent", fragmentName);
            mListener.onButtonClicked(bundle);
        }
    }


    private boolean isWorkflowChangeMedSchedule() {
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        return model.getClass().getSimpleName().equals(ChangeMedSchedule.TAG)?true:false;
    }
}