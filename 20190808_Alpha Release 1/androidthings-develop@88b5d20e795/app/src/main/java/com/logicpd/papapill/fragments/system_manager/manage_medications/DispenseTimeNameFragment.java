package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BundleFactory;

public class DispenseTimeNameFragment extends BaseHomeFragment {

    public static final String TAG = "DispenseTimeNameFragment";

    private Button btnNext;
    private EditText etDispenseName;
    private UserEntity user;
    private DispenseTimeEntity dispenseTime;
    private boolean isEditMode = false;
    private boolean isFromSchedule;
    private TextView tvTitle;

    public DispenseTimeNameFragment() {
        // Required empty public constructor
    }

    public static DispenseTimeNameFragment newInstance() {
        return new DispenseTimeNameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_dispense_time_name, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            dispenseTime = (DispenseTimeEntity) bundle.getSerializable("dispensetime");
            if (dispenseTime != null && dispenseTime.getDispenseName() != null) {
                etDispenseName.setText(dispenseTime.getDispenseName());
            }
            if (bundle.containsKey("isEditMode")) {
                isEditMode = bundle.getBoolean("isEditMode");
                if (isEditMode) {
                    String s = "EDIT DISPENSING TIME NAME";
                    tvTitle.setText(s);
                }
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        tvTitle = view.findViewById(R.id.textview_title);
        etDispenseName = view.findViewById(R.id.edittext_dispenseTime_name);
        etDispenseName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etDispenseName.getText().length() > 3 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etDispenseName.getText().toString().length() > 3) {
                if (isEditMode) {
                    //edit existing record
                    dispenseTime.setDispenseName(etDispenseName.getText().toString());
                    bundle.putSerializable("dispensetime", dispenseTime);
                    bundle.putSerializable("user", user);
                    bundle.putBoolean("isEditMode", isEditMode);
                    bundle.putString("fragmentName", "DispenseTimeFragment");
                    mListener.onButtonClicked(bundle);
                } else {
                    //adding a new record
                    DispenseTimeEntity dispenseTime = new DispenseTimeEntity();
                    dispenseTime.setDispenseName(etDispenseName.getText().toString());
                    bundle.putSerializable("dispensetime", dispenseTime);
                    bundle.putBoolean("isEditMode", isEditMode);
                    bundle.putBoolean("isFromSchedule", isFromSchedule);
                    bundle.putSerializable("user", user);
                    bundle.putString("fragmentName", "DispenseTimeFragment");
                    mListener.onButtonClicked(bundle);
                }

            } else {
                TextUtils.showToast(getActivity(), "Please enter a valid dispense time name");
            }
        }
    }
}
