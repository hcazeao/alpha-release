package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;

/**
 * EditDispenseTimesFragment
 *
 * @author alankilloren
 */
public class EditDispenseTimesFragment extends BaseHomeFragment {

    public static final String TAG = "EditDispenseTimesFragment";

    private Button btnAdd, btnEdit, btnDelete;
    private UserEntity user;
    private boolean isFromSchedule;

    public EditDispenseTimesFragment() {
        // Required empty public constructor
    }

    public static EditDispenseTimesFragment newInstance() {
        return new EditDispenseTimesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_edit_dispense_times, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
        if (model.getDispenseTimes(false).size() == 0) {
            TextUtils.disableButton(btnEdit);
            TextUtils.disableButton(btnDelete);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnAdd = view.findViewById(R.id.button_add_dispenseTime);
        btnAdd.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_edit_dispenseTime);
        btnEdit.setOnClickListener(this);
        btnDelete = view.findViewById(R.id.button_delete_dispenseTime);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnAdd) {
            int MAX_dispenseTimeS = 24;//TODO change or move this as need be
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

            if (model.getDispenseTimes(false).size() == MAX_dispenseTimeS) {
                bundle.putString("fragmentName", "MaxDispenseTimesFragment");
                mListener.onButtonClicked(bundle);
            } else {
                bundle.putBoolean("isFromSchedule", isFromSchedule);
                bundle.putString("fragmentName", "DispenseTimeNameFragment");
                bundle.putSerializable("user", user);
                mListener.onButtonClicked(bundle);
            }
        }
        if (v == btnEdit) {
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putString("fragmentName", "SelectEditDispenseTimeFragment");
            bundle.putSerializable("user", user);
            mListener.onButtonClicked(bundle);
        }
        if (v == btnDelete) {
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putString("fragmentName", "SelectDeleteDispenseTimeFragment");
            bundle.putSerializable("user", user);
            mListener.onButtonClicked(bundle);
        }
    }
}