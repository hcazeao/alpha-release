package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.AddNewMedication;
import com.logicpd.papapill.wireframes.workflows.ChangeMedSchedule;
import com.logicpd.papapill.databinding.FragmentManageMedsBinding;
import com.logicpd.papapill.wireframes.workflows.EditNamedDispenseTime;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;
import com.logicpd.papapill.wireframes.workflows.RemoveMedication;

import java.util.List;

/**
 * Fragment for System Manager...Manage Medications
 *
 * @author alankilloren
 */
public class ManageMedsFragment extends BaseHomeFragment {

    public static final String TAG = "ManageMedsFragment";
    private FragmentManageMedsBinding mBinding;
    public boolean hasMedication = false;

    public ManageMedsFragment() {
        // Required empty public constructor
    }

    public static ManageMedsFragment newInstance() {
        return new ManageMedsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_meds, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        List<MedicationEntity> medicationList = model.getMedications();
        hasMedication = (medicationList.size() == 0)?false:true;
        mBinding.invalidateAll();
    }

    public void onClickNewMed() {
        ((MainActivity) this.getActivity()).enterFeature(AddNewMedication.TAG);
        setBundle("SelectUserForMedsFragment", "SystemKeyFragment");
    }

    public void onClickRefill() {
        ((MainActivity) this.getActivity()).enterFeature(RefillMedication.TAG);
        setBundle("SelectUserForRefillFragment", "SystemKeyFragment");
    }

    public void onClickRemoveMed() {
        ((MainActivity) getActivity()).enterFeature(RemoveMedication.TAG);
        setBundle("SelectUserForRemoveMedFragment", "SystemKeyFragment");
    }

    public void onClickChangeMedSchedule() {
        ((MainActivity)getActivity()).enterFeature(ChangeMedSchedule.TAG);
        setBundle("SelectUserForChangeScheduleFragment", "SystemKeyFragment");
    }

    public void onClickEditTimes() {
        ((MainActivity)getActivity()).enterFeature(EditNamedDispenseTime.TAG);
        setBundle("EditDispenseTimesFragment", "SystemKeyFragment");
    }

    private void setBundle(String authFragment,
                           String fragmentName){
        Bundle bundle = new Bundle();
        bundle.putString("authFragment", authFragment);
        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}
