package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * MedicationAddedFragment
 *
 * @author alankilloren
 */
public class MedicationAddedFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationAddedFragment";

    private Button btnAddMed, btnDone;
    private UserEntity user;
    private MedicationEntity medication;
    private TextView tvMedication;
    private boolean isFromSchedule;

    public MedicationAddedFragment() {
        // Required empty public constructor
    }

    public static MedicationAddedFragment newInstance() {
        return new MedicationAddedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_medication_added, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            tvMedication.setText(medication.getMedicationName() + " " + medication.getStrengthMeasurement());
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnAddMed = view.findViewById(R.id.button_add_med);
        btnAddMed.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvMedication = view.findViewById(R.id.textview_medicationName);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            if (isFromSchedule) {
                // send something back to MainActivity, telling it we entered a new medication and to update the schedule
                bundle.putBoolean("updateSchedule", true);
            }
            bundle.putString("fragmentName", "Home");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnAddMed) {
            bundle.putString("removeAllFragmentsUpToCurrent", "SelectUserForMedsFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}