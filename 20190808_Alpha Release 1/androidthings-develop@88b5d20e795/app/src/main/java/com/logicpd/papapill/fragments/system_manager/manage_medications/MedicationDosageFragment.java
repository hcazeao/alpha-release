package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

/**
 * MedicationDosageFragment
 *
 * @author alankilloren
 */
public class MedicationDosageFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationDosageFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private Button btnNext;
    private EditText etDosage;

    public MedicationDosageFragment() {
        // Required empty public constructor
    }

    public static MedicationDosageFragment newInstance() {
        return new MedicationDosageFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_medication_dosage, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (medication != null) {
                etDosage.setText(medication.getDosageInstructions());
            }
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etDosage = view.findViewById(R.id.edittext_medication_dosage);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etDosage.getText().toString().length() > 1) {
                medication.setDosageInstructions(etDosage.getText().toString());
                bundle.putSerializable("user", user);
                bundle.putSerializable("medication", medication);
                bundle.putString("fragmentName", "MedicationQuantityMessageFragment");
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please enter dosage instructions");
            }
        }
    }
}