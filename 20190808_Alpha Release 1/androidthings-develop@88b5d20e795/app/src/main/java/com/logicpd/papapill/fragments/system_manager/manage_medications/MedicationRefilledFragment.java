package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

import org.apache.log4j.chainsaw.Main;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class MedicationRefilledFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationRefilledFragment";

    private LinearLayout contentLayout;
    private Button btnRefillMed, btnDone;
    private UserEntity user;
    private MedicationEntity medication;
    private TextView tvMedication;
    private boolean isFromRefill;
    protected BaseWireframe mModel;

    public MedicationRefilledFragment() {
        // Required empty public constructor
    }

    public static MedicationRefilledFragment newInstance() {
        return new MedicationRefilledFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_medication_refilled, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromRefill")) {
                isFromRefill = bundle.getBoolean("isFromRefill");
            }
        }

        mModel = ((MainActivity)getActivity()).getFeatureModel();
        medication = mModel.getMedication();
        tvMedication.setText(medication.getMedicationName() + " " + medication.getStrengthValue() + " " + medication.getStrengthMeasurement());
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        contentLayout = view.findViewById(R.id.layout_content);
        btnRefillMed = view.findViewById(R.id.button_refill_med);
        btnRefillMed.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvMedication = view.findViewById(R.id.textview_medicationName);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            bundle.putString("removeAllFragmentsUpToCurrent", RefillMedication.RefillMedicationEnum.ManageMeds.toString());
            ((MainActivity)getActivity()).exitFeature();
        }
        if (v == btnRefillMed) {
            bundle.putString("removeAllFragmentsUpToCurrent", RefillMedication.RefillMedicationEnum.SelectRefillMedication.toString());
        }
        mListener.onButtonClicked(bundle);
    }
}