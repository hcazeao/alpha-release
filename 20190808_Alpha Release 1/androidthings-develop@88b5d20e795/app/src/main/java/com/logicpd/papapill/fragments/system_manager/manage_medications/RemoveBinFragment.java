package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.BuildConfig;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.gpio.LockManager;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.device.tinyg.commands.CommandRotateCarousel;
import com.logicpd.papapill.enums.BuildFlavorsEnum;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.MedLocaEnum;
import com.logicpd.papapill.enums.SelectMedLocationUIState;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.utils.TimerUtil;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

/**
 * RemoveBinFragment
 *
 * @author alankilloren
 */
public class RemoveBinFragment extends BaseHomeFragment implements IntentMsgInterface, OnTimerListener {

    public static final String TAG = "RemoveBinFragment";

    private Button btnHelp, btnCancel;
    private UserEntity user;
    private MedicationEntity medication;
    private boolean isRemoveMedication, isFromRefill, isFromSchedule, isFromUserDelete;
    private TextView tvRemoveBin;
    private String CMD_ROTATE_CAROUSEL;
    private SelectMedLocationUIState mState;
    private final int BIN_NOT_AVAILABLE = -1;
    private int binLocation = BIN_NOT_AVAILABLE;
    private final String MOVE_SUCCESS = "moveSuccess";
    private boolean isMoveDone = false;
    private TimerUtil mTimer;

    Sensor door;

    public RemoveBinFragment() {
        // Required empty public constructor
    }

    public static RemoveBinFragment newInstance() {
        return new RemoveBinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CMD_ROTATE_CAROUSEL = CommandRotateCarousel.class.getSimpleName();
        mState = SelectMedLocationUIState.UNDECIDED;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_remove_bin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();

        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            binLocation = medication.getMedicationLocation();
            setMoveDone(bundle);

            String s;
            if (isMoveDone && medication != null) {
                s = String.format("%s %d", getString(R.string.slide_device_door_open_and_remove_bin),
                        medication.getMedicationLocation());
                // Unlock the door lock
                LockManager.getInstance().UnlockDoor();
                // Set the mux to the door sensor to begin polling for proximity
                door = new Sensor(BoardDefaults.I2C_PORT,
                        BoardDefaults.I2C_MUX_DOOR_CHANNEL,
                        AppConfig.getInstance().getDoorThresholdHigh());
                // Start the timer to poll door sensor to check if door is opened
                if(null==mTimer) {
                    mTimer = new TimerUtil(this);
                }
                mTimer.start(PROXIMITY_READ_TIMER_MS);
            } else {
                s = getString(R.string.please_wait);
            }
            tvRemoveBin.setText(s);

            if (bundle.containsKey("isRemoveMedication")) {
                isRemoveMedication = bundle.getBoolean("isRemoveMedication");
            }
            if (bundle.containsKey("isFromRefill")) {
                isFromRefill = bundle.getBoolean("isFromRefill");
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
        mState = SelectMedLocationUIState.SELECTED_BIN;

        // move as necessary
        if(!isMoveDone) {
            moveBin();
        }
    }

    /*
     * On a breadboard, there are 2 states.
     * a. state: move-not-done (disabled buttons)
     * b. state: move-done (feedback from tinyG -> enabled buttons)
     */
    private void setMoveDone(Bundle bundle) {
        if(bundle.containsKey(MOVE_SUCCESS)) {
            isMoveDone = bundle.getBoolean(MOVE_SUCCESS);
            return;
        }

        BuildFlavorsEnum flavor = BuildFlavorsEnum.valueOf(BuildConfig.FLAVOR.toUpperCase());
        isMoveDone = (flavor==BuildFlavorsEnum.BREADBOARD)?false:true;
    }

    protected void moveBin() {
        Log.d(TAG, "Moving bin" + binLocation);

        if (AppConfig.getInstance().isTinyGAvailable) {
            TinyGDriver.getInstance().doMoveBin4User(binLocation);
            Log.d(TAG, "Set OnRotateCompleteListener");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnHelp = view.findViewById(R.id.button_help);
        btnHelp.setOnClickListener(this);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        tvRemoveBin = view.findViewById(R.id.textview_remove_bin);
    }

    /*
     * handle action message from intent (send from command)
     */
    public boolean handleIntent(String command,
                                String state,
                                String msg,
                                String version) {   // idempotent
        Log.d(TAG, String.format("handleAction() command: %s state: %s msg: %s",
                command,
                state,
                msg));

        switch (mState) {
            case SELECTED_BIN:
                if (command.equals(CMD_ROTATE_CAROUSEL) &&
                        msg.equals(IntentMsgEnum.success.toString()) &&
                        null != medication &&
                        !isMoveDone) {  // once is all we need

                    Log.d(TAG, "binLocation:"+binLocation);
                    // successful move -- keep it at this bin location
                    ((MainActivity) getActivity()).startCarouselPosService(CarouselPosCmdEnum.bin, binLocation);
                    isMoveDone = true;
                    createBundle("RemoveBinFragment");
                    /*
                     * Update screen message for user to open door
                     */
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void onTimerFinished() {
        // Read the door proximity sensor.
        int doorProximity = door.getSensorValue();
        Log.d(TAG, "Door Proximity Read: " + doorProximity);

        // Check if door is open. If so, advance to the next fragment, otherwise, check again
        // after another timer event.
        if (doorProximity != PROXIMITY_ERROR && doorProximity < AppConfig.getInstance().getDoorThresholdLow()) {
            Log.d(TAG, "Door is open, going to next screen.");
            String fragmentName = (isRemoveMedication) ?
                    "RemoveMedicationFragment" :
                    "LoadMedicationFragment";
            createBundle(fragmentName);
        } else {
            // Door is still closed, check again after the next timer elapses.
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnHelp) {
            // display help/video for remove bin
            createBundle("RemoveBinHelpFragment");
        }
    }

    protected void createBundle(String fragmentName) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        bundle.putSerializable("medication", medication);
        bundle.putBoolean("isFromSchedule", isFromSchedule);
        bundle.putBoolean("isFromUserDelete", isFromUserDelete);
        bundle.putBoolean("isRemoveMedication", isRemoveMedication);

        if(isMoveDone)
            bundle.putBoolean(MOVE_SUCCESS, isMoveDone);

        if(isFromUserDelete)
            bundle.putBoolean("isFromUserDelete", isFromUserDelete);

        if(isRemoveMedication)
            bundle.putBoolean("isRemoveMedication", isRemoveMedication);

        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}