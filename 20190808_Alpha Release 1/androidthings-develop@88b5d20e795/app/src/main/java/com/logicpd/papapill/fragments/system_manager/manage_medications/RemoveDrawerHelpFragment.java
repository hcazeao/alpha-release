package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;

/**
 * RemoveDrawerHelpFragment
 *
 * @author alankilloren
 */
public class RemoveDrawerHelpFragment extends BaseHomeFragment {

    public static final String TAG = "RemoveDrawerHelpFragment";

    private Button btnDone;
    private TextView tvTitle;

    public RemoveDrawerHelpFragment() {
        // Required empty public constructor
    }

    public static RemoveDrawerHelpFragment newInstance() {
        return new RemoveDrawerHelpFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_remove_drawer_help, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isRemoveMedication")) {
                boolean isRemoveMedication = bundle.getBoolean("isRemoveMedication");
                if (isRemoveMedication) {
                    String s = "REMOVE MED FROM DRAWER HELP SCREEN/VIDEO";
                    tvTitle.setText(s);
                }
            }
        }
        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            backButton.performClick();
        }
    }
}