package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.ChangeMedSchedule;

public class ScheduleUpdatedFragment extends BaseHomeFragment {
    public static final String TAG = "ScheduleUpdatedFragment";
    private Button btnDone;
    private MedicationEntity medication;

    public static ScheduleUpdatedFragment newInstance() {
        return new ScheduleUpdatedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_schedule_updated, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            medication = (MedicationEntity) bundle.getSerializable("medication");
        }
        setupViews(view);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        TextView txtMedInfo = view.findViewById(R.id.txt_medication_info);
        txtMedInfo.setText(medication.getMedicationName() + " " + medication.getStrengthMeasurement());

        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            ((MainActivity)getActivity()).exitFeature(ChangeMedSchedule.TAG);
            bundle.putString("fragmentName", ChangeMedSchedule.ChangeMedScheduleEnum.SystemKey.toString());
            mListener.onButtonClicked(bundle);
        }
    }
}
