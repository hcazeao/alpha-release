package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentManageMedsNoRefillBinding;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;
import com.logicpd.papapill.wireframes.workflows.RemoveMedication;

public class SelectBinNotRefillMedFragment extends BaseHomeFragment {
    public static final String TAG = "SelectBinNotRefillMedFragment";
    private FragmentManageMedsNoRefillBinding mBinding;

    public SelectBinNotRefillMedFragment() {

    }

    public static SelectBinNotRefillMedFragment newInstance() {
        return new SelectBinNotRefillMedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_meds_no_refill, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
    }

    /*
     * leaving the refill feature
     */
    public void onClickCancel() {
        gotoFragment(RefillMedication.RefillMedicationEnum.ManageMeds.toString(), null);
        ((MainActivity)getActivity()).exitFeature();
    }

    public void onClickRemoveExist() {
        RefillMedication model = (RefillMedication)((MainActivity) getActivity()).getFeatureModel();
        ((MainActivity) getActivity()).enterFeature(RemoveMedication.TAG);

        Bundle bundle = new Bundle();
        bundle.putSerializable("user", model.getUser());
        bundle.putSerializable("medication", model.getMedication());
        gotoFragment(RemoveMedication.RemoveMedicationEnum.ConfirmRemoveMedication.toString(), bundle);
    }

    private void gotoFragment(String fragmentName,
                              Bundle bundle) {
        if(null == bundle)
            bundle = new Bundle();

        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}
