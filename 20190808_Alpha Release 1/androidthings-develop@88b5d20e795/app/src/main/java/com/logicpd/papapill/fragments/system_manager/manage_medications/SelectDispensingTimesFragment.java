package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.DispenseTimesAdapter;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectDispensingTimesFragment extends BaseHomeFragment {

    public static final String TAG = "SelectDispensingTimesFragment";

    LinearLayout contentLayout;
    UserEntity user;
    TextView tvTitle, tvEmpty;
    Button btnAdd, btnNext;
    DispenseTimesAdapter adapter;
    List<DispenseTimeEntity> dispenseTimeList;
    private RecyclerView.LayoutManager mLayoutManager;
    RecyclerView recyclerView;
    MedicationEntity medication;
    boolean isFromSchedule, isEditMode;

    public SelectDispensingTimesFragment() {
        // Required empty public constructor
    }

    public static SelectDispensingTimesFragment newInstance() {
        return new SelectDispensingTimesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_select_dispense_times, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
            if (bundle.containsKey("isEditMode")) {
                isEditMode = bundle.getBoolean("isEditMode");
            }
        }

        dispenseTimeList = model.getDispenseTimes(false);

        // go through dispenseTimeList and check only the one(s) associated with that scheduleitem
        for (DispenseTimeEntity dispenseTime : dispenseTimeList) {
            dispenseTime.setActive(false);
            ScheduleEntity scheduleItem = model.getScheduleItem(user, medication, dispenseTime.getId());
            if (scheduleItem != null) {
                dispenseTime.setActive(true);
            }
        }

        //sort list by time
        Collections.sort(dispenseTimeList, new Comparator<DispenseTimeEntity>() {
            DateFormat f = new SimpleDateFormat("h:mm a", Locale.getDefault());

            @Override
            public int compare(DispenseTimeEntity o1, DispenseTimeEntity o2) {
                try {
                    return f.parse(o1.getDispenseTime()).compareTo(f.parse(o2.getDispenseTime()));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        adapter = new DispenseTimesAdapter(getActivity(), dispenseTimeList, true, false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new DispenseTimesAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
               /* DispenseTime dispenseTime = dispenseTimeList.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("user", user);
                bundle.putSerializable("dispensetime", dispenseTime);
                bundle.putString("fragmentName", "");
                mListener.onButtonClicked(bundle);*/
                adapter.toggleChecked(position);

                isEditMode = true;  // changed occured -- persist in EnterDailyQuantityPerDoseFragment
            }
        });

        if (dispenseTimeList.size() == 0) {
            tvEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvTitle = view.findViewById(R.id.textview_title);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.recyclerview_dispenseTimes_list);
        btnAdd = view.findViewById(R.id.button_add_dispenseTime);
        btnAdd.setOnClickListener(this);
        tvEmpty = view.findViewById(R.id.textview_add_dispenseTime);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnAdd) {
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putString("fragmentName", "EditDispenseTimesFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnNext) {
            // update db with current list
            List<DispenseTimeEntity> dispenseTimes = adapter.getListFromAdapter();
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            for (DispenseTimeEntity dispenseTime : dispenseTimes) {
                model.updateDispenseTime(dispenseTime);
            }
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putBoolean("isEditMode", isEditMode);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putString("fragmentName", "DirectionsQuantityPerDoseFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}
