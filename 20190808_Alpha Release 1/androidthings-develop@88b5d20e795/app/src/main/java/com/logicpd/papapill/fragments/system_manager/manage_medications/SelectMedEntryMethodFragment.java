package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectMedEntryMethodFragment extends BaseHomeFragment {

    public static final String TAG = "SelectMedEntryMethodFragment";

    private UserEntity user;
    private Button btnBarcode, btnManual;

    public SelectMedEntryMethodFragment() {
        // Required empty public constructor
    }

    public static SelectMedEntryMethodFragment newInstance() {
        return new SelectMedEntryMethodFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selectmedentrymethod, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("user")) {
                user = (UserEntity) bundle.getSerializable("user");
            }
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnBarcode = view.findViewById(R.id.button_barcode);
        btnBarcode.setOnClickListener(this);
        btnManual = view.findViewById(R.id.button_manual);
        btnManual.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnBarcode) {
            bundle.putString("fragmentName", "ScanBarCodeFragment");
            bundle.putSerializable("user", user);
        }

        if (v == btnManual) {
            bundle.putString("fragmentName", "ImportantMessageFragment");
            bundle.putSerializable("user", user);
        }
        mListener.onButtonClicked(bundle);
    }
}