package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.device.tinyg.commands.CommandRotateCarousel;
import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.MedLocaEnum;
import com.logicpd.papapill.enums.SelectMedLocationUIState;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.util.ArrayList;
import java.util.List;

/**
 * SelectMedLocationFragment
 *
 * @author alankilloren
 */
public class SelectMedLocationFragment extends BaseHomeFragment  {

    public static final String TAG = "SelectMedLocationFragment";
    private Button btnBin, btnFridge, btnDrawer, btnOther;
    private UserEntity user;
    private MedicationEntity medication;
    private boolean isFromSchedule, isFromRefill;
    private int binLocation;
    private int mNumOfBins;
    private final int BIN_NOT_AVAILABLE = -1;
    private SelectMedLocationUIState mState;

    public SelectMedLocationFragment() {
        // Required empty public constructor

    }

    public static SelectMedLocationFragment newInstance() {
        return new SelectMedLocationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNumOfBins = getResources().getInteger(R.integer.numOfBins);
    }

    @Override
    public void onResume() {
        super.onResume();
        mState = SelectMedLocationUIState.UNDECIDED;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_select_med_location, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable(BundleEnums.user.toString());
            medication = (MedicationEntity) bundle.getSerializable(BundleEnums.medication.toString());
            if (bundle.containsKey(BundleEnums.isFromSchedule.toString())) {
                isFromSchedule = bundle.getBoolean(BundleEnums.isFromSchedule.toString());
            }
            if (bundle.containsKey(BundleEnums.isFromRefill.toString())) {
                isFromRefill = bundle.getBoolean(BundleEnums.isFromRefill.toString());
            }
        }
    }

    /*
     * Is there at least 1 bin for new medication ?
     * - can't rely on medication table row count because
     * there might be medication(s) in drawer or fridge.
     * @return: true if at least 1 bin available.
     */
    protected boolean hasBin4NewMed() {
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        List<MedicationEntity> medicationList = model.getMedications();
        int NUM_OF_BINS = this.getResources().getInteger(R.integer.numOfBins);

        // for sure there is at least 1 bin
        if (medicationList.size() < NUM_OF_BINS)
            return true;

        // else -> check if any medication row(s) are drawer or fridge or other
        int binCount = 0;
        for (MedicationEntity med : medicationList) {
            // index for drawer(97), fridge(98), other(99)
            if (med.getMedicationLocation() <= NUM_OF_BINS)
                binCount++;

            // bin are filled ?
            if(binCount >= NUM_OF_BINS)
            {
                return false;
            }
        }
        return true;
    }

    @Override
    protected  void setupViews(View view) {
        super.setupViews(view);

        btnBin = view.findViewById(R.id.button_bin);

        if(!hasBin4NewMed()) {
            TextUtils.disableButton(btnBin);
        }
        btnBin.setOnClickListener(this);
        btnDrawer = view.findViewById(R.id.button_drawer);
        btnDrawer.setOnClickListener(this);
        btnFridge = view.findViewById(R.id.button_fridge);
        btnFridge.setOnClickListener(this);
        btnOther = view.findViewById(R.id.button_other);
        btnOther.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle;

        if (v == btnBin) {
            mState = SelectMedLocationUIState.SELECTED_BIN;
            binLocation = getNextAvailableBin();
            if (BIN_NOT_AVAILABLE == binLocation || binLocation > mNumOfBins) {
                /*
                 * out of bin error !
                 */
                Log.e(TAG, "invalid bin location:" + binLocation);
                return;
            }

            medication.setMedicationLocation(binLocation);
            medication.setMedicationLocationName(MedLocaEnum.BIN.toString());
            Bundle b = createBundle(user, medication, isFromSchedule, isFromRefill, "RemoveBinFragment");
            mListener.onButtonClicked(b);
        }
        if (v == btnDrawer) {
            mState = SelectMedLocationUIState.SELECTED_DRAWER;
            int drawerLocation = this.getResources().getInteger(R.integer.drawerLocation);
            medication.setMedicationLocation(drawerLocation);//making this something separate from bin #s
            medication.setMedicationLocationName(MedLocaEnum.DRAWER.toString());
            bundle = createBundle(user, medication, isFromSchedule, isFromRefill, "PlaceMedDrawerFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnFridge) {
            mState = SelectMedLocationUIState.SELECTED_FRIDGE;
            int frdgeLocation = this.getResources().getInteger(R.integer.refridgeratorLocation);
            medication.setMedicationLocation(frdgeLocation);//making this something separate from bin #s
            medication.setMedicationLocationName(MedLocaEnum.REFRIGERATOR.toString());
            bundle = createBundle(user, medication, isFromSchedule, isFromRefill, "PlaceMedRefrigeratorFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnOther) {
            mState = SelectMedLocationUIState.SELECTED_OTHER;
            int otherLocation = this.getResources().getInteger(R.integer.otherLocation);
            medication.setMedicationLocation(otherLocation);//making this something separate from bin #s
            medication.setMedicationLocationName(MedLocaEnum.OTHER.toString());
            bundle = createBundle(user, medication, isFromSchedule, isFromRefill, "OtherLocationNameFragment");
            mListener.onButtonClicked(bundle);
        }
    }

    private Bundle createBundle(UserEntity user,
                                MedicationEntity medication,
                                boolean isFromSchedule,
                                boolean isFromRefill,
                                String fragmentname) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(BundleEnums.user.toString(), user);
        bundle.putSerializable(BundleEnums.medication.toString(), medication);
        bundle.putBoolean(BundleEnums.isFromSchedule.toString(), isFromSchedule);
        bundle.putString(BundleEnums.fragmentName.toString(), fragmentname);
        return bundle;
    }

    /**
     * Go through medications and find first empty bin
     * - no smarts, no rules, no algorithm
     * BigO performance of logN
     *
     * @return Integer of bin #
     */
    private int getNextAvailableBin() {
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        List<MedicationEntity> medications = model.getMedicationsByBin();
        List<Integer> bins = new ArrayList<Integer>();
        int NUM_OF_BINS = this.getResources().getInteger(R.integer.numOfBins);

        for (int j = 1; j <= NUM_OF_BINS; j++) {
            int availableBin = j;

            for (MedicationEntity medication : medications) {
                if (medication.getMedicationLocation() == j) {
                    availableBin = BIN_NOT_AVAILABLE;
                    break;
                }
            }

            if (BIN_NOT_AVAILABLE != availableBin)
                return availableBin;
        }
        // none available -- should never happen ?
        return BIN_NOT_AVAILABLE;
    }
}