package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

/**
 * SelectMedScheduleFragment
 *
 * @author alankilloren
 */
public class SelectMedScheduleFragment extends BaseHomeFragment {

    public static final String TAG = "SelectMedScheduleFragment";

    private Button btnScheduled, btnAsNeeded, btnBoth;
    private UserEntity user;
    private MedicationEntity medication;

    public SelectMedScheduleFragment() {
        // Required empty public constructor
    }

    public static SelectMedScheduleFragment newInstance() {
        return new SelectMedScheduleFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_select_med_sched, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnAsNeeded = view.findViewById(R.id.button_as_needed);
        btnAsNeeded.setOnClickListener(this);
        btnBoth = view.findViewById(R.id.button_both);
        btnBoth.setOnClickListener(this);
        btnScheduled = view.findViewById(R.id.button_as_scheduled);
        btnScheduled.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        if (v == btnAsNeeded) {
            persistMedication(MedScheduleTypeEnum.AS_NEEDED);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putString(BundleEnums.fragmentName.toString(), "MaximumUnitsFragment");
        }
        else if (v == btnBoth) {
            persistMedication(MedScheduleTypeEnum.BOTH);
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromSchedule", true);
            bundle.putSerializable("medication", medication);
            bundle.putString(BundleEnums.fragmentName.toString(), "MaximumUnitsFragment");
            bundle.putString(BundleEnums.nextFragmentName.toString(), "SelectDispensingTimesFragment");
        }
        else if (v == btnScheduled) {
            persistMedication(MedScheduleTypeEnum.SCHEDULED);
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromSchedule", true);
            bundle.putSerializable("medication", medication);
            bundle.putString(BundleEnums.fragmentName.toString(), "SelectDispensingTimesFragment");
            mListener.onButtonClicked(bundle);
        }
        mListener.onButtonClicked(bundle);
    }
    private void persistMedication(MedScheduleTypeEnum medicationScheduleType)
    {
        medication.setMedicationScheduleType(medicationScheduleType.value);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

        if(medication.getId()>0) {
            model.updateMedication(medication);
        }
        else {
            model.addMedication(medication);
        }
    }
}
