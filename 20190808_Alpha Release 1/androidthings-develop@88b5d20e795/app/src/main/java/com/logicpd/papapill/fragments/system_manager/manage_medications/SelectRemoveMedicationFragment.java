package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.MedicationsAdapter;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * SelectRemoveMedicationFragment
 *
 * @author alankilloren
 */
public class SelectRemoveMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "SelectRemoveMedicationFragment";

    private UserEntity user;
    private TextView tvTitle, tvEmpty;
    private Button btnNext;
    private List<MedicationEntity> medicationList;
    private RecyclerView recyclerView;
    private boolean isFromUserDelete;

    public SelectRemoveMedicationFragment() {
        // Required empty public constructor
    }

    public static SelectRemoveMedicationFragment newInstance() {
        return new SelectRemoveMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
        }
        String s = "SELECT A MEDICATION TO REMOVE";
        tvTitle.setText(s);

        medicationList = model.getMedicationsForUser(user);
        //sort list by bin#, etc.
        Collections.sort(medicationList, new Comparator<MedicationEntity>() {
            @Override
            public int compare(MedicationEntity o1, MedicationEntity o2) {
                return o1.getMedicationLocation() - o2.getMedicationLocation();
            }
        });
        MedicationsAdapter adapter = new MedicationsAdapter(getActivity(), medicationList, false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new MedicationsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                MedicationEntity medication = medicationList.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("user", user);
                bundle.putSerializable("medication", medication);
                bundle.putBoolean("isFromUserDelete", isFromUserDelete);
                bundle.putString("fragmentName", "ConfirmRemoveMedicationFragment");
                mListener.onButtonClicked(bundle);
            }
        });

        if (medicationList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
            btnNext.setText("DONE");
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvTitle = view.findViewById(R.id.textview_title);
        tvEmpty = view.findViewById(R.id.textview_empty);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        btnNext.setVisibility(View.GONE);
        recyclerView = view.findViewById(R.id.recyclerview_list);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            bundle.putString("removeAllFragmentsUpToCurrent", "ManageMedsFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
