package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.workflows.AddNewUser;

public class AddUserFragment extends BaseHomeFragment {

    public static final String TAG = "AddUserFragment";

    private Button btnNext;
    private EditText etUsername;
    private UserEntity user;

    public AddUserFragment() {
        // Required empty public constructor
    }

    public static AddUserFragment newInstance() {
        return new AddUserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_add_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            if (user != null)
                etUsername.setText(user.getUserName());
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        //TextUtils.disableButton(btnNext);
        etUsername = view.findViewById(R.id.edittext_add_user);
        etUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.d(AppConstants.TAG, "in beforeTextChanged()");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.i(AppConstants.TAG, "in onTextChanged() - " + start);
/*                if (start > 0) {
                    TextUtils.enableButton(btnNext);
                } else {
                    TextUtils.disableButton(btnNext);
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.d(AppConstants.TAG, "in afterTextChanged()");
            }
        });
        etUsername.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etUsername.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            String fragmentName;
            if (etUsername.getText().toString().length() > 1 &&
                    etUsername.getText().toString().length() < 11) {
                if (user == null) {
                    user = new UserEntity();
                }
                user.setUserName(etUsername.getText().toString());
                bundle.putBoolean("isFromAddNewUser", true);
                fragmentName=AddNewUser.AddNewUserEnum.AddUserPin.toString();
            }
            else {
                // invalid nickname
                fragmentName=AddNewUser.AddNewUserEnum.InvalidNickname.toString();
            }
            bundle.putString("fragmentName", fragmentName);
            bundle.putSerializable("user", user);
            mListener.onButtonClicked(bundle);

            etUsername.setText("");
        }
    }
}
