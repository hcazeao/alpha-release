package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.NotificationSettingRepository;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DeleteContact;

public class ConfirmDeleteContactFragment extends BaseHomeFragment {
    public static final String TAG = "ConfirmDeleteContactFragment";

    private Button btnCancel, btnDelete;

    private ContactEntity contact;

    public ConfirmDeleteContactFragment() {
        // Required empty public constructor
    }

    public static ConfirmDeleteContactFragment newInstance() {
        return new ConfirmDeleteContactFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_confirm_delete_contact, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            contact = (ContactEntity) bundle.getSerializable("contact");
        }

        if(null==contact){
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            contact = (ContactEntity) model.getMapItem("contact");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnDelete = view.findViewById(R.id.button_delete_contact);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnDelete) {
            // delete contact & notification settings
            boolean returnVal = new ContactRepository().delete(contact);
            if (returnVal) {
                new NotificationSettingRepository().deleteByContactId(contact.getId());
                bundle.putString("fragmentName", DeleteContact.DeleteContactEnum.ContactDeleted.toString());
                mListener.onButtonClicked(bundle);
            }
            else {
                // problem deleting contact
                TextUtils.showToast(getActivity(), "Problem deleting contact");
            }
        }
    }
}
