package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;

public class ConfirmDeleteMedsFragment extends BaseHomeFragment {
    public static final String TAG = "ConfirmDeleteMedsFragment";
    private Button btnCancel, btnNext;
    private UserEntity user;
    private boolean isFromUserDelete;

    public ConfirmDeleteMedsFragment() {
        // Required empty public constructor
    }

    public static ConfirmDeleteMedsFragment newInstance() {
        return new ConfirmDeleteMedsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_confirm_delete_meds, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnNext) {
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromUserDelete", isFromUserDelete);
            bundle.putString("fragmentName", "SelectRemoveMedicationFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}
