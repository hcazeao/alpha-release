package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.utils.TextUtils;

public class ConfirmDeleteUserFragment extends BaseHomeFragment {
    public static final String TAG="ConfirmDeleteUserFragment";
    private Button btnCancel, btnDelete;
    private String username;
    private TextView customText;
    private UserEntity user;

    public ConfirmDeleteUserFragment() {
        // Required empty public constructor
    }

    public static ConfirmDeleteUserFragment newInstance() {
        return new ConfirmDeleteUserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_confirm_delete_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            username = user.getUserName();
            customText.setText(getResources().getString(R.string.confirm_delete_custom_text) + " " + username + "\n\n"
                    + getResources().getString(R.string.confirm_delete_custom_text2));
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnDelete = view.findViewById(R.id.button_delete_user);
        btnDelete.setOnClickListener(this);
        customText = view.findViewById(R.id.textview_confirm_delete_custom_text);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnDelete) {
            boolean returnVal = new UserRepository().delete(user);
            if (returnVal) {
                //delete contacts for user
                new ContactRepository().deleteByUserId(user.getId());
                bundle.putString("fragmentName", "UserDeletedFragment");
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "A problem occurred while deleting this user");
            }
        }
    }
}
