package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.wireframes.workflows.DeleteContact;
import com.logicpd.papapill.wireframes.workflows.ManageContacts;

public class ContactDeletedFragment extends BaseHomeFragment {
    public static final String TAG = "ContactDeletedFragment";
    private Button btnDone;

    public ContactDeletedFragment() {
        // Required empty public constructor
    }

    public static ContactDeletedFragment newInstance() {
        return new ContactDeletedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_deleted, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            //go home
            homeButton.performClick();
        }

        /*
         * OK to call too many times, we are going home.
         */
        ((MainActivity)getActivity()).exitFeature(DeleteContact.TAG); // leave DeleteContact -- possibly
        ((MainActivity)getActivity()).exitFeature(ManageContacts.TAG); // leave ManageContacts
        mListener.onButtonClicked(bundle);
    }
}