package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.ContactsAdapter;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.workflows.AddNewContact;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.ManageContacts;

import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class ContactListFragment extends BaseHomeFragment {

    public static final String TAG = "ContactListFragment";

    private LinearLayout contentLayout;
    private Button btnNewContact, btnEdit;
    private RecyclerView recyclerView;
    private List<ContactEntity> contactList;
    private ContactEntity selectedContact;

    private UserEntity user;
    private TextView emptyText, tvTitle;
    private ContactsAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;

    public ContactListFragment() {
        // Required empty public constructor
    }

    public static ContactListFragment newInstance() {
        return new ContactListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromAddNewUser")) {
                if (bundle.getBoolean("isFromAddNewUser")) {
                    isFromAddNewUser = true;
                }
            }
            isFromNotifications = bundle.getBoolean("isFromNotifications");
        }

        //tvTitle.setText("SELECT CONTACT TO VIEW/EDIT");

        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

        contactList = model.getContacts();
        adapter = new ContactsAdapter(getActivity(), contactList, false, true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);
        if (contactList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText(getString(R.string.press_new_contact_to_add_a_contact));
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
        }

        adapter.setOnItemClickListener(new ContactsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                clearAllExcept(position);
                selectedContact = contactList.get(position);
                TextUtils.enableButton(btnEdit);
            }
        });

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    private void clearAllExcept(int position) {
        int i = 0;
        for (ContactEntity contact : contactList) {
            boolean isSelected = (position==i++)?true:false;
            contact.setSelected(isSelected);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        contentLayout = view.findViewById(R.id.layout_content);
        btnNewContact = view.findViewById(R.id.button_new_contact);
        btnNewContact.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_view_edit);
        btnEdit.setOnClickListener(this);
        TextUtils.disableButton(btnEdit);

        recyclerView = view.findViewById(R.id.recyclerview_contact_list);
        emptyText = view.findViewById(R.id.textview_add_contact);

        //tvTitle=view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnNewContact) {
            onClickNewContact();
        }

        else if (v==btnEdit){
            onClickViewEdit();
        }
    }

    private void onClickNewContact() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
        bundle.putBoolean("isFromNotifications", isFromNotifications);
        bundle.putString("fragmentName", AddNewContact.AddNewContactEnum.ContactName.toString());
        mListener.onButtonClicked(bundle);
    }

    private void onClickViewEdit() {
        //go to VerifyContactInfoFragment
        Bundle bundle = new Bundle();
        bundle.putString("fragmentName", ManageContacts.ManageContactsEnum.ContactInfo.toString());
        bundle.putBoolean("isEditMode", true);
        bundle.putSerializable("contact", selectedContact);
        mListener.onButtonClicked(bundle);
    }
}
