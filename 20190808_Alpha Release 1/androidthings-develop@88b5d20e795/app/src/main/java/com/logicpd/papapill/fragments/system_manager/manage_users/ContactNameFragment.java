package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.utils.TextUtils;

public class ContactNameFragment extends BaseHomeFragment {

    public static final String TAG = "ContactNameFragment";

    private Button btnNext;
    private EditText etContactName;
    private UserEntity user;
    private ContactEntity contact;
    private boolean isEditMode = false;
    private TextView tvTitle;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private boolean isFromChangePIN = false;

    public ContactNameFragment() {
        // Required empty public constructor
    }

    public static ContactNameFragment newInstance() {
        return new ContactNameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_name, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isEditMode")) {
                if (bundle.getBoolean("isEditMode")) {
                    isEditMode = true;
                }
            }
            contact = (ContactEntity) bundle.getSerializable("contact");
            if (contact != null) {
                etContactName.setText(contact.getName());
            }
            tvTitle.setText("ENTER CONTACT'S FULL NAME");
            isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            isFromNotifications = bundle.getBoolean("isFromNotifications");
            isFromChangePIN = bundle.getBoolean("isFromChangePIN");

            // user should be null
            user = (UserEntity) bundle.getSerializable("user");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etContactName = view.findViewById(R.id.edittext_contact_name);
        tvTitle = view.findViewById(R.id.textview_title);
        etContactName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etContactName.getText().length() > 2 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etContactName.getText().toString().length() > 2) {
                bundle.putBoolean("isFromChangePIN", isFromChangePIN);
                bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
                bundle.putBoolean("isFromNotifications", isFromNotifications);
                bundle.putString("fragmentName", "NotificationPreferencesFragment");

                // user should be null
                bundle.putSerializable("user", user);
                if (isEditMode) {
                    bundle.putBoolean("isEditMode", isEditMode);
                    contact.setName(etContactName.getText().toString());
                    bundle.putSerializable("contact", contact);
                    mListener.onButtonClicked(bundle);
                } else {
                    //add new
                    // remove userId in beta, should retrieve from join of contact - notification setting - user
                    ContactEntity contact = new ContactEntity();
                    // user should be null
                    if(null!=user) {
                        contact.setUserId(user.getId());
                    }
                    contact.setName(etContactName.getText().toString());
                    bundle.putSerializable("contact", contact);
                    mListener.onButtonClicked(bundle);
                }
            } else {
                TextUtils.showToast(getActivity(), "Please enter a valid name");
            }


        }
    }
}
