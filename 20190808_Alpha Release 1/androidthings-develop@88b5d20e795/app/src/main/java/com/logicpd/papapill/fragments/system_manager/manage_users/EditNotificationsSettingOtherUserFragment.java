package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentManageUsersEditForOtherUserBinding;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DeleteContact;

public class EditNotificationsSettingOtherUserFragment extends BaseHomeFragment {
    public static final String TAG = "EditNotificationsSettingOtherUserFragment";
    private FragmentManageUsersEditForOtherUserBinding mBinding;

    public EditNotificationsSettingOtherUserFragment() {}

    public EditNotificationsSettingOtherUserFragment newInstance() { return new EditNotificationsSettingOtherUserFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_users_edit_for_other_user, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
    }

    /*
     * go straight to Confirm Delete Contact
     */
    public void onClickSkip() {
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        Bundle bundle = new Bundle();
        String fragmentName = model.getNextFragmentName(TAG);
        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }

    /*
     * 1. select the next user
     * 2. goto Edit notification settings
     */
    public void onClickEdit() {
        DeleteContact model = (DeleteContact) ((MainActivity)getActivity()).getFeatureModel();
        UserEntity user = model.getNextUser();
        if(null!=user) {
            model.addMapItem("user", user);
            Bundle bundle = new Bundle();
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", EditNotificationSettingsFragment.class.getSimpleName());
            mListener.onButtonClicked(bundle);
        }
    }
}
