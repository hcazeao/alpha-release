package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

public class EditUserPinFragment extends BaseHomeFragment {
    public static final String TAG = "EditUserPinFragment";
    private LinearLayout contentLayout;
    private Button btnNext;
    private EditText etUserPIN;
    private UserEntity user;

    public EditUserPinFragment() {
        // Required empty public constructor
    }

    public static EditUserPinFragment newInstance() {
        return new EditUserPinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_add_user_pin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            etUserPIN.setText(user.getPin());
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etUserPIN = view.findViewById(R.id.edittext_add_user_pin);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etUserPIN.getText().toString().length() == 4) {
                user.setPin(etUserPIN.getText().toString());
                BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
                model.updateUser(user);
                bundle.putSerializable("user", user);
                /*bundle.putString("fragmentName", "VerifyUserInfoFragment");
                mListener.onButtonClicked(bundle);*/
                backButton.performClick();
            } else {
                TextUtils.showToast(getActivity(), "Your PIN must be 4 numbers");
            }
        }
    }
}
