package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.adapters.ContactsAdapter;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;

import java.util.ArrayList;
import java.util.List;

public class NotificationContactsFragment extends BaseHomeFragment  {

    public static final String TAG = "NotificationContactsFragment";

    private Button btnNewContact, btnDone;
    private RecyclerView recyclerView;
    private List<ContactEntity> contactList;
    private List<ContactEntity> selectedContactList;

    private UserEntity user;
    private ContactsAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView emptyText;
    private boolean isFromAddNewUser = false;

    public NotificationContactsFragment() {
        // Required empty public constructor
    }

    public static NotificationContactsFragment newInstance() {
        return new NotificationContactsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_notification_contacts, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            if (bundle.containsKey("isFromAddNewUser")) {
                isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            }
        }
        if (contactList == null) {
            contactList = new ArrayList<>();
        }
        contactList.clear();
        List<ContactEntity> contacts = (List<ContactEntity>)new ContactRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        contactList.addAll(contacts);
        adapter = new ContactsAdapter(getActivity(), contactList, true, false);
        adapter.setOnItemClickListener(new ContactsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                //open ContactCategoryFragment when list item tapped on
                ContactEntity contact = contactList.get(position);
                bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
                bundle.putSerializable("contact", contact);
                bundle.putSerializable("user", user);
                bundle.putString("fragmentName", "ContactCategoryFragment");
                mListener.onButtonClicked(bundle);
            }
        });
        //}
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        if (contactList == null || contactList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText("Press NEW CONTACT to add contact");
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnNewContact = view.findViewById(R.id.button_new_contact);
        btnNewContact.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recyclerview_notification_list);
        emptyText = view.findViewById(R.id.textview_add_contact);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            if (isFromAddNewUser) {
                bundle.putString("fragmentName", "NewUserSetupCompleteFragment");
                mListener.onButtonClicked(bundle);
            } else {
                bundle.putString("removeAllFragmentsUpToCurrent", "ManageUsersFragment");
                mListener.onButtonClicked(bundle);
            }
        }
        if (v == btnNewContact) {
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putBoolean("isFromNotifications", true);
            bundle.putString("fragmentName", "ContactNameFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}
