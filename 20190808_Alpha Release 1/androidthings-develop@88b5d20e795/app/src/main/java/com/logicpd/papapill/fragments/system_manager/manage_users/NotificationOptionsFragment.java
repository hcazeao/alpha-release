package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;

public class NotificationOptionsFragment extends BaseHomeFragment {
    public static final String TAG = "NotificationOptionsFragment";
    private String username, pin;

    public NotificationOptionsFragment() {
        // Required empty public constructor
    }

    public static NotificationOptionsFragment newInstance() {
        return new NotificationOptionsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_notification_options, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            username = bundle.getString("username");
            pin = bundle.getString("userpin");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

    }
}
