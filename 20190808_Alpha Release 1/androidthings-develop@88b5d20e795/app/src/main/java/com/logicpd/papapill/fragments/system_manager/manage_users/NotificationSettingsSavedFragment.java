package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DeleteContact;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class NotificationSettingsSavedFragment extends BaseHomeFragment {

    public static final String TAG = "NotificationSettingsSavedFragment";

    private Button btnDone, btnAnother;
    private UserEntity user;
    private boolean isFromAddNewUser = false;
    private BaseWireframe mModel;

    public NotificationSettingsSavedFragment() {
        // Required empty public constructor
    }

    public static NotificationSettingsSavedFragment newInstance() {
        return new NotificationSettingsSavedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_notification_settings_saved, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            if (bundle.containsKey("isFromAddNewUser")) {
                isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            }
        }

        // don't show button in DeleteContact
        if(isWorkflowDeleteContact()) {
            btnAnother.setVisibility(View.GONE);
            btnDone.setText(getString(R.string.next));

            String str = String.format("%s FOR %s",
                                getString(R.string.notification_settings_for_this_contact_),
                                user.getUserName());

            TextView subTitle = view.findViewById(R.id.txt_sub_title);
            subTitle.setText(str);
        }
    }

    private boolean isWorkflowDeleteContact() {
        mModel = ((MainActivity)getActivity()).getFeatureModel();
        if(mModel.getClass().getSimpleName().equals(DeleteContact.TAG)) {
            return true;
        }
        return false;
    }


    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        btnAnother = view.findViewById(R.id.button_another);
        btnAnother.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            //DeleteContact workflow
            if(isWorkflowDeleteContact()) {
                bundle.putSerializable("user", user);
                String fragmentName = mModel.getNextFragmentName(TAG);
                bundle.putString("fragmentName", fragmentName);
            }
            else if (isFromAddNewUser) {
                bundle.putSerializable("user", user);
                bundle.putString("fragmentName", "NewUserSetupCompleteFragment");
            } else {
                bundle.putString("fragmentName", "Home");
            }
        }
        if (v == btnAnother) {
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putString("removeAllFragmentsUpToCurrent", "NotificationContactsFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
