package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.adapters.ContactsAdapter;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;

import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectContactEditNotificationFragment extends BaseHomeFragment {

    public static final String TAG = "SelectContactEditNotificationFragment";

    private Button btnNext;
    private RecyclerView recyclerView;
    private List<ContactEntity> contactList;
    private List<ContactEntity> selectedContactList;
    private UserEntity user;
    private TextView emptyText, tvTitle;
    private ContactsAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public SelectContactEditNotificationFragment() {
        // Required empty public constructor
    }

    public static SelectContactEditNotificationFragment newInstance() {
        return new SelectContactEditNotificationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_select_contact_edit_notification_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }

        contactList = (List<ContactEntity>)new ContactRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        adapter = new ContactsAdapter(getActivity(), contactList, true, false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);
        if (contactList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText("Press NEW CONTACT to add a contact");
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
        }

        adapter.setOnItemClickListener(new ContactsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                //go to VerifyContactInfoFragment
                if (user != null) {
                    Log.d(AppConstants.TAG, "User " + user.getUserName() + " selected contact " + contactList.get(position).getName());
                } else {
                    Log.d(AppConstants.TAG, "Contact " + contactList.get(position).getName() + " selected");
                }
                Bundle bundle = new Bundle();
                bundle.putString("fragmentName", "EditNotificationSettingsFragment");
                bundle.putBoolean("isEditMode", true);
                bundle.putSerializable("user", user);
                bundle.putSerializable("contact", contactList.get(position));
                mListener.onButtonClicked(bundle);
            }
        });
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        btnNext.setVisibility(View.GONE);
        /*btnEdit = view.findViewById(R.id.button_edit);
        btnEdit.setOnClickListener(this);
        btnEdit.setVisibility(View.GONE);
*/
        recyclerView = view.findViewById(R.id.recyclerview_notification_list);
        emptyText = view.findViewById(R.id.textview_add_contact);

        //tvTitle=view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            //TODO - do we really need this?
        }
        mListener.onButtonClicked(bundle);
    }
}
