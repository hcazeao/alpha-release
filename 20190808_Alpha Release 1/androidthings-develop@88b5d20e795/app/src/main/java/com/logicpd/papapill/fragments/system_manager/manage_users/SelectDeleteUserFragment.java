package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.util.List;

public class SelectDeleteUserFragment extends BaseHomeFragment {
    public static final String TAG = "SelectDeleteUserFragment";

    LinearLayout contentLayout;
    Button btnUserA, btnUserB;
    List<UserEntity> userList;

    public SelectDeleteUserFragment() {
        // Required empty public constructor
    }

    public static SelectDeleteUserFragment newInstance() {
        return new SelectDeleteUserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_select_delete_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

        userList = model.getUsers();
        if (userList.size() > 0) {
            btnUserA.setText(userList.get(0).getUserName());
            if (userList.size() == 2) {
                btnUserB.setText(userList.get(1).getUserName());
            }
        }

        if (userList.size() == 1) {
            btnUserA.setVisibility(View.VISIBLE);
            btnUserB.setVisibility(View.GONE);
        } else if (userList.size() == 2) {
            btnUserA.setVisibility(View.VISIBLE);
            btnUserB.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);

        btnUserA = view.findViewById(R.id.button_select_user_a);
        btnUserA.setOnClickListener(this);
        btnUserB = view.findViewById(R.id.button_select_user_b);
        btnUserB.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

        if (v == btnUserA) {
            bundle.putSerializable("user", userList.get(0));

            if (model.getMedicationsForUser(userList.get(0)).size() > 0) {
                bundle.putBoolean("isFromUserDelete", true);
                bundle.putString("fragmentName", "ConfirmDeleteMedsFragment");
            } else {
                bundle.putString("fragmentName", "ConfirmDeleteUserFragment");
            }
        }
        if (v == btnUserB) {
            bundle.putSerializable("user", userList.get(1));
            if (model.getMedicationsForUser(userList.get(1)).size() > 0) {
                bundle.putBoolean("isFromUserDelete", true);
                bundle.putString("fragmentName", "ConfirmDeleteMedsFragment");
            } else {
                bundle.putString("fragmentName", "ConfirmDeleteUserFragment");
            }
        }
        mListener.onButtonClicked(bundle);
    }
}
