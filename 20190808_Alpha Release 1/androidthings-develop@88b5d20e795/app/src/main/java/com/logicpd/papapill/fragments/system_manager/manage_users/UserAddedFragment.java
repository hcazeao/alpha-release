package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;

public class UserAddedFragment extends BaseHomeFragment {
    public static final String TAG = "UserAddedFragment";
    private TextView tvWelcome;
    private Button btnSkip, btnNotifications;
    private UserEntity user;

    public UserAddedFragment() {
        // Required empty public constructor
    }

    public static UserAddedFragment newInstance() {
        return new UserAddedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_user_added, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        backButton.setVisibility(View.GONE);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            tvWelcome.setText("WELCOME, " + user.getUserName() + "!");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        tvWelcome = view.findViewById(R.id.textview_welcome);
        btnSkip = view.findViewById(R.id.button_skip);
        btnSkip.setOnClickListener(this);

        btnNotifications = view.findViewById(R.id.button_notification_settings);
        btnNotifications.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnSkip) {
            bundle.putString("fragmentName", "ManageUsersFragment");
        }
        if (v == btnNotifications) {
            bundle.putBoolean("isFromAddNewUser", true);
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "NotificationContactsFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}