package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

public class VerifyNicknameFragment extends BaseHomeFragment {

    public static final String TAG="VerifyNicknameFragment";
    private Button btnEdit, btnOK;
    private TextView tvUserInfo;
    private UserEntity user;

    public VerifyNicknameFragment() {
        // Required empty public constructor
    }

    public static VerifyNicknameFragment newInstance() {
        return new VerifyNicknameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_verify_nickname, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //handle bundle
            user = (UserEntity) bundle.getSerializable("user");

            String sb = null;
            if (user != null) {
                sb = "NICKNAME: " + user.getUserName();
            }
            tvUserInfo.setText(sb);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_edit);
        btnEdit.setOnClickListener(this);
        tvUserInfo = view.findViewById(R.id.textview_verify_user_info);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOK) {
            // save to db
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            model.updateUser(user);
            homeButton.performClick();
        }
        if (v == btnEdit) {
            bundle.putSerializable("user", user);
            backButton.performClick();
        }
        mListener.onButtonClicked(bundle);
    }
}
