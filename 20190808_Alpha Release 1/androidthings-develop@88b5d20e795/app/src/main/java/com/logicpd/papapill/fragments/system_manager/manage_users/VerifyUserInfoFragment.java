package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.UserRepository;

public class VerifyUserInfoFragment extends BaseHomeFragment {
    public static final String TAG="VerifyUserInfoFragment";
    private Button btnEdit, btnOK;
    private TextView tvUserInfo;
    private UserEntity user;
    private boolean isFromAddNewUser;

    public VerifyUserInfoFragment() {
        // Required empty public constructor
    }

    public static VerifyUserInfoFragment newInstance() {
        return new VerifyUserInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_verify_user_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //handle bundle
            user = (UserEntity) bundle.getSerializable("user");
            ContactEntity contact = (ContactEntity) bundle.getSerializable("contact");
            String sb = null;
            if (contact != null) {
                sb = String.format("NICKNAME: %s\nUSER PIN: %s\nPIN RECOVERY CONTACT: %s", user.getUserName(),  user.getPin(),contact.getName());
            }
            tvUserInfo.setText(sb);
            if (bundle.containsKey("isFromAddNewUser")) {
                isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_edit);
        btnEdit.setOnClickListener(this);
        tvUserInfo = view.findViewById(R.id.textview_verify_user_info);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOK) {
            // save to db
            new UserRepository().insert(user);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putString("fragmentName", "UserAddedFragment");
            bundle.putSerializable("user", user);
        }
        if (v == btnEdit) {
            bundle.putSerializable("user", user);
            bundle.putString("removeAllFragmentsUpToCurrent", "AddUserFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
