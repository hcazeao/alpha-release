package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;

/**
 * Fragment for System Manager...System Settings...Connect to Wireless Network
 *
 * @author alankilloren
 */
public class WirelessNetworkFragment extends BaseHomeFragment {
    public static final String TAG = "WirelessNetworkFragment";
    private Button btnWifi;
    private boolean isFromSetup;

    public WirelessNetworkFragment() {
        // Required empty public constructor
    }

    public static WirelessNetworkFragment newInstance() {
        return new WirelessNetworkFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_settings_wireless, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnWifi = view.findViewById(R.id.button_wifi);
        btnWifi.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnWifi) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putString("fragmentName", "SelectWifiNetworkFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
