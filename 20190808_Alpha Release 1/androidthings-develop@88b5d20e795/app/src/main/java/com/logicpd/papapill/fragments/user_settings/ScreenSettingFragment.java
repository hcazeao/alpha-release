package com.logicpd.papapill.fragments.user_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

/**
 * Fragment for User Settings...Screen Brightness
 *
 * @author alankilloren
 */
public class ScreenSettingFragment extends BaseHomeFragment {

    public static final String TAG = "ScreenSettingFragment";

    private Button btnDone;
    private SeekBar seekBar;
    private UserEntity user;

    public ScreenSettingFragment() {
        // Required empty public constructor
    }

    public static ScreenSettingFragment newInstance() {
        return new ScreenSettingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_settings_screen_brightness, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            if (user.getScreenBrightness() == 0) {
                seekBar.setProgress(128);//default
            } else {
                seekBar.setProgress(user.getScreenBrightness());
            }
        }

    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        seekBar = view.findViewById(R.id.seekbar_brightness);
        seekBar.setMax(255);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Settings.System.putInt(getActivity().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, progress);
                //TODO The above line does not work on Android Things as there is currently no support for controlling displays from the system level
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            // save value to db
            user.setScreenBrightness(seekBar.getProgress());
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            int returnVal = model.updateUser(user);
            bundle.putSerializable("user", user);
            backButton.performClick();
        }
    }
}
