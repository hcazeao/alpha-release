package com.logicpd.papapill.fragments.user_settings;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

public class TempUserPinFragment extends BaseHomeFragment {

    public static final String TAG = "TempUserPinFragment";

    private Button btnOK;
    private EditText etUserPin;
    private String authFragment;
    private int authAttempts = 1;

    UserEntity user;

    public TempUserPinFragment() {
        // Required empty public constructor
    }

    public static TempUserPinFragment newInstance() {
        return new TempUserPinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_temp_user_pin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("authFragment")) {
                authFragment = bundle.getString("authFragment");
            }
            if (bundle.containsKey("user")) {
                user = (UserEntity) bundle.getSerializable("user");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        TextUtils.disableButton(btnOK);
        etUserPin = view.findViewById(R.id.edittext_temp_user_pin);
        etUserPin.requestFocus();
        etUserPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.d(AppConstants.TAG, "in beforeTextChanged()");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.i(AppConstants.TAG, "in onTextChanged() - " + start);
                if (start > 2) {
                    TextUtils.enableButton(btnOK);
                } else {
                    TextUtils.disableButton(btnOK);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.d(AppConstants.TAG, "in afterTextChanged()");
            }
        });
        etUserPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etUserPin.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnOK.performClick();
                    handled = true;
                }
                return handled;
            }
        });

        TextUtils.showKeyboard(getActivity(), etUserPin);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = this.getArguments();

        if (v == btnOK) {
            if (etUserPin.getText().toString().length() == 4) {
                if (etUserPin.getText().toString().equals(AppConstants.DEFAULT_SYSTEM_KEY)) {
                    bundle.putBoolean("removeFragment", true);
                    bundle.putString("fragmentName", authFragment);
                    bundle.putBoolean("isFromTempUserPinFragment", true);
                    mListener.onButtonClicked(bundle);
                } else {
                    bundle.putInt("authAttempts", authAttempts);
                    if (authAttempts < 3) {
                        authAttempts += 1;
                    }
                    bundle.putString("fragmentName", "IncorrectUserPinFragment");
                    mListener.onButtonClicked(bundle);
                }
            }
            etUserPin.setText("");
        }
    }
}
