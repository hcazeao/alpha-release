package com.logicpd.papapill.fragments.user_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class UserPinChangedFragment extends BaseHomeFragment {

    public static final String TAG = "UserPinChangedFragment";

    private Button btnDone;

    public UserPinChangedFragment() {
        // Required empty public constructor
    }

    public static UserPinChangedFragment newInstance() {
        return new UserPinChangedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_user_pin_changed, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //TODO - handle passed in bundle
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            // TODO in Beta: We should be returning to the workflow that triggered the user pin reset.
            // However, since multiple sub-workflows can trigger user pin reset, we cannot simply
            // go to user settings since the fragment might not be in the back stack. For now, just
            // go back to home.
            //bundle.putString("removeAllFragmentsUpToCurrent", "UserSettingsFragment");
            bundle.putString("fragmentName", "Home");
        }
        mListener.onButtonClicked(bundle);
    }
}