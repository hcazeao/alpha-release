package com.logicpd.papapill.fragments.user_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

public class VerifyUserPinFragment extends BaseHomeFragment {
    public static final String TAG = "VerifyUserPinFragment";
    private Button btnEdit, btnOK;
    private TextView tvUserInfo;
    private UserEntity user;
    private boolean isFromAddNewUser, isFromChangePIN;

    public VerifyUserPinFragment() {
        // Required empty public constructor
    }

    public static VerifyUserPinFragment newInstance() {
        return new VerifyUserPinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_verify_pin_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //handle bundle
            user = (UserEntity) bundle.getSerializable("user");
            ContactEntity contact = (ContactEntity) bundle.getSerializable("contact");
            String sb = null;
            if (contact != null) {
                sb = "NEW USER PIN: " + user.getPin() +
                        "\nPIN RECOVERY CONTACT: " + contact.getName();
            }
            tvUserInfo.setText(sb);
            if (bundle.containsKey("isFromAddNewUser")) {
                isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            }
            if (bundle.containsKey("isFromChangePIN")) {
                isFromChangePIN = bundle.getBoolean("isFromChangePIN");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_edit);
        btnEdit.setOnClickListener(this);
        tvUserInfo = view.findViewById(R.id.textview_verify_user_info);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOK) {
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            bundle.putBoolean("isFromChangePIN", isFromChangePIN);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putString("fragmentName", "UserPinChangedFragment");
            bundle.putSerializable("user", user);
        }
        if (v == btnEdit) {
            bundle.putString("removeAllFragmentsUpToCurrent", "UserPinFragment");
            bundle.putSerializable("user", user);
        }
        mListener.onButtonClicked(bundle);
    }
}
