package com.logicpd.papapill.interfaces;

public interface IntentMsgInterface {
    public boolean handleIntent(String command,
                                String state,
                                String msg,
                                String version);
}
