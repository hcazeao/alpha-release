package com.logicpd.papapill.interfaces;

import com.logicpd.papapill.enums.CarouselPosCmdEnum;

public interface OnServiceListener {

    void startMedicationScheduleService();
    void stopMedicationScheduleService();

    void startCarouselPosService(CarouselPosCmdEnum command,
                                 int binId);
    void stopCarouselPosService();
}
