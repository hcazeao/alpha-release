package com.logicpd.papapill.receivers;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.logicpd.papapill.App;
import com.logicpd.papapill.device.enums.CommandState;
import com.logicpd.papapill.device.tinyg.commands.CommandHomeAll;
import com.logicpd.papapill.device.tinyg.commands.CommandRotateCarousel;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.IntentParamsEnum;
import com.logicpd.papapill.enums.IntentTypeEnum;
import com.logicpd.papapill.fragments.my_medication.DispenseMedsFragment;
import com.logicpd.papapill.interfaces.IntentMsgInterface;

import java.util.List;

/*
 * Inter component communication
 * This class provides a static sender and dynamic receiver for intent
 * - 1st implementation for sending intent between tinyG commands -> mainActivity for response.
 * - This class is designed to be instantiated in MainActivity specifically.
 */
public class IntentBroadcast extends BroadcastReceiver {
    public static String TAG = IntentBroadcast.class.getSimpleName();
    private android.app.FragmentManager mFragmentMgr;
    public static String INTENT_MESSAGE="msg";
    public static String INTENT_POSITION="pos";
    protected String mCommand;
    protected String mState;
    protected String mMsg;
    protected String mVersion;

    public IntentBroadcast(android.app.FragmentManager fm)
    {
        mFragmentMgr = fm;
    }

    /*
     * Send: Broadcast an intent to MainActivity->mReceiver
     * params: source class (command)
     *          state (device state)
     *          msg (success or failed or etc)
     * return: true if success
     */
    public static boolean send(IntentTypeEnum type,
                               String command,
                               String state,
                               String msg,
                               String version)
    {
        try {
            //  Log.d(TAG, String.format("Broadcasting command: %s state: %s msg: %s", command, state, msg));

            Intent intent = new Intent(type.toString());
            intent.putExtra(IntentParamsEnum.command.toString(), command);
            intent.putExtra(IntentParamsEnum.state.toString(), state);
            intent.putExtra(IntentParamsEnum.msg.toString(), msg);
            intent.putExtra(IntentParamsEnum.version.toString(), version);
            App.getContext().sendBroadcast(intent);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    /*
     * Receive: intent from TinyG command(s)
     * NOTE: async implemented ok but need UIThread
     * for CannotRetrieveMedsFragment page when updateReady()
     * params: context
     *         intent (detail in IntentParamsEnum)
     */
    @Override
    public void onReceive(Context context,
                          Intent intent) {
        final PendingResult pendingResult = goAsync();
        AsyncTask<Intent, Integer, String> asyncTask = new AsyncTask<Intent, Integer, String>() {
            @Override
            protected String doInBackground(Intent... params) {
                // parse message content
                onParse(params[0]);
                /*
                 * Pass intent messages to topMost fragment if it can handle it.
                 * Fragment must have IntentMsgInterface with method: handleIntent()
                 */
                if (null != mFragmentMgr) {
                    List<Fragment> list = mFragmentMgr.getFragments();
                    Fragment topFragment = list.get(list.size() - 1);

                    boolean isHandled = onHandleIntentMsgInterface(topFragment);

                    if (!isHandled) {
                        Log.d(TAG, "onReceive() message was not handled");
                    }
                }

                // possibly completed carousel position movement
                if(mState == CommandState.COMPLETE_STATE.toString() &&
                        mCommand == CommandRotateCarousel.class.getSimpleName()) {

                }
                pendingResult.finish();
                return "";
            }
        };
        asyncTask.execute(intent);
    }

    /*
     * Parse intent message into class object properties
     * params: intent
     */
    protected void onParse(Intent intent)
    {
        // parse message content
        mCommand = intent.getStringExtra(IntentParamsEnum.command.toString());
        mState = intent.getStringExtra(IntentParamsEnum.state.toString());
        mMsg = intent.getStringExtra(IntentParamsEnum.msg.toString());
        mVersion = intent.getStringExtra(IntentParamsEnum.version.toString());
/*        Log.d(TAG, String.format("onReceive() action:%s command: %s state: %s msg: %s",
                intent.getAction().toString(),
                mCommand,
                mState,
                mMsg));*/
    }

    /*
     * If top fragment is of type IntentMsgInterface, execute HandleIntent() method
     * param: top fragment
     * return: true if executed
     */
    protected boolean onHandleIntentMsgInterface(Fragment topFragment)
    {
        Class<?>[] interfaces = topFragment.getClass().getInterfaces();
        String interfaceName = IntentMsgInterface.class.getSimpleName();

        for (Class<?> i : interfaces) {
            String thisName = i.getSimpleName();
            // Log.d(TAG, String.format("i name: %s interface name: %s", thisName, interfaceName));
            if (thisName.equals(interfaceName)) {
                ((IntentMsgInterface) topFragment).handleIntent(mCommand, mState, mMsg, mVersion);
                return true;
            }
        }

        return false;
    }
}
