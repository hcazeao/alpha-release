package com.logicpd.papapill.room;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;

import com.logicpd.papapill.App;
import com.logicpd.papapill.BuildConfig;
import com.logicpd.papapill.enums.BuildFlavorsEnum;
import com.logicpd.papapill.room.dao.BinDao;
import com.logicpd.papapill.room.dao.ContactDao;
import com.logicpd.papapill.room.dao.DispenseEventDao;
import com.logicpd.papapill.room.dao.DispenseTimeDao;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.MedicationDao;
import com.logicpd.papapill.room.dao.ScheduleDao;
import com.logicpd.papapill.room.dao.NotificationSettingDao;
import com.logicpd.papapill.room.dao.SystemDao;
import com.logicpd.papapill.room.dao.UserDao;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.SystemEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.transform.Result;


@Database(entities = {BinEntity.class,
        UserEntity.class,
        MedicationEntity.class,
        ScheduleEntity.class,
        NotificationSettingEntity.class,
        DispenseTimeEntity.class,
        ContactEntity.class,
        SystemEntity.class,
        DispenseEventEntity.class}, version = 1, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase
{
    public abstract BinDao binDao();
    public abstract MedicationDao medicationDao();
    public abstract DispenseTimeDao dispenseTimeDao();
    public abstract UserDao userDao();
    public abstract ScheduleDao scheduleDao();
    public abstract NotificationSettingDao settingDao();
    public abstract SystemDao systemDao();
    public abstract ContactDao contactDao();
    public abstract DispenseEventDao dispenseEventDao();

    public static final String DATABASE_NAME = "PapaPillRoom.db";

    /*
     * TODO Add content provider/resolver when external app needs access
     * Singleton pattern [no content provider / resolver for external requests]
     */
    private static volatile AppDatabase INSTANCE;

    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData<>();

    public static AppDatabase getDatabase(final Context context) {

        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {

                    File file = App.getContext().getDatabasePath(DATABASE_NAME);
                    boolean exists = file.exists();

                    /*
                     * All debug + not build server will use JOURNAL MODE
                     */
                    BuildFlavorsEnum flavor = BuildFlavorsEnum.valueOf(BuildConfig.FLAVOR.toUpperCase());

                    if(BuildConfig.DEBUG &&
                            flavor!=BuildFlavorsEnum.BUILDSERVER) {
                                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                AppDatabase.class, DATABASE_NAME)
                                .allowMainThreadQueries()                   // TODO: switch to background thread in Beta
                                .setJournalMode(JournalMode.TRUNCATE)
                                .build();
                    }
                    /*
                     * All release or debug Build server runs WAL mode
                     * (Performance mode; DAO tests require it)
                     */
                    else {
                        INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                AppDatabase.class, DATABASE_NAME)
                                .allowMainThreadQueries()                   // TODO: switch to background thread in Beta
                                .build();
                    }
                    // TODO add migration here for future versions
                    if(!exists) {
                        createTables(INSTANCE);
                        addPreDefinedDispenseTimes(INSTANCE.dispenseTimeDao());
                    }

                    INSTANCE.setDatabaseCreated();
                }
            }
        }

        return INSTANCE;
    }

    /**
     * This is called during first-time run to establish pre-defined dispense times in the db
     */
    protected static void addPreDefinedDispenseTimes(DispenseTimeDao dao) {

        HashMap<String, String> times = new HashMap<>();
        times.put("EARLY MORNING", "6:00 AM");
        times.put("BREAKFAST", "7:00 AM");
        times.put("MID-MORNING", "10:00 AM");
        times.put("LUNCH", "12:00 PM");
        times.put("MID-AFTERNOON", "2:00 PM");
        times.put("LATE-AFTERNOON", "5:00 PM");
        times.put("DINNER", "6:00 PM");
        times.put("EVENING", "7:00 PM");
        times.put("BEDTIME", "9:30 PM");
        times.put("LATE-NIGHT", "11:00 PM");

        List<DispenseTimeEntity> list = new ArrayList<DispenseTimeEntity>();
        for (String key : times.keySet()) {
            DispenseTimeEntity dispenseTime = new DispenseTimeEntity();
            dispenseTime.setDispenseName(key);
            dispenseTime.setDispenseTime(times.get(key));
            list.add(dispenseTime);
        }
        dao.insertAll(list);
    }

    /*
     * Either define migration + version which explicitly defines each table
     * Or execute insert to instantiate each table.
     * Choose the 2nd option for now.
     */
    protected static boolean createTables(AppDatabase appDatabase)
    {
        UserEntity userEntity = new UserEntity();
        userEntity.setPatientName("patient");
        userEntity.setPin("pin");
        userEntity.setUserName("user");
        int userId = (int)(long)appDatabase.userDao().insert(userEntity);

        BinEntity binEntity = new BinEntity();
        appDatabase.binDao().insert(binEntity);

        ContactEntity contactEntity = new ContactEntity();
        contactEntity.setUserId(userId);
        int contactId = (int)(long)appDatabase.contactDao().insert(contactEntity);

        DispenseTimeEntity dispenseTimeEntity = new DispenseTimeEntity();
        dispenseTimeEntity.setUserId(userId);
        dispenseTimeEntity.setDispenseName("name");
        dispenseTimeEntity.setDispenseTime("time");
        int dispenseId = (int)(long)appDatabase.dispenseTimeDao().insert(dispenseTimeEntity);

        MedicationEntity medicationEntity = new MedicationEntity(userId);
        medicationEntity.setUserId(userId);
        medicationEntity.setMedicationName("name");
        int medId = (int)(long)appDatabase.medicationDao().insert(medicationEntity);

        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medId, dispenseId);
        scheduleEntity.setUserId(userId);
        scheduleEntity.setMedicationId(medId);
        scheduleEntity.setDispenseTimeId(dispenseId);
        appDatabase.scheduleDao().insert(scheduleEntity);

        NotificationSettingEntity notificationSetting = new NotificationSettingEntity(userId, contactId);
        notificationSetting.setUserId(userId);
        notificationSetting.setContactId(contactId);
        appDatabase.settingDao().insert(notificationSetting);

        SystemEntity systemEntity = new SystemEntity();
        appDatabase.systemDao().insert(systemEntity);

        deleteAll();
        return true;
    }

    /*
     * Delete All tables (drop rows)
     * Call getDatabase() before using this
     */
    public static boolean deleteAll()
    {
        AppDatabase appDatabase = INSTANCE;
        if(null==appDatabase)
            return false;

        appDatabase.userDao().deleteAll();
        appDatabase.binDao().deleteAll();
        appDatabase.contactDao().deleteAll();
        appDatabase.systemDao().deleteAll();
        appDatabase.settingDao().deleteAll();
        appDatabase.scheduleDao().deleteAll();
        appDatabase.medicationDao().deleteAll();
        appDatabase.dispenseTimeDao().deleteAll();
        return true;
    }

    /*
     * return the appropriate data access object for operation
     */
    public IBaseDao getDAO(String className)
    {
        if(className.equals(UserDao.class.getName()))
        {
            return binDao();
        }
        else if(className.equals(MedicationDao.class.getName()))
        {
            return medicationDao();
        }
        else if(className.equals(DispenseTimeDao.class.getName()))
        {
            return dispenseTimeDao();
        }
        else if(className.equals(UserDao.class.getName()))
        {
            return userDao();
        }
        else if(className.equals(ScheduleDao.class.getName()))
        {
            return scheduleDao();
        }
        else if(className.equals(SystemDao.class.getName()))
        {
            return systemDao();
        }
        else if(className.equals(ContactDao.class.getName()))
        {
            return contactDao();
        }
        else
        {
            return settingDao();
        }
    }

    public LiveData<Boolean> getDatabaseCreated() {
        return mIsDatabaseCreated;
    }

    private void setDatabaseCreated(){
        mIsDatabaseCreated.postValue(true);
    }
}
