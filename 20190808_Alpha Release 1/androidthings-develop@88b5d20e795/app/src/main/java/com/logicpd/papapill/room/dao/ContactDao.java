package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.android.gms.vision.L;
import com.logicpd.papapill.room.entities.ContactEntity;

import java.util.List;

@Dao
public interface ContactDao extends IBaseDao {

        @Query("SELECT * FROM contacts ORDER BY name ASC")
        List<ContactEntity> getAll();

        @Query("SELECT * FROM contacts WHERE id = :id")
        ContactEntity get(int id);

        @Query("SELECT * FROM contacts WHERE userId = :userId")
        List<ContactEntity> getByUserId(int userId);

        @Insert
        Long[] insertAll(List<ContactEntity> entities);

        @Insert
        Long insert(ContactEntity entity);

        @Update
        int update(ContactEntity entity);

        @Query("DELETE FROM contacts")
        int deleteAll();

        @Query("DELETE FROM contacts WHERE id = :id")
        int delete(int id);

        @Query("DELETE FROM contacts WHERE userId = :userId")
        int deleteByUserId(int userId);
}
