package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.logicpd.papapill.room.entities.DispenseTimeEntity;

import java.util.List;

@Dao
public interface DispenseTimeDao extends IBaseDao {

        @Query("SELECT * FROM dispenseTimes ORDER BY id ASC")
        List<DispenseTimeEntity> getAll();

        @Query("SELECT COUNT(*) FROM dispenseTimes WHERE dispenseTime = :dispenseTime")
        int getCountByTime(String dispenseTime);

        @Query("SELECT * FROM dispenseTimes WHERE isActive=1 ORDER BY id ASC")
        List<DispenseTimeEntity> getByActive();

        @Query("SELECT * FROM dispenseTimes WHERE id = :id")
        DispenseTimeEntity get(int id);

        @Insert
        Long[] insertAll(List<DispenseTimeEntity> entities);

        @Insert
        Long insert(DispenseTimeEntity entity);

        @Update
        int update(DispenseTimeEntity entity);

        @Query("DELETE FROM dispenseTimes")
        int deleteAll();

        @Query("DELETE FROM dispenseTimes WHERE id = :id")
        int delete(int id);

        @Query("DELETE FROM dispenseTimes WHERE dispenseName = :name")
        int delete(String name);
}
