package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.logicpd.papapill.room.entities.SystemEntity;

import java.util.List;

@Dao
public interface SystemDao extends IBaseDao {

    @Query("SELECT * from systems ORDER BY id ASC")
    List<SystemEntity> getAll();

    @Query("SELECT * from systems WHERE id = :id")
    SystemEntity get(int id);

    @Insert
    Long[] insertAll(List<SystemEntity> entities);

    @Insert
    Long insert(SystemEntity entity);

    @Update
    int update(SystemEntity entity);

    @Query("UPDATE systems SET serialnumber= :value WHERE id= :id")
    int updateSerialNumberById(int id, String value);

    @Query("UPDATE systems SET systemkey= :value WHERE id= :id")
    int updateSystemKeyById(int id, String value);

    @Query("DELETE FROM systems")
    int deleteAll();

    @Query("DELETE FROM systems WHERE id = :id")
    int delete(int id);
}
