package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import java.io.ByteArrayOutputStream;

/*
 * 1. Add a location entity to mediate between medication and bins in beta.
 * 2. Add fridge, drawer, other entities for other destinations (or change bins to a generic storage entity)
 */

/*
 * Data result from Detector in the analysis of camera image of the bin.
 */
@Entity(tableName = "bins")
public class BinEntity implements IBaseEntity {

    @PrimaryKey
    private int id;

    @Nullable
    private byte[] detectImage;
    private int pillCount;
    private int upperLeftX;
    private int upperLeftY;
    private int lowerRightX;
    private int lowerRightY;
    @Nullable
    private String pillTemplate;
    private int runAnalysis;
    private int fillLevel;      // initialize at -1, 0 - 100 (percent)

    public void setId( int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public byte[] getDetectImage() {
        return detectImage;
    }

    public void setDetectImage(byte[] detectImage) {
    }

    public void setDetectImage(Bitmap detectImage) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        detectImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
        this.detectImage = stream.toByteArray();
    }

    public int getPillCount() {
        return pillCount;
    }

    public void setPillCount(int pillCount) {
        this.pillCount = pillCount;
    }

    public int getUpperLeftX() {
        return upperLeftX;
    }

    public void setUpperLeftX(int upperLeftX) {
        this.upperLeftX = upperLeftX;
    }

    public int getUpperLeftY() {
        return upperLeftY;
    }

    public void setUpperLeftY(int upperLeftY) {
        this.upperLeftY = upperLeftY;
    }

    public int getLowerRightX() {
        return lowerRightX;
    }

    public void setLowerRightX(int lowerRightX) {
        this.lowerRightX = lowerRightX;
    }

    public int getLowerRightY() {
        return lowerRightY;
    }

    public void setLowerRightY(int lowerRightY) {
        this.lowerRightY = lowerRightY;
    }

    public String getPillTemplate() {
        return pillTemplate;
    }

    public void setPillTemplate(String pillTemplate) {
        this.pillTemplate = pillTemplate;
    }

    public int getRunAnalysis() {
        return runAnalysis;
    }

    public void setRunAnalysis(int runAnalysis) {
        this.runAnalysis = runAnalysis;
    }

    public int getFillLevel() {
        int INIT_VALUE = -1;
        int FLOOR_PERCENT = 0;
        int CEILING_PERCENT = 100;

        if(fillLevel < INIT_VALUE)
            return INIT_VALUE;

        else if(fillLevel > CEILING_PERCENT)
            return CEILING_PERCENT;

        return fillLevel;
    }

    public void setFillLevel(int fillLevel) {

        this.fillLevel = fillLevel;
    }

}