package com.logicpd.papapill.room.entities;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

// add foreign key with cascade delete from schedule
@Entity(tableName = "dispenseTimes")
        /*foreignKeys = { @ForeignKey(onDelete=CASCADE,
                entity = UserEntity.class,
                parentColumns = "id",
                childColumns = "userId")},

        indices = { @Index("userId")})*/
public class DispenseTimeEntity implements IBaseEntity, Serializable {

    @PrimaryKey(autoGenerate=true)
    private int id;
    private int userId;
    @NonNull
    private String dispenseName;
    @NonNull
    private String dispenseTime;
    private boolean isActive;
    private int dispenseAmount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id =id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDispenseName() {
        return dispenseName;
    }

    public void setDispenseName(String dispenseName) {
        this.dispenseName = dispenseName;
    }

    public String getDispenseTime() {
        return dispenseTime;
    }

    public void setDispenseTime(String dispenseTime) {
        this.dispenseTime = dispenseTime;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getDispenseAmount() {
        return dispenseAmount;
    }

    public void setDispenseAmount(int dispenseAmount) {
        this.dispenseAmount = dispenseAmount;
    }
}