package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.TypeConverters;

import com.logicpd.papapill.room.utils.TimestampConverter;

import java.io.Serializable;
import java.util.Date;

public class JoinScheduleDispense implements Serializable {

    private int scheduleId;                 // assume schedule id = dispenseTime id
    private int userId;
    private int medicationId;
    private int dispenseTimeId;
    private int dispenseAmount;
    private int recurrence;
    private int timesRecurred;

    @TypeConverters({TimestampConverter.class})
    private Date nextProcessDate;

    private String scheduleDate;
    private String scheduleDay;
    private String dispenseTime;
    private String dispenseName;
    private boolean isActive;

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int id) {
        this.scheduleId = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(int medicationId) {
        this.medicationId = medicationId;
    }

    public int getDispenseTimeId() {
        return dispenseTimeId;
    }

    public void setDispenseTimeId(int dispenseTimeId) {
        this.dispenseTimeId = dispenseTimeId;
    }

    public int getDispenseAmount() {
        return dispenseAmount;
    }

    public void setDispenseAmount(int dispenseAmount) {
        this.dispenseAmount = dispenseAmount;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int scheduleType) {
        this.recurrence = scheduleType;
    }

    public int getTimesRecurred() {
        return timesRecurred;
    }

    public void setTimesRecurred(int timesRecurred) {
        this.timesRecurred = timesRecurred;
    }

    public void incrementTimesRecurred() {
        this.timesRecurred++;
    }

    public Date getNextProcessDate() {
        return nextProcessDate;
    }

    public void setNextProcessDate(Date nextProcessDate) {
        this.nextProcessDate = nextProcessDate;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getScheduleDay() {
        return scheduleDay;
    }

    public void setScheduleDay(String scheduleDay) {
        this.scheduleDay = scheduleDay;
    }

    public String getDispenseTime() {
        return dispenseTime;
    }

    public void setDispenseTime(String dispenseTime) {
        this.dispenseTime = dispenseTime;
    }

    public String getDispenseName() {
        return dispenseName;
    }

    public void setDispenseName(String dispenseName) {
        this.dispenseName = dispenseName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public DispenseTimeEntity getDispenseTimeEntity()
    {
        DispenseTimeEntity dispenseTimeEntity = new DispenseTimeEntity();
        dispenseTimeEntity.setId(this.dispenseTimeId);
        dispenseTimeEntity.setUserId(this.userId);
        dispenseTimeEntity.setDispenseName(this.dispenseName);
        //dispenseTimeEntity.setDispenseAmount(this.dispenseAmount);
        dispenseTimeEntity.setDispenseTime(this.dispenseTime);
        dispenseTimeEntity.setActive(this.isActive);
        return dispenseTimeEntity;
    }

    public ScheduleEntity getScheduleEntity()
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(this.userId, this.medicationId, this.dispenseTimeId);
        scheduleEntity.setId(this.scheduleId);
        scheduleEntity.setScheduleDay(this.scheduleDay);
        scheduleEntity.setScheduleDate(this.scheduleDate);
        scheduleEntity.setRecurrence(this.recurrence);
        scheduleEntity.setTimesRecurred(this.timesRecurred);
        scheduleEntity.setNextProcessDate(this.nextProcessDate);
        scheduleEntity.setDispenseAmount(this.dispenseAmount);
        return scheduleEntity;
    }
}
