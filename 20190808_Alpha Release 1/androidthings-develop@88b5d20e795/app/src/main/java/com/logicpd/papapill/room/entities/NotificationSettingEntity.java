package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "settings",
        foreignKeys = { @ForeignKey(onDelete=CASCADE,
                        entity = UserEntity.class,
                        parentColumns = "id",
                        childColumns = "userId"),

                        @ForeignKey(onDelete=CASCADE,
                        entity = ContactEntity.class,
                        parentColumns = "id",
                        childColumns = "contactId")},

        indices = { @Index("userId"),
                    @Index("contactId")})

public class NotificationSettingEntity implements IBaseEntity {

    @PrimaryKey(autoGenerate=true)
    @NonNull
    private int id;
    private int userId;
    private int contactId;
    private int itemId;
    @Nullable
    private String settingName;
    private boolean isTextSelected;
    private boolean isVoiceSelected;
    private boolean isEmailSelected;

    public NotificationSettingEntity(int userId,
                                     int contactId)
    {
        this.userId = userId;
        this.contactId = contactId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public boolean isTextSelected() {
        return isTextSelected;
    }

    public void setTextSelected(boolean textSelected) {
        isTextSelected = textSelected;
    }

    public boolean isVoiceSelected() {
        return isVoiceSelected;
    }

    public void setVoiceSelected(boolean voiceSelected) {
        isVoiceSelected = voiceSelected;
    }

    public boolean isEmailSelected() {
        return isEmailSelected;
    }

    public void setEmailSelected(boolean emailSelected) {
        isEmailSelected = emailSelected;
    }

}