package com.logicpd.papapill.room.repositories;

import android.os.AsyncTask;
import android.util.Log;

import com.logicpd.papapill.App;
import com.logicpd.papapill.interfaces.OnAsyncEventListener;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;

public class BaseRepository implements IRepository {

    protected AppDatabase db;
    protected String TAG = "BaseRepository";
    protected OnAsyncEventListener listener = null;

    public BaseRepository()
    {
        db = AppDatabase.getDatabase(App.getContext());
    }

    public BaseRepository(OnAsyncEventListener listener)
    {
        db = AppDatabase.getDatabase(App.getContext());
        this.listener = listener;
    }

    /*
     * Below list the base operators for ALL tables
     * Default: INSERT_ALL,INSERT,
     *          QUERY_ALL,QUERY_BY_ID,
     *          UPDATE,
     *          DELETE,DELETE_ALL
     */
    public List<IBaseEntity> readAll()
    {
        return (List<IBaseEntity>)syncOp(CRUDEnum.QUERY_ALL, null);
    }

    public IBaseEntity read(IBaseEntity entity)
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity)entity));
        return (IBaseEntity)syncOp(CRUDEnum.QUERY_BY_ID, list);
    }

    public boolean insertAll(List<IBaseEntity> list)
    {
        asyncOp(CRUDEnum.INSERT_ALL, list);
        return true;
    }

    public boolean insert(IBaseEntity entity) {
        return insert(entity, false);
    }

    public boolean insert(IBaseEntity dispenseTime,
                           boolean isSync){
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) dispenseTime));

        if(isSync) {
            syncOp(CRUDEnum.INSERT, list);
        }
        else {
            asyncOp(CRUDEnum.INSERT, list);
        }
        return true;
    }

    public boolean deleteAll()
    {
        asyncOp(CRUDEnum.DELETE_ALL, null);
        return true;
    }

    public boolean delete(IBaseEntity entity)
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) entity));
        asyncOp(CRUDEnum.DELETE, list);
        return true;
    }

    public boolean update(IBaseEntity entity)
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) entity));
        asyncOp(CRUDEnum.UPDATE, list);
        return true;
    }

    protected class DbAsync extends AsyncTask<Object, Void, Object> {

        DbAsync(AppDatabase db) { }

        @Override
        protected Object doInBackground(final Object... params)
        {
            Object retVal = null;

            try {
                List<IBaseEntity> entities = (List<IBaseEntity>) params[0];
                CRUDEnum op = (CRUDEnum) params[1];
                retVal = crudOp(getDAO(), op, entities);
                return retVal;
                // retVal = method.invoke(INSTANCE.getDAO(className), op, (List<UserEntity>) (Object) entities);

                /*
                 * block other transaction
                 * if apply Command Query Responsibility Segregation (CQRS)
                 * I would allow concurrent read and queue for serial writes.
                 */
                //INSTANCE.beginTransaction();
                //INSTANCE.endTransaction();
            }
            catch (Exception ex)
            {
                // log cat here
                Log.e(TAG, "doInBackground:" + ex);
            }

            return retVal;
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        protected void onPostExecute(Object result)
        {
            /*
             * call back or set liveData here
             */
            if(null!=listener)
                listener.OnPostExecute(result);
        }
    }

    /*
     * Operation running on main thread
     * - this is a temporary bridge of existing queries
     */
    public Object syncOp(CRUDEnum op,
                       final List<IBaseEntity> entities) {
        try
        {
            return crudOp(getDAO(), op, entities);
        }
        catch (Exception ex)
        {
            Log.e(TAG, "syncOp:" + ex);
        }
        return null;
    }

    public IBaseDao getDAO()
    {
        return null;
    }

    /*
     * Async operation
     */
    public void asyncOp(CRUDEnum op,
                       final List<IBaseEntity> entities) {
        new BaseRepository.DbAsync(db).execute(entities, op);
    }

    /*
     * virtual template -- expect override
     *
     * Create/Read/Update/Delete operations
     * Default: INSERT_ALL,INSERT,
     *          QUERY_ALL,QUERY_BY_ID,
     *          UPDATE,
     *          DELETE,DELETE_ALL
     */
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        /*
         * virtual method to be overrided
         */
        return null;
    }
}
