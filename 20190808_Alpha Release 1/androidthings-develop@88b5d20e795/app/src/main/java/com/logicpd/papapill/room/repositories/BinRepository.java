package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.interfaces.OnAsyncEventListener;
import com.logicpd.papapill.room.dao.BinDao;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;

import java.util.ArrayList;
import java.util.List;

public class BinRepository extends BaseRepository {

    public BinRepository()
    {
        super();
    }

    public BinRepository(OnAsyncEventListener listener)
    {
        super(listener);
    }

    public boolean delete(int id)
    {
        BinEntity entity = new BinEntity();
        entity.setId(id);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        asyncOp(CRUDEnum.DELETE, list);
        return true;
    }

    public BinEntity read(int id)
    {
        BinEntity entity = new BinEntity();
        entity.setId(id);
        return (BinEntity)read(entity);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<BinEntity> list = (List<BinEntity>)(List<?>) entities;
        switch (op)
        {
            case INSERT_ALL:
                return ((BinDao)dao).insertAll(list);

            case INSERT:
                return ((BinDao)dao).insert(list.get(0));

            case QUERY_ALL:
                return ((BinDao)dao).getAll();

            case QUERY_BY_ID:
                return ((BinDao)dao).get(list.get(0).getId());

            case UPDATE:
                return ((BinDao)dao).update(list.get(0));

            case DELETE:
                return ((BinDao)dao).delete(list.get(0).getId());

            case DELETE_ALL:
                return ((BinDao)dao).deleteAll();
        }

        return false;
    }

    public IBaseDao getDAO()
    {
        return db.binDao();
    }
}
