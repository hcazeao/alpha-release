package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.dao.ContactDao;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.UserDao;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class ContactRepository extends BaseRepository{

    public ContactRepository()
    {
        super();
    }

    public ContactEntity read(int id)
    {
        ContactEntity entity = new ContactEntity();
        entity.setId(id);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (ContactEntity)syncOp(CRUDEnum.QUERY_BY_ID, list);
    }

    @Override
    public boolean delete(IBaseEntity contact){
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) contact));
        return (boolean)syncOp(CRUDEnum.DELETE, list);
    }

    public boolean deleteByUserId(int userId)
    {
        // remove userId in beta, should retrieve from join of contact - notification setting - user
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        ContactEntity contact = new ContactEntity();
        contact.setUserId(userId);
        list.add(((IBaseEntity)contact));
        asyncOp(CRUDEnum.DELETE_BY_USER_ID, list);
        return true;
    }

    public List<ContactEntity> getByUserId(int userId)
    {
        // remove userId in beta, should retrieve from join of contact - notification setting - user
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        ContactEntity contact = new ContactEntity();
        contact.setUserId(userId);
        list.add(((IBaseEntity)contact));
        return (List<ContactEntity>)syncOp(CRUDEnum.QUERY_BY_USER_ID, list);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities) {
        if(null==dao)
            return null;

        List<ContactEntity> list = (List<ContactEntity>)(List<?>) entities;
        switch (op)
        {
            case INSERT_ALL:
                Long[] ids = ((ContactDao)dao).insertAll(list);
                int i =0;
                for(ContactEntity item : list) {
                    item.setId((int)(long)ids[i++]);
                }
                return ids;

            case INSERT:
                int id = (int)(long)((ContactDao)dao).insert(list.get(0));
                list.get(0).setId(id);
                return id;

            case QUERY_ALL:
                return ((ContactDao)dao).getAll();

            case QUERY_BY_USER_ID:
                return ((ContactDao)dao).getByUserId(list.get(0).getUserId());

            case QUERY_BY_ID:
                return ((ContactDao)dao).get(list.get(0).getId());

            case UPDATE:
                ((ContactDao)dao).update(list.get(0));
                return true;

            case DELETE:
                ((ContactDao)dao).delete(list.get(0).getId());
                return true;

            case DELETE_BY_USER_ID:
                ((ContactDao)dao).deleteByUserId(list.get(0).getUserId());
                return true;

            case DELETE_ALL:
                ((ContactDao)dao).deleteAll();
                return true;
        }

        return false;
    }


    public IBaseDao getDAO()
    {
        return db.contactDao();
    }
}
