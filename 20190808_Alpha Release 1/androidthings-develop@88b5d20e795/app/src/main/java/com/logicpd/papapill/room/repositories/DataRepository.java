package com.logicpd.papapill.room.repositories;

import android.arch.lifecycle.MediatorLiveData;

import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.List;

public class DataRepository {

    private static DataRepository sInstance;

    private AppDatabase mDatabase;
    private MediatorLiveData<List<UserEntity>> mObservableUsers;
    private MediatorLiveData<List<BinEntity>> mObservableBins;
    private MediatorLiveData<List<MedicationEntity>> mObservableMedications;
    private MediatorLiveData<List<ScheduleEntity>> mObservableSchedules;
    private MediatorLiveData<List<DispenseTimeEntity>> mObservableDispenseTimes;
    private MediatorLiveData<List<NotificationSettingEntity>> mObservableSettings;

    private DataRepository(final AppDatabase database) {
        mDatabase = database;
        mObservableUsers = new MediatorLiveData<>();
        mObservableBins = new MediatorLiveData<>();

/*        mObservableUsers.addSource(mDatabase.userDao().getAll(),
                usersEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservableUsers.postValue(usersEntities);
                    }
                });

        mObservableBins.addSource(mDatabase.binDao().getAll(),
                binEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservableBins.postValue(binEntities);
                    }
                });*/
    }

   /* public static DataRepository getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                }
            }
        }
        return sInstance;
    }*/

    /**
     * Get the list of products from the database and get notified when the data changes.
     */
/*    public LiveData<List<UserEntity>> getUsers() {
        return mObservableUsers;
    }
    public LiveData<List<UserEntity>> loadUsers()
    {
        return mDatabase.userDao().getAll();
    }
    public LiveData<UserEntity> loadUser(int id)
    {
        return mDatabase.userDao().get(id);
    }

    public LiveData<List<BinEntity>> getBins() {
        return mObservableBins;
    }
    public LiveData<List<BinEntity>> loadBins()
    {
        return mDatabase.binDao().getAll();
    }
    public LiveData<BinEntity> loadBin(int id)
    {
        return mDatabase.binDao().get(id);
    }*/
}


