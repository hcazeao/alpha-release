package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.dao.DispenseTimeDao;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.UserDao;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class DispenseTimeRepository extends BaseRepository{

    public DispenseTimeRepository()
    {
        super();
    }

    public boolean isDuplicateDispenseTimeFound(String sTime) {
        DispenseTimeEntity entity = new DispenseTimeEntity();
        entity.setDispenseTime(sTime);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return ((int)syncOp(CRUDEnum.QUERY_COUNT_BY_DISPENSE_TIME, list) > 0) ? true : false;
    }

    public List<DispenseTimeEntity> getByIsActiveOnly()
    {
        DispenseTimeEntity entity = new DispenseTimeEntity();
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<DispenseTimeEntity>)syncOp(CRUDEnum.QUERY_BY_ACIVE, list);
    }

    public DispenseTimeEntity read(int dispenseTimeId)
    {
        DispenseTimeEntity entity = new DispenseTimeEntity();
        entity.setId(dispenseTimeId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (DispenseTimeEntity)syncOp(CRUDEnum.QUERY_BY_ID, list);
    }

    public void deleteByName(String name) {
        DispenseTimeEntity entity = new DispenseTimeEntity();
        entity.setDispenseName(name);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        asyncOp(CRUDEnum.DELETE_BY_NAME, list);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<DispenseTimeEntity> list = (List<DispenseTimeEntity>)(List<?>) entities;
        switch (op)
        {

            case INSERT_ALL:
                Long[] ids = ((DispenseTimeDao)dao).insertAll(list);
                int i =0;
                for(DispenseTimeEntity item : list) {
                    item.setId((int)(long)ids[i++]);
                }
                return ids;

            case INSERT:
                int id = (int)(long)((DispenseTimeDao)dao).insert(list.get(0));
                list.get(0).setId(id);
                return id;

            case QUERY_COUNT_BY_DISPENSE_TIME:
                return ((DispenseTimeDao)dao).getCountByTime(list.get(0).getDispenseTime());

            case QUERY_ALL:
                return ((DispenseTimeDao)dao).getAll();

            case QUERY_BY_ID:
                return ((DispenseTimeDao)dao).get(list.get(0).getId());

            case QUERY_BY_ACIVE:
                return ((DispenseTimeDao)dao).getByActive();

            case UPDATE:
                return ((DispenseTimeDao)dao).update(list.get(0));

            case DELETE:
                return ((DispenseTimeDao)dao).delete(list.get(0).getId());

            case DELETE_BY_NAME:
                return ((DispenseTimeDao)dao).delete(list.get(0).getDispenseName());

            case DELETE_ALL:
                return ((DispenseTimeDao)dao).deleteAll();
        }

        return false;
    }


    public IBaseDao getDAO()
    {
        return db.dispenseTimeDao();
    }
}
