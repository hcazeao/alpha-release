package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.entities.IBaseEntity;

import java.util.List;

public interface IRepository {
     Object crudOp(IBaseDao dao,
                   CRUDEnum op,
                   List<IBaseEntity> entities);

     IBaseDao getDAO();
}
