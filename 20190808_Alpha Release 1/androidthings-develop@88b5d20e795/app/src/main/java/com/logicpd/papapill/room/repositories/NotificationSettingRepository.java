package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.App;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.NotificationSettingDao;
import com.logicpd.papapill.room.dao.UserDao;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinContactSettingUser;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class NotificationSettingRepository extends BaseRepository {

    public NotificationSettingRepository()
    {
        super();
    }

    public List<NotificationSettingEntity> readAll(int userId, int contactId)
    {
        NotificationSettingEntity settingEntity = new NotificationSettingEntity(userId, contactId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(settingEntity);
        return (List<NotificationSettingEntity>) syncOp(CRUDEnum.QUERY_BY_USER_ID_CONTACT_ID, list);
    }

    public List<NotificationSettingEntity> readAllByContactId(int contactId)
    {
        NotificationSettingEntity settingEntity = new NotificationSettingEntity(0, contactId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(settingEntity);
        return (List<NotificationSettingEntity>) syncOp(CRUDEnum.QUERY_BY_CONTACT_ID, list);
    }

    public boolean deleteByUserIdContactId( int userId,
                                            int contactId)
    {
        NotificationSettingEntity settingEntity = new NotificationSettingEntity(userId, contactId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(settingEntity);
        asyncOp(CRUDEnum.DELETE_BY_USER_ID_CONTACT_ID, list);
        return true;
    }

    /*
     * Delete by contactId
     */
    public boolean deleteByContactId(int contactId)
    {
        NotificationSettingEntity settingEntity = new NotificationSettingEntity(0, contactId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(settingEntity);
        asyncOp(CRUDEnum.DELETE_BY_CONTACT_ID, list);
        return true;
    }

    /*
     * Get by userId, settingId
     */
    public List<NotificationSettingEntity> getByUserIdSettingId(int userId)
    {
        NotificationSettingEntity settingEntity = new NotificationSettingEntity(userId, 0);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(settingEntity);
        return (List<NotificationSettingEntity>)syncOp(CRUDEnum.QUERY_BY_USER_ID, list);
    }

    /*
     * Get All : Join contact - notification setting - user
     */
    public List<JoinContactSettingUser> getJoinAll()
    {
        return (List<JoinContactSettingUser>)syncOp(CRUDEnum.QUERY_JOIN_ALL, null);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<NotificationSettingEntity> list = (List<NotificationSettingEntity>)(List<?>) entities;
        switch (op)
        {
            case INSERT_ALL:
            Long[] ids = ((NotificationSettingDao)dao).insertAll(list);
            int i =0;
            for(NotificationSettingEntity item : list) {
                item.setId((int)(long)ids[i++]);
            }
            return ids;

            case INSERT:
                int id = (int)(long)((NotificationSettingDao)dao).insert(list.get(0));
                list.get(0).setId(id);
                return id;

            case QUERY_ALL:
                return ((NotificationSettingDao)dao).getAll();

            case QUERY_BY_CONTACT_ID:
                return ((NotificationSettingDao)dao).getByContactId(list.get(0).getContactId());

            case QUERY_BY_ID:
                return ((NotificationSettingDao)dao).get(list.get(0).getId());

            case QUERY_BY_USER_ID_CONTACT_ID:
                return ((NotificationSettingDao)dao).getByUserIdContactId(list.get(0).getUserId(), list.get(0).getContactId());

            case QUERY_BY_USER_ID:
                return ((NotificationSettingDao)dao).getByUserId(list.get(0).getUserId());

            case UPDATE:
                return ((NotificationSettingDao)dao).update(list.get(0));

            case DELETE:
                return ((NotificationSettingDao)dao).delete(list.get(0).getId());

            case DELETE_BY_USER_ID_CONTACT_ID:
                return ((NotificationSettingDao)dao).deleteByUserIdContactId(list.get(0).getUserId(), list.get(0).getContactId());

            case DELETE_BY_CONTACT_ID:
                return ((NotificationSettingDao)dao).deleteByContactId(list.get(0).getContactId());

            case DELETE_ALL:
                return  ((NotificationSettingDao)dao).deleteAll();

                // JOINS

            case QUERY_JOIN_ALL:
                return ((NotificationSettingDao)dao).getAllJoin();
        }

        return false;
    }

    public IBaseDao getDAO()
    {
        return db.settingDao();
    }
}
