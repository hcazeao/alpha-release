package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.SystemDao;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.SystemEntity;

import java.util.ArrayList;
import java.util.List;

public class SystemRepository extends BaseRepository {

    public SystemRepository()
    {
        super();
    }

    public boolean insert(String key)
    {
        SystemEntity systemEntity = new SystemEntity();
        systemEntity.setSystemKey(key);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) systemEntity));
        asyncOp(CRUDEnum.INSERT, list);
        return true;
    }

    public boolean update(int id, String key)
    {
        SystemEntity systemEntity = new SystemEntity();
        systemEntity.setSystemKey(key);
        systemEntity.setId(id);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) systemEntity));

        asyncOp(CRUDEnum.UPDATE_SYSTEM_KEY_BY_ID, list);
        return true;
    }

    public boolean update(IBaseEntity systemEntity)
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add((systemEntity));
        asyncOp(CRUDEnum.UPDATE, list);
        return true;
    }

    public boolean updateSerialNumber(int id,
                                      String serialNumber)
    {
        SystemEntity systemEntity = new SystemEntity();
        systemEntity.setSerialNumber(serialNumber);
        systemEntity.setId(id);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) systemEntity));

        asyncOp(CRUDEnum.UPDATE_SERIAL_NUM_BY_ID, list);
        return true;
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<SystemEntity> list = (List<SystemEntity>)(List<?>) entities;
        switch (op)
        {
            case INSERT_ALL:
                Long[] ids = ((SystemDao)dao).insertAll(list);
                int i=0;
                for(SystemEntity system : list)
                {
                    system.setId((int)(long)ids[i++]);
                }
                return ids;

            case INSERT:
                int id = (int)(long) ((SystemDao)dao).insert(list.get(0));
                list.get(0).setId(id);
                return id;

            case QUERY_ALL:
                return ((SystemDao)dao).getAll();

            case QUERY_BY_ID:
                return ((SystemDao)dao).get(list.get(0).getId());

            case UPDATE:
                return ((SystemDao)dao).update(list.get(0));

            case UPDATE_SERIAL_NUM_BY_ID:
                return ((SystemDao)dao).updateSerialNumberById(list.get(0).getId(), list.get(0).getSerialNumber());

            case UPDATE_SYSTEM_KEY_BY_ID:
                return ((SystemDao)dao).updateSystemKeyById(list.get(0).getId(), list.get(0).getSerialNumber());

            case DELETE:
                return ((SystemDao)dao).delete(list.get(0).getId());

            case DELETE_ALL:
                return ((SystemDao)dao).deleteAll();
        }

        return false;
    }

    public IBaseDao getDAO()
    {
        return db.systemDao();
    }
}
