package com.logicpd.papapill.room.utils;

import android.arch.persistence.room.TypeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonthDayYearConverter {
    static DateFormat df = new SimpleDateFormat("MMM dd yyyy");

    @TypeConverter
    public static Date fromString(String value) {
        if (value != null) {
            try {
                return df.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    @TypeConverter
    public static String toString(Date value) {
        return value == null ? null : df.format(value);
    }
}
