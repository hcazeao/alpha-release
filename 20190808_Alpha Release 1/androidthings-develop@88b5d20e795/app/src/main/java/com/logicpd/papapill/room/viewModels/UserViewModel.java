package com.logicpd.papapill.room.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import android.databinding.ObservableField;
import android.view.KeyEvent;
import android.widget.TextView;

import com.logicpd.papapill.room.repositories.DataRepository;
import com.logicpd.papapill.room.entities.UserEntity;

// TODO implement view models ?
public class UserViewModel extends AndroidViewModel implements TextView.OnEditorActionListener
{
    //private final LiveData<UserEntity> mObservableUser;
    public ObservableField<UserEntity> user = new ObservableField<>();

    private final int mUserId;

    public UserViewModel(@NonNull Application application,
                         DataRepository repository,
                            final int userId) {
        super(application);
        mUserId = userId;
        //mObservableUser = repository.loadUser(mUserId);
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
    {
       /* switch (textView.getId())
        {
            case R.id.txtEdit:
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    // code
                }
                break;
        }*/
        return false;
    }

   /* public LiveData<UserEntity> getObservableUser() {
        return mObservableUser;
    }

    public void setProduct(UserEntity userEntity) {
        this.user.set(userEntity);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application mApplication;

        private final int mProductId;

        private final DataRepository mRepository;

        public Factory(@NonNull Application application, int productId) {
            mApplication = application;
            mProductId = productId;
            mRepository = DataRepository.getInstance(AppDatabase.getDatabase(application.getBaseContext()));
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new UserViewModel(mApplication, mRepository, mProductId);
        }
    }*/
}
