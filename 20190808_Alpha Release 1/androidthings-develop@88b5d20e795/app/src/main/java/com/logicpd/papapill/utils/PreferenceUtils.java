package com.logicpd.papapill.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.logicpd.papapill.App;
import com.logicpd.papapill.enums.PreferenceEnum;
import com.logicpd.papapill.misc.AppConstants;

/**
 * Utilities for handling preferences
 *
 * @author alankilloren
 */
public class PreferenceUtils {
    private static String IS_CAMERA_CALIBRATED = "isCameraCalibrated";
    private static String IS_FIRST_TIME_RUN = "isFirstTimeRun";
    private static String SSID = "SSID";
    private static String CAROUSEL_POS = "carouselPos";
    public static String DISPENSE_COUNT = "dispenseCount";
    private static String CAROUSEL_POS_INTERVAL = "carouselPosInterval";
    public static int DEFAULT_INTERVAL_SECOND = 1000;
    public static int MIN_INTERVAL_SECOND = 200;
    public static int DEFAULT_BIN_HOME_ID = 0;

    // Audio and Visual Settings
    public static String IS_AUDIO_TONE_INDICATORS_ENABLED = "AudioToneIndicatorsEnabled";
    public static String IS_VISUAL_INDICATORS_ENABLED = "VisualIndicatorsEnabled";
    public static String IS_TOUCHSCREEN_CLICKS_ENABLED = "TouchscreenClicksEnabled";
    public static String AUDIO_VOLUME = "AudioVolume";
    public static int DEFAULT_AUDIO_VOLUME = 50;

    public static String SCREEN_BRIGHTNESS = "ScreenBrightness";
    public static int DEFAULT_SCREEN_BRIGHTNESS = 50;

    public static int getDispenseCount() {
        int count = (int)getValue(DISPENSE_COUNT, PreferenceEnum.INTEGER, 1);
        setValue(DISPENSE_COUNT, PreferenceEnum.INTEGER, count+1);
        return count;
    }

    public static void setAudioVolume(int percent) {
        setValue(AUDIO_VOLUME, PreferenceEnum.INTEGER, percent);
    }

    public static int getAudioVolume() {
        int percent = (int)getValue(AUDIO_VOLUME, PreferenceEnum.INTEGER, DEFAULT_AUDIO_VOLUME);
        return percentFilter(percent);
    }

    public static void setScreenBrightness(int percent) {
        setValue(SCREEN_BRIGHTNESS, PreferenceEnum.INTEGER, percent);
    }

    public static int getScreenBrightness() {
        int percent = (int)getValue(SCREEN_BRIGHTNESS, PreferenceEnum.INTEGER, DEFAULT_SCREEN_BRIGHTNESS);
       return percentFilter(percent);
    }

    protected static int percentFilter(int value){
        if(value < 0)
            return 0;
        else if (value > 100)
            return 100;
        else
            return value;
    }

    public static void setTouchscreenClicked(boolean b) {
        setValue(IS_TOUCHSCREEN_CLICKS_ENABLED, PreferenceEnum.BOOLEAN, b);
    }

    public static boolean getTouchscreenClicked() {
        return (boolean)getValue(IS_TOUCHSCREEN_CLICKS_ENABLED, PreferenceEnum.BOOLEAN, true);
    }

    public static void setAudioToneIndicators(boolean b) {
        setValue(IS_AUDIO_TONE_INDICATORS_ENABLED, PreferenceEnum.BOOLEAN, b);
    }

    public static boolean getAudioToneIndicators() {
        return (boolean)getValue(IS_AUDIO_TONE_INDICATORS_ENABLED, PreferenceEnum.BOOLEAN, true);
    }

    public static void setVisualIndicatorsEnabled(boolean b) {
        setValue(IS_VISUAL_INDICATORS_ENABLED, PreferenceEnum.BOOLEAN, b);
    }

    public static boolean getVisualIndicatorsEnabled() {
        return (boolean)getValue(IS_VISUAL_INDICATORS_ENABLED, PreferenceEnum.BOOLEAN, true);
    }

    public static void setFirstTimeRun(boolean b) {
        setValue(IS_FIRST_TIME_RUN, PreferenceEnum.BOOLEAN, b);
    }

    public static boolean getFirstTimeRun() {
        return (boolean)getValue(IS_FIRST_TIME_RUN, PreferenceEnum.BOOLEAN, true);
    }

    public static void setSSID(String ssid) {
        setValue(SSID, PreferenceEnum.STRING, ssid);
    }

    public static String getSSID() {
        return (String)getValue(SSID, PreferenceEnum.STRING, null);
    }

    public static void setCarouselPosInterval(int binNumber) {
        setValue(CAROUSEL_POS_INTERVAL, PreferenceEnum.INTEGER, binNumber);
    }

    public static int getCarouselPosInterval() {
        // default home bin location
        int milliSec = (int)getValue(CAROUSEL_POS_INTERVAL, PreferenceEnum.INTEGER, DEFAULT_INTERVAL_SECOND);
        return (milliSec > MIN_INTERVAL_SECOND)?milliSec:DEFAULT_INTERVAL_SECOND;
    }

    public static void setCameraIsCalibrated(boolean b) {
        setValue(IS_CAMERA_CALIBRATED, PreferenceEnum.BOOLEAN, b);
    }

    public static boolean getIsCameraCalibrated() {
        return (boolean)getValue(IS_CAMERA_CALIBRATED, PreferenceEnum.BOOLEAN, false);
    }

    private static void setValue(String name,
                                   PreferenceEnum type,
                                   Object value)
    {
        SharedPreferences prefs = getPreference();
        SharedPreferences.Editor editor = prefs.edit();
        switch(type)
        {
            case STRING:
                editor.putString(name, (String)value);
                break;

            case BOOLEAN:
                editor.putBoolean(name, (boolean)value);
                break;

            case INTEGER:
                editor.putInt(name, (int)value);
                break;
        }
        editor.apply();
    }

    protected static Object getValue(String name,
                                     PreferenceEnum type,
                                     Object defaultValue)
    {
        SharedPreferences prefs = getPreference();
        switch(type)
        {
            case STRING:
                return prefs.getString(name, (String)defaultValue);

            case BOOLEAN:
                return prefs.getBoolean(name, (boolean)defaultValue);

            case INTEGER:
                return prefs.getInt(name, (int)defaultValue);
        }
        return null;
    }

    private static SharedPreferences getPreference()
    {
        return App.getContext().getSharedPreferences(AppConstants.TAG, Context.MODE_PRIVATE);
    }
}
