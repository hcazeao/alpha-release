package com.logicpd.papapill.utils;

public class ReflectionUtil {
    public static Class<?> getClassPath(String simpleName){
        String[] paths = new String[] { "com.logicpd.papapill.fragments.help.",
                "com.logicpd.papapill.fragments.my_medication.",
                "com.logicpd.papapill.fragments.power_on.",
                "com.logicpd.papapill.fragments.user_settings.",
                "com.logicpd.papapill.fragments.",
                "com.logicpd.papapill.fragments.system_manager.manage_medications.",
                "com.logicpd.papapill.fragments.system_manager.manage_users.",
                "com.logicpd.papapill.fragments.system_manager.system_settings.",
                "com.logicpd.papapill.fragments.system_manager."};

        for(int i=0; i<paths.length; i++) {

            Class<?> clazz = getClass(paths[i], simpleName);
            if (null != clazz) {
                return clazz;
            }
        }
        return null;
    }

    private static Class<?> getClass(String path,
                                     String simpleName) {
        try {
            Class<?> clazz = Class.forName( path + simpleName);
            return clazz;
        }
        catch (Exception ex) {
            return null;
        }
    }
}
