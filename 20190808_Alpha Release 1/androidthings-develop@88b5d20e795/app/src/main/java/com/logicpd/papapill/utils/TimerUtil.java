package com.logicpd.papapill.utils;

import android.os.CountDownTimer;
import android.util.Log;

import com.logicpd.papapill.interfaces.OnTimerListener;

public class TimerUtil {
    public static final String TAG = "TimerUtil";
    private OnTimerListener mListener;
    private CountDownTimer timer;

    public TimerUtil(OnTimerListener listener) {
        mListener = listener;
    }

    public void start() {

        timer = new CountDownTimer(500, 1) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try{
                    mListener.onTimerFinished();
                }catch(Exception e){
                    Log.e(TAG, "onFinish() : " + e.toString());
                }
            }
        }.start();
    }

    public void start(int ms) {

        timer = new CountDownTimer(ms, 1) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try{
                    mListener.onTimerFinished();
                }catch(Exception e){
                    Log.e(TAG, "onFinish() : " + e.toString());
                }
            }
        }.start();
    }
}
