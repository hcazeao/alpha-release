package com.logicpd.papapill.wireframes;

import android.os.Bundle;
import android.util.Log;

import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

import java.lang.reflect.Constructor;

/*
 * TODO implement wireframes classes as defined in documents.
 *
 * wireframes solve 4 problems
 * 1. abstract version of wireframe documents (replace with JetPack navigation and dynamic modules ?)
 * 2. provide architecture models (MV-ish) for the life of the workflow.
 * 3. clean up current daisey chain style local variables.
 * 4. provide a UI state machine.
 */
public class BundleFactory {
    public static final String TAG = "BundleFactory";

    public static IWireframe createModel(String wireframe,
                                         BaseWireframe parentModel) {
        return getWorkflow(wireframe, parentModel);
    }

    private static IWireframe getWorkflow(String wireframe,
                                          BaseWireframe parentModel)
    {
        try {
            Class<?> clazz = Class.forName("com.logicpd.papapill.wireframes.workflows." + wireframe);
            Constructor<?> constructor = clazz.getConstructor(BaseWireframe.class);
            Object model = constructor.newInstance(parentModel);
            return (IWireframe)model;
        }
        catch (Exception ex){
            // provide base model if not found
            return new BaseWireframe(parentModel);
        }
    }
}
