package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class ChangeAVSettings extends BaseWireframe {
    public static String TAG = "ChangeAVSettings";

    public ChangeAVSettings(BaseWireframe parentModel) {
        super(parentModel);
    }

}
