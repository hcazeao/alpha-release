package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.os.Bundle;
import android.util.Log;

import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DirectionsQuantityPerDoseFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.EnterDailyQuantityPerDoseFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.MedicationAddedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ScheduleUpdatedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectChangeMedScheduleFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectDispensingTimesFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectUserForChangeScheduleFragment;

public class ChangeMedSchedule extends BaseWireframe {
    public final static String TAG = "ChangeMedSchedule";

    public enum ChangeMedScheduleEnum {
        ManageMeds {
            public String toString() {
                return ManageMedsFragment.class.getSimpleName();
            }
        },
        SystemKey{
            public String toString() {return SystemKeyFragment.class.getSimpleName();}
        },
        SelectUserForChangeSchedule{
            public String toString() {return SelectUserForChangeScheduleFragment.class.getSimpleName();}
        },
        SelectChangeMedSchedule{
            public String toString() {return SelectChangeMedScheduleFragment.class.getSimpleName();}
        },
        SelectDispensingTimes{
            public String toString() {return SelectDispensingTimesFragment.class.getSimpleName();}
        },
        DirectionsQuantityPerDose{
            public String toString() {return DirectionsQuantityPerDoseFragment.class.getSimpleName();}
        },
        EnterDailyQuantityPerDose{
            public String toString() {return EnterDailyQuantityPerDoseFragment.class.getSimpleName();}
        },
        ScheduleUpdated{
            public String toString() {return ScheduleUpdatedFragment.class.getSimpleName();}
        }
    }

    public ChangeMedSchedule(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * Wireframe for workflow ChangeMedSchedule
     */
    public Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(BundleEnums.wireframe.toString(), this.getClass().getSimpleName());
        bundle.putString(BundleEnums.fragmentName.toString(), SystemKeyFragment.class.getSimpleName());
        bundle.putString(BundleEnums.endFragment.toString(), ScheduleUpdatedFragment.class.getSimpleName());
        bundle.putString(BundleEnums.homeFragment.toString(), ManageMedsFragment.class.getSimpleName());
        return bundle;
    }

    /*
     * Update bundle for next fragment
     */
    public Bundle updateBundle(Bundle bundle,
                               String currentFragmentName){
        String fragmentName = ChangeMedScheduleEnum.ManageMeds.toString();

        try {
            ChangeMedScheduleEnum valueEnum = ChangeMedScheduleEnum.valueOf(currentFragmentName);
            switch (valueEnum) {
                case ManageMeds:
                    fragmentName = ChangeMedScheduleEnum.SystemKey.toString();
                    break;

                case SystemKey:
                    fragmentName = ChangeMedScheduleEnum.SelectUserForChangeSchedule.toString();
                    break;

                case SelectUserForChangeSchedule:
                    fragmentName = ChangeMedScheduleEnum.SelectChangeMedSchedule.toString();
                    break;

                case SelectChangeMedSchedule:
                    fragmentName = ChangeMedScheduleEnum.SelectDispensingTimes.toString();
                    break;

                case SelectDispensingTimes:
                    fragmentName = ChangeMedScheduleEnum.DirectionsQuantityPerDose.toString();
                    break;

                case DirectionsQuantityPerDose:
                    fragmentName = ChangeMedScheduleEnum.EnterDailyQuantityPerDose.toString();
                    break;

                case EnterDailyQuantityPerDose:
                    fragmentName = ChangeMedScheduleEnum.ScheduleUpdated.toString();
                    break;

                default:
                case ScheduleUpdated:
                    fragmentName = ChangeMedScheduleEnum.ManageMeds.toString();
                    break;
            }
        }
        catch (Exception ex)
        {
            Log.e(TAG, "updateBundle() failed"+ ex);
        }
        bundle.putString(BundleEnums.fragmentName.toString(), fragmentName);
        return bundle;
    }

    public boolean isEndFragment(String fragmentName)
    {
        return (fragmentName.equals(ChangeMedScheduleEnum.ScheduleUpdated.toString()))?
                true:
                false;
    }
}
