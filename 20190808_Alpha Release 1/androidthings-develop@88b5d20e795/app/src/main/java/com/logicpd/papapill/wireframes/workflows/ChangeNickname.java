package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.SystemManagerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditUserNicknameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.SelectEditUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.VerifyUserInfoFragment;

public class ChangeNickname extends BaseWireframe {
    public static String TAG = "ChangePIN";

    public ChangeNickname(BaseWireframe parentModel) {
        super(parentModel);
    }

    public enum ChangeNicknameEnum {
        Home {
            public String toString() {
                return HomeFragment.class.getSimpleName();
            }
        },
        SystemManager {
            public String toString() {
                return SystemManagerFragment.class.getSimpleName();
            }
        },
        ManageUsers {
            public String toString() {
                return ManageUsersFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        SelectEditUser {
            public String toString() {
                return SelectEditUserFragment.class.getSimpleName();
            }
        },
        EditUser {
            public String toString() {
                return EditUserFragment.class.getSimpleName();
            }
        },
        EditUserNickname {
            public String toString() {

                return EditUserNicknameFragment.class.getSimpleName();
            }
        },
        VerifyUserInfo {
            public String toString() {

                return VerifyUserInfoFragment.class.getSimpleName();
            }
        },
    }
}
