package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.user_settings.UserSettingsFragment;

public class ChangePIN extends BaseWireframe {
    public static String TAG = "ChangePIN";

    public ChangePIN(BaseWireframe parentModel) {
        super(parentModel);
    }

    public enum ChangePINEnum {
        Home {
            public String toString() {
                return HomeFragment.class.getSimpleName();
            }
        },
        UserSettings {
            public String toString() {
                return UserSettingsFragment.class.getSimpleName();
            }
        },
    }
}
