package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;

public class ChangeSystemKey extends BaseWireframe {
    public static String TAG = "ChangeSystemKey";

    public ChangeSystemKey(BaseWireframe parentModel) {
        super(parentModel);
    }

}
