package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;

public class CheckFillStatus extends BaseWireframe {
    public static String TAG = "CheckFillStatus";

    public CheckFillStatus(BaseWireframe parentModel) {
        super(parentModel);
    }

}
