package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;

public class ConnectWiFi extends BaseWireframe {
    public static String TAG = "ConnectWiFi";

    public ConnectWiFi(BaseWireframe parentModel) {
        super(parentModel);
    }

}
