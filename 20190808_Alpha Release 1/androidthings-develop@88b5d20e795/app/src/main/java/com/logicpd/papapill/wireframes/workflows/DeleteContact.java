package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.util.Log;

import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.CannotDeleteContactFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ConfirmDeleteContactFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactDeletedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactInfoFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditNotificationSettingsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditNotificationsSettingOtherUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.NotificationSettingsSavedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.SelectUserEditNotificationsFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.NotificationSettingRepository;

import java.util.ArrayList;
import java.util.List;

public class DeleteContact extends BaseWireframe {
    public static final String TAG = "DeleteContact";
    private List<UserEntity> usersEdited;

    public DeleteContact(BaseWireframe parentModel) {
        super(parentModel);
        usersEdited = new ArrayList<UserEntity>();
    }

    // continue ManageContacts
    public enum DeleteContactEnum {
        // button 'DELETE CONTACT' on ContactInfo

        ConfirmDeleteContact  {
            public String toString() {
                return ConfirmDeleteContactFragment.class.getSimpleName();
            }
        },

        ContactDeleted {
            public String toString() {
                return ContactDeletedFragment.class.getSimpleName();
            }
        },

        // -> can't delete the only contact for user
        CannotDeleteContact {
            public String toString() {
                return CannotDeleteContactFragment.class.getSimpleName();
            }
        },

        SelectUserEditNotifications {
            public String toString() {
                return SelectUserEditNotificationsFragment.class.getSimpleName();
            }
        },
        EditNotificationSettings {
            public String toString() {
                return EditNotificationSettingsFragment.class.getSimpleName();
            }
        },
        NotificationSettingsSaved {
            public String toString() {
                return NotificationSettingsSavedFragment.class.getSimpleName();
            }
        },
        EditNotificationsSettingOtherUser {
            public String toString() {
                return EditNotificationsSettingOtherUserFragment.class.getSimpleName();
            }
        }
        // Edit next user / goto ConfirmDeleteContact
    }

    @Override
    public String getNextFragmentName(String currentFragmentName){
        switch (currentFragmentName) {
            case SelectUserEditNotificationsFragment.TAG:
                return EditNotificationSettingsFragment.class.getSimpleName();

            case NotificationSettingsSavedFragment.TAG:
                if(hasEditedAllUsers()) {
                    return (isDeleteOK()) ?
                            ConfirmDeleteContactFragment.class.getSimpleName():
                            CannotDeleteContactFragment.class.getSimpleName();
                }
                else {
                    return DeleteContactEnum.EditNotificationsSettingOtherUser.toString();
                }

            case EditNotificationsSettingOtherUserFragment.TAG:
            case EditNotificationSettingsFragment.TAG:
                return (isDeleteOK()) ?
                        ConfirmDeleteContactFragment.class.getSimpleName():
                        CannotDeleteContactFragment.class.getSimpleName();
        }
        return null;
    }

    /*
     * Override base addMapItem
     * 1. call base
     * 2. keep count of edited user(s)
     */
    public void addMapItem(String key,
                           Object item) {
        super.addMapItem(key, item);

        if(key == "user") {
            setEditedUser((UserEntity)item);
        }
    }

    /*
     * SelectUserEditNotificationsFragment request
     * keep count of edited user(s)
     */
    public void setEditedUser(UserEntity user) {
        for(UserEntity u : usersEdited) {
            if(u.getId() == user.getId())
                return;
        }
        usersEdited.add(user);
    }

    /*
     * EditNotificationsSettingOtherUser's request
     */
    public UserEntity getNextUser() {
        // already edited all
        List<UserEntity> users = getUsers();
        if(usersEdited.size() == users.size())
            return null;

        // find user that is not on the edited list
        for(UserEntity user : users) {
            boolean isEdited = false;
            for(UserEntity edited : usersEdited){
                if(user.getId() == edited.getId()) {
                    isEdited = true;
                }
            }
            if (!isEdited)
                return user;
        }
        return null;
    }

    public void deleteNotifications4Contact(ContactEntity contact) {
        new NotificationSettingRepository().deleteByContactId(contact.getId());
        new ContactRepository().update(contact);
    }

    protected boolean hasEditedAllUsers() {
        return (usersEdited.size() == numOfUsers()) ? true:false;
    }

    protected boolean isDeleteOK() {
        try {
            // contact id is not recovery
            ContactEntity contact = getContact();
            return (((ManageContacts) parent).isRecoveryContactId(contact.getId()))?false:true;
        }
        catch (Exception ex){
            Log.e(TAG, "isDeleteOK() failed:"+ex);
        }
        return false;
    }
}
