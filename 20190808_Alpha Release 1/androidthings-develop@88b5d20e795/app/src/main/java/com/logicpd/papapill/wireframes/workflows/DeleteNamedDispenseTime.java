package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.os.Bundle;
import android.util.Log;

import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ConfirmDeleteDispenseTimeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeDeletedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeSummaryFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.EditDispenseTimesFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectDeleteDispenseTimeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectEditDispenseTimeFragment;

public class DeleteNamedDispenseTime extends BaseWireframe {
    public final static String TAG = "DeleteNamedDispenseTime";

    public enum DeleteNamedDispenseTimeEnum {
        ManageMeds {
            public String toString() {
                return ManageMedsFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        EditDispenseTimes {
            public String toString() {
                return EditDispenseTimesFragment.class.getSimpleName();
            }
        },
        SelectDeleteDispenseTime {
            public String toString() {
                return SelectDeleteDispenseTimeFragment.class.getSimpleName();
            }
        },
        ConfirmDeleteDispenseTime {
            public String toString() {
                return ConfirmDeleteDispenseTimeFragment.class.getSimpleName();
            }
        },
        DispenseTimeDeleted {
            public String toString() {
                return DispenseTimeDeletedFragment.class.getSimpleName();
            }
        }
    }

    public DeleteNamedDispenseTime(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * Wireframe for workflow EditNamedDispenseTime
     */
    public Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(BundleEnums.wireframe.toString(), this.getClass().getSimpleName());
        bundle.putString(BundleEnums.fragmentName.toString(), SystemKeyFragment.class.getSimpleName());
        bundle.putString(BundleEnums.endFragment.toString(), DispenseTimeSummaryFragment.class.getSimpleName());
        bundle.putString(BundleEnums.homeFragment.toString(), ManageMedsFragment.class.getSimpleName());
        return bundle;
    }

    /*
     * Update bundle for next fragment
     */
    public Bundle updateBundle(Bundle bundle,
                               String currentFragmentName) {
        String fragmentName = DeleteNamedDispenseTimeEnum.ManageMeds.toString();

        try {
            DeleteNamedDispenseTimeEnum valueEnum = DeleteNamedDispenseTimeEnum.valueOf(currentFragmentName);
            switch (valueEnum) {
                case ManageMeds:
                    fragmentName = DeleteNamedDispenseTimeEnum.SystemKey.toString();
                    break;

                case SystemKey:
                    fragmentName = DeleteNamedDispenseTimeEnum.EditDispenseTimes.toString();
                    break;

                case EditDispenseTimes:
                    fragmentName = DeleteNamedDispenseTimeEnum.SelectDeleteDispenseTime.toString();
                    break;

                case SelectDeleteDispenseTime:
                    fragmentName = DeleteNamedDispenseTimeEnum.ConfirmDeleteDispenseTime.toString();
                    break;

                case ConfirmDeleteDispenseTime:
                    fragmentName = DeleteNamedDispenseTimeEnum.DispenseTimeDeleted.toString();
                    break;

                default:
                case DispenseTimeDeleted:
                    fragmentName = DeleteNamedDispenseTimeEnum.ManageMeds.toString();
                    break;
            }
        }
        catch (Exception ex)
        {
            Log.e(TAG, "updateBundle() failed"+ ex);
        }
        bundle.putString(BundleEnums.fragmentName.toString(), fragmentName);
        return bundle;
    }

    public boolean isEndFragment(String fragmentName)
    {
        return (fragmentName.equals(DeleteNamedDispenseTimeEnum.DispenseTimeDeleted.toString()))?
                true:
                false;
    }
}
