package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;

public class DispenseStrategy {

    static public String getFragmentName(int medicationLocation)
    {

        if (medicationLocation <= App.getContext().getResources().getInteger(R.integer.numOfBins)) {
            return "CupNotPresentFragment";
        } else if (medicationLocation == App.getContext().getResources().getInteger(R.integer.drawerLocation)) {
            return "DispenseMedsDrawerFragment";
        } else if (medicationLocation == App.getContext().getResources().getInteger(R.integer.refridgeratorLocation)) {
            return "DispenseMedsFridgeFragment";
        } else {
            //(medicationLocation == App.getContext().getResources().getInteger(R.integer.otherLocation))
            return "DispenseMedsOtherFragment";
        }
    }
}
