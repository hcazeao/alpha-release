package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.os.Bundle;
import android.util.Log;

import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeSummaryFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.EditDispenseTimesFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectEditDispenseTimeFragment;

public class EditNamedDispenseTime extends BaseWireframe {
    public final static String TAG = "EditNamedDispenseTime";

    public enum EditNamedDispenseTimeEnum {
        ManageMeds {
            public String toString() {
                return ManageMedsFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        EditDispenseTimes {
            public String toString() {
                return EditDispenseTimesFragment.class.getSimpleName();
            }
        },
        SelectEditDispenseTime {
            public String toString() {
                return SelectEditDispenseTimeFragment.class.getSimpleName();
            }
        },
        DispenseTimeName {
            public String toString() {
                return DispenseTimeNameFragment.class.getSimpleName();
            }
        },
        DispenseTime {
            public String toString() {
                return DispenseTimeFragment.class.getSimpleName();
            }
        },
        DispenseTimeSummary {
            public String toString() {
                return DispenseTimeSummaryFragment.class.getSimpleName();
            }
        }
    }

    public EditNamedDispenseTime(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * Wireframe for workflow EditNamedDispenseTime
     */
    public Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(BundleEnums.wireframe.toString(), this.getClass().getSimpleName());
        bundle.putString(BundleEnums.fragmentName.toString(), SystemKeyFragment.class.getSimpleName());
        bundle.putString(BundleEnums.endFragment.toString(), DispenseTimeSummaryFragment.class.getSimpleName());
        bundle.putString(BundleEnums.homeFragment.toString(), ManageMedsFragment.class.getSimpleName());
        return bundle;
    }

    /*
     * Update bundle for next fragment
     */
    public Bundle updateBundle(Bundle bundle,
                               String currentFragmentName) {
        String fragmentName = EditNamedDispenseTimeEnum.ManageMeds.toString();

        try {
            EditNamedDispenseTimeEnum valueEnum = EditNamedDispenseTimeEnum.valueOf(currentFragmentName);
            switch (valueEnum) {
                case ManageMeds:
                    fragmentName = EditNamedDispenseTimeEnum.SystemKey.toString();
                    break;

                case SystemKey:
                    fragmentName = EditNamedDispenseTimeEnum.EditDispenseTimes.toString();
                    break;

                case EditDispenseTimes:
                    fragmentName = EditNamedDispenseTimeEnum.SelectEditDispenseTime.toString();
                    break;

                case SelectEditDispenseTime:
                    fragmentName = EditNamedDispenseTimeEnum.DispenseTimeName.toString();
                    break;

                case DispenseTimeName:
                    fragmentName = EditNamedDispenseTimeEnum.DispenseTime.toString();
                    break;

                case DispenseTime:
                    fragmentName = EditNamedDispenseTimeEnum.DispenseTimeSummary.toString();
                    break;

                default:
                case DispenseTimeSummary:
                    fragmentName = EditNamedDispenseTimeEnum.ManageMeds.toString();
                    break;
            }
        }
        catch (Exception ex)
        {
            Log.e(TAG, "updateBundle() failed"+ ex);
        }
        bundle.putString(BundleEnums.fragmentName.toString(), fragmentName);
        return bundle;
    }

    public boolean isEndFragment(String fragmentName)
    {
        return (fragmentName.equals(EditNamedDispenseTimeEnum.DispenseTimeSummary.toString()))?
                true:
                false;
    }
}

