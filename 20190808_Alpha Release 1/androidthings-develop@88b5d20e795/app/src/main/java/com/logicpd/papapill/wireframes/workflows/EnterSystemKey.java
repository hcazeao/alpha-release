package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.IncorrectSystemKeyFragment;
import com.logicpd.papapill.fragments.RecoverSystemKeyFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.SystemManagerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;

public class EnterSystemKey extends BaseWireframe {

    public EnterSystemKey(BaseWireframe parentModel) {
        super(parentModel);
    }

    public static String TAG = "EnterSystemKey";

    public enum EnterSystemKeyEnum {
        Home {
            public String toString() {

                return HomeFragment.class.getSimpleName();
            }
        },
        SystemManager {
            public String toString() {

                return SystemManagerFragment.class.getSimpleName();
            }
        },
        ManageUsers {
            public String toString() {

                return ManageUsersFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {

                return SystemKeyFragment.class.getSimpleName();
            }
        },

        // SystemKey failed
        IncorrectSystemKey {
            public String toString() {

                return IncorrectSystemKeyFragment.class.getSimpleName();
            }
        },

        // I forgot
        RecoverSystemKey {
            public String toString() {

                return RecoverSystemKeyFragment.class.getSimpleName();
            }
        },
    }
}
