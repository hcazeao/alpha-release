package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;

public class ExpiredCertificate extends BaseWireframe {
    public static String TAG = "ExpiredCertificate";
    public ExpiredCertificate(BaseWireframe parentModel) {
        super(parentModel);
    }

}
