package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.system_manager.manage_medications.CannotRefillMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.RemoveBinFragment;

public class FromBin extends BaseWireframe {
    public final static String TAG = "FromBin";

    public FromBin(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * clean up any schedule and dispenseTime rows that might have been left from previous
     */
    public void initialize() {
        cleanup();
    }

    public enum FromBinEnum {
        CannotRetrieveMed {
            public String toString() {return CannotRefillMedicationFragment.class.getSimpleName();}
        },
        RemoveBin {
            public String toString() {return RemoveBinFragment.class.getSimpleName();}
        }
    }
}
