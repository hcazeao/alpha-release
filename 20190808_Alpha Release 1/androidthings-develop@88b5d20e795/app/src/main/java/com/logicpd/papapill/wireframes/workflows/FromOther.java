package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class FromOther extends BaseWireframe {
    public static String TAG = "FromOther";
    public FromOther(BaseWireframe parentModel) {
        super(parentModel);
    }

}
