package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.my_medication.GetAsNeededMedicationFragment;

public class Home extends BaseWireframe {
    public static String TAG = "Home";

    public Home(BaseWireframe parentModel) {
        super(parentModel);
    }


    public enum HomeEnum {
        Home {
            public String toString() {

                return HomeFragment.class.getSimpleName();
            }
        }
    }
}
