package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class LoadRefrigerator extends BaseWireframe {
    public static String TAG = "LoadRefrigerator";

    public LoadRefrigerator(BaseWireframe parentModel) {
        super(parentModel);
    }

}
