package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.SystemManagerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactInfoFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactListFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.NotificationSettingRepository;
import com.logicpd.papapill.room.repositories.UserRepository;

import java.util.List;

public class ManageContacts extends BaseWireframe {
    public static final String TAG = "ManageContacts";
    public ManageContacts(BaseWireframe parentModel) {
        super(parentModel);
    }

    public enum ManageContactsEnum {
        Home {
            public String toString() {
                return HomeFragment.class.getSimpleName();
            }
        },
        SystemManager {
            public String toString() {
                return SystemManagerFragment.class.getSimpleName();
            }
        },
        ManageUsers {
            public String toString() {
                return ManageUsersFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        ContactList {
            public String toString() {
                return ContactListFragment.class.getSimpleName();
            }
        },
        ContactInfo {
            public String toString() {
                return ContactInfoFragment.class.getSimpleName();
            }
        }
    }

    /*
     * Each user is associated to at least 1 contact.
     * This is a special contact with PIN recovery privilege.
     * For now, this contact 'cannot' be dis-associated from user.
     * DeleteContact workflow cannot remove this contact.
     */
    public boolean isRecoveryContactId(int contactId){
        List<UserEntity> users = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        for(UserEntity user : users){
            if(contactId == user.getRecoveryContactId())
                return true;
        }
        return false;
    }

    /*
     * Check if a contact has notification(s)
     */
    public boolean hasNotifications4Contact(int contactId){
        List<NotificationSettingEntity> list = new NotificationSettingRepository().readAllByContactId(contactId);
        return (null==list || list.size()==0)?
                false:
                true;
    }
}
