package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class PauseMedication extends BaseWireframe {
    public static String TAG = "PauseMedication";
    public PauseMedication(BaseWireframe parentModel) {
        super(parentModel);
    }

}
