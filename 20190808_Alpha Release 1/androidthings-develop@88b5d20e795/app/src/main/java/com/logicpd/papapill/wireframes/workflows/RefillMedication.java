package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.enums.MedLocaEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.BasePlaceMedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ConfirmMedMaxUnitMinTimeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ConfirmPatientNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ConfirmRefillDosageFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ConfirmRefillMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.CannotRefillMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.IncorrectPatientNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.MedicationFitInstructionsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.MedicationQuantityFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.MedicationQuantityMessageFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.MedicationRefilledFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.MedicationRemovedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.OtherLocationNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.PlaceMedDrawerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.PlaceMedOtherFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.PlaceMedRefrigeratorFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.RemoveBinFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectBinNotRefillMedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectRefillMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectUseByDateFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectUserForRefillFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;

import java.util.List;

public class RefillMedication extends BaseWireframe {
    public final static String TAG = "RefillMedication";


    public RefillMedication(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * used in SelectUserForRefill
     */
    public String getUserName() {
        UserEntity user = (UserEntity) this.getMapItem("user");
        if(null == user)
            return "";

        return user.getUserName();
    }

    /*
     * clean up any schedule and dispenseTime rows that might have been left from previous
     */
    public void initialize() {
        cleanup();
    }

    /*
     * After dispense, it is necessary to remove schedule and dispenseTime
     */
    @Override
    public void cleanup() {
    }

    /*
     * used in IncorrectPatientNameFragment
     * Get the user that is not selected
     * Return: null if no other user available
     */
    public UserEntity getOtherUser() {
        List<UserEntity> users = this.getUsers();
        if(users.size()<2)
            return null;

        UserEntity selected = this.getUser();
        return (users.get(0).getId() == selected.getId())?
                users.get(1):
                users.get(0);
    }

    /*
     * used in SelectRefillMedicationFragment
     */
    public List<MedicationEntity> getMedications4User() {
        UserEntity user = this.getUser();
        return new MedicationRepository().getByUserId(user.getId());
    }

    /*
     * this can be better described as a graph or nodes
     */
    public enum RefillMedicationEnum {
        ManageMeds {
            public String toString() {
                return ManageMedsFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        SelectUserForRefill {
            public String toString() { return SelectUserForRefillFragment.class.getSimpleName(); }
        },
        SelectRefillMedication {
            public String toString() { return SelectRefillMedicationFragment.class.getSimpleName(); }
        },

        // SelectRefillMedication -> rejected
        SelectBinNotRefillMed {
            public String toString() { return SelectBinNotRefillMedFragment.class.getSimpleName(); }
        },
        // SelectRefillMedication -> happy path
        ConfirmPatientName {
            public String toString() { return ConfirmPatientNameFragment.class.getSimpleName();}
        },

        // ConfirmPatientName -> CORRECT
        ConfirmRefillMedication {
            public String toString() { return ConfirmRefillMedicationFragment.class.getSimpleName();}
        },
        // ConfirmPatientName -> INCORRECT
        IncorrectPatientName {
            public String toString() { return IncorrectPatientNameFragment.class.getSimpleName(); }
        },

        // ConfirmRefillMedication -> CORRECT
        ConfirmRefillDosage {
            public String toString() {return ConfirmRefillDosageFragment.class.getSimpleName(); }
        },
        // ConfirmRefillDosage/ConfirmRefillMedication -> INCORRECT
        CannotRefillMedication {
            public String toString() { return CannotRefillMedicationFragment.class.getSimpleName(); }
        },

        // ConfirmRefillDosage -> CORRECT
        MedicationQuantityMessage {
            public String toString() { return MedicationQuantityMessageFragment.class.getSimpleName(); }
        },

        // MedicationQuantityMessage -> happy path
        MedicationQuantity {
            public String toString() { return MedicationQuantityFragment.class.getSimpleName(); }
        },

        // MedicationQuantity -> happy path
        SelectUseByDate {
            public String toString() { return SelectUseByDateFragment.class.getSimpleName();}
        },

        // SelectUseByDate -> happy path
        ConfirmMedMaxUnitMinTime {
            public String toString() { return ConfirmMedMaxUnitMinTimeFragment.class.getSimpleName(); }
        },

        // Load workflow [bin, other, fridge, drawer]

        MedicationRefilled {
            public String toString() { return MedicationRefilledFragment.class.getSimpleName(); }
        }
    }

    @Override
    public String getNextFragmentName(String currentFragmentName){
        switch (currentFragmentName){
            // returning from SelectBinNotRefillMed->Remove med
            case MedicationRemovedFragment.TAG:
                return RefillMedicationEnum.ConfirmPatientName.toString();

            case MedicationQuantityFragment.TAG:
                return RefillMedicationEnum.SelectUseByDate.toString();

            case SelectUseByDateFragment.TAG:
                if(getMedication().getMedicationScheduleType() == MedScheduleTypeEnum.SCHEDULED.value) {
                    return getLoadMedLocationFragment();
                }
                else {
                    return RefillMedicationEnum.ConfirmMedMaxUnitMinTime.toString();
                }

            case ConfirmMedMaxUnitMinTimeFragment.TAG:
                return getLoadMedLocationFragment();

            case BasePlaceMedFragment.TAG:
            case MedicationFitInstructionsFragment.TAG:
            case PlaceMedDrawerFragment.TAG:
            case PlaceMedRefrigeratorFragment.TAG:
            case PlaceMedOtherFragment.TAG:
                return RefillMedicationEnum.MedicationRefilled.toString();
        }
        return RefillMedicationEnum.ManageMeds.toString();
    }

    public String getLoadMedLocationFragment() {
        MedLocaEnum location;

        try {
            location = MedLocaEnum.valueOf(getMedication().getMedicationLocationName());
        }
        catch (Exception ex){
            // other gets a user custom name; so it will likely fail
            location = MedLocaEnum.OTHER;
        }

        switch(location) {
            case BIN:// 0->14
                return RemoveBinFragment.class.getSimpleName();

            case DRAWER:
                return PlaceMedDrawerFragment.class.getSimpleName();

            case REFRIGERATOR:
                return PlaceMedRefrigeratorFragment.class.getSimpleName();

            default:
            case OTHER:
                return OtherLocationNameFragment.class.getSimpleName();
        }
    }
}
