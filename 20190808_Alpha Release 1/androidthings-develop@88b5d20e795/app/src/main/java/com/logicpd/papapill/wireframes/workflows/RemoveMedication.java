package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ConfirmRemoveMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.MedicationRemovedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.RemoveDrawerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.RemoveFridgeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.RemoveMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.RemoveOtherFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectRefillMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectRemoveMedicationFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectUserForRefillFragment;

public class RemoveMedication extends BaseWireframe {
    public final static String TAG = "RemoveMedication";

    public enum RemoveMedicationEnum {
        ManageMeds {
            public String toString() {
                return ManageMedsFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        SelectUserForRefill {
            public String toString() { return SelectUserForRefillFragment.class.getSimpleName(); }
        },
        SelectRemoveMedication {
            public String toString() { return SelectRemoveMedicationFragment.class.getSimpleName(); }
        },
        ConfirmRemoveMedication {
            public String toString() { return ConfirmRemoveMedicationFragment.class.getSimpleName(); }
        },

        /*
         * TODO: identify these in between fragments
         */

        RemoveMedication {
            public String toString() {
                return RemoveMedicationFragment.class.getSimpleName();
            }
        },
        RemoveDrawer {
            public String toString() {
                return RemoveDrawerFragment.class.getSimpleName();
            }
        },
        RemoveFridge {
            public String toString() {
                return RemoveFridgeFragment.class.getSimpleName();
            }
        },
        RemoveOther {
            public String toString() {
                return RemoveOtherFragment.class.getSimpleName();
            }
        },

        // RemoveMedication/RemoveDrawer/RemoveFridge/RemoveOther -> MedicationRemoved
        MedicationRemoved {
            public String toString() {
                return MedicationRemovedFragment.class.getSimpleName();
            }
        }
    }

    public RemoveMedication(BaseWireframe parentModel) {
        super(parentModel);
    }

    public String getNextFragmentName(String currentFragmentName) {

        switch (currentFragmentName) {
            case SelectUserForRefillFragment.TAG:
                return RemoveMedicationEnum.SelectRemoveMedication.toString();

            case SelectRemoveMedicationFragment.TAG:
                return RemoveMedicationEnum.ConfirmRemoveMedication.toString();

            case RemoveFridgeFragment.TAG:
            case RemoveDrawerFragment.TAG:
            case RemoveOtherFragment.TAG:
            case RemoveMedicationFragment.TAG:
                return RemoveMedicationEnum.MedicationRemoved.toString();

            case MedicationRemovedFragment.TAG:
                return RemoveMedicationEnum.ManageMeds.toString();
        }

        if(null==parent)
            return parent.getNextFragmentName(currentFragmentName);

        return ManageMedsFragment.class.getSimpleName();
    }
}
