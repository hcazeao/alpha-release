package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class ReviewMedicationSchedule extends BaseWireframe {
    public static String TAG = "ReviewMedicationSchedule";
    public ReviewMedicationSchedule(BaseWireframe parentModel) {
        super(parentModel);
    }

}
