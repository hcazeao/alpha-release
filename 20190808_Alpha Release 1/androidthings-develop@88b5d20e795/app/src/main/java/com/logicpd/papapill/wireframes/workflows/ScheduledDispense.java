package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.os.Bundle;

import com.logicpd.papapill.fragments.my_medication.AlarmReceivedFragment;
import com.logicpd.papapill.fragments.my_medication.HowUFeelingFragment;
import com.logicpd.papapill.fragments.my_medication.InsufficientMedicationSupplyFragment;
import com.logicpd.papapill.fragments.my_medication.ScheduledMedicationTakenFragment;
import com.logicpd.papapill.fragments.user_settings.UserPinFragment;

public class ScheduledDispense extends BaseWireframe {
    public final static String TAG = "ScheduledDispense";

    public enum ScheduledDispenseEnum {
        AlarmReceived {
            public String toString() {
                return AlarmReceivedFragment.class.getSimpleName();
            }
        },
        UserPin {
            public String toString() {
                return UserPinFragment.class.getSimpleName();
            }
        },

        /*
         * workflow: bin / drawer / fridge / other
         */

        HowUFeeling {
            public String toString() {
                return HowUFeelingFragment.class.getSimpleName();
            }
        },
        ScheduledMedicationTaken {
            public String toString() {
                return ScheduledMedicationTakenFragment.class.getSimpleName();
            }
        },

        /* Alternate destination from UserPin */
        InsufficientMedicationSupply {
            public String toString() {
                return InsufficientMedicationSupplyFragment.class.getSimpleName();
            }
        }
    }

    public ScheduledDispense(BaseWireframe parentModel){
        super(parentModel);
    }

    public Bundle createBundle() {
        Bundle bundle = new Bundle();
        return bundle;
    }

    public Bundle updateBundle(Bundle bundle,
                               String currentFragmentName) {
        return null;
    }

    public boolean isEndFragment(String fragmentName)
    {
        return (fragmentName.equals(ScheduledDispenseEnum.ScheduledMedicationTaken.toString()))?true:false;
    }
}
