package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class SelectColorTheme extends BaseWireframe {
    public static String TAG = "SelectColorTheme";
    public SelectColorTheme(BaseWireframe parentModel) {
        super(parentModel);
    }

}
