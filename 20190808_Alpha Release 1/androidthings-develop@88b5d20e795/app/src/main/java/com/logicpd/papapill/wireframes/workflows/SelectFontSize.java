package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class SelectFontSize extends BaseWireframe {
    public static String TAG = "SelectFontSize";
    public SelectFontSize(BaseWireframe parentModel) {
        super(parentModel);
    }

}
