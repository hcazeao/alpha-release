package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.os.Bundle;
import android.util.Log;

import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.my_medication.GetScheduledMedEarlyFragment;
import com.logicpd.papapill.fragments.my_medication.GetSingleDoseEarlyFragment;
import com.logicpd.papapill.fragments.my_medication.MedicationTakenEarlyFragment;
import com.logicpd.papapill.fragments.my_medication.MyMedicationFragment;
import com.logicpd.papapill.fragments.my_medication.SelectUserForGetMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeSummaryFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.List;

public class SingleDoseEarly extends BaseWireframe {
    public final static String TAG = "SingleDoseEarly";

    public enum SingleDoseEarlyEnum {
        MyMedication {
            public String toString() {
                return MyMedicationFragment.class.getSimpleName();
            }
        },
        SelectUserForGetMeds {
            public String toString() {
                return SelectUserForGetMedsFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        GetScheduledMedEarly {
            public String toString() {
                return GetScheduledMedEarlyFragment.class.getSimpleName();
            }
        },
        GetSingleDoseEarly {
            public String toString() {
                return GetSingleDoseEarlyFragment.class.getSimpleName();
            }
        },

        /*
         * BIN, DRAWER, FRIDGE, OTHER
         */
        MedicationTakenEarly {
            public String toString() {
                return MedicationTakenEarlyFragment.class.getSimpleName();
            }
        },
    }

    public SingleDoseEarly(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * Wireframe for workflow SingleDoseEarly
     */
    public Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(BundleEnums.wireframe.toString(), TAG);
        bundle.putSerializable(BundleEnums.model.toString(), this );
        bundle.putString(BundleEnums.fragmentName.toString(), GetScheduledMedEarlyFragment.class.getSimpleName());
        bundle.putString(BundleEnums.endFragment.toString(), MedicationTakenEarlyFragment.class.getSimpleName());
        bundle.putString(BundleEnums.homeFragment.toString(), MyMedicationFragment.class.getSimpleName());
        return bundle;
    }

    /*
     * Update bundle for next fragment
     */
    public Bundle updateBundle(Bundle bundle,
                               String currentFragmentName) {
        String fragmentName = SingleDoseEarlyEnum.MyMedication.toString();

        try {
            SingleDoseEarlyEnum valueEnum = SingleDoseEarlyEnum.valueOf(currentFragmentName);
            switch (valueEnum) {
                case MyMedication:
                    fragmentName = SingleDoseEarlyEnum.SelectUserForGetMeds.toString();
                    break;

                case SelectUserForGetMeds:
                    fragmentName = SingleDoseEarlyEnum.SystemKey.toString();
                    break;

                case SystemKey:
                    fragmentName = SingleDoseEarlyEnum.GetScheduledMedEarly.toString();
                    break;

                case GetScheduledMedEarly:
                    fragmentName = SingleDoseEarlyEnum.GetSingleDoseEarly.toString();
                    break;

                case GetSingleDoseEarly:
                    fragmentName = SingleDoseEarlyEnum.MedicationTakenEarly.toString();
                    break;

                default:
                case MedicationTakenEarly:
                    fragmentName = SingleDoseEarlyEnum.MyMedication.toString();
                    break;
            }
        }
        catch (Exception ex)
        {
            Log.e(TAG, "updateBundle() failed"+ ex);
        }
        bundle.putString(BundleEnums.fragmentName.toString(), fragmentName);
        return bundle;
    }

    public boolean isEndFragment(String fragmentName)
    {
        return (fragmentName.equals(SingleDoseEarlyEnum.MedicationTakenEarly.toString()))?
                true:
                false;
    }
}
