package com.logicpd.papapill;

import com.logicpd.papapill.utils.CalendarUtils;

import org.junit.Test;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class CalendarUtilsUnitTest {

    @Test
    public void setCalendarToNextDispenseTime_Test() {

        Calendar now = Calendar.getInstance();
        Calendar c = Calendar.getInstance();

        for (int hours = 0; hours < 24; hours++) {
            for (int minutes = 0; minutes < 60; minutes++) {

                Date date = new Date();
                now.setTime(date);

                CalendarUtils.setCalendarToNextDispenseTime(c, hours, minutes);

                assertEquals(c.get(Calendar.HOUR_OF_DAY), hours);
                assertEquals(c.get(Calendar.MINUTE), minutes);

                int minElapseMinutes = 0;
                int maxElapseMinutes = 60*24;

                long elapseMin = Duration.between(now.toInstant(), c.toInstant()).toMinutes();

                assertTrue(minElapseMinutes <= elapseMin && elapseMin < maxElapseMinutes);
            }
        }
    }

    @Test
    public void setCalendarToNextDispenseTimeOnDayOfWeek_Test() {

        Calendar now = Calendar.getInstance();
        Calendar c = Calendar.getInstance();

        for (int hours = 0; hours < 24; hours++) {
            for (int minutes = 0; minutes < 60; minutes++) {
                for (int dayOfWeek = 1; dayOfWeek <= 7; dayOfWeek++) {

                    Date date = new Date();
                    now.setTime(date);

                    CalendarUtils.setCalendarToNextDispenseTimeOnDayOfWeek(c, hours, minutes, dayOfWeek);

                    assertEquals(c.get(Calendar.DAY_OF_WEEK), dayOfWeek);
                    assertEquals(c.get(Calendar.HOUR_OF_DAY), hours);
                    assertEquals(c.get(Calendar.MINUTE), minutes);

                    int minElapseMinutes = 0;
                    int maxElapseMinutes = 60*24*7; // 60 min/hr, 24 hr/day, 7 days/wk

                    long elapseMin = Duration.between(now.toInstant(), c.toInstant()).toMinutes();

                    assertTrue(minElapseMinutes <= elapseMin && elapseMin < maxElapseMinutes);
                }
            }
        }
    }
}
