package com.logicpd.papapill.espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.logicpd.papapill.App;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.repositories.SystemRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.room.utils.SimpleTimeConverter;
import com.logicpd.papapill.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BaseJiraTest {

    protected UserEntity user;
    protected  int[] contactIds;
    protected  int[] userIds;
    protected  int[] medIds;

    public void initialize()
    {
        Context appContext = InstrumentationRegistry.getTargetContext();
        PreferenceUtils.setFirstTimeRun(false);
        AppDatabase.deleteAll();
        new SystemRepository().insert(AppConstants.DEFAULT_SYSTEM_KEY);

        contactIds = new int[2];
        contactIds[0] = insertContact("abby@mail.com");
        contactIds[1] = insertContact("britany@mail.com");

        userIds = new int[2];
        userIds[0] = insertUser(contactIds[0], "abby", "1234", "abby");
        userIds[1] = insertUser(contactIds[1], "britany", "1234", "britany");

        medIds = new int[5];
        medIds[0] = insertMedication(userIds[0], "amoxicillin", "1mg", 4, 5,"abby", MedScheduleTypeEnum.BOTH, MedPausedEnum.NOT_PAUSED);
        medIds[1] = insertMedication(userIds[0], "botox", "2mg", 2, 5,"abby", MedScheduleTypeEnum.AS_NEEDED, MedPausedEnum.NOT_PAUSED);
        medIds[2] = insertMedication(userIds[0], "claritin", "3mg", 3, 5,"abby", MedScheduleTypeEnum.SCHEDULED, MedPausedEnum.IS_PAUSED);
        medIds[3] = insertMedication(userIds[0], "dulera", "3mg", 3, 5,"abby", MedScheduleTypeEnum.SCHEDULED, MedPausedEnum.NOT_PAUSED);
    }

    protected void cleanup() {
        AppDatabase.deleteAll();
    }

    protected int insertContact(String emailAddress) {
        ContactEntity contact = new ContactEntity();
        contact.setEmail(emailAddress);
        contact.setEmailSelected(true);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(contact);
        int contactId = (int)new ContactRepository().syncOp(CRUDEnum.INSERT, list);
        return contactId;
    }

    protected int insertUser(int contactId,
                                  String username,
                                  String pin,
                                  String patientname) {
        user = new UserEntity();
        user.setUserName(username);
        user.setPin(pin);
        user.setPatientName(patientname);
        user.setRecoveryContactId(contactId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(user);
        int userId = (int)new UserRepository().syncOp(CRUDEnum.INSERT, list);
        return userId;
    }

    protected int insertMedication(int userId,
                                        String medicationName,
                                        String measure,
                                        int maxUnit,
                                        int quantity,
                                        String patientName,
                                        MedScheduleTypeEnum type,
                                        MedPausedEnum isPaused) {
        MedicationEntity medication = new MedicationEntity(userId);
        medication.setMedicationName(medicationName);
        medication.setStrengthMeasurement(measure);
        medication.setMaxUnitsPerDay(maxUnit);
        medication.setPatientName(patientName);
        medication.setMedicationQuantity(quantity);
        medication.setMedicationScheduleType(type.value);
        medication.setPaused(isPaused.value);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(medication);
        int medicationId = (int)new MedicationRepository().syncOp(CRUDEnum.INSERT, list);
        return medicationId;
    }

    protected int createSchedule(int userId,
                               int medId,
                               int dispenseTimeId,
                               ScheduleRecurrenceEnum recurrence,
                               String day,
                               String date,
                               Date nextProcessDate) {
        ScheduleEntity schedule = new ScheduleEntity(userId, medId, dispenseTimeId);
        schedule.setScheduleDay(day);
        schedule.setScheduleDate(date);
        schedule.setRecurrence(recurrence.value);
        schedule.setDispenseAmount(2);
        schedule.setNextProcessDate(nextProcessDate);

        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(schedule);
        Object id = new ScheduleRepository().syncOp(CRUDEnum.INSERT, list);
        return Integer.parseInt(id.toString());
    }

    protected int createDispenseTime(int userId,
                                   String name,
                                   String timeString) {
        // create dispenseTime object
        DispenseTimeEntity dispenseTime = new DispenseTimeEntity();
        dispenseTime.setDispenseTime(timeString);
        dispenseTime.setDispenseName(name);
        dispenseTime.setUserId(userId);
        dispenseTime.setDispenseAmount(2);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(dispenseTime);
        Object id = new DispenseTimeRepository().syncOp(CRUDEnum.INSERT, list);
        return Integer.parseInt(id.toString());
    }

    protected String buildTimeString(long timeInMilli) {
        Date nextDispenseTime = new Date(timeInMilli);
        String timeString = SimpleTimeConverter.dateToTimestamp(nextDispenseTime);
        return timeString;
    }
}
