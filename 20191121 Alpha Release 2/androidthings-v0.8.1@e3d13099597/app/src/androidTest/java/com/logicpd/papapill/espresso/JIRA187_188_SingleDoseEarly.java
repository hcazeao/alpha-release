package com.logicpd.papapill.espresso;

import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.fragments.my_medication.GetScheduledMedEarlyFragment;
import com.logicpd.papapill.fragments.my_medication.SelectPauseMedicationFragment;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.room.utils.SimpleTimeConverter;
import com.logicpd.papapill.room.utils.TimeUtil;
import com.logicpd.papapill.wireframes.BaseWireframe;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class JIRA187_188_SingleDoseEarly extends BaseJiraTest {

    protected List<DispenseTimeEntity> dispenseTimes;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    /*
     * populate database with mock contents
     */
    @Override
   public void initialize() {
        super.initialize();
    }

    /*
     * Execute wireframe critical Single dose fragments after PIN screen.
     * - NOTE: requires build-variant touchscreenDebug (non breadboard)
     */
    @Test
    public void testHappyPath() {
        // mock data for bin dispense
        initialize();

        // create dispense times
        int[] dispenseTimeIds = new int[3];
        long ONE_HOUR = 60 * 60 * 1000;
        Date now = new Date();
        int userId = userIds[0];
        String hourPrior = buildTimeString(now.getTime() - ONE_HOUR);
        dispenseTimeIds[0] = createDispenseTime(userId, "morning", hourPrior);
        String hourLater = buildTimeString(now.getTime() + ONE_HOUR);
        dispenseTimeIds[1] = createDispenseTime(userId, "lunch", hourLater);
        String fivehourLater = buildTimeString(now.getTime() + ONE_HOUR*2);
        dispenseTimeIds[2] = createDispenseTime(userId, "dinner", fivehourLater);

        // create schedule
        int[] scheduleIds = new int[6];
        Date morningProcessDate = new Date(now.getTime() - ONE_HOUR);
        scheduleIds[0] = createSchedule(userId, medIds[0], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);
        scheduleIds[1] = createSchedule(userId, medIds[2], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);

        Date lunchProcessDate = new Date(now.getTime() + ONE_HOUR);
        scheduleIds[2] = createSchedule(userId, medIds[0], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);
        scheduleIds[3] = createSchedule(userId, medIds[2], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);

        Date dinnerProcessDate = new Date(now.getTime() + ONE_HOUR*2);
        scheduleIds[4] = createSchedule(userId, medIds[0], dispenseTimeIds[2], ScheduleRecurrenceEnum.DAILY, null, null, dinnerProcessDate);
        scheduleIds[5] = createSchedule(userId, medIds[2], dispenseTimeIds[2], ScheduleRecurrenceEnum.DAILY, null, null, dinnerProcessDate);

        SystemClock.sleep(1000);

        // load 1st wireframe fragment of this workflow
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        user = model.getUserByID(userId);
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);

        // GetScheduledMedEarlyFragment
        MainActivity main = mActivityRule.getActivity();
        Fragment fragment = new GetScheduledMedEarlyFragment();
        main.addFragment(fragment, bundle, GetScheduledMedEarlyFragment.class.getName());

        /*
         * TO DO: look at notes from previous when testing clicks on multiple screens...
         */

        // click button SINGLE DOSE         GetScheduledMedEarlyFragment
        onView(withId(R.id.button_single_dose)).perform(click());

        // click button DISPENSE NOW        GetSingleDoseEarlyFragment
        onView(withId(R.id.button_dispense_now)).perform(click());


        // click button PROCEED (DEV)       DispenseMedsFragment
        onView(withId(R.id.button_developer)).perform(click());


        // click button PROCEED (DEV)       DispensingMedsFragment
        onView(withId(R.id.button_developer)).perform(click());


        // click button OK                  TakeMedsFragment
        onView(withId(R.id.button_ok)).perform(click());


        // click button OK                  ReturnCupFragment
        onView(withId(R.id.button_ok)).perform(click());


        // click button DONE                MedicationTakenEarlyFragment
        onView(withId(R.id.button_dispense_now)).perform(click());

        cleanup();
    }

    /*
     * Find does that is being dispensed early
     * A dose - a collection of medications and schedules and pill quantities
     * - No Paused
     * - No missed Dose(s)
     * - nextProcessDate is today (meaning not dispensed)
     * - nearest schedule from now
     * - might be today or tomorrow or further out
     *
     * WARNING: this test will fail if run later in the evening (beyond 10:00pm)
     */
    @Test
    public void testQueryDose4Early() {
        initialize();

        int userId = userIds[0];

        // create dispense times
        int[] dispenseTimeIds = new int[3];
        long ONE_HOUR = 60 * 60 * 1000;
        Date now = new Date();
        String hourPrior = buildTimeString(now.getTime() - ONE_HOUR);
        dispenseTimeIds[0] = createDispenseTime(userId, "morning", hourPrior);
        String hourLater = buildTimeString(now.getTime() + ONE_HOUR);
        dispenseTimeIds[1] = createDispenseTime(userId, "lunch", hourLater);
        String fivehourLater = buildTimeString(now.getTime() + ONE_HOUR*2);
        dispenseTimeIds[2] = createDispenseTime(userId, "dinner", fivehourLater);

        // create schedule
        int[] scheduleIds = new int[6];
        Date morningProcessDate = new Date(now.getTime() - ONE_HOUR);
        scheduleIds[0] = createSchedule(userId, medIds[0], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);
        scheduleIds[1] = createSchedule(userId, medIds[2], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);

        Date lunchProcessDate = new Date(now.getTime() + ONE_HOUR);
        scheduleIds[2] = createSchedule(userId, medIds[0], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);
        scheduleIds[3] = createSchedule(userId, medIds[2], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);

        Date dinnerProcessDate = new Date(now.getTime() + ONE_HOUR*2);
        scheduleIds[4] = createSchedule(userId, medIds[0], dispenseTimeIds[2], ScheduleRecurrenceEnum.DAILY, null, null, dinnerProcessDate);
        scheduleIds[5] = createSchedule(userId, medIds[2], dispenseTimeIds[2], ScheduleRecurrenceEnum.DAILY, null, null, dinnerProcessDate);

        // now < nextProcessDate, sorted ASC
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        List<JoinScheduleDispense> list = model.getNextScheduledItems(userId, new Date());
        assertEquals(list.size(), 2);
        assertEquals(list.get(0).getScheduleId(), scheduleIds[2]);
        assertEquals(list.get(1).getScheduleId(), scheduleIds[4]);
        cleanup();
    }

    /*
     * Presume we have dispensed early and updated the schedule,
     * find a schedule that is after and nearest (next day)
     */
    @Test
    public void testQueryDose4EarlyTomorrow() {
        // should be the following item(s) after above result ?
        initialize();
        int userId = userIds[0];

        // create dispense times
        int[] dispenseTimeIds = new int[2];
        long ONE_HOUR = 60 * 60 * 1000;
        long TWENTY_FOUR_HOURS = ONE_HOUR * 24;
        Date now = new Date();
        String hourPrior = buildTimeString(now.getTime() - ONE_HOUR);
        dispenseTimeIds[0] = createDispenseTime(userId, "morning", hourPrior);
        String hourLater = buildTimeString(now.getTime() + ONE_HOUR);
        dispenseTimeIds[1] = createDispenseTime(userId, "lunch", hourLater);

        /*
         * Next Process Date is being updated to 'tomorrow'
         * This simulates the successful 'single dose early'
         */
        int[] scheduleIds = new int[4];
        Date morningProcessDate = new Date(now.getTime() - ONE_HOUR + TWENTY_FOUR_HOURS);
        scheduleIds[0] = createSchedule(userId, medIds[0], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);
        scheduleIds[1] = createSchedule(userId, medIds[2], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);

        Date lunchProcessDate = new Date(now.getTime() + ONE_HOUR + TWENTY_FOUR_HOURS);
        scheduleIds[2] = createSchedule(userId, medIds[0], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);
        scheduleIds[3] = createSchedule(userId, medIds[2], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);

        // the first item on the list is tomorrow morning
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        List<JoinScheduleDispense> list = model.getNextScheduledItems(userId, new Date());
        assertEquals(list.size(), 2);
        assertEquals(list.get(0).getScheduleId(), scheduleIds[0]);
        cleanup();
    }
}
