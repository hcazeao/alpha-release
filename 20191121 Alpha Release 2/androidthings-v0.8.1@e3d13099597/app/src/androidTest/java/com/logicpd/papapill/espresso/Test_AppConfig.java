package com.logicpd.papapill.espresso;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.activities.DeveloperActivity;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.AppConfig;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class Test_AppConfig {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testParseJSON()
    {
        AppConfig appConfig = AppConfig.getInstance();
        String str = getJSON();
        try {
            appConfig.parseJson(str);
            assertTrue(85.732==appConfig.getConvergentRho());
            assertTrue(85.732==appConfig.getRho0());

            assertEquals(1024, appConfig.getImageWidth());
            assertEquals(768, appConfig.getImageHeight());
            assertEquals(3, appConfig.getDetectionsReturned());
            assertTrue(32==appConfig.getDeadzoneRhoLimit());
            assertTrue(4==appConfig.getDeadzoneThetaIntercept());
            assertTrue(-0.00067938==appConfig.getMmm());
            assertTrue(0.155755136== appConfig.getMmb());
            assertTrue(Math.toRadians(1.27)==appConfig.getRotation());
            assertTrue(0==appConfig.getConvergentTheta());
            assertEquals(487, appConfig.getConvergentPixelX());
            assertEquals(367, appConfig.getConvergentPixelY());
            assertTrue(10.50==appConfig.getBinThetaMax());
            assertTrue(-10.50== appConfig.getBinThetaMin());
            assertTrue(40.0== appConfig.getBinRhoMax());
            assertTrue(50.5== appConfig.getBinDepth());
            assertTrue(0.21== appConfig.getTemplateSlope());
            assertTrue(2.02== appConfig.getAbsPositionOffset());
            assertTrue(30==appConfig.getCamera2TinyGRhoOffset());
            assertTrue(0==appConfig.getCamera2TinyGThetaOffset());
            assertEquals(true, appConfig.getCameraAvailable());
            assertEquals(true, appConfig.getTinyGAvailable());
        }
        catch (Exception ex)
        {
            fail("testParseJSON ex:" + ex);
        }
    }

    private String getJSON()
    {
        return "{\"cameraMatrix\": \"2100.19084858603, 0, 512, 0, 2102.014004258706, 384, 0, 0, 1\",\"distortionCoefficients\": \"-2.501651516989398, 3.298401788511177, 0, 0, 0\",\"rho0\": 85.732,\"imageWidth\": 1024,\"imageHeight\": 768,\"detectionsReturned\": 3,\"deadzoneRhoLimit\": 32,\"deadzoneThetaIntercept\": 4,\"mmm\": -0.00067938,\"mmb\": 0.155755136,\"rotation\": 1.27,\"convergentTheta\": 0,\"convergentRho\": 0,\"convergentPixelX\": 487,\"convergentPixelY\": 367,\"binThetaMax\": 10.50,\"binThetaMin\": -10.50,\"binRhoMax\": 40.0,\"binDepth\": 50.5,\"templateSlope\": 0.21,\"absPositionOffset\": 2.02,\"templateUpperLeftXY\": \"0, 247, 215, 1, 246, 214, 2, 245, 214, 3, 244, 213, 4, 243, 212, 5, 242, 212, 6, 241, 211, 7, 240, 210, 8, 239, 210, 9, 238, 209, 10, 236, 208, 11, 235, 207, 12, 234, 207, 13, 233, 206, 14, 232, 205, 15, 230, 205, 16, 229, 204, 17, 228, 203, 18, 227, 202, 19, 226, 201, 20, 224, 201, 21, 223, 200, 22, 222, 199, 23, 221, 198, 24, 219, 197, 25, 218, 196, 26, 217, 196, 27, 215, 195, 28, 214, 194, 29, 213, 193, 30, 211, 192, 31, 210, 191, 32, 209, 190, 33, 207, 189, 34, 206, 189, 35, 204, 188, 36, 203, 187, 37, 201, 186, 38, 200, 185, 39, 198, 184, 40, 197, 183, 41, 195, 182, 42, 194, 181, 43, 192, 180, 44, 190, 179, 45, 189, 178, 46, 187, 177, 47, 185, 176, 48, 184, 175, 49, 182, 174, 50, 180, 173, 51, 179, 172, 52, 177, 170, 53, 175, 169, 54, 173, 168, 55, 172, 167, 56, 170, 166, 57, 168, 165, 58, 166, 163, 59, 164, 162, 60, 162, 161, 61, 160, 160, 62, 158, 159, 63, 157, 157, 64, 154, 156, 65, 152, 155, 66, 150, 153, 67, 148, 152, 68, 146, 151, 69, 144, 149, 70, 142, 148, 71, 140, 147, 72, 138, 145, 73, 135, 144, 74, 133, 142, 75, 131, 141, 76, 128, 140, 77, 126, 138, 78, 124, 137, 79, 121, 135, 80, 119, 133, 81, 116, 132, 82, 114, 130, 83, 111, 129, 84, 109, 127, 85, 106, 125, 86, 103, 124, 87, 101, 122, 88, 98, 120, 89, 95, 118, 90, 92, 117, 91, 89, 115, 92, 87, 113, 93, 84, 111, 94, 81, 109, 95, 78, 107, 96, 75, 105, 97, 71, 103, 98, 68, 101, 99, 65, 99, 100, 62, 97\",\"camera2TinyGRhoOffset\":30,\"camera2TinyGThetaOffset\":0,\"isCameraAvailable\":true,\"isTinyGAvailable\":true }";
    }
}
