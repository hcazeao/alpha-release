package com.logicpd.papapill.espresso;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.utils.TimeUtil;
import com.logicpd.papapill.services.ScheduleProcessor;
import com.logicpd.papapill.wireframes.BaseWireframe;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/*
 * Test for schedule process - trigger for dispense.
 */
@RunWith(AndroidJUnit4.class)
public class Test_ScheduleProcessor extends BaseJiraTest {

    private ScheduleProcessor scheduleProcessor;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    public void initialize()
    {
        // create data for users, meds, contacts
        super.initialize();

        scheduleProcessor = new ScheduleProcessor();
    }

    /*
     * Make sure schedules.nextProcessDate can increment daily, weekly, month to N degree.
     * WARNING: Monthly requires further definition because days of a month are different.
     */
    @Test
    public void testGetNextProcessDate() {
        initialize();
        int userId = userIds[0];

        // create dispense times
        int[] dispenseTimeIds = new int[2];
        long ONE_HOUR = 60 * 60 * 1000;
        long TWENTY_FOUR_HOURS = ONE_HOUR * 24;
        Date now = new Date();
        String hourPrior = buildTimeString(now.getTime() - ONE_HOUR);
        dispenseTimeIds[0] = createDispenseTime(userId, "morning", hourPrior);
        String hourLater = buildTimeString(now.getTime() + ONE_HOUR);
        dispenseTimeIds[1] = createDispenseTime(userId, "lunch", hourLater);
        /*
         * Next Process Date is being updated to 'tomorrow'
         * This simulates the successful 'single dose early'
         */
        int[] scheduleIds = new int[4];
        Date morningProcessDate = new Date(now.getTime() - ONE_HOUR);
        scheduleIds[0] = createSchedule(userId, medIds[0], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);
        scheduleIds[1] = createSchedule(userId, medIds[2], dispenseTimeIds[0], ScheduleRecurrenceEnum.DAILY, null, null, morningProcessDate);

        Date lunchProcessDate = new Date(now.getTime() + ONE_HOUR);
        scheduleIds[2] = createSchedule(userId, medIds[0], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);
        scheduleIds[3] = createSchedule(userId, medIds[2], dispenseTimeIds[1], ScheduleRecurrenceEnum.DAILY, null, null, lunchProcessDate);

        // the first item on the list is tomorrow morning
        BaseWireframe model = mActivityRule.getActivity().getFeatureModel();
        List<JoinScheduleDispense> list = model.getNextScheduledItems(userId, new Date());
        JoinScheduleDispense schedule = list.get(0);

        /*
         * The following date
         */
        Calendar nextProcessDate = scheduleProcessor.getNextProcessDate(schedule, DispenseEventTypeEnum.SCHEDULED);
        int month = nextProcessDate.get(Calendar.MONTH);
        int day = nextProcessDate.get(Calendar.DAY_OF_MONTH);
        int hr = nextProcessDate.get(Calendar.HOUR_OF_DAY);
        int min = nextProcessDate.get(Calendar.MINUTE);

        /*
         * The subsequent following date
         */
        Calendar earlyDoseDate = scheduleProcessor.getNextProcessDate(schedule, DispenseEventTypeEnum.EARLY_DISPENSE);
        int emonth = earlyDoseDate.get(Calendar.MONTH);
        int eday = earlyDoseDate.get(Calendar.DAY_OF_MONTH);
        int ehr = earlyDoseDate.get(Calendar.HOUR_OF_DAY);
        int emin = earlyDoseDate.get(Calendar.MINUTE);

        assertEquals(min, emin);
        assertEquals(hr, ehr);

        if(eday > day) {
            assertEquals(eday-1, day);
            assertEquals(emonth, month);
        }
        else if(month==12){
            assertEquals(emonth, 0);
            assertEquals(eday, 1);
        }
        else {
            assertEquals(emonth-1, month);
            assertEquals(eday, 1);
        }

        int today = now.getDate();
        assertEquals(day, today);

        cleanup();
    }

    @Test
    public void testProcessDispenseEvents()
    {
        //initialize();
    }

    @Test
    public void testProcessSchedule()
    {
        initialize();
    }
}
