package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_MedicationDAO extends BaseTestDAO{
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    public void cleanup()
    {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.medicationDao().deleteAll();
        db.userDao().deleteAll();
    }

    @Test
    public void testCRUDSingleRow() {
        if(null==db)
            initialize();

        // create
        int userId = (int)(long)userIds[0];
        MedicationEntity medicationInsert = new MedicationEntity(userId);
        medicationInsert.setMedicationName("name");
        medicationInsert.setMedicationNickname("nickname");
        medicationInsert.setStrengthValue(2);
        medicationInsert.setStrengthMeasurement("measure");
        medicationInsert.setDosageInstructions("instruction");
        medicationInsert.setTimeBetweenDoses(3);
        medicationInsert.setMaxUnitsPerDay(4);
        medicationInsert.setMaxNumberPerDose(5);
        medicationInsert.setMedicationQuantity(6);
        medicationInsert.setUseByDate("date");
        medicationInsert.setFillDate("fillDate");
        medicationInsert.setMedicationLocation(7);
        medicationInsert.setMedicationLocationName("location");
        medicationInsert.setPaused(true);
        medicationInsert.setMedicationScheduleType(8);
        medicationInsert.setPatientName("Patient");

        // delete
        db.medicationDao().deleteAll();

        // insert -- db returns id created
        Long id = db.medicationDao().insert(medicationInsert);

        // read
        MedicationEntity medicationRead = db.medicationDao().get((int)(long)id);
        assertEquals(medicationRead.getMedicationName(), "name");
        assertEquals(medicationRead.getMedicationNickname(), "nickname");
        assertEquals(medicationRead.getUserId(), userId);
        assertEquals(medicationRead.getStrengthValue(), 2);
        assertEquals(medicationRead.getStrengthMeasurement(), "measure");
        assertEquals(medicationRead.getDosageInstructions(), "instruction");
        assertEquals(medicationRead.getTimeBetweenDoses(), 3);
        assertEquals(medicationRead.getMaxUnitsPerDay(), 4);
        assertEquals(medicationRead.getMaxNumberPerDose(), 5);
        assertEquals(medicationRead.getMedicationQuantity(), 6);
        assertEquals(medicationRead.getUseByDate(), "date");
        assertEquals(medicationRead.getFillDate(), "fillDate");
        assertEquals(medicationRead.getMedicationLocation(), 7);
        assertEquals(medicationRead.getMedicationLocationName(), "location");
        assertEquals(medicationRead.isPaused(), true);
        assertEquals(medicationRead.getMedicationScheduleType(), 8);
        assertEquals(medicationRead.getPatientName(), "Patient");
        cleanup();
    }

    @Test
    public void testCRUDManyRows() {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        db.medicationDao().insertAll(list);

        // read
        List<MedicationEntity> listRead = db.medicationDao().getAll();
        int i=0;
        for(MedicationEntity medicationRead : listRead)
        {
            int userId = (int)(long)userIds[i];
            assertEquals(medicationRead.getMedicationName(), "name"+i);
            assertEquals(medicationRead.getMedicationNickname(), "nickname"+i);
            assertEquals(medicationRead.getUserId(), userId);
            assertEquals(medicationRead.getStrengthValue(), 2+i);
            assertEquals(medicationRead.getStrengthMeasurement(), "measure"+i);
            assertEquals(medicationRead.getDosageInstructions(), "instruction"+i);
            assertEquals(medicationRead.getTimeBetweenDoses(), 3+i);
            assertEquals(medicationRead.getMaxUnitsPerDay(), 4+i);
            assertEquals(medicationRead.getMaxNumberPerDose(), 5+i);
            assertEquals(medicationRead.getMedicationQuantity(), 6+i);
            assertEquals(medicationRead.getUseByDate(), "date"+i);
            assertEquals(medicationRead.getFillDate(), "fillDate"+i);
            assertEquals(medicationRead.getMedicationLocation(), 7+i);
            assertEquals(medicationRead.getMedicationLocationName(), "location"+i);
            MedPausedEnum isPaused = (0==i%2)?MedPausedEnum.IS_PAUSED:MedPausedEnum.NOT_PAUSED;
            assertEquals(medicationRead.isPaused(), isPaused.value);
            MedScheduleTypeEnum scheduleType = MedScheduleTypeEnum.values()[i%3];
            assertEquals(medicationRead.getMedicationScheduleType(), scheduleType.value);
            assertEquals(medicationRead.getPatientName(), "Patient"+i);
            i++;
        }
        cleanup();
    }

    @Test
    public void testGetPatientNames()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert -- db returns a list of ids
        List<MedicationEntity> list = createManyRows();
        Long[] ids = db.medicationDao().insertAll(list);

        // read
        int userId = (int)(long)userIds[1];
        List<String> names = db.medicationDao().getPatientNames(userId);
        assertEquals(names.get(0), "Patient1");
        cleanup();
    }

    @Test
    public void testGetCountByMedicationNameStrengthMeasurement()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        Long[] ids = db.medicationDao().insertAll(list);

        // read
        int count = db.medicationDao().getCountByMedicationNameStrengthMeasurement("name2", "measure2");
        assertEquals(count, 1);

        count = db.medicationDao().getCountByMedicationNameStrengthMeasurement("name2", "measure3");
        assertEquals(count, 0);
        cleanup();
    }

    @Test
    public void testGetSortedBin()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        db.medicationDao().insertAll(list);

        // read
        List<MedicationEntity> listSorted = db.medicationDao().getSortedBin();
        assertEquals(listSorted.size(), 10);
        int i=0;
        for(MedicationEntity medicationEntity : listSorted)
        {
            assertEquals(medicationEntity.getPatientName(), "Patient"+i );
            i++;
        }
        cleanup();
    }

    @Test
    public void testGetByUserId()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        Long[] ids = db.medicationDao().insertAll(list);

        // read
        int userId = (int)(long)userIds[1];
        List<MedicationEntity> listSorted = db.medicationDao().getByUserId(userId);
        assertEquals(listSorted.get(0).getPatientName(), "Patient1");
        cleanup();
    }

    @Test
    public void testGetByUserIdScheduleType()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        Long[] ids = db.medicationDao().insertAll(list);

        // read
        int userId = (int)(long)userIds[1];
        List<MedicationEntity> listSorted = db.medicationDao().getByUserIdScheduleType(userId, MedScheduleTypeEnum.AS_NEEDED.value, MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listSorted.get(0).getPatientName(), "Patient1");
        cleanup();
    }

    @Test
    public void testCascade()
    {
        if(null==db)
            initialize();

        // delete
        db.userDao().deleteAll();
        db.medicationDao().deleteAll();

        // insert
        UserEntity user = new UserEntity();
        user.setUserName("user");
        user.setPin("pin");
        user.setPatientName("patient");
        int userId = (int)(long)db.userDao().insert(user);

        MedicationEntity medicationEntity = new MedicationEntity(userId);
        medicationEntity.setUserId((int)(long)userId);
        medicationEntity.setMedicationName("name");
        medicationEntity.setMaxUnitsPerDay(10);
        Long medId = db.medicationDao().insert(medicationEntity);

        // cascade delete automatically removes dependencies
        db.userDao().delete((int)(long)userId);
        MedicationEntity nonExist = db.medicationDao().get((int)(long)medId);
        assertEquals(nonExist, null);
        cleanup();
    }

    @Test
    public void testGetMedicationPaused()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        db.medicationDao().insertAll(list);

        // read
        List<MedicationEntity> listRead = db.medicationDao().getAll(MedPausedEnum.IS_PAUSED.value);
        assertEquals(listRead.size(), 5);

        listRead = db.medicationDao().getAll(MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listRead.size(), 5);
        cleanup();
    }

    @Test
    public void testGetAsNeededMedication()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        db.medicationDao().insertAll(list);

        // read

        // schedule type: BOTH, IS_PAUSED
        List<MedicationEntity> listRead = db.medicationDao().getAsNeededByUserId((int)(long)userIds[0], MedPausedEnum.IS_PAUSED.value);
        assertEquals(listRead.size(), 1);

        listRead = db.medicationDao().getAsNeededByUserId((int)(long)userIds[0], MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listRead.size(), 0);

        // schedule type: AS_NEEDED, NOT_PAUSED
        listRead = db.medicationDao().getAsNeededByUserId((int)(long)userIds[1], MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listRead.size(), 1);

        listRead = db.medicationDao().getAsNeededByUserId((int)(long)userIds[1], MedPausedEnum.IS_PAUSED.value);
        assertEquals(listRead.size(), 0);

        // schedule type: SCHEDULED, NOT_PAUSED
        listRead = db.medicationDao().getAsNeededByUserId((int)(long)userIds[2], MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listRead.size(), 0);

        listRead = db.medicationDao().getAsNeededByUserId((int)(long)userIds[2], MedPausedEnum.IS_PAUSED.value);
        assertEquals(listRead.size(), 0);
        cleanup();
    }

    @Test
    public void testGetScheduledMedication()
    {
        if(null==db)
            initialize();

        // delete
        db.medicationDao().deleteAll();

        // insert
        List<MedicationEntity> list = createManyRows();
        db.medicationDao().insertAll(list);

        // read

        // schedule type: BOTH, IS_PAUSED
        List<MedicationEntity> listRead = db.medicationDao().getScheduledByUserId((int)(long)userIds[0], MedPausedEnum.IS_PAUSED.value);
        assertEquals(listRead.size(), 1);

        listRead = db.medicationDao().getScheduledByUserId((int)(long)userIds[0], MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listRead.size(), 0);

        // schedule type: AS_NEEDED, NOT_PAUSED
        listRead = db.medicationDao().getScheduledByUserId((int)(long)userIds[1], MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listRead.size(), 0);

        listRead = db.medicationDao().getScheduledByUserId((int)(long)userIds[1], MedPausedEnum.IS_PAUSED.value);
        assertEquals(listRead.size(), 0);

        // schedule type: SCHEDULED, NOT_PAUSED
        listRead = db.medicationDao().getScheduledByUserId((int)(long)userIds[2], MedPausedEnum.NOT_PAUSED.value);
        assertEquals(listRead.size(), 0);

        listRead = db.medicationDao().getScheduledByUserId((int)(long)userIds[2], MedPausedEnum.IS_PAUSED.value);
        assertEquals(listRead.size(), 1);
        cleanup();
    }

    private List<MedicationEntity> createManyRows()
    {
        List<MedicationEntity> list = new ArrayList<MedicationEntity>();
        for(int i=0; i<10; i++)
        {
            int userId = (int)(long)userIds[i];
            MedicationEntity medicationInsert = new MedicationEntity(userId);
            medicationInsert.setMedicationName("name"+i);
            medicationInsert.setMedicationNickname("nickname"+i);
            medicationInsert.setStrengthValue(2+i);
            medicationInsert.setStrengthMeasurement("measure"+i);
            medicationInsert.setDosageInstructions("instruction"+i);
            medicationInsert.setTimeBetweenDoses(3+i);
            medicationInsert.setMaxUnitsPerDay(4+i);
            medicationInsert.setMaxNumberPerDose(5+i);
            medicationInsert.setMedicationQuantity(6+i);
            medicationInsert.setUseByDate("date"+i);
            medicationInsert.setFillDate("fillDate"+i);
            medicationInsert.setMedicationLocation(7+i);
            medicationInsert.setMedicationLocationName("location"+i);
            MedPausedEnum isPaused = (0==i%2)?MedPausedEnum.IS_PAUSED:MedPausedEnum.NOT_PAUSED;
            medicationInsert.setPaused(isPaused.value);
            MedScheduleTypeEnum scheduleType = MedScheduleTypeEnum.values()[i % 3];
            medicationInsert.setMedicationScheduleType(scheduleType.value);
            medicationInsert.setPatientName("Patient"+i);
            list.add(medicationInsert);
        }
        return list;
    }
}
