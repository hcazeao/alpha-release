package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;

import org.bouncycastle.jce.provider.symmetric.ARC4;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class Test_ScheduleDAO extends BaseTestDAO {

    private Long[] medicationIds;
    private Long[] dispenseIds;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    public void cleanup()
    {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.scheduleDao().deleteAll();
        db.userDao().deleteAll();
    }

    @Test
    public void testCRUDSingleRow() {
        if(null==db)
            initialize();

        // create
        int userId = (int)(long)userIds[0];
        int medId = (int)(long)medicationIds[0];
        int dispenseId = (int)(long)dispenseIds[0];
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medId, dispenseId);
        scheduleEntity.setDispenseAmount(5);
        scheduleEntity.setRecurrence(6);
        scheduleEntity.setScheduleDate("date");
        scheduleEntity.setScheduleDay("day");

        // delete
        db.scheduleDao().deleteAll();

        // insert
        Long id = db.scheduleDao().insert(scheduleEntity);

        // read
        ScheduleEntity scheduleRead = db.scheduleDao().get((int)(long)id);
        assertEquals(scheduleRead.getUserId(), userId);
        assertEquals(scheduleRead.getMedicationId(), medId);
        assertEquals(scheduleRead.getDispenseTimeId(), dispenseId);
        assertEquals(scheduleRead.getDispenseAmount(), 5);
        assertEquals(scheduleRead.getRecurrence(), 6);
        assertEquals(scheduleRead.getScheduleDate(), "date");
        assertEquals(scheduleRead.getScheduleDay(), "day");

        // delete single
        db.scheduleDao().delete((int)(long)id);
        ScheduleEntity nonExist = db.scheduleDao().get((int)(long)id);
        assertEquals(nonExist, null);
        cleanup();
    }

    @Test
    public void testCRUDManyRows() {
        if(null==db)
            initialize();

        // delete
        db.scheduleDao().deleteAll();

        // insert
        List<ScheduleEntity> list = createManyRows();
        Long[] ids = db.scheduleDao().insertAll(list);

        // read
        List<ScheduleEntity> listRead = db.scheduleDao().getAll();
        int i=0;
        for(ScheduleEntity scheduleRead : listRead)
        {
            int userId = (int)(long)userIds[i];
            assertEquals(scheduleRead.getUserId(), userId);
            int medId = (int)(long)medicationIds[i];
            assertEquals(scheduleRead.getMedicationId(), medId);
            int dispenseId = (int)(long)dispenseIds[i];
            assertEquals(scheduleRead.getDispenseTimeId(), dispenseId);
            assertEquals(scheduleRead.getDispenseAmount(), 5+i);
            assertEquals(scheduleRead.getRecurrence(), 6+i);
            assertEquals(scheduleRead.getScheduleDate(), "date"+i);
            assertEquals(scheduleRead.getScheduleDay(), "day"+i);
            i++;
        }
        cleanup();
    }

    @Test
    public void testGetCountByDispenseTimeId()
    {
        if(null==db)
            initialize();

        // delete
        db.scheduleDao().deleteAll();

        // insert
        List<ScheduleEntity> list = createManyRows();
        Long[] ids = db.scheduleDao().insertAll(list);

        // read
        int dispenseId = (int)(long)dispenseIds[5];
        int count = db.scheduleDao().getCountByDispenseTimeId(dispenseId);
        assertEquals(count, 1);
        cleanup();
    }

    @Test
    public void testGetByUserIdMedicationIdDispenseTimeId()
    {
        if(null==db)
            initialize();

        // delete
        db.scheduleDao().deleteAll();

        // insert
        List<ScheduleEntity> list = createManyRows();
        Long[] ids = db.scheduleDao().insertAll(list);

        // read
        int userId = (int)(long)userIds[2];
        int medId = (int)(long)medicationIds[2];
        int dispenseId = (int)(long)dispenseIds[2];

        ScheduleEntity scheduleEntity = db.scheduleDao().getByUserIdMedicationIdDispenseTimeId(userId, medId, dispenseId);
        assertEquals(scheduleEntity.getScheduleDate(), "date2");

        int userId7 = (int)(long)userIds[7];
        ScheduleEntity nonExist = db.scheduleDao().getByUserIdMedicationIdDispenseTimeId(userId7, medId, dispenseId);
        assertEquals(nonExist, null);
        cleanup();
    }

    @Test
    public void testGetByMedicationId()
    {
        if(null==db)
            initialize();

        // delete
        db.scheduleDao().deleteAll();

        // insert
        List<ScheduleEntity> list = createManyRows();
        Long[] ids = db.scheduleDao().insertAll(list);

        // read
        int medId = (int)(long)medicationIds[3];
        List<ScheduleEntity> listRead = db.scheduleDao().getByMedicationId(medId);
        assertEquals(listRead.get(0).getScheduleDate(), "date3");
        cleanup();
    }

    @Test
    public void testDeleteByUserIdMedicationId()
    {
        if(null==db)
            initialize();

        // delete
        db.scheduleDao().deleteAll();

        // insert
        List<ScheduleEntity> list = createManyRows();
        Long[] ids = db.scheduleDao().insertAll(list);

        // read
        int userId = (int)(long)userIds[2];
        int medId = (int)(long)medicationIds[2];

        db.scheduleDao().deleteByUserIdMedicationId(userId, medId);
        List<ScheduleEntity> listRead = db.scheduleDao().getByMedicationId(medId);
        assertEquals(listRead.size(), 0);
        cleanup();
    }

    private List<ScheduleEntity> createManyRows()
    {
        List<ScheduleEntity> list = new ArrayList<ScheduleEntity>();
        for(int i=0; i<10; i++) {
            int userId = (int)(long)userIds[i];
            int medId = (int)(long)medicationIds[i];
            int dispenseId = (int)(long)dispenseIds[i];

            ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medId, dispenseId);
            scheduleEntity.setUserId(userId);
            scheduleEntity.setMedicationId(medId);
            scheduleEntity.setDispenseTimeId(dispenseId);
            scheduleEntity.setDispenseAmount(5+i);
            scheduleEntity.setRecurrence(6+i);
            scheduleEntity.setScheduleDate("date"+i);
            scheduleEntity.setScheduleDay("day"+i);
            list.add(scheduleEntity);
        }
        return list;
    }

    private void createManyDispenseTimes()
    {
        // create many BinEntities
        List<DispenseTimeEntity> list = new ArrayList<DispenseTimeEntity>();
        for(int i=0; i<10; i++)
        {
            int userId = (int)(long)userIds[i];
            DispenseTimeEntity dispenseTimeInsert = new DispenseTimeEntity();
            dispenseTimeInsert.setUserId(userId);
            dispenseTimeInsert.setDispenseName("name"+i);
            dispenseTimeInsert.setDispenseTime("02:10:1"+i);
            dispenseTimeInsert.setDispenseAmount(2+i);
            dispenseTimeInsert.setActive(true);
            list.add(dispenseTimeInsert);
        }
        dispenseIds = db.dispenseTimeDao().insertAll(list);
    }

    private void createMedicationRows()
    {
        List<MedicationEntity> list = new ArrayList<MedicationEntity>();
        for(int i=0; i<10; i++)
        {
            int userId = (int)(long)userIds[i];
            MedicationEntity medicationInsert = new MedicationEntity(userId);
            medicationInsert.setMedicationName("name"+i);
            medicationInsert.setMedicationNickname("nickname"+i);
            medicationInsert.setStrengthValue(2+i);
            medicationInsert.setStrengthMeasurement("measure"+i);
            medicationInsert.setDosageInstructions("instruction"+i);
            medicationInsert.setTimeBetweenDoses(3+i);
            medicationInsert.setMaxUnitsPerDay(4+i);
            medicationInsert.setMaxNumberPerDose(5+i);
            medicationInsert.setMedicationQuantity(6+i);
            medicationInsert.setUseByDate("date"+i);
            medicationInsert.setFillDate("fillDate"+i);
            medicationInsert.setMedicationLocation(7+i);
            medicationInsert.setMedicationLocationName("location"+i);
            medicationInsert.setPaused(true);
            medicationInsert.setMedicationScheduleType(8+i);
            medicationInsert.setPatientName("Patient"+i);
            list.add(medicationInsert);
        }
        medicationIds = db.medicationDao().insertAll(list);
    }

    @Override
    public void initialize()
    {
        super.initialize();
        createManyDispenseTimes();
        createMedicationRows();
    }
}
