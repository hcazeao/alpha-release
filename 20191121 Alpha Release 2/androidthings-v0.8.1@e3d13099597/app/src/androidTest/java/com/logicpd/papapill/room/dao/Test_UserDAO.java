package com.logicpd.papapill.room.dao;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.entities.SystemEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_UserDAO {

    private Long[] ids;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);


    public void cleanup() {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());
        db.userDao().deleteAll();
    }

    @Test
    public void testCRUDSingleRow() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("name");
        userEntity.setPin("pin");
        userEntity.setPatientName("patient");
        userEntity.setAudioVolume(1);
        userEntity.setFontSize(2);
        userEntity.setScreenBrightness(3);
        userEntity.setVoice(4);
        userEntity.setTheme(5);
        userEntity.setRecoveryContactId(6);
        userEntity.setUserNumber(7);

        AppDatabase db = AppDatabase.getDatabase(App.getContext());

        // delete
        db.userDao().deleteAll();

        // insert
        Long id = db.userDao().insert(userEntity);

        // read
        UserEntity read = db.userDao().get((int)(long)id);
        assertEquals(read.getUserName(), "name");
        assertEquals(read.getPin(), "pin");
        assertEquals(read.getPatientName(), "patient");
        assertEquals(read.getAudioVolume(), 1);
        assertEquals(read.getFontSize(), 2);
        assertEquals(read.getScreenBrightness(), 3);
        assertEquals(read.getVoice(), 4);
        assertEquals(read.getTheme(), 5);
        assertEquals(read.getRecoveryContactId(), 6);
        assertEquals(read.getUserNumber(), 7);

        // delete single
        db.userDao().delete((int)(long)id);
        UserEntity nonExist = db.userDao().get((int)(long)id);
        assertEquals(nonExist, null);
        cleanup();
    }

    @Test
    public void testCRUDManyRows() {
        AppDatabase db = setupDB();

        // read
        List<UserEntity> listRead = db.userDao().getAll();
        int i=0;
        for(UserEntity read : listRead)
        {
            assertEquals(read.getUserName(), "name"+i);
            assertEquals(read.getPin(), "pin"+i);
            assertEquals(read.getPatientName(), "patient"+i);
            assertEquals(read.getAudioVolume(), 1+i);
            assertEquals(read.getFontSize(), 2+i);
            assertEquals(read.getScreenBrightness(), 3+i);
            assertEquals(read.getVoice(), 4+i);
            assertEquals(read.getTheme(), 5+i);
            assertEquals(read.getRecoveryContactId(), 6+i);
            assertEquals(read.getUserNumber(), 7+i);
            i++;
        }
        cleanup();
    }

    @Test
    public void testGetByUsername() {
        AppDatabase db = setupDB();

        UserEntity listRead = db.userDao().getByUsername("name3");
        assertEquals(listRead.getUserName(), "name3");
        cleanup();
    }

    private AppDatabase setupDB()
    {
        AppDatabase db = AppDatabase.getDatabase(App.getContext());

        // delete
        db.userDao().deleteAll();

        // insert
        List<UserEntity> list = createManyRows();
       ids = db.userDao().insertAll(list);
        return db;
    }

    private List<UserEntity> createManyRows()
    {
        List<UserEntity> list = new ArrayList<UserEntity>();
        for(int i=0; i<10; i++) {
            UserEntity userEntity = new UserEntity();
            userEntity.setUserName("name"+i);
            userEntity.setPin("pin"+i);
            userEntity.setPatientName("patient"+i);
            userEntity.setAudioVolume(1+i);
            userEntity.setFontSize(2+i);
            userEntity.setScreenBrightness(3+i);
            userEntity.setVoice(4+i);
            userEntity.setTheme(5+i);
            userEntity.setRecoveryContactId(6+i);
            userEntity.setUserNumber(7+i);
            list.add(userEntity);
        }
        return list;
    }
}
