package com.logicpd.papapill.room.repositories;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.Rule;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

public class BaseTestRepository {
    protected Long[] userIds;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    protected void create10Users()
    {
        // insert
        List<IBaseEntity> listUsers = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++) {
            UserEntity user = new UserEntity();
            user.setUserName("user" + i);
            user.setPatientName("patient" + i);
            user.setPin("pin" + i);
            listUsers.add(user);
        }
        userIds = (Long[])new UserRepository().syncOp(CRUDEnum.INSERT_ALL, listUsers);
    }
}
