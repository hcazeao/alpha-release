package com.logicpd.papapill.room.repositories;

import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_DispenseRepository extends BaseTestRepository {
    private Long[] dispenseIds;

    @AfterClass
    static public void cleanup()
    {
        new UserRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new DispenseTimeRepository().syncOp(CRUDEnum.DELETE_ALL, null);
    }

    @Test
    public void testSyncOp() {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Users();
        create10DispenseTimes();

        // test QUERY_ALL
        List<DispenseTimeEntity> list = (List<DispenseTimeEntity>) new DispenseTimeRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(list.size(), 10);

        // test UPDATE
        DispenseTimeEntity entity = list.get(3);
        entity.setDispenseName("updateName");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(entity);
        new DispenseTimeRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        DispenseTimeEntity read = (DispenseTimeEntity) new DispenseTimeRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(read.getDispenseName(), "updateName");
    }

    private void create10DispenseTimes()
    {
        // create many BinEntities
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++)
        {
            DispenseTimeEntity dispenseTimeInsert = new DispenseTimeEntity();
            int userId = (int)(long)userIds[i];
            dispenseTimeInsert.setUserId(userId);
            dispenseTimeInsert.setDispenseName("name"+i);
            dispenseTimeInsert.setDispenseTime("02:10:1"+i);
            dispenseTimeInsert.setDispenseAmount(2+i);
            dispenseTimeInsert.setActive(true);
            list.add(dispenseTimeInsert);
        }
        dispenseIds = (Long[])new DispenseTimeRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }
}
