package com.logicpd.papapill.room.repositories;

import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)

public class Test_MedicationRepository  extends BaseTestRepository {

    private Long[] medicationIds;

    @AfterClass
    static public void cleanup()
    {
        new UserRepository().syncOp(CRUDEnum.DELETE_ALL, null);
        new MedicationRepository().syncOp(CRUDEnum.DELETE_ALL, null);
    }

    @Test
    public void testSyncOp() {
        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Users();
        create10Medications();

        // test QUERY_ALL
        List<MedicationEntity> list = (List<MedicationEntity>) new MedicationRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(list.size(), 10);

        // test UPDATE
        MedicationEntity entity = list.get(3);
        entity.setPatientName("patientUpdate");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(entity);
        new MedicationRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        MedicationEntity medRead = (MedicationEntity) new MedicationRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(medRead.getPatientName(), "patientUpdate");
    }

    private void create10Medications()
    {
        // create many BinEntities
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++)
        {
            int id = (int)(long)userIds[i];
            MedicationEntity medication = new MedicationEntity(id);
            medication.setMedicationName("name"+i);
            list.add(medication);
        }
        medicationIds = (Long[])new MedicationRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }
}
