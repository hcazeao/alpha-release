package com.logicpd.papapill.room.repositories;

import android.support.test.runner.AndroidJUnit4;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.SystemEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Test_SystemRepository {

    private Long[] systemIds;

    @AfterClass
    static public void cleanup()
    {
        new SystemRepository().syncOp(CRUDEnum.DELETE_ALL, null);
    }

    @Test
    public void testSyncOp() {

        // start with a clean slate
        cleanup();

        // test INSERT_ALL
        create10Systems();

        // test QUERY_ALL
        List<SystemEntity> list = (List<SystemEntity>) new SystemRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        assertEquals(list.size(), 10);

        // test UPDATE
        SystemEntity entity = list.get(3);
        entity.setSystemKey("updateKey");
        List<IBaseEntity> listUpdate = new ArrayList<IBaseEntity>();
        listUpdate.add(entity);
        new SystemRepository().syncOp(CRUDEnum.UPDATE, listUpdate);

        // test QUERY_BY_ID
        SystemEntity systemRead = (SystemEntity) new SystemRepository().syncOp(CRUDEnum.QUERY_BY_ID, listUpdate);
        assertEquals(systemRead.getSystemKey(), "updateKey");
    }

    private void create10Systems()
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        for(int i=0; i<10; i++) {
            SystemEntity systemEntity = new SystemEntity();
            systemEntity.setSystemKey("key"+i);
            systemEntity.setSerialNumber("number"+i);
            list.add(systemEntity);
        }
        systemIds = (Long[])new SystemRepository().syncOp(CRUDEnum.INSERT_ALL, list);
    }
}
