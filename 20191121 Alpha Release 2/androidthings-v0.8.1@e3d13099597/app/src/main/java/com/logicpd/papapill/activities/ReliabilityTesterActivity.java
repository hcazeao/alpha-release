package com.logicpd.papapill.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.logicpd.papapill.BuildConfig;
import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.gpio.PowerManager;
import com.logicpd.papapill.device.gpio.TinyGResetManager;
import com.logicpd.papapill.enums.BuildFlavorsEnum;
import com.logicpd.papapill.fragments.reliability_testing.ReliabilityHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.misc.CloudManager;
import com.logicpd.papapill.receivers.ConnectionReceiver;
import com.logicpd.papapill.receivers.IntentBroadcast;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.utils.FragmentUtils;
import com.logicpd.papapill.utils.ReflectionUtil;
import com.logicpd.papapill.utils.StatsUtil;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

/**
 * NOTE: This activity is for a Reliability VOR to determine areas of improvement in the Papapill
 * device. There are only two fragments that this activity handles -- test setup and the testing
 * itself. This activity will not be in the final app.
 */
public class ReliabilityTesterActivity extends Activity implements OnButtonClickListener {

    public static final String TAG = "ReliabilityTesterActivity";

    private FrameLayout rootLayout;

    private TinyGDriver tg = TinyGDriver.getInstance();
    private CloudManager cloudManager;

    protected BroadcastReceiver mReceiver;
    private PowerManager pm = PowerManager.getInstance();

    /* OpenCV requires an initialization for the Android/Jsva version of their toolset at the
     * activity level.  This is that initialization.
     */
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    //mOpenCvCameraView.enableView();
                }
                break;

                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    private BroadcastReceiver updateUIReceiver;
    private TextView tvTestStatus, tvSuccessCount, tvFailureCount;

    private void registerUpdateUIReceiver() {
        updateUIReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "Update UI with new Stats");

                tvTestStatus = findViewById(R.id.id_status_update_view);
                tvSuccessCount = findViewById(R.id.id_successes_update_view);
                tvFailureCount = findViewById(R.id.id_failures_update_view);

                if (StatsUtil.getInstance().getIsRunningStatus()) {
                    tvTestStatus.setText("Running");
                } else {
                    tvTestStatus.setText("Stopped");
                }
                tvSuccessCount.setText(StatsUtil.getInstance().getSuccesses().toString());
                tvFailureCount.setText(StatsUtil.getInstance().getFailures().toString());
            }
        };
        registerReceiver(updateUIReceiver, new IntentFilter("updateUI"));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reliability_tester);
        rootLayout = findViewById(R.id.layout_root);

        AppConfig appConfig = AppConfig.getInstance();
        appConfig.loadFile(this.getApplicationContext());

        // Release the TinyG from reset
        TinyGResetManager.getInstance().resetTinyG();
        Log.d(TAG, "TinyG out of reset");

        createReceiver();   // for listening to TinyG commands
        registerUpdateUIReceiver();

        checkConnection();//check to see if we are connected to network

        // Initialize the Tinyg driver.
        if(!tg.initialize(true)) {
            Log.e(TAG, "Failed to initialize Tinyg driver.");
        }

        // Disconnect battery so you can pull the plug on the device
        pm.disconnectBattery();

        // Start with the Home fragment
        FragmentUtils.showFragment(this, new ReliabilityHomeFragment(), rootLayout, null, "Home");
    }

    @Override
    public void onButtonClicked(Bundle bundle)
    {
        String fragmentName;

        if (bundle != null) {
            fragmentName = bundle.getString("fragmentName");
            //displays a fragment and adds to back stack
            if (fragmentName != null) {
                inflateFragment(fragmentName, bundle);
            }
        }
    }

    /*
     * Inflate a fragment and put on top of view stack
     */
    private void inflateFragment(String fragmentName, Bundle bundle) {
        FragmentUtils.removeAllFragments(this);
        switch(fragmentName) {
            case "Home":
                fragmentName = ReliabilityHomeFragment.TAG;
                // intentional fall through

            default:
                Class<?> clazz = ReflectionUtil.getClassPath(fragmentName);
                try {
                    Log.i(TAG, "Showing Fragment: " + fragmentName);
                    FragmentUtils.showFragment(this, (Fragment) clazz.newInstance(), rootLayout, bundle, fragmentName);
                }
                catch (Exception ex){
                    Log.e(TAG, "failed to find fragment package:"+ex);
                }
        }
    }

    protected void createReceiver()
    {
        if(null==mReceiver) {
            /*
             * need to extend this for use here
             */
            mReceiver = new IntentBroadcast(getFragmentManager());
        }
    }

    private void checkConnection() {
        boolean isConnected = ConnectionReceiver.isConnected();
        if (!isConnected) {
            //TODO show a No Internet Alert or Dialog?
            Log.e(AppConstants.TAG, "No network connection!");
        } else {
            Log.d(AppConstants.TAG, "Connected to network");

            //we're connected to the internet, so initialize Cloud communications
            cloudManager = CloudManager.getInstance(this);
            cloudManager.setOnCloudCallbackListener(new CloudManager.CloudCallbackListener() {
                @Override
                public void onCloudCallback(String msg, Object object) {
                    Log.d(AppConstants.TAG, "MainActivity.OnCloudCallbackListener - Message received from IoT hub: " + msg);

                    //TODO handle callbacks from the cloud for this activity here
                }
            });
            cloudManager.connect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cloudManager != null && cloudManager.isConnected()) {
            cloudManager.close();
        }

        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }

        if (updateUIReceiver != null) {
            unregisterReceiver(updateUIReceiver);
        }

        Log.d(TAG, "Activity Destroyed");
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }

        if (updateUIReceiver != null) {
            unregisterReceiver(updateUIReceiver);
        }

        Log.d(TAG, "Activity Destroyed");
    }

    @Override
    protected void onResume() {
        super.onResume();
        createReceiver();
        registerReceiver(mReceiver, new IntentFilter(IntentBroadcast.INTENT_MESSAGE));

        BuildFlavorsEnum flavor = BuildFlavorsEnum.valueOf(BuildConfig.FLAVOR.toUpperCase());
        switch(flavor) {
            case BREADBOARD:
            case VISIONONLY:
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
                break;
        }
        FirebaseApp.initializeApp(this);
    }

}
