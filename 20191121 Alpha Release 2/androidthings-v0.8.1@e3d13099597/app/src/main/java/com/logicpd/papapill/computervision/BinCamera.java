package com.logicpd.papapill.computervision;

import com.logicpd.papapill.enums.CameraIndexEnum;

public class BinCamera extends BaseCamera {

    public BinCamera() {
        super(CameraIndexEnum.BIN.value);
    }
}
