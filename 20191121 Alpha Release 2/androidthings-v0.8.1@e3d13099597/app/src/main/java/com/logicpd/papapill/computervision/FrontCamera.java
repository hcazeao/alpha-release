package com.logicpd.papapill.computervision;

import com.logicpd.papapill.enums.CameraIndexEnum;

public class FrontCamera extends BaseCamera {

    public FrontCamera() {
        super(CameraIndexEnum.FRONT.value);
    }
}
