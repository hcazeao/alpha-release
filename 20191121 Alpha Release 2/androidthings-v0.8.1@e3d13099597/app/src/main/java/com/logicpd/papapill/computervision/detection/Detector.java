package com.logicpd.papapill.computervision.detection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.logicpd.papapill.App;
import com.logicpd.papapill.computervision.ImageUtility;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.models.CoordinateData;
import com.logicpd.papapill.interfaces.OnDetectionCompleteListener;
import com.logicpd.papapill.room.entities.BinEntity;
import com.logicpd.papapill.room.repositories.BinRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * This class manages the processing of the image to find, locate and count medications in a bin.
 * For confirming bin identity, this will call a barcode reading class.
 * Logic PD - Brady Hustad - 2018-06-29 - Initial Creation
 * Logic PD - Brady Hustad - 2018-07-19 - Built Thresholding and Contours, removing extra camera visuals.
 * Logic PD - Brady Hustad - 2019-02-20 - Refactor and Clean up Code for Alpha Final
 *     And added tuning variables.
 */
public class Detector {

    //Class Variables
    public static final String TAG = "Detector";
    private static final boolean flgWriteImage = true; //Set to true to write the testing images, false to save cycles and not write.
    private boolean mTestRun = false; //Set to true by the automated integration tests to make the analysis work specifically for testing situations.
    private boolean mCannyReplacementTestRun = false;
    private Mat mCannyReplaceMat;

    //Tuning Variables.  These are variables that likely will have to be tuned as part of implementing
    //within the final environment, and as the system is improved.
    private static final int CONTOUR_AREA_LIMIT = 950; //This is how big of an area the threshold picks up we should keep.  An m&m is around 10000.  Most random bright points are around 500 or less.
    private static final int EDGE_LOW_THRESHOLD = 20; //This is the current low threshold for the edge detection. (13)
    private static final int EDGE_HIGH_THRESHOLD = 40; //This is the current high threshold for the edge detection. (32)
    private static final int PILL_BEHIND_BLOB_THRESHOLD = 75; //This is for confirming something other than empty bin is behind a detection center of mass.
    private static final double MAXIMUM_DETECTED_SIZE = 1.45; //This is the maximum size from the blob detection allowed from a mean size.
    private static final double MINIMUM_DETECTED_SIZE = 0.6; //This is the minimum szie from teh blobl detection allowed from a mean size.

    //Chosen Pills
    private double fillMmZ = 0d;
    private double fillZ = 0d;
    private ArrayList<PillLocation> pills = new ArrayList<>();

    //Upper Left points for image reduction to resize and match on templates for fill.
    private int startOfCutX = 0;
    private int startOfCutY = 0;

    //The contour map to cut the inverted image to
    private Mat mContours;

    //The Bin ID for database and improved information available.
    private byte[] mCurrentImageBytes;
    private Context mainContext = null;
    private int mBinId = -1;
    private BinEntity mBinData = null;

    public Detector() {
        if (mainContext == null) {
            mainContext = App.getContext();
        }

        // create picture directory if necessary
        ImageUtility.initializePictureDirectory();

        mCannyReplaceMat = new Mat();
    }

    //********************************************************
    //* Main Methods
    //********************************************************

    /*
     * This method takes an image, parses it into a histogram of the various colors, and determines
     * where the bin edges are, then determines if those are in the correct place, and if not, returns
     * a value to rotate on the theta.  0.0 will returned if either no result is determined, or no
     * rotation is required.
     */
    public void storeBinImage(final byte[] imageBytes, int binId) {

        Mat matColor = new Mat();
        mCurrentImageBytes = imageBytes;

        Bitmap bmapImage = BitmapFactory.decodeByteArray(mCurrentImageBytes, 0, mCurrentImageBytes.length);
        Utils.bitmapToMat(bmapImage, matColor); //matColor is Type CV_8UC4; (Type = 24)
        matColor = removeDistortion(matColor, bmapImage);
        Utils.matToBitmap(matColor, bmapImage);

        ImageUtility.storeImage(bmapImage, "/bin-" + binId + "-image");
    }

    /*
     * This is an overload to allow a specific test.  Once this test is determined this code, and the according tests,
     * should be deprecated and deleted.  This tests if the improved edge detection makes a signification difference in the
     * ability to select pills.
     */
    public DetectionData processImage(final byte[] imageBytes, int binId, boolean testRun, final byte[] edgePic, boolean edgeTest) {
        mCannyReplacementTestRun = edgeTest;

        Bitmap bmapImage = BitmapFactory.decodeByteArray(edgePic, 0, edgePic.length);
        Utils.bitmapToMat(bmapImage, mCannyReplaceMat); //matColor is Type CV_8UC4; (Type = 24)

        return processImage(imageBytes, binId, testRun);
    }

    /*
     * This is an overload to allow a test run.  This will set a class variable that allows
     * critical decisions to use test settings when applicable.
     */
    public DetectionData processImage(final byte[] imageBytes, int binId, boolean testRun) {
        mTestRun = testRun;
        return processImage(imageBytes, binId);
    }

    /*
     * This is an overload for capturing the bin id.
     */
    public DetectionData processImage(final byte[] imageBytes, int binId) {
        //Handle Bin Id
        mBinId = binId;
        return processImage(imageBytes);
    }

    /*
     * This receives the image from the camera and instigates the processing of the image and the
     * locating of the medication.  This will, in the future, also estimate pill count, and confirm
     * proper bin delivery.
     */
    public DetectionData processImage(final byte[] imageBytes) {
        Log.d(TAG, "Detection->processImage started.");

        long startTime = System.currentTimeMillis();
        mCurrentImageBytes = imageBytes;
        Mat matColor = new Mat();
        Mat matOutput;
        Mat matOriginal = new Mat();
        //DatabaseHelper dbHelper  = DatabaseHelper.getInstance(mainContext);
        if (mTestRun) {
            mBinData = null;
        } else {
            if (mBinId > 0) {
                mBinData = null;
                //NOTE: This was going to help with speed by doing off-time analysis of the bin
                //better determining how much of the picture to cut and where to focus analysis.
                //The off-time analysis method was never written, so the database writes simply
                //wasted cycles. This is still a likely time saver in the future.

                //mBinData = new BinRepository().read(mBinId);
                //Log.d(TAG, "mBinData from db");
            }
        }

        Bitmap bmapImage = BitmapFactory.decodeByteArray(mCurrentImageBytes, 0, mCurrentImageBytes.length);
        Utils.bitmapToMat(bmapImage, matColor); //matColor is Type CV_8UC4; (Type = 24)

        if (flgWriteImage)
            ImageUtility.storeImage(bmapImage, "/1a-Original");

        // *********************************
        // * STEP: Undistort Image
        // *********************************
        if (!mTestRun)
            matColor = removeDistortion(matColor, bmapImage);

        Log.d(TAG, "TIMER: Distortion done->" + (System.currentTimeMillis() - startTime));

        //Always setup after a picking process to re-analyze the bin
        if (mBinData == null) {
            mBinData = new BinEntity();
            mBinData.setUpperLeftX(-1);
            mBinData.setUpperLeftY(-1);
            mBinData.setLowerRightX(-1);
            mBinData.setLowerRightY(-1);
        }

//        if (mBinId > 0) {
//            Utils.matToBitmap(matColor, bmapImage);
//            mBinData.setDetectImage(bmapImage);
//            mBinData.setId(mBinId);
//        }

        //This starts using the pre-stored information to improve and speed analysis
//        Log.d(TAG, "Bin ID:" + mBinId + " ULX:" + mBinData.getUpperLeftX());
        if (mBinId > 0 && mBinData.getUpperLeftX() >=0) {
            //Process using stored information

            /*
             * never use this loop for development ...
             */
            Log.d(TAG, "mBinData > 0, upperLeftX >= 0");

            // *********************************
            // * STEP: Remove Excess Image
            // *********************************
            matColor = trimToFocusArea(matColor, bmapImage);

            //This also removes anything out of the bounding box of the bin, removing lots of pixels.
            bmapImage = Bitmap.createBitmap(bmapImage, 0, 0, matColor.cols(), matColor.rows());

        } else {
            //Process without using any prestored information
            Log.d(TAG, "no prestored mBinData");

            // *********************************
            // * STEP: Remove Out of Bin Image
            // *********************************
            matColor = removeOutsideBin(matColor, bmapImage);
            Log.d(TAG, "TIMER: removeOutsideBin done->" + (System.currentTimeMillis() - startTime));

            //This also removes anything out of the bounding box of the bin, removing lots of pixels.
            bmapImage = Bitmap.createBitmap(bmapImage, 0, 0, matColor.cols(), matColor.rows());

            // *********************************
            // * STEP: Remove Area Around Pills
            // *********************************
            matColor = removeAroundPills(matColor, bmapImage);
            Log.d(TAG, "TIMER: removeAroundPills done->" + (System.currentTimeMillis() - startTime));
            bmapImage = Bitmap.createBitmap(bmapImage, 0, 0, matColor.cols(), matColor.rows());
        }

        matColor.copyTo(matOriginal);

        // *********************************
        // * STEP: Create Edge Image
        // *********************************
        Mat matEdge = edgeDetection(matColor, bmapImage);
        Log.d(TAG, "TIMER: edgeDetection done->" + (System.currentTimeMillis() - startTime));

        // *********************************
        // * STEP: Reverse Edge and create solid pill structures
        // *********************************
        matOutput = reverseEdge(matEdge, bmapImage);
        Log.d(TAG, "TIMER: reverseEdge done->" + (System.currentTimeMillis() - startTime));

        // *********************************
        // * STEP: Add Blobs to pills
        // *********************************
        matOutput = detectBlob(matOutput, matOriginal, bmapImage);
        Log.d(TAG, "TIMER: detectBlob done->" + (System.currentTimeMillis() - startTime));

        // **********************************
        // * STEP: Use Watershed Method
        // * ********************************
        //Mat matOutput = detectByWatershed(matColor, matEdge, bmapImage);

        //This is primarily for testing and viewing results.  Will not be used in final product.
        //Though it should be left in code for future debugging and product growth.
        Utils.matToBitmap(matOutput, bmapImage);
        if (flgWriteImage) {
            ImageUtility.storeImage(bmapImage, "/6-Final");
        }

        //Not sure this helps, there is lots of 'debate' on the web, but since it seems something isn't
        //cleaning well I figure it can't hurt.
        matOutput.release();
        matEdge.release();
        matColor.release();
        matOriginal.release();

        DetectionData sendData = new DetectionData();
        if (pills.size() > 0) {
            sendData.setStatus(DetectionStatus.SUCCESSFUL);
            sendData.setErrorMessage("");
        } else {
            sendData.setStatus(DetectionStatus.FAILED);
            sendData.setErrorMessage("No suitable pills found within the picking zone");
        }
        sendData.setSelectedPill(bmapImage);
        sendData.setSelectedPills(pills);

        mListener.onDetectionCompleted(sendData);

        long endTime = System.currentTimeMillis();
        Log.d(TAG, "*****TIMER: Time for Process->" + (endTime - startTime));

        //Update database
//        if (mBinId > 0 && !mTestRun) {
//            BinRepository bin = new BinRepository();
//            bin.delete(mBinId);
//            bin.insert(mBinData);
//            Log.d(TAG, "Database Record Written for Bin: " + mBinId);
//        }

        return sendData;
    }

    /*
     * This uses the found calibration from the calibration on the camera to remove distortion
     * from the image and allow direct comparison of pixel to location for rho, theta, z
     * conversion.  Could also help with detection.
     */
    private Mat removeDistortion(Mat original, Bitmap bmapImage) {
        Mat undistort = new Mat();

        AppConfig cfg = AppConfig.getInstance();
        Imgproc.undistort(original, undistort, cfg.getMatCameraMatrix(), cfg.getMatDistortionCoefficients());

        if (flgWriteImage) {
            Utils.matToBitmap(undistort, bmapImage);
            ImageUtility.storeImage(bmapImage, "/1b-Undistorted");
        }

        return undistort;
    }

    /*
     * This removes everything out of the bin to eliminate bad intensities and various other
     * detractors for a valid detect.
     */
    private Mat removeOutsideBin(Mat inputMat, Bitmap bmapImage) {
        File dir = Environment.getExternalStorageDirectory();
        String templatePath = dir.getPath() + "/Pictures/";

        if (mTestRun) {
            templatePath += "testImages/Mask3bottom.jpg";
        } else {
            templatePath += AppConfig.getInstance().getTemplateFilename() + ".jpg";
        }

        Mat template = new Mat(inputMat.rows(), inputMat.cols(), CvType.CV_8UC3, Scalar.all(0));
        template.create(inputMat.rows(), inputMat.cols(), CvType.CV_8UC4);
        template = Imgcodecs.imread(templatePath, Imgcodecs.IMREAD_COLOR );
        Imgproc.cvtColor(template, template, Imgproc.COLOR_BGR2BGRA);

        List<MatOfPoint> contourList = new ArrayList<>();
        Mat hierarchy = new Mat();
        Mat boundingTemp = new Mat();
        Imgproc.cvtColor(template, boundingTemp, Imgproc.COLOR_BGR2GRAY);
        Imgproc.findContours(boundingTemp, contourList, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        int startX = 10000;
        int endX = -1;
        int startY = 10000;
        int endY = -1;

        Log.d(TAG, "Countours in Template:" + contourList.size());
        for (int i = 0; i < contourList.size(); i++ ) {
            Rect rect = Imgproc.boundingRect(contourList.get(i));
            if(rect.x < startX)
                startX = rect.x;
            if(rect.x + rect.width > endX)
                endX = rect.x + rect.width;
            if(rect.y < startY)
                startY = rect.y;
            if(rect.y + rect.height > endY)
                endY = rect.y + rect.height;

            //Log.d(TAG, "bounding box: (" + startX + ", " + startY + "),(" + endX + ", " + endY + ")");
        }

        startOfCutX = startX;
        startOfCutY = startY;

        //Log.d(TAG, "Template has channels: " + template.channels() + " depth: " + template.depth() + " and rows: " + template.rows() + " and cols: " + template.cols());

        Mat outputMat = new Mat();
        inputMat.copyTo(outputMat, template);
        Mat newOutputMat = outputMat.submat(startY, endY, startX, endX);

        if (flgWriteImage) {
            Utils.matToBitmap(outputMat, bmapImage);
            ImageUtility.storeImage(bmapImage, "/1c-Removal");
        }

        template.release();

        //return outputMat;
        return newOutputMat;
    }

    /*
     * This removes everything determined by a Area of Interest (AOI) that was figured out
     * by the post process analysis process run and stored within the database.  This is used
     * in replacement of the template and contour cut.
     */
    private Mat trimToFocusArea(Mat inputMat, Bitmap bmapImage) {

        startOfCutX = mBinData.getUpperLeftX();
        startOfCutY = mBinData.getUpperLeftY();

        Mat newOutputMat = inputMat.submat(startOfCutY, mBinData.getLowerRightY(), startOfCutX, mBinData.getLowerRightX());

        if (flgWriteImage) {
            bmapImage = Bitmap.createBitmap(bmapImage, 0, 0, newOutputMat.cols(), newOutputMat.rows());
            Utils.matToBitmap(newOutputMat, bmapImage);
            ImageUtility.storeImage(bmapImage, "/1c-Removal");
        }

        return newOutputMat;
    }

    /*
     * This replaces the original method and does not use intensity thresholds that hopefully is more
     * robust from machine to machine, lighting situation to lighting situation.
     */
    private Mat removeAroundPills(Mat inputMat, Bitmap bmapImage) {
        Mat matColor = new Mat();
        //mCurrentImageBytes = imageBytes;

        //Bitmap bmapImage = BitmapFactory.decodeByteArray(mCurrentImageBytes, 0, mCurrentImageBytes.length);
        //Utils.bitmapToMat(bmapImage, matColor); //matColor is Type CV_8UC4; (Type = 24)
        //matColor = removeDistortion(matColor, bmapImage);

        inputMat.copyTo(matColor);

        //***************************************************
        //Subtract Picture Test
        //***************************************************
        Bitmap emptyBin;
        // NOTE: /testImages/EmptyBin is a rendering of a black bin on a white background, but
        // bin-1-image is an actual photo of a 0% calibration bin -- opposites!!!!
        if (mTestRun) {
            emptyBin = ImageUtility.retrieveImage("/testImages/EmptyBin");
        } else {
            emptyBin = ImageUtility.retrieveImage("bin-1-image");
        }
        Mat matBigEmpty = new Mat();
        Mat matSubtract = new Mat();
        Utils.bitmapToMat(emptyBin, matBigEmpty);
        Mat matEmpty = matBigEmpty.submat(startOfCutY, matColor.rows() + startOfCutY, startOfCutX, matColor.cols() + startOfCutX);

        Core.subtract(matColor, matEmpty, matSubtract);

        //*******************************************************
        //Cut away background for analysis.  Doesn't necessarily
        //black correctly, so there for remove it from thought
        //only use bottom of bin for pill selection area.
        //While we already cut to this, this removes any built up
        //errors so I kept this clean loop in process.
        //*******************************************************

        Bitmap bottomBin;
        if (mTestRun) {
            bottomBin = ImageUtility.retrieveImage("/testImages/Mask3bottom");
        } else {
            bottomBin = ImageUtility.retrieveImage(AppConfig.getInstance().getTemplateFilename());
        }
        Mat matBottom = new Mat();
        Utils.bitmapToMat(bottomBin, matBottom);
        Mat matCutBottom = matBottom.submat(startOfCutY, matColor.rows() + startOfCutY, startOfCutX, matColor.cols() + startOfCutX);

        //Cut away the subtraction
        Mat newSubtract = new  Mat();
        matSubtract.copyTo(newSubtract, matCutBottom);

        //Cutaway the empty
        Mat newEmpty = new Mat();
        matEmpty.copyTo(newEmpty, matCutBottom);

        if (flgWriteImage) {
            Utils.matToBitmap(matSubtract, bmapImage);
            ImageUtility.storeImage(bmapImage, "/H1-Subtract");
        }
        matSubtract.release();
        matCutBottom.release();
        matBottom.release();
        matBigEmpty.release();
        matEmpty.release();

        //***************************************************
        //Histogram Analysis
        //***************************************************
        Mat matEmptyGray = new Mat();
        Mat matGray = new Mat();
        Mat histogram = new Mat();
        Mat histogramEmpty = new Mat();
        MatOfFloat ranges = new MatOfFloat(0f, 256f);
        MatOfInt histSize = new MatOfInt(64);

        Imgproc.cvtColor(newEmpty, matEmptyGray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.cvtColor(newSubtract, matGray, Imgproc.COLOR_BGR2GRAY);

        Imgproc.GaussianBlur(matEmptyGray, matEmptyGray, new Size(7,7), 3);
        Imgproc.GaussianBlur(matGray, matGray, new Size(7,7), 3);
        Imgproc.threshold(matGray, matGray, 15, 255, Imgproc.THRESH_BINARY);

        if (flgWriteImage) {
            Utils.matToBitmap(matGray, bmapImage);
            ImageUtility.storeImage(bmapImage, "/H2-Gray");
        }

        Imgproc.calcHist(Collections.singletonList(matEmptyGray), new MatOfInt(0), new Mat(), histogramEmpty, histSize, ranges);
        Imgproc.calcHist(Collections.singletonList(matGray), new MatOfInt(0), new Mat(), histogram, histSize, ranges);

        //Now do grey scale
        Mat calcHist = new Mat(64, 1, CvType.CV_32FC1);

        double[] histArray = new double[64];
        double[] emptyArray = new double[64];

        for (int i = 0; i < histogram.rows(); i++) {
            double[] ans = histogram.get(i, 0);
            double[] emptyAns = histogramEmpty.get(i, 0);
            histArray[i] = ans[0];
            emptyArray[i] = emptyAns[0];
        }

        double[] calcArray = new double[64];
        for (int i = 0; i < histArray.length; i++) {
            if (emptyArray[i] > 20000.0) {
                calcArray[i] = 0.0;
            } else if (emptyArray[i] > histArray[i]) {
                calcArray[i] = 0.0;
            } else {
                calcArray[i] = Math.pow(histArray[i] - emptyArray[i], 2);
            }
        }
        calcHist.put(0, 0, calcArray);

        Mat matBack = new Mat();
        Imgproc.calcBackProject(Collections.singletonList(matGray), new MatOfInt(0), calcHist, matBack, ranges, 0.1); //Replaced histogram with emptyHist

        //TODO: This one could be important for a bit more as this is a final level of tweaking, and depending on
        //'how' we do things above this is the reverser for binary colors.  I would say, let's get through the
        // next round of revisions, and if our new 'lack of reverse' stands, we can then remove this.
        //Core.bitwise_not(matBack, matBack);
        //Imgproc.threshold(matBack, matBack, 220, 255, Imgproc.THRESH_BINARY);

        if (flgWriteImage) {
            Utils.matToBitmap(matBack, bmapImage);
            ImageUtility.storeImage(bmapImage, "/H3-GrayBack");
        }

        //****************************************************************************
        //Contour Build and Final Pill Area
        //****************************************************************************
        Mat hierarchy = new Mat();
        List<MatOfPoint> contourList = new ArrayList<>();

        Imgproc.findContours(matBack, contourList, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        Log.d(TAG, "Contours Found: ->" + contourList.size());

        mContours = new Mat(matBack.rows(), matBack.cols(), CvType.CV_8UC3, Scalar.all(0));
        mContours.create(matBack.rows(), matBack.cols(), CvType.CV_8UC4);
        mContours.setTo(Scalar.all(0));

        for (int i = 0; i < contourList.size(); i++) {
            double contourArea = Imgproc.contourArea(contourList.get(i));

            //It seems a 'pill' size of the current size is around 9000 and the random detritus is 500...
            //very, very small pills will need something different....
            if (contourArea > CONTOUR_AREA_LIMIT) {
                //Log.d(TAG, "Contour Area:->" + contourArea);
                Imgproc.drawContours(mContours, contourList, i, new Scalar(255, 255, 255, 255), -1);
                //Imgproc.drawContours(mContours, contourList, i, new Scalar(255, 255, 255, 255), 1);
            }
        }

        if (flgWriteImage) {
            Utils.matToBitmap(mContours, bmapImage);
            ImageUtility.storeImage(bmapImage, "/H4-PillArea");
        }

        int startX = 10000;
        int endX = -1;
        int startY = 10000;
        int endY = -1;

        if (contourList.size() > 0) {
            for (int i = 0; i < contourList.size(); i++) {
                Rect rect = Imgproc.boundingRect(contourList.get(i));
                if (rect.x < startX)
                    startX = rect.x;
                if (rect.x + rect.width > endX)
                    endX = rect.x + rect.width;
                if (rect.y < startY)
                    startY = rect.y;
                if (rect.y + rect.height > endY)
                    endY = rect.y + rect.height;

                //Log.d(TAG, "bounding box: (" + startX + ", " + startY + "),(" + endX + ", " + endY + ")");
            }
        } else {
            // TODO Beta: Revisit this workaround to see if there is a better way to handle this.
            // If no contours were found, we end up passing invalid parameters to submat which then
            // throws an assert error. To get around that for now, we'll just force the mat to 1x1
            // for cases where we find 0 contours. This causes the "remove area around pills" portion
            // of the detection algorithm to proceed with a 1x1 pixel which will always return 0
            // pills detected.
            startX = 0;
            endX = 1;
            startY = 0;
            endY = 1;
        }

        startOfCutX += startX;
        startOfCutY += startY;

        Mat outputMat = new Mat();
        inputMat.copyTo(outputMat, mContours);
        Mat newOutputMat = outputMat.submat(startY, endY, startX, endX);
        mContours = mContours.submat(startY, endY, startX, endX);

        //Log.d(TAG, "Template has channels: " + template.channels() + " depth: " + template.depth() + " and rows: " + template.rows() + " and cols: " + template.cols());

        int topX = -1;
        int topY = 10000;
        double slope = AppConfig.getInstance().getTemplateSlope();
        int coreX = 100;
        bmapImage = Bitmap.createBitmap(bmapImage, 0, 0, mContours.cols(), mContours.rows());
        Utils.matToBitmap(mContours, bmapImage);
        //Log.d(TAG, "Pixel Count is: " + bmapImage.getHeight() * bmapImage.getWidth());
        for (int y = 0; y < bmapImage.getHeight(); y++) {
            for (int x = 0; x < bmapImage.getWidth(); x++) {
                int pix = bmapImage.getPixel(x, y);
                int red = Color.red(pix);
                int blue = Color.blue(pix);
                int green = Color.green(pix);
                double maxY = y + ((coreX - bmapImage.getWidth()) * slope);
                if (pix > 0 || red > 0 || blue > 0 || green > 0) {
                    //Log.d(TAG, "Pixel has a value" + pix);
                    double newY = y + ((coreX - x) * slope);
                    if (newY < topY) {
                        topY = (int)newY;
                        topX = 100;
                        Log.d(TAG, "x, y: (" + x + ", " + y + " ) - New top X, Y: (" + topX + ", " + topY + ").");
                    }
                }
            }
        }

        fillZ = getFillLevelByUpperLeftPoint(topX + startOfCutX, topY + startOfCutY);
        //fillZ = getFillLevelBySlopeLine(topX + startOfCutY, topY + startOfCutY);
        fillMmZ = AppConfig.getInstance().getBinDepth() - (AppConfig.getInstance().getBinDepth() * (fillZ / 100));

        if (flgWriteImage) {
            bmapImage = Bitmap.createBitmap(bmapImage, 0, 0, newOutputMat.cols(), newOutputMat.rows());
            Utils.matToBitmap(newOutputMat, bmapImage);
            ImageUtility.storeImage(bmapImage, "/3-Cleaned");
        }

        return newOutputMat;
    }

    /*
     * This handles the edge detection that limits the picture and remove everything but the edges
     * for use with the object detection.  NOTE: Height analysis will have to be done
     * prior to here as the pic has been chopped to the minimal needs.
     */
    private Mat edgeDetection(Mat inputMat, Bitmap bmapImage) {
        Mat matEdge = new Mat(inputMat.rows(), inputMat.cols(), CvType.CV_8UC1, Scalar.all(0));
        Mat outputMat = new Mat(inputMat.rows(), inputMat.cols(), CvType.CV_8UC4, Scalar.all(0));

        //This simply replaces the canny with a 'seperately created edge picture'
        if (mCannyReplacementTestRun) {
            mCannyReplaceMat.copyTo(matEdge);
            matEdge.convertTo(matEdge, CvType.CV_8UC4);
        } else {
            Imgproc.GaussianBlur(inputMat, inputMat, new Size(7, 7), 5);

            if (flgWriteImage) {
                Utils.matToBitmap(inputMat, bmapImage);
                ImageUtility.storeImage(bmapImage, "/4a-Blur");
            }

            Imgproc.Canny(inputMat, matEdge, EDGE_LOW_THRESHOLD, EDGE_HIGH_THRESHOLD, 3, false);
            matEdge.convertTo(matEdge, CvType.CV_8UC4);
            Imgproc.cvtColor(matEdge, matEdge, Imgproc.COLOR_GRAY2BGRA);
        }

        matEdge.copyTo(outputMat, mContours);

        int kernelSize = 1;
        Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(kernelSize, kernelSize));
        Imgproc.erode(outputMat, outputMat, erodeElement, new Point(-1, -1), 4);
        kernelSize = 1;
        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(kernelSize, kernelSize));
        Imgproc.dilate(outputMat, outputMat, dilateElement, new Point(-1, -1), 1);

        if (flgWriteImage) {
            Utils.matToBitmap(outputMat, bmapImage);
            ImageUtility.storeImage(bmapImage, "/4b-Canny");
        }

        Mat hierarchy = new Mat();
        List<MatOfPoint> contourList = new ArrayList<>();
        Imgproc.cvtColor(outputMat, matEdge, Imgproc.COLOR_BGR2GRAY);
        Imgproc.findContours(matEdge, contourList, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

        Log.d(TAG, "Edge Contours Found: ->" + contourList.size());

        Mat edgeContours = new Mat(matEdge.rows(), matEdge.cols(), CvType.CV_8UC3, Scalar.all(0));
        edgeContours.create(matEdge.rows(), matEdge.cols(), CvType.CV_8UC4);
        edgeContours.setTo(Scalar.all(0));

        //Currently just drop all very, very small contours
        for (int i = 0; i < contourList.size(); i++) {
            MatOfPoint2f mop2f = new MatOfPoint2f(contourList.get(i).toArray());
            double arcLength = Imgproc.arcLength(mop2f, false);

            if (arcLength > 20 && mop2f.toList().size() > 5) {
                Imgproc.drawContours(edgeContours, contourList, i, new Scalar(255,255,255,255), 5);
            }
        }

        if (flgWriteImage) {
            Utils.matToBitmap(edgeContours, bmapImage);
            ImageUtility.storeImage(bmapImage, "/4c-Edge");
        }

        matEdge.release();
        outputMat.release();

        return edgeContours;
    }

    /*
     * This method takes the edge picture, inverses the colors (black and white), and then creates
     * highly accurate 'blobs' of the pills.
     */
    private Mat reverseEdge(Mat inputMat, Bitmap bmapImage) {
        //TODO: Still doesn't handle writing on the pill very well, likely writing will impact detection until that is solved.
        Mat invertMat = new Mat();
        Mat outputMat = new Mat();
        int erosionSize = 6;
        int dilationSize = 1;
        Mat erosionElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*erosionSize + 1, 2*erosionSize+1));
        Mat dilationElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilationSize + 1, 2*dilationSize+1));

        // Create binary image from source image
        Mat bw = new Mat();
        Imgproc.cvtColor(inputMat, bw, Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(bw, bw, 40, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);

        Imgproc.cvtColor(mContours, mContours, Imgproc.COLOR_BGR2GRAY);
        bw.copyTo(outputMat, mContours);

        Imgproc.erode(outputMat, outputMat, erosionElement);
        Imgproc.dilate(outputMat, outputMat, dilationElement);

        outputMat.convertTo(outputMat, CvType.CV_8UC4);
        Imgproc.cvtColor(outputMat, outputMat, Imgproc.COLOR_GRAY2BGRA);

        if (flgWriteImage) {
            Utils.matToBitmap(outputMat, bmapImage);
            ImageUtility.storeImage(bmapImage, "/5a-Inverse");
        }

        invertMat.release();

        return outputMat;
    }

    /*
     * This method uses the Simple Blob Detector from OpenCV to attempt to detect the pills via a
     * general function.  This may work well enough, but the biggest concern is if the memory of
     * the IoT device will be able to accommodate the needs of Blob Detection.
     */
    private Mat detectBlob(Mat inputMat, Mat finalMat, Bitmap bmapImage) {
        Mat greyMat = new Mat();
        Mat threshMat = new Mat();
        Mat outputMat = new Mat();

        // ***********************************
        // *  Deprecated FeatureDetector for simple blob
        // ***********************************
        FeatureDetector detector = FeatureDetector.create(FeatureDetector.SIMPLEBLOB);


        //Read the parameter file.
        File dir = Environment.getExternalStorageDirectory();
        detector.read(dir.getPath() + "/blob.xml");

        MatOfKeyPoint keypoints = new MatOfKeyPoint();
        detector.detect(inputMat, keypoints);
        List<KeyPoint> lstKeyPoints = keypoints.toList();

        //Log.d(TAG, "Keypoint Count:->" + lstKeyPoints.size());
        List<KeyPoint> newKeyList = new ArrayList<>();
        inputMat.copyTo(outputMat);
        for (KeyPoint kp: lstKeyPoints) {
            double[] ptChans = finalMat.get((int)kp.pt.y, (int)kp.pt.x);
            //Log.d(TAG, "Chans 0:" + ptChans[0] + " 1:" + ptChans[1] + " 2:" + ptChans[2]);
            if ((ptChans[0] + ptChans[1] + ptChans[2]) > PILL_BEHIND_BLOB_THRESHOLD) {
                //NOTE: This 75 could be challenging and remove positive hits on black pills.  We don't have any black pills to test, but
                //to be frank, black pills are likely not going to work in the first place.
                Imgproc.circle(outputMat, kp.pt, 3, new Scalar(0, 0, 255), 8, 8, 0);
                Imgproc.circle(outputMat, kp.pt, (int) (kp.size / 2), new Scalar(255, 0, 0), 3, 8, 0);
                newKeyList.add(kp);
            }
        }

        if (flgWriteImage) {
            Utils.matToBitmap(outputMat, bmapImage);
            ImageUtility.storeImage(bmapImage, "/5b-Blob");
        }

        // **********************************
        // * STEP: Choose a Pill to pick up
        // **********************************
        finalMat.copyTo(outputMat);

        //Imgproc.cvtColor(outputMat, outputMat, Imgproc.COLOR_GRAY2BGR);
        for (KeyPoint kp: newKeyList) {
            Imgproc.circle(outputMat, kp.pt, 2, new Scalar(0, 0, 255), 8, 8, 0);
            Imgproc.circle(outputMat, kp.pt, 4, new Scalar(0, 0, 255), 3, 8, 0);
        }

        outputMat = choosePill(newKeyList, outputMat);

        keypoints.release();
        greyMat.release();
        threshMat.release();

        return outputMat;
    }

    /*
     * This method takes the key points and chooses the best in the area.  It selects by size, which
     * means the largest, or brightest, blob is the one selected.  Currently this selects the
     * first largest in the list, so if two are identical sized (highly rare), then the first of the
     * two would be selected.  Currently this doesn't bar any areas from selection so corners and
     * edges are valid.
     */
    private Mat choosePill(List<KeyPoint> lstKeyPoints, Mat inputMat) {

        KeyPoint chosenKp = new KeyPoint();
        chosenKp.size = 0;
        int cntPoints = AppConfig.getInstance().getDetectionsReturned();
        int i = 0;
        float chosenSize; 

        List<KeyPoint> tempList = new ArrayList<>();

        //Sort the keypoint list by size.
        List<KeyPoint> sortedFullList = quickSortKeyPoints(lstKeyPoints);
        int max = sortedFullList.size();

        if(max == 0)
            return inputMat;

        if (max % 2 == 0) {
            //Even
            chosenSize = sortedFullList.get((max - 2) / 2).size;
        } else {
            //Odd
            chosenSize = sortedFullList.get((max - 1) / 2).size;
        }

        if (sortedFullList.size() <= 3) {
            cntPoints = sortedFullList.size();
            tempList = sortedFullList;
        }else {
            //Remove outliers
            int maxRemoves = sortedFullList.size() - 2;
            boolean flgRemoved = false;
            int ptrList = 0;

            while (!flgRemoved && maxRemoves > 0) {
                //Log.d(TAG, "keypoint size: " + sortedFullList.get(ptrList).size);
                if (sortedFullList.get(ptrList).size > chosenSize * MAXIMUM_DETECTED_SIZE || sortedFullList.get(ptrList).size < chosenSize * MINIMUM_DETECTED_SIZE) {
                    //Don't add it.
                    maxRemoves--;
                } else {
                    //Add it.
                    tempList.add(sortedFullList.get(ptrList));
                }
                ptrList++;
                if (ptrList >= sortedFullList.size()) {
                    flgRemoved = true;
                }
            }
        }

        while ( i < cntPoints && i < lstKeyPoints.size() && tempList.size() > 0) {
            sortedFullList = tempList;
            tempList = new ArrayList<>();

            //Add the biggest connection first
            boolean flgFound = false;
            for (KeyPoint tl: sortedFullList) {
                if (!flgFound) {
                    chosenKp = tl;
                    flgFound = true;
                } else {
                    tempList.add(tl);
                }
            }
            //Log.d(TAG, "Selected kp size" + chosenKp.size);

            //Convert the Point
            CoordinateData convPoint = ImageUtility.convertPoint(chosenKp.pt.x + startOfCutX, chosenKp.pt.y + startOfCutY, fillZ);
            convPoint.mmZ = fillMmZ;

            Log.d(TAG, "Point to pick: degrees->" + convPoint.degrees + " radius->" + convPoint.radius );

            //double radiusLimit = Math.abs(convPoint.radius);
            if (convPoint.radius <= AppConfig.getInstance().getDeadzoneRhoMaxLimit() &&
                    convPoint.radius >= AppConfig.getInstance().getDeadzoneRhoMinLimit()) {
                double thetaLimit = (-convPoint.radius/20) + AppConfig.getInstance().getDeadzoneThetaIntercept(); //the slop intercept of deadzone y = x/12 + 4;
                if (Math.abs(convPoint.degrees) <= thetaLimit) {
                    //Draw Red Circles on selections
                    Imgproc.circle(inputMat, chosenKp.pt, 2, new Scalar(255, 0, 0), 8, 8, 0);
                    Imgproc.circle(inputMat, chosenKp.pt, 4, new Scalar(255, 0, 0), 6, 8, 0);

                    //Add to pills
                    pills.add(new PillLocation(convPoint, chosenKp.pt));
                    i++;

                } else {
                    Log.d(TAG, "DEADZONE FAILURE - THETA Rho:" + convPoint.radius + " Theta:" + convPoint.degrees);
                }
            } else {
                Log.d(TAG, "DEADZONE FAILURE - RHO Rho:" + convPoint.radius + " Theta:" + convPoint.degrees);
            }

            chosenKp = new KeyPoint();
        }

        return inputMat;
    }

    //An implementation of a quick sort to organize the keypoints by size.
    private List<KeyPoint> quickSortKeyPoints(List<KeyPoint> startList) {
        //Select the first of the list.
        if (startList.size() > 1) {
            KeyPoint one = startList.get(0);
            List<KeyPoint> higher = new ArrayList<>();
            List<KeyPoint> lower = new ArrayList<>();

            startList.remove(0);

            for (KeyPoint kp: startList) {
                if (kp.size >= one.size) {
                    higher.add(kp);
                } else
                    lower.add(kp);
            }

            List<KeyPoint> finalList = new ArrayList<>(quickSortKeyPoints(higher));
            finalList.add(one);
            finalList.addAll(quickSortKeyPoints(lower));

            return finalList;

        }  else
            return startList;
    }

    /*
     * This method uses the principle that the relation between pixel position against
     * side of bin and fill level is a linear relation.  This therefore uses a pre-determined
     * slope and intercept to calculate the fill level based on the intecept of the slope of the
     * initial impact of the ul point that hits white.
     */
    private int getFillLevelBySlopeLine(int curX, int curY) {
        int fillLevel = -1;

        double slope = AppConfig.getInstance().getTemplateSlope();
        double initialIntercept = curY - slope * curX;

        double fl = -1.03452 * initialIntercept + 210.9622;
        fillLevel = (int)Math.round(fl);
        if (fillLevel < 0) {
            fillLevel = 0;
        } else if (fillLevel > 100) {
            fillLevel = 100;
        }

        return fillLevel;
    }


    /*
     * This method finds the fill to match
     */
    private int getFillLevelByUpperLeftPoint(int curX, int curY) {
        //Upper Left Corner of each template to use this method to find fill level.
        int[][] ulPoint = AppConfig.getInstance().getTemplateUpperLeftXY();

        Log.d(TAG, "ulPoint: " + ulPoint.length);

        //double slope = 9.0 / 50.0;
        double slope = AppConfig.getInstance().getTemplateSlope();

        int fillLevel = -1;
        for (int fill = 0; fill < 101; fill++) {
            double val = ulPoint[fill][1];
            double newY = curY + (val - curX) * slope;

            if (newY > ulPoint[fill][2]) {
                Log.d(TAG, "fill: " + fill + " x:" + val + " y:" + newY + " ulPoint:(" + ulPoint[fill][1] + ", " + ulPoint[fill][2] + ")");
                fillLevel = fill;
                break;
            }
        }
        //If it never fall within the boundary, it is likely filled up and beyond the barriers of the template used to discern full.
        if (fillLevel == -1) {
            fillLevel = 100;
        }

        return fillLevel;
    }

    private OnDetectionCompleteListener mListener = new OnDetectionCompleteListener() {
        @Override
        public void onDetectionCompleted(DetectionData data) {
            //Empty
        }
    };

}
