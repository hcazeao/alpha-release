package com.logicpd.papapill.data;

import android.support.annotation.Nullable;
import android.util.Log;

import com.logicpd.papapill.enums.DispenseEventResultEnum;
import com.logicpd.papapill.enums.FeelingEnum;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.DispenseEventRepository;
import com.logicpd.papapill.room.repositories.MedicationRepository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This model holds data/state of dispense events during dispensing. This is currently
 * used to facilitate passing of info back and forth between the AlarmedReceivedFragment,
 * DispenseMedsFragment, and DispensingMedsFragment.
 */
public class DispenseEventsModel implements Serializable {
    public static String TAG = "DispenseEventsModel";

    // This index is used to keep track of which dispenseEventItem is currently being dispensed
    // by the system. Always starts at 0 and gets incremented after a successful dispense.
    private int index = 0;
    public UserEntity user;

    // This contains the list of dispense events items (joined with schedules and medications).
    // It has all the data required for DispenseMeds and DispensingMeds fragments.
    public List<JoinDispenseEventScheduleMedication> dispenseEventItems = new ArrayList<>();

    public int getIndex() { return index; }

    // Increment the internal index counter ONLY if there are more dispense events.
    public void incrementIndexIfNotLastDispense() {
        if (!this.isLastDispense()) {
            this.index++;
        }
    }

    // Returns true if index points to the last dispense event in the list.
    public boolean isLastDispense() {
        Log.d(TAG, "dispenseEventItems.size:"+dispenseEventItems.size());
        return (index >= dispenseEventItems.size() - 1);
    }

    public JoinDispenseEventScheduleMedication getCurrent() {
        return (this.isIndexValid() ? dispenseEventItems.get(index) : null);
    }

    private boolean isIndexValid() {
        if (this.dispenseEventItems == null || this.dispenseEventItems.isEmpty()) {
            return false;
        }
        return (this.index < this.dispenseEventItems.size());
    }

    public String getMedicationText() {
        JoinDispenseEventScheduleMedication current = getCurrent();
        int dispenseAmount = current.getDispenseAmount();
        int dispenseCounter = dispenseAmount - current.getPillsRemaining() + 1;

        return String.format(" %d OF %d %s %s %s",
                dispenseCounter,
                dispenseAmount,
                current.getMedicationName(),
                current.getStrengthValue(),
                current.getStrengthMeasurement());
    }

    public void gotoExpiredState() {
        DispenseEventEntity dispenseEventEntity = getCurrent().getDispenseEventEntity();
        dispenseEventEntity.setResult(DispenseEventResultEnum.EXPIRED.value);
        new DispenseEventRepository().update(dispenseEventEntity);
    }

    public void gotoDispenseState() {
        JoinDispenseEventScheduleMedication currentDispenseEvent = getCurrent();
        currentDispenseEvent.setResult(DispenseEventResultEnum.DISPENSING.value);
        currentDispenseEvent.setDispenseStartDate(new Date());
        DispenseEventEntity entity =  currentDispenseEvent.getDispenseEventEntity();
        new DispenseEventRepository().update(entity);
    }

    public void gotoCompleteState() {
        // dispense(s) are done !
        DispenseEventEntity dispenseEventEntity = getCurrent().getDispenseEventEntity();
        dispenseEventEntity.setResult(DispenseEventResultEnum.DISPENSE_COMPLETE.value);
        new DispenseEventRepository().update(dispenseEventEntity);
    }

    public void gotoFailedState() {
        DispenseEventEntity dispenseEventEntity = getCurrent().getDispenseEventEntity();
        dispenseEventEntity.setResult(DispenseEventResultEnum.DISPENSE_FAILED_MANUALLY_RETRIEVED.value);
        new DispenseEventRepository().update(dispenseEventEntity);

    }
    public void decrementMedicationQuantity() {
        getCurrent().decrementMedicationQuantity();
        MedicationEntity medicationEntity = getCurrent().getMedicationEntity();
        new MedicationRepository().update(medicationEntity);
    }

    public void decrementMedicationQuantityFromPillsRemaining() {
        getCurrent().setMedicationQuantity(getCurrent().getMedicationQuantity() - getCurrent().getPillsRemaining());
        MedicationEntity medication = getCurrent().getMedicationEntity();
        new MedicationRepository().update(medication);
    }

    public int decrementPillsRemaining() {
        getCurrent().decrementPillsRemaining();
        DispenseEventEntity dispenseEventEntity = getCurrent().getDispenseEventEntity();
        new DispenseEventRepository().update(dispenseEventEntity);
        return getCurrent().getPillsRemaining();
    }

    public void setFeeling(FeelingEnum feeling) {
        getCurrent().setFeeling(feeling.value);
        DispenseEventEntity dispenseEventEntity = getCurrent().getDispenseEventEntity();
        new DispenseEventRepository().update(dispenseEventEntity);
    }
}
