package com.logicpd.papapill.data;

import com.logicpd.papapill.room.entities.UserEntity;

import java.io.Serializable;

public class Notification implements Serializable {
    private String subject;
    private String content;
    private UserEntity user;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
