package com.logicpd.papapill.data.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.room.entities.MedicationEntity;

import java.util.List;

/**
 * FillStatusAdapter
 *
 * @author alankilloren
 */
public class FillStatusAdapter extends RecyclerView.Adapter<FillStatusAdapter.DataObjectHolder> {
    private List<MedicationEntity> medications;

    public FillStatusAdapter(Context context, List<MedicationEntity> resultList) {
        this.medications = resultList;
    }


    @NonNull
    @Override
    public FillStatusAdapter.DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fill_status_item, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FillStatusAdapter.DataObjectHolder holder, int position) {
        MedicationEntity medication = medications.get(position);
        StringBuilder sb = new StringBuilder();
        if (medication.getMedicationLocation() > 0 && medication.getMedicationLocation() < 15) {
            //it's a bin
            sb.append("BIN ");
            if (medication.getMedicationLocation() < 10) {
                sb.append("0");
            }
            sb.append(medication.getMedicationLocation());
        } else {
            //drawer, fridge, other
            sb.append(medication.getMedicationLocationName());
        }

        sb.append(": ").append(medication.getMedicationName())
                .append(" ")
                .append(medication.getStrengthMeasurement());
        holder.tvLine1.setText(sb.toString());

        String s = medication.getMedicationQuantity() + " PILLS " + calculateDays(medication);
        holder.tvLine2.setText(s);
    }

    private String calculateDays(MedicationEntity med)
    {
        MedScheduleTypeEnum scheduleType = MedScheduleTypeEnum.values()[med.getMedicationScheduleType()];
        int days = med.getMedicationQuantity();

        switch(scheduleType)
        {
            case BOTH:
            case AS_NEEDED:
                return "(" + (days / med.getMaxUnitsPerDay()) + " DAYS) REMAINING ";

            case SCHEDULED:
                break;
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return medications.size();
    }

    class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tvLine1, tvLine2;

        DataObjectHolder(View itemView) {
            super(itemView);
            tvLine1 = itemView.findViewById(R.id.textview_line1);
            tvLine2 = itemView.findViewById(R.id.textview_line2);
        }
    }
}
