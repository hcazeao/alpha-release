package com.logicpd.papapill.data.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.room.entities.MedicationEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MedicationsAdapter
 *
 * @author alankilloren
 */
public class MedicationsAdapter extends RecyclerView.Adapter<MedicationsAdapter.DataObjectHolder> {
    private MedicationsAdapter.MyClickListener myClickListener;
    private List<MedicationEntity> medications;
    private boolean showCheckBox;
    private int selectedPosition = -1;
    private int medLocation = -1;
    private Map<Integer, DataObjectHolder> map;

    public MedicationsAdapter(Context context,
                              List<MedicationEntity> resultList,
                              boolean showCheckBox) {
        this.showCheckBox = showCheckBox;
        this.medications = resultList;
        this.map = new HashMap<Integer, DataObjectHolder>();
    }


    @NonNull
    @Override
    public MedicationsAdapter.DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.medication_item, parent, false);

        return new DataObjectHolder(view);
    }

    public boolean setCheck(int position,
                            boolean isChecked) {
        MedicationsAdapter.DataObjectHolder holder = map.get(position);
        if(null!=holder) {
            holder.checkSelected.setChecked(isChecked);
            return true;
        }
        return false;
    }

    @Override
    public void onBindViewHolder(@NonNull MedicationsAdapter.DataObjectHolder holder,
                                 int position) {
        map.put(position, holder);
        StringBuilder sb = new StringBuilder();
        MedicationEntity med = medications.get(position);

        if (!showCheckBox) {
            holder.checkSelected.setVisibility(View.GONE);
        } else {
            holder.checkSelected.setVisibility(View.VISIBLE);
        }

        boolean isCheck = (selectedPosition ==position)?true:false;
        holder.checkSelected.setChecked(isCheck);

        medLocation = med.getMedicationLocation();

        if (medLocation > 0 && medLocation < 15) {
            //show bin #
            if (med.getMedicationLocation() < 10) {
                sb.append("0");
            }
            sb.append(med.getMedicationLocation())
                    .append(": ")
                    .append(med.getMedicationName())
                    .append(" ")
                    .append(med.getStrengthMeasurement());
            holder.tvMedicationName.setText(sb.toString());
        } else {
            //show location name
            sb.append(med.getMedicationLocationName())
                    .append(": ")
                    .append(med.getMedicationName())
                    .append(" ")
                    .append(med.getStrengthMeasurement());
            holder.tvMedicationName.setText(sb.toString());
        }
    }

    @Override
    public int getItemCount() {
        return medications.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvMedicationName;
        CheckBox checkSelected;

        DataObjectHolder(View itemView) {
            super(itemView);
            tvMedicationName = itemView.findViewById(R.id.textview_medicationName);
            tvMedicationName.setOnClickListener(this);
            checkSelected = itemView.findViewById(R.id.checkbox_medication_item);
            checkSelected.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            selectedPosition = getAdapterPosition();
            myClickListener.onItemClick(selectedPosition, v);
            notifyDataSetChanged();
        }
    }

   /* public void toggleChecked(int position) {
        selectedPosition = position;

        notifyDataSetChanged();
    }*/

    public void setOnItemClickListener(MedicationsAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }
}
