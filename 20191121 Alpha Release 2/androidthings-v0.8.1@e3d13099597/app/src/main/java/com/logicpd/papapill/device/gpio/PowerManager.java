package com.logicpd.papapill.device.gpio;

import android.util.Log;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;
import com.logicpd.papapill.device.tinyg.BoardDefaults;

import java.io.IOException;

/**
 * This simple class utilizes the Android Things Peripheral Manager to control
 * the GPIO pins for monitoring power:
 *
 * GPIO_POWER_SOURCE - input: high - battery powered, low - AC powered
 * GPIO_LOW_BATTERY - input: high - battery < 11.5V, low - battery > 11.5V
 * GPIO_DEAD_BATTERY - input: high - battery < 11.2V, low - battery > 11.2V
 * GPIO_BATTERY_DISCONNECT - output: high - disconnects battery, low - connects battery
 */
public class PowerManager {

    public String TAG;
    private Gpio mAcPowered, mBatteryDisconnect, mLowBattery, mDeadBattery;

    private PowerManager() {
        TAG = this.getClass().getName();
        this.initialize();
    }

    private static class LazyHolder {
        private static final PowerManager INSTANCE = new PowerManager();
    }

    public static PowerManager getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * Open connection to GPIO peripheral manager.
     */
    private void initialize() {
        try {
            mAcPowered = PeripheralManager.getInstance().openGpio(BoardDefaults.GPIO_POWER_SOURCE);
            // Initialize the pin as an input
            mAcPowered.setDirection(Gpio.DIRECTION_IN);
            // In contradiction to the notes in Papapill Alpha Patch Board schematic, if AC power is
            // present, it turns on FET Q4 which drives POWER_SOURCE_2 to ground. If AC power is not
            // present, R18 pulls POWER_SOURCE_2 up to 3.3V.
            mAcPowered.setActiveType(Gpio.ACTIVE_LOW);

            mLowBattery = PeripheralManager.getInstance().openGpio(BoardDefaults.GPIO_LOW_BATTERY);
            // Initialize the pin as an input
            mLowBattery.setDirection(Gpio.DIRECTION_IN);
            // The op-amp is set up to be an inverting comparitor, so when the battery goes low, the
            // output turns the open collector off and the pull-ups bring the signal high
            mLowBattery.setActiveType(Gpio.ACTIVE_HIGH);

            mDeadBattery = PeripheralManager.getInstance().openGpio(BoardDefaults.GPIO_DEAD_BATTERY);
            // Initialize the pin as an input
            mDeadBattery.setDirection(Gpio.DIRECTION_IN);
            // The op-amp is set up to be an inverting comparitor, so when the battery goes low, the
            // output turns the open collector off and the pull-ups bring the signal high
            mDeadBattery.setActiveType(Gpio.ACTIVE_HIGH);

            mBatteryDisconnect = PeripheralManager.getInstance().openGpio(BoardDefaults.GPIO_BATTERY_DISCONNECT);
            mBatteryDisconnect.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            Log.i("GPIO", "Opened GPIOs for Power Source");
        } catch (IOException | RuntimeException e) {
            Log.e("GPIO","Failed to Open GPIO for PowerManager");
        }
    }

    /**
     * Close connection to GPIO peripheral manager.
     */
    private void close() {
        if (!successfullyClosedGpioPin(mAcPowered)) {
            Log.d(TAG, "Couldn't close mAcPowered");
        }

        if (!successfullyClosedGpioPin(mBatteryDisconnect)) {
            Log.d(TAG, "Couldn't close mBatteryDisconnect");
        }

        if (!successfullyClosedGpioPin(mLowBattery)) {
            Log.d(TAG, "Couldn't close mLowBattery");
        }

        if (!successfullyClosedGpioPin(mDeadBattery)) {
            Log.d(TAG, "Couldn't close mDeadBattery");
        }
    }

    private boolean successfullyClosedGpioPin(Gpio pin) {
        boolean result = false;
        if (pin != null) {
            try {
                pin.close();
                pin = null;
                result = true;
            } catch (IOException e) {
                Log.e("GPIO","Failed to close GPIO pin");
            }
        }
        return result;
    }

    /**
     * Returns true if the system is powered by wall AC.
     * @return boolean
     */
    public boolean isAcPowered() {
        boolean isAcPowered = false;
        try {
            isAcPowered = mAcPowered.getValue();
        } catch (IOException e) {
            Log.w(TAG, "Unable to read Power Source", e);
        }

        return isAcPowered;
    }

    /**
     * Returns true if the battery voltage is at or below 11.5V.
     * @return boolean
     */
    public boolean isBatteryLow() {
        boolean isBatteryLow = false;
        try {
            isBatteryLow = mLowBattery.getValue();
        } catch (IOException e) {
            Log.w(TAG, "Unable to read Low Battery", e);
        }

        return isBatteryLow;
    }

    /**
     * Returns true if the battery is at or below 11.2V.
     * @return boolean
     */
    public boolean isBatteryDead() {
        boolean isBatteryDead = false;
        try {
            isBatteryDead = mDeadBattery.getValue();
        } catch (IOException e) {
            Log.w(TAG, "Unable to read Dead Battery", e);
        }

        return isBatteryDead;
    }

    /**
     * Disconnects the battery from powering the system. If the system is not running on AC power,
     * this will turn the system off.
     */
    public void disconnectBattery() {
        try {
            mBatteryDisconnect.setDirection(Gpio.DIRECTION_OUT_INITIALLY_HIGH);
            mBatteryDisconnect.setActiveType(Gpio.ACTIVE_HIGH);
            mBatteryDisconnect.setValue(true);
        } catch(IOException e) {
            Log.w(TAG, "Unable to Disconnect Battery");
        }
    }

    /**
     * This ensures that the system can be powered by the battery in the event of AC power loss.
     */
    public void connectBattery() {
        try {
            mBatteryDisconnect.setDirection(Gpio.DIRECTION_OUT_INITIALLY_HIGH);
            mBatteryDisconnect.setActiveType(Gpio.ACTIVE_HIGH);
            mBatteryDisconnect.setValue(false);
        } catch(IOException e) {
            Log.w(TAG, "Unable to Connect Battery");
        }
    }
}
