package com.logicpd.papapill.device.i2c;

import android.util.Log;
import com.google.android.things.pio.I2cDevice;
import com.google.android.things.pio.PeripheralManager;
import java.io.IOException;

/**
 * This simple class utilizes the Android Things Peripheral Manager to use the i2c bus to turn
 * the dispensing cup light on and off. The LED driver is a TI LP5562.
 *
 * The LED driver is located with the Cup proximity sensor behind a TI TCA9548A used to multiplex
 * the i2c clock and data lines. The mux needs to be changed to the desired sensor before it can
 * communicate with the desired sensor or there will be cross talk. Care must be taken that only one
 * sensor is enabled at a time. Multiple sensors are allowed and the bridge is set to the channel
 * the sensor is instantiated with.
 *
 * Initialization of the LED driver is taken from 7.3.2 Direct I2C Register PWM Control Example of
 * the data sheet:
 *
 * http://www.ti.com/lit/ds/symlink/lp5562.pdf
 *
 * If this device is used with an i2c mux, it is assumed that there is a single service or module
 * that communicates with all devices connected through this mux. If that is not the case, a
 * semaphore will be needed in the mux class and users of the mux will need to take the semaphore
 * before the operation and clear it after the operation to ensure i2c avoid any cross
 * communication.
 */
public class CupLight {
    private static String i2cPort;
    private byte muxLocation;

    private static final int I2C_CUP_LIGHT_ADDR = 0x30;
    private static I2cDevice muxDevice;
    private I2cDevice ledDriver;

    public CupLight(String port, byte muxLocationAddress) {
        this.i2cPort = port;
        this.muxLocation = muxLocationAddress;
        this.initialize();
    }

    private static final int LED_CTRL_REG = 0x00;
    private static final byte LED_CHIP_ENABL_BIT = 0x40;
    private static final int LED_CFG_CTRL_REG = 0x08;
    private static final byte LED_INTERNAL_CLK_BIT = 0x01;
    private static final int LED_MAPPING_REG = 0x70;
    private static final byte LED_ALL_I2C_CONTROL_BITS = 0x00;
    private static final int LED_BLUE_PWM_REG= 0x02;
    private static final int LED_GREEN_PWM_REG= 0x03;
    private static final int LED_RED_PWM_REG= 0x04;
    private static final int LED_WHITE_PWM_REG= 0x0e;

    // Ensure the LED driver for the cup light exists
    private void initialize() {
        Mux.getInstance().setChannel(this.muxLocation);
        try {
            ledDriver = PeripheralManager.getInstance().openI2cDevice(this.i2cPort, I2C_CUP_LIGHT_ADDR);
            // Enable the driver, use the internal clock, direct PWM control, turn all LEDs off
            ledDriver.writeRegByte(LED_CTRL_REG, LED_CHIP_ENABL_BIT);
            ledDriver.writeRegByte(LED_CFG_CTRL_REG, LED_INTERNAL_CLK_BIT);
            ledDriver.writeRegByte(LED_MAPPING_REG, LED_ALL_I2C_CONTROL_BITS);
            // Writing 0 as the PWM value turns the driver output to off, 0xff is completely on
            ledDriver.writeRegByte(LED_BLUE_PWM_REG, (byte)0);
            ledDriver.writeRegByte(LED_GREEN_PWM_REG, (byte)0);
            ledDriver.writeRegByte(LED_RED_PWM_REG, (byte)0);
            ledDriver.writeRegByte(LED_WHITE_PWM_REG, (byte)0);
        } catch (IOException | RuntimeException e) {
            Log.e("I2C","Failed to Intialize I2C LED Driver for Cup Light.");
        }

        CloseI2cDevice(ledDriver);
    }

    private void CloseI2cDevice(I2cDevice device) {
        if (device != null) {
            try {
                device.close();
            } catch (IOException e) {
                Log.e("I2C","Unable to close I2C device");
            }
        }
    }

    private void setCupLight(boolean isLightOn) {
        byte pwmValue = 0x00;

        Mux.getInstance().setChannel(this.muxLocation);

        if (isLightOn) {
            pwmValue = (byte)0xff;
        }

        try {
            ledDriver = PeripheralManager.getInstance().openI2cDevice(this.i2cPort, I2C_CUP_LIGHT_ADDR);
            // 0xff turns the PWM completely on
            ledDriver.writeRegByte(LED_WHITE_PWM_REG, pwmValue);
        } catch (IOException | RuntimeException e) {
            if (isLightOn) {
                Log.e("I2C","Failed to turn on Cup Light.");
            } else {
                Log.e("I2C","Failed to turn off Cup Light.");
            }
        }
        CloseI2cDevice(ledDriver);
    }

    public void setCupLightOn() {
        Log.i("I2C", "Turning Cup Light On");
        setCupLight(true);
    }

    public void setCupLightOff() {
        Log.i("I2C", "Turning Cup Light Off");
        setCupLight(false);
    }
}

