package com.logicpd.papapill.device.i2c;
import android.util.Log;
import com.google.android.things.pio.I2cDevice;
import com.google.android.things.pio.PeripheralManager;

import java.io.IOException;

/**
 * Mux represents the TI TCA9548A I2C multiplexer. I2C port is hard coded to I2C1.
 *
 * It is assumed that there is a single service or module that communicates with all devices
 * connected through this mux. If that is not the case, a semaphore will be needed to ensure i2c
 * bus operations are complete before the next operation to avoid any cross communication. The
 * semaphore will need to be taken before the i2c operation and released after the operation is
 * complete to avoid any cross communication.
 */
public class Mux {
    private static I2cDevice muxDevice;
    private static final String I2C_PORT = "I2C1";
    private static final int I2C_MUX_ADDRESS = 0x70;

    private Mux() {
        this.initialize();
    }

    private void initialize() {
        try {
            // Open the bridge
            muxDevice = PeripheralManager.getInstance().openI2cDevice(I2C_PORT, I2C_MUX_ADDRESS);
        } catch (IOException | RuntimeException e) {
            Log.e("I2C","Failed to Intialize I2C muxDevice.");
        }
    }
    private static class LazyHolder {
        private static final Mux INSTANCE = new Mux();
    }

    public static Mux getInstance() {
        return LazyHolder.INSTANCE;
    }

    public static void setChannel(byte channel) {
        try {
            byte [] buffer = new byte[1];
            buffer[0] = channel;
            muxDevice.write(buffer, 1);
        } catch (IOException | RuntimeException e) {
            Log.e("I2C","Failed writing to muxDevice.");
        }
    }
}
