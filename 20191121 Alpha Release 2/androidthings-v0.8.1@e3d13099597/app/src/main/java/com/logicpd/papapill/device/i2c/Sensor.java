package com.logicpd.papapill.device.i2c;

import android.util.Log;
import com.google.android.things.pio.I2cDevice;
import com.google.android.things.pio.PeripheralManager;
import com.logicpd.papapill.data.AppConfig;

import java.io.IOException;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;

/**
 * This simple class utilizes the Android Things Peripheral Manager to use the i2c bus to get
 * sensor status from the door, drawer, and cup sensors to determine if the respective items are
 * present or open/closed.
 *
 * Sensor information: https://www.vishay.com/docs/84150/vcnl3020.pdf
 *
 * Since the sensor does not have a programmable address, a TI TCA9548A is used to multiplex the i2c
 * clock and data lines. The mux needs to be changed to the desired sensor before it can
 * communicate with the desired sensor or there will be cross talk. Care must be taken that only one
 * sensor is enabled at a time. Multiple sensors are allowed and the bridge is set to the channel
 * the sensor is instantiated with.
 *
 * It is assumed that there is a single service or module that communicates with all devices
 * connected through mux. If that is not the case, a semaphore will be needed in the mux class that
 * takes the semaphore before the operation and clears it after the operation to ensure i2c
 * avoid any cross communication.
 */
public class Sensor {

    private static String i2cPort;
    private static final int I2C_SENSOR_ADDRESS = 0x13;
    private static final int I2C_SENSOR_CTRL_REG = 0x80;
    private static final byte I2C_SENSOR_CTRL_ENABLE_SAMPLING = 0x03;
    private static final int I2C_SENSOR_IR_LED_REG = 0x83;
    private static final byte I2C_SENSOR_IR_LED_CURRENT = 0x14;
    private static final int I2C_SENSOR_PROX_RESULT_MSB_REG = 0x87;
    private static final int I2C_SENSOR_PROX_RESULT_LSB_REG = 0x88;

    private byte muxLocation;
    private int thresholdClosed;

    private I2cDevice sensor;

    public Sensor(String port, byte muxLocationAddress, int thresholdClosed) {
        this.i2cPort = port;
        this.muxLocation = muxLocationAddress;
        this.thresholdClosed = thresholdClosed;
        this.initialize();
    }

    // Ensure the sensor has automatic sensor sampling enabled
    private void initialize() {
        Mux.getInstance().setChannel(this.muxLocation);

        try {
            // Enable the prox_en and selftimed_en bits in the control register, this allows for the
            // sensor manage sampling for proximity automatically.
            sensor = PeripheralManager.getInstance().openI2cDevice(this.i2cPort, I2C_SENSOR_ADDRESS);
            sensor.writeRegByte(I2C_SENSOR_CTRL_REG, I2C_SENSOR_CTRL_ENABLE_SAMPLING);
            // Configure the IR LED current to be 200 mA (maximum).
            sensor.writeRegByte(I2C_SENSOR_IR_LED_REG, I2C_SENSOR_IR_LED_CURRENT);
        } catch (IOException | RuntimeException e) {
            Log.e("I2C","Failed to Initialize I2C sensor.");
        }

        CloseI2cDevice(sensor);
    }

    private void CloseI2cDevice(I2cDevice device) {
        if (device != null) {
            try {
                device.close();
            } catch (IOException e) {
                Log.e("I2C","Unable to close I2C device");
            }
        }
    }

    // TODO Beta - for some reason, the first read keeps failing, retries should be added
    public Integer getSensorValue() {
        Log.i("I2C", "Sensor Proximity Read");

        int return_val = 0;

        Mux.getInstance().setChannel(this.muxLocation);

        try {
            sensor = PeripheralManager.getInstance().openI2cDevice(this.i2cPort, I2C_SENSOR_ADDRESS);

            byte ready = sensor.readRegByte(I2C_SENSOR_CTRL_REG & 0xff);
            if ((ready & 0x20) != 0x20) { // -93 is 0xa3
                Log.i("I2C", "Sensor reading not ready");
            } else {
                // readRegWord() reads in little endian order, thus separate reads
                int sensor_value = ((sensor.readRegByte(I2C_SENSOR_PROX_RESULT_MSB_REG) & 0xff) << 8) |
                                    (sensor.readRegByte(I2C_SENSOR_PROX_RESULT_LSB_REG) & 0xff);
                return_val = (sensor_value & 0xffff);  // sensor value is ready now
            }
        } catch (IOException | RuntimeException e) {
            Log.e("I2C","Failed to find I2C sensor.");
        }

        CloseI2cDevice(sensor);

        return return_val;
    }

    public boolean isSensorClosed() {
        int sensorValue = getSensorValue();
        if (sensorValue == 0) {
            // i2c read not ready, assume true since most sensors are rarely open
            return true;
        }
        return (sensorValue > thresholdClosed);
    }
}
