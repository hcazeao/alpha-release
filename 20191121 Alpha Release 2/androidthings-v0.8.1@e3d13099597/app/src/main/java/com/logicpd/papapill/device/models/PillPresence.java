package com.logicpd.papapill.device.models;

import com.logicpd.papapill.device.tinyg.MnemonicManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This object contains pill presence information from the TinyG. The TinyG has a calibrated
 * threshold value for vacuum pressure differential and uses that against a pressure reading to
 * determine whether or not a pill is attached to dip tube head.
 */
public class PillPresence {

    public Boolean isPillPresent;

    public PillPresence() { }

    /**
     * Method to parse the JSON string containing the boolean value we receive from the TinyG.
     * @param js
     * @return
     * @throws JSONException
     */
    public boolean parseResponse(JSONObject js) throws JSONException {
        if (js.has(MnemonicManager.MNEMONIC_JSON_RESPONSE)) {
            JSONObject r = js.getJSONObject(MnemonicManager.MNEMONIC_JSON_RESPONSE);
            if (r.has(MnemonicManager.MNEMONIC_GROUP_PSI)) {
                isPillPresent = (1 == r.getInt(MnemonicManager.MNEMONIC_GROUP_PSI));
                return true;
            }
        }
        return false;
    }
}
