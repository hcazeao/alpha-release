package com.logicpd.papapill.device.tinyg;

import android.util.Log;

import com.logicpd.papapill.data.AppConfig;

/**
 * This class contains several global parameters that factor in calculating
 * motor movements throughout the system.
 *
 * Frank approved my next implementation to change indexing.
 * It is bin0 (Carousel HOME), then clockwise, 1, 2 ....14.
 */
public final class CoordinateManager {
    public static final String TAG = "CoordinateManager";
    private static final int BIN_COUNT = 15;
    private static final int CIRCLE_DEGREE = 360;
    private static final int HALF_CIRCLE_DEGREE = CIRCLE_DEGREE/2;
    private static final int BIN_ANGLE = (CIRCLE_DEGREE / BIN_COUNT);
    private static final double DOOR_OFFSET = -53.5;  // degree added to put the bin in the door

    public static final int DISPENSE_BIN_OFFSET = 111;

    // HOME bin should always be 0.
    public static final int HOME_BIN = 0;

    /*
     * Home is 0 and it is trusted that absPositionOffset is set correctly so that homing the
     * carousel via the high level interface (setting carousel position to 0), puts the carousel in
     * home. The optical switch is not used.
     */
    public static int getHomeBinOffset()
    {
        return HOME_BIN;
    }

    /**
     * Given the binId (a value stored in the database representing a bin),
     * return the ideal location from the calibration file.
     * @param binId
     * @return
     */
    public static double getBinLocationDegrees(int binId) {
        if(binId < 0) {
            Log.e(TAG, "Invalid binId");
            return 0;
        }

        double binLocation = 0;

        switch (binId) {
            case 1:
                binLocation = AppConfig.getInstance().getBin1location();
                break;
            case 2:
                binLocation = AppConfig.getInstance().getBin2location();
                break;
            case 3:
                binLocation = AppConfig.getInstance().getBin3location();
                break;
            case 4:
                binLocation = AppConfig.getInstance().getBin4location();
                break;
            case 5:
                binLocation = AppConfig.getInstance().getBin5location();
                break;
            case 6:
                binLocation = AppConfig.getInstance().getBin6location();
                break;
            case 7:
                binLocation = AppConfig.getInstance().getBin7location();
                break;
            case 8:
                binLocation = AppConfig.getInstance().getBin8location();
                break;
            case 9:
                binLocation = AppConfig.getInstance().getBin9location();
                break;
            case 10:
                binLocation = AppConfig.getInstance().getBin10location();
                break;
            case 11:
                binLocation = AppConfig.getInstance().getBin11location();
                break;
            case 12:
                binLocation = AppConfig.getInstance().getBin12location();
                break;
            case 13:
                binLocation = AppConfig.getInstance().getBin13location();
                break;
            default:
                // 14 and higher force to 14
                binLocation = AppConfig.getInstance().getBin14location();
                break;
        }

        return binLocation;
    }

    /**
     * Given the binId (a value stored in the database representing a bin),
     * return the ideal location in degrees as an offset for the door (user manual access to load meds)
     * @param binId
     * @return
     */
    public static double getDoorOffset(int binId) {
        return getBinLocationDegrees(binId) + DOOR_OFFSET;
    }

    /**
     * Returns the shortest path to move from current angle to target angle.
     * @param current
     * @param target
     * @return
     */
    public static double getShortestAngle(double current, double target) {
        // Take the difference between the two input angles. Normalize the angle
        // to within [-180, 180) range to find the shortest path.
        double diff = (target - current + HALF_CIRCLE_DEGREE) % CIRCLE_DEGREE - HALF_CIRCLE_DEGREE;
        return (diff < -HALF_CIRCLE_DEGREE) ? (diff + CIRCLE_DEGREE) : diff;
    }
}
