package com.logicpd.papapill.device.tinyg;

import android.util.Log;

import com.logicpd.papapill.computervision.detection.DetectionData;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.interfaces.OnBinReadyListener;
import com.logicpd.papapill.interfaces.OnErrorListener;
import com.logicpd.papapill.receivers.IntentBroadcast;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.serial.ResponseParser;
import com.logicpd.papapill.device.serial.SerialDriver;
import com.logicpd.papapill.device.serial.SerialWriter;
import com.logicpd.papapill.device.tinyg.commands.CommandFullDispense;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.interfaces.OnLimitSwitch;
import com.logicpd.papapill.computervision.detection.Detector;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * This class handles interfacing with the TinyG motor control board.
 */
public class TinyGDriver
{
    public String TAG;
    public static ArrayBlockingQueue<String> txQueue = new ArrayBlockingQueue<>(50000);
    public static ArrayBlockingQueue<String> rxQueue = new ArrayBlockingQueue<>(10000);

    private final SerialDriver serialDriver = SerialDriver.getInstance();
    public ResponseParser responseParser = new ResponseParser();
    public SerialWriter serialWriter = new SerialWriter(txQueue);
    public CommandManager commandManager = CommandManager.getInstance();
    public MachineManager machineManager = MachineManager.getInstance();

    /**
     * This class is a lazy-loaded singleton. It follows the intialization-on-demand holder idiom.
     * See: https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
     */
    private TinyGDriver()
    {
        TAG = this.getClass().getName();
    }

    private static class LazyHolder {
        private static final TinyGDriver INSTANCE = new TinyGDriver();
    }

    public static TinyGDriver getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * Opens the serial driver connection. Start our running threads. Configure the TinyG.
     * @return
     */
    public boolean initialize(boolean configureTinyG) {

        // Create and start our threads.
        Thread commandThread = new Thread(commandManager);
        commandThread.setName("CommandManager");
        commandThread.setDaemon(true);
        commandThread.start();

        Thread serialWriterThread = new Thread(serialWriter);
        serialWriterThread.setName("SerialWriter");
        serialWriterThread.setDaemon(true);
        serialWriterThread.start();

        Thread responseParserThread = new Thread(responseParser);
        responseParserThread.setName("ResponseParser");
        responseParserThread.setDaemon(true);
        responseParserThread.start();

        // Connect to the serial port.
        if (this.serialDriver.initialize()) {
            // On BBv2, it seems the TinyG will not respond to commands after a power on reset
            // unless an initial carriage return is transmitted to "wake up" the UART comms. This
            // behavior was not seen on BBv1. Perhaps this is a side effect of switching from UART
            // over USB to a direct UART?
            priorityWriteByte((byte)0xD);
            // Send initial configuration settings to TinyG.
            this.doTinygInit(configureTinyG);
            return true;
        }
        return false;
    }

    /**
     * Add an entry to the Rx queue. Response parser thread handles parsing JSON objects in queue.
     * @param line
     */
    public synchronized void appendRxQueue(String line) {
        TinyGDriver.rxQueue.add(line);
    }

    /**
     * Send a single fully formed message to the TinyG. This method will call the
     * SerialWriter method to add msg to queue.
     * @param msg
     * @throws java.lang.Exception
     */
    public synchronized void write(String msg) {
        TinyGDriver.getInstance().serialWriter.addCommandToBuffer(msg);
    }

    public void reset()
    {
        commandManager.requestReset(true);
    }

    /**
     * Send a single character message to the TinyG. This method will call the
     * SerialDriver write method to bypass the message queue.
     * @param b
     * @throws Exception
     */
    public void priorityWriteByte(Byte b) {
        TinyGDriver.getInstance().serialDriver.writeByte(b);
    }

    /**
     * Sends the ^x character to the TinyG to trigger SW reset.
     * @throws Exception
     */
    public void softwareReset() {
        priorityWriteByte(CommandBuilder.CMD_APPLY_RESET);
    }

    /**
     * Configures the TinyG with default settings.
     */
    public void doTinygInit(boolean configureTinyG) {
        String param = String.format("%s", configureTinyG);
        commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_INIT, param, null);
    }

    /**
     * prep for dispense camera photo.
     * @param binId
     */
    public void doPrepDispense(int binId,
                               int dispenseNumber) {
        String param = String.format("%s %s", binId, dispenseNumber);
        commandManager.callCommand(DeviceCommand.COMMAND_PRE_DISPENSE, param, null);
    }

    /**
     * Fully dispense a pill from a bin number.
     */
    public void doFullDispense(byte[] imageBytes,
                               int dispenseNumber) {
        String param = String.format("%s", dispenseNumber);
        commandManager.callCommand(DeviceCommand.COMMAND_FULL_DISPENSE, param, imageBytes);
    }

    /**
     * Acquire a pill and home the Z stage for use in Reliability Testing
     *
     * @param imageBytes -- for identifying pills
     * @param dispenseNumber -- number of pills to dispense
     */
    public void doReliabilityAcquirePill(byte[] imageBytes,
                               int dispenseNumber) {
        String param = String.format("%s", dispenseNumber);
        commandManager.callCommand(DeviceCommand.COMMAND_RELIABILITY_ACQUIRE_PILL, param, imageBytes);
    }

    public void doReliabilityMechanicalAcquire(int dispenseNumber) {
        String param = String.format("%s", dispenseNumber);
        commandManager.callCommand(DeviceCommand.COMMAND_RELIABILITY_MECHANICAL_ACQUIRE, param, null);
    }

    /*
     * Current bin index from extrusion (unused bin) is arbitrary.
     * Access door location is also not final.
     */
    public void doMoveBin4User(int binId) {
        double targetPosition = CoordinateManager.getDoorOffset(binId);
        String param = String.format("%f 0.3", targetPosition);
        Log.d(TAG, String.format("binId: %d targetPosition: %f param: %s", binId, targetPosition, param));
        commandManager.callCommand(DeviceCommand.COMMAND_ROTATE_CAROUSEL, param, null);
    }

    /*
     * Move carousel to home position
     */
    public void doMoveCarouselHome() {

        double targetPosition = CoordinateManager.getHomeBinOffset();
        String param = String.format("%f 0.3", targetPosition);
        Log.d(TAG, String.format(" targetPosition: %f param: %s", targetPosition, param));
        commandManager.callCommand(DeviceCommand.COMMAND_ROTATE_CAROUSEL, param, null);
    }

    public boolean isStateMachineIdle()
    {
        return commandManager.isIdle();
    }

    public void readPositions()
    {
        commandManager.callCommand(DeviceCommand.COMMAND_ROTATE_CAROUSEL, CarouselPosCmdEnum.home.toString(), null);
    }
}
