package com.logicpd.papapill.device.tinyg.commands;

import android.util.Log;

import com.logicpd.papapill.device.enums.CommandState;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.gpio.LightringManager;
import com.logicpd.papapill.device.tinyg.CommandManager;
import com.logicpd.papapill.device.tinyg.CoordinateManager;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.IntentTypeEnum;
import com.logicpd.papapill.receivers.IntentBroadcast;

import static com.logicpd.papapill.device.enums.CommandState.COMPLETE_STATE;
import static com.logicpd.papapill.device.enums.CommandState.PRE_DISPENSE_WAIT_FOR_PARAM_ERROR_DISPLAY;


public class CommandPreDispense extends BaseCommand  {
    // Identifier for this command.
    private static final DeviceCommand identifier = DeviceCommand.COMMAND_PRE_DISPENSE;
    private String TAG;
    private int binId;
    private int dispenseNumber;

    public CommandPreDispense() {
        super(identifier);
        TAG = this.getClass().getSimpleName();
    }

    @Override
    public void execute() {

        switch (operation) {
            case OP_STATE_START:
                // Convert the parameter string that was passed into individual arguments
                // separate by spaces.
                String[] paramArray = getParamArray();
                // Full dispense is expected to have only 1 parameter (the bin Id). Ensure
                // we have that amount before continuing (ignore any additional parameters).
                if (paramArray.length < 1) {
                    String msg = "Command failed due to too few or bad parameters.";
                    Log.e(name, msg);

                    IntentBroadcast.send(IntentTypeEnum.msg,
                            TAG,
                            PRE_DISPENSE_WAIT_FOR_PARAM_ERROR_DISPLAY.toString(),
                            IntentMsgEnum.paramError.toString(),
                            "unknown");
                    return;
                }

                /*
                 * TODO Debug light issue.
                 * Turn on light prior to motor control.
                 * (because light sometimes fail if turn on after motor controls).
                 */
                LightringManager.getInstance().configureOutput(true);
                binId = Integer.parseInt(paramArray[0]);
                dispenseNumber = Integer.parseInt(paramArray[1]);

                // Kickoff our sequence by calling our first sub command. We first need
                // to raise the Z axis so that it is fully retracted and out of the carousel's way.
                CommandManager.getInstance().callCommand(
                        DeviceCommand.COMMAND_MOTOR_HOME, "z", null);

                setState(CommandState.PRE_DISPENSE_WAITING_FOR_Z_HOME);
                break;

            case OP_STATE_RUN:
                switch (state) {
                    case PRE_DISPENSE_WAITING_FOR_Z_HOME:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_MOTOR_HOME)) {

                            // Home the rho axis so the camera is in prime position to take the
                            // bin picture.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_MOTOR_HOME, "x", null);

                            setState(CommandState.PRE_DISPENSE_WAITING_FOR_R_HOME);
                        }
                        break;

                    case PRE_DISPENSE_WAITING_FOR_R_HOME:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_MOTOR_HOME)) {

                            // Get the bin location from the passed in binId.
                            double targetPosition = CoordinateManager.getBinLocationDegrees(binId);
                            String params = String.format("%f 0.3", targetPosition);

                            // Once both Z and Rho are homed, its now safe to rotate the carousel
                            // to the proper bin location.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_ROTATE_CAROUSEL, params, null);

                            setState(CommandState.PRE_DISPENSE_WAITING_FOR_CAROUSEL_MOVE);
                        }
                        break;

                    case PRE_DISPENSE_WAITING_FOR_CAROUSEL_MOVE:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_ROTATE_CAROUSEL)) {

                            // Now center the camera in the middle of the bin.
                            // NOTE: This command is currently disabled and thus the state is
                            // effectively a NOP. In the current alpha breadboard, homing r and
                            // theta already puts us at the optimal camera position to take a
                            // picture of the bin. This may not be the case in the future.
                            //CommandManager.getInstance().callCommand(
                            //DeviceCommand.COMMAND_MOTOR_MOVE, "x 25 2000", null);

                            // Advance our state variable.
                            setState(CommandState.PRE_DISPENSE_WAITING_FOR_CAMERA_CENTERED);
                        }
                        break;

                    case PRE_DISPENSE_WAITING_FOR_CAMERA_CENTERED:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_MOTOR_MOVE)) {

                            // Advance our state variable.
                            setState(CommandState.COMPLETE_STATE);

                            IntentBroadcast.send(IntentTypeEnum.msg, TAG,
                                    COMPLETE_STATE.toString(),
                                    IntentMsgEnum.success.toString(),
                                    String.valueOf(dispenseNumber));
                        }
                        break;
                }
                break;

            default:
                break;
        }
    }
}
