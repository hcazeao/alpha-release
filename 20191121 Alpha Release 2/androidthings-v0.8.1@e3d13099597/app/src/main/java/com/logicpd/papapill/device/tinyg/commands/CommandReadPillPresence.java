// TODO: Header goes here

package com.logicpd.papapill.device.tinyg.commands;

import android.util.Log;

import com.logicpd.papapill.device.models.PillPresence;
import com.logicpd.papapill.device.enums.CommandState;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.tinyg.CommandBuilder;
import com.logicpd.papapill.device.tinyg.TinyGDriver;

import org.json.JSONException;
import org.json.JSONObject;

import static com.logicpd.papapill.device.enums.CommandState.COMPLETE_STATE;

public final class CommandReadPillPresence extends BaseCommand {

    // Identifier for this command.
    private static final DeviceCommand identifier = DeviceCommand.COMMAND_READ_IS_PILL_PRESENT;
    private String TAG;

    // Object to hold status of pill presence data.
    public PillPresence pillPresence;

    public CommandReadPillPresence() {
        super(identifier);
        TAG = this.getClass().getSimpleName();
    }

    /**
     * Read motor axes positions.
     */
    @Override
    public void execute() {

        switch (operation) {
            case OP_STATE_START:
                // Grab the data object that was passed into the command and make sure it is
                // not null. We can't complete execution if we don't have a place to put our
                // result data.
                if (data instanceof PillPresence) {
                    pillPresence = (PillPresence) data;
                } else {
                    Log.e(name, "Command failed. Data argument is wrong type or null.");
                    return;
                }

                // Now go ahead and query the position from the TinyG.
                TinyGDriver.getInstance().write(CommandBuilder.CMD_IS_PILL_PRESENT);

                // Mark this command as no longer idle.
                setState(CommandState.WAITING_FOR_RESPONSE);
                break;

            case OP_STATE_RUN:
                switch (state) {
                    case WAITING_FOR_RESPONSE:
                        // Now we must wait for the response parser thread to receive a response
                        // from the TinyG. When it does, it will call command manager's bind
                        // response method which will match the response with the command. If the
                        // response was ours, we will see our response variable become populated.
                        if (!response.isEmpty()) {
                            try {
                                // Convert our response string to JSON.
                                JSONObject responseJson = new JSONObject(response);

                                // Parse the JSON response which populates the data object with the
                                // results of the command. If the parse was successful, we're done.
                                if(pillPresence.parseResponse(responseJson)) {
                                    Log.d(name, "Is Pill Present = " + pillPresence.isPillPresent);
                                    setState(COMPLETE_STATE);
                                }

                            } catch (JSONException ex) {
                                Log.e(name, "Exception while parsing response: " + ex);
                            }
                        }
                        break;
                }
                break;

            default:
                break;
        }
    }
}