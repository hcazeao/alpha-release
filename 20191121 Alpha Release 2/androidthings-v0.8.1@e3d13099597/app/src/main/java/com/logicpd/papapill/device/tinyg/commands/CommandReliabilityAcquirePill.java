package com.logicpd.papapill.device.tinyg.commands;

import android.util.Log;

import com.logicpd.papapill.computervision.detection.DetectionData;
import com.logicpd.papapill.computervision.detection.Detector;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.enums.CommandState;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.gpio.LightringManager;
import com.logicpd.papapill.device.models.PickupData;
import com.logicpd.papapill.device.models.PositionData;
import com.logicpd.papapill.device.tinyg.CommandBuilder;
import com.logicpd.papapill.device.tinyg.CommandManager;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.IntentTypeEnum;
import com.logicpd.papapill.receivers.IntentBroadcast;

import static com.logicpd.papapill.device.enums.CommandState.REL_ACQUIRE_PILL_WAIT_FOR_PICK_ERROR_DISPLAY;
import static com.logicpd.papapill.device.enums.CommandState.REL_ACQUIRE_PILL_WAIT_FOR_VISION_ERROR_DISPLAY;

/**
 * The same as FullDispense, but stops after acquiring a pill. The ReliabilityTesterFragment then
 * moves the pill to the next bin.
 */
public final class CommandReliabilityAcquirePill extends BaseCommand {

    // Identifier for this command.
    private static final DeviceCommand identifier = DeviceCommand.COMMAND_RELIABILITY_ACQUIRE_PILL;
    private String TAG;

    // Internal parameters.
    private int dispenseNumber;

    // Data objects to pass to other sub commands.
    private PickupData pickupData = new PickupData();
    private PositionData positionData = new PositionData();

    // Holds coordinates generated from OpenCV Pill Detection algorithm.
    private byte[] mImageBytes;
    private DetectionData mDetectionData=null;

    public CommandReliabilityAcquirePill() {
        // Pass our identifier into the super's constructor. This will associate the
        // identifier with the command object. The identifier is a final property of
        // super so it can only be assigned during super's construction.
        super(identifier);
        TAG = this.getClass().getSimpleName();
    }

    /**
     * This is the core command of the entire dispense system. It contains all the
     * logic required to ultimately dispense a pill from a bin. All other command
     * can be thought of as sub commands to the full dispense command, as somewhere
     * along the line, we'll make a call to all existing existing commands from full
     * dispense. It performs the initial carousel and r-stage movement to position
     * the camera to take a snapshot of the bin we want to dispense from. It will
     * then call into the pill recognition algorithm to determine where exactly to
     * position the pickup head. Once positioned over a suspected pill location, it
     * will lower the pickup head in an attempt to grab the pill. If the grab was
     * successful, we will dispense the pill into the dispense bin. If the grab was
     * not successful, there are a number of recovery sequences from most desired to
     * least desired to execute in an attempt to try again.
     */
    @Override
    public void execute() {
        double fullRadiusMove;

        switch (operation) {
            case OP_STATE_START:
                mImageBytes = (byte[])data;
                String[] paramArray = getParamArray();
                dispenseNumber = Integer.parseInt(paramArray[0]);

                setState(CommandState.REL_ACQUIRE_PILL_CAMERA_PICTURE_DONE);
                break;

            case OP_STATE_RUN:
                switch (state) {

                    case REL_ACQUIRE_PILL_CAMERA_PICTURE_DONE:
                        Detector detector = new Detector();
                        mDetectionData = detector.processImage(mImageBytes);
                        if(null == mDetectionData ||
                                null == mDetectionData.getSelectedPills() ||
                                mDetectionData.getSelectedPills().isEmpty())
                        {
                            /*
                             * vision failed to detect.
                             * - motor home and report error.
                             */
                            setState(CommandState.REL_ACQUIRE_PILL_WAIT_FOR_VISION_ERROR_DISPLAY);
                            break;
                        }

                        Log.d(name, "Finished mVisualizer process image");
                        Log.d(name, String.format("r = %f, theta = %f, z = %f, mmZ = %f",
                                mDetectionData.getSelectedPills().get(0).getCoordinateData().radius,
                                mDetectionData.getSelectedPills().get(0).getCoordinateData().degrees,
                                mDetectionData.getSelectedPills().get(0).getCoordinateData().z,
                                mDetectionData.getSelectedPills().get(0).getCoordinateData().mmZ));



                        // Copy mmz to pickup data to be used later in fast z move.
                        pickupData.mmz = mDetectionData.getSelectedPills().get(0).getCoordinateData().mmZ;

                        // Use the coordinates returned from OpenCV pill detection algorithm to
                        // reposition the pickup head over the nearest/best pill. Reposition the
                        // r-axis first.
                        fullRadiusMove = mDetectionData.getSelectedPills().get(0).getCoordinateData().radius + AppConfig.getInstance().camera2TinyGRhoOffset;
                        String cmdReposR = String.format("x %f 2000", fullRadiusMove);

                        CommandManager.getInstance().callCommand(
                                DeviceCommand.COMMAND_MOTOR_MOVE, cmdReposR, null);

                        // Advance our state variable.
                        setState(CommandState.REL_ACQUIRE_PILL_WAITING_FOR_R_REPOSITION);

                        break;

                    case REL_ACQUIRE_PILL_WAITING_FOR_R_REPOSITION:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_MOTOR_MOVE)) {

                            // Read from the position sensor one more time to find our current
                            // absolute theta position.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_READ_POSITION, "", positionData);

                            // Advance our state variable.
                            setState(CommandState.REL_ACQUIRE_PILL_WAITING_FOR_READ_POSITION);
                        }
                        break;

                    case REL_ACQUIRE_PILL_WAITING_FOR_READ_POSITION:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_READ_POSITION)) {

                            // R-axis has been repositioned, now reposition theta-axis to the
                            // target (absolute) position, which is the current absolute position
                            // minus the pill's theta offset + a camera-pickup nozzle offset.
                            double targetPosition = positionData.getAbsoluteDegrees(true)
                                    + mDetectionData.getSelectedPills().get(0).getCoordinateData().degrees
                                    + AppConfig.getInstance().camera2TinyGThetaOffset;

                            String params = String.format("%f 0.3", targetPosition);

                            // Call the Rotate Carousel command to accurately position the pickup
                            // head to within 0.3 degrees of the pill's (absolute) theta location.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_ROTATE_CAROUSEL, params, null);

                            // Advance our state variable.
                            setState(CommandState.REL_ACQUIRE_PILL_WAITING_FOR_THETA_REPOSITION);
                        }
                        break;

                    case REL_ACQUIRE_PILL_WAITING_FOR_THETA_REPOSITION:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_ROTATE_CAROUSEL)) {

                            // Both R and Theta has been repositioned. The pickup head is now
                            // supposed to be directly over a pill. Time to pick it up.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_MOTOR_PICKUP, "", pickupData);

                            // Advance our state variable.
                            setState(CommandState.REL_ACQUIRE_PILL_MOVING_TO_PICKUP_PILL);
                        }
                        break;

                    case REL_ACQUIRE_PILL_MOVING_TO_PICKUP_PILL:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_MOTOR_PICKUP)) {

                            // Check the result of the pickup command.
                            setState(CommandState.COMPLETE_STATE);
                            if (pickupData.success) {
                                // Finished
                                IntentBroadcast.send(IntentTypeEnum.msg,
                                        TAG,
                                        CommandState.COMPLETE_STATE.toString(),
                                        IntentMsgEnum.acquired.toString(),
                                        String.valueOf(dispenseNumber));
                            } else {
                                // Otherwise, the pickup command indicates failed. Here depending
                                // on the pill type, we can try to perform a nudge or jog as a retry.
                                // This doesn't require a full homing of the pickup head as only a
                                // slight raise is required. If retries don't work after some time,
                                // we may need call the pickup command with a new location. Or
                                // possibly perform a "carousel jostle" and retake the picture.
                                // TODO: Find optimal retry strategy. For now, do the same as above.
                                // For now, simply do nothing and advance our state.

                                // we hit a limit at this time --> so just reset for now (maybe do above later)
                                // Turn off vacuum pump.
                                // TODO BETA: Refactor CMD_COOLANT_OFF to reflect the vacuum pump
                                TinyGDriver.getInstance().write(CommandBuilder.CMD_COOLANT_OFF);

                                // Turn off the light ring.
                                LightringManager.getInstance().configureOutput(false);

                                // it will hit a limit error if not picked
                                Log.d("CommandFullDispense", "onLimitError()");
                                IntentBroadcast.send(IntentTypeEnum.msg,
                                        TAG,
                                        REL_ACQUIRE_PILL_WAIT_FOR_PICK_ERROR_DISPLAY.toString(),
                                        IntentMsgEnum.pillNotPresentError.toString(),
                                        String.valueOf(dispenseNumber));
                            }
                        }
                        break;

                    case REL_ACQUIRE_PILL_WAIT_FOR_VISION_ERROR_DISPLAY:
                        // Turn off vacuum pump.
                        TinyGDriver.getInstance().write(CommandBuilder.CMD_COOLANT_OFF);

                        // Turn off the light ring.
                        LightringManager.getInstance().configureOutput(false);

                        Log.d(TAG, "onVisionError()");
                        setState(CommandState.COMPLETE_STATE);
                        IntentBroadcast.send(IntentTypeEnum.msg,
                                            TAG,
                                            REL_ACQUIRE_PILL_WAIT_FOR_VISION_ERROR_DISPLAY.toString(),
                                            IntentMsgEnum.visionError.toString(),
                                            String.valueOf(dispenseNumber));
                        break;

                }
                break;

            default:
                break;
        }
    }
}
