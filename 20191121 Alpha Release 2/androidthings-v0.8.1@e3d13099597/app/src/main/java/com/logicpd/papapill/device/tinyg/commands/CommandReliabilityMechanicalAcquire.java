package com.logicpd.papapill.device.tinyg.commands;

import android.util.Log;

import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.enums.CommandState;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.models.PickupData;
import com.logicpd.papapill.device.models.PositionData;
import com.logicpd.papapill.device.tinyg.CommandManager;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.IntentTypeEnum;
import com.logicpd.papapill.receivers.IntentBroadcast;

/**
 * The same as ReliabilityAcquirePill, but without looking for a pill to ensure testing of
 * mechanical reliability.
 */
public final class CommandReliabilityMechanicalAcquire extends BaseCommand {

    // Identifier for this command.
    private static final DeviceCommand identifier = DeviceCommand.COMMAND_RELIABILITY_MECHANICAL_ACQUIRE;
    private String TAG;

    // Internal parameters.
    private int dispenseNumber;

    // Data objects to pass to other sub commands.
    private PickupData pickupData = new PickupData();
    private PositionData positionData = new PositionData();

    /* A typical pill location for a single pill:
     * r = 25.152305, theta = -0.249377, mmZ = 50.500000 */
    private final double PILL_LOCATION_R = 25.152305;
    private final double PILL_LOCATION_THETA = -0.249377;
    private final double PILL_LOCATION_MMZ = 50.5;

    public CommandReliabilityMechanicalAcquire() {
        // Pass our identifier into the super's constructor. This will associate the
        // identifier with the command object. The identifier is a final property of
        // super so it can only be assigned during super's construction.
        super(identifier);
        TAG = this.getClass().getSimpleName();
    }

    /**
     * This mimics the full dispense command, but assumes a pill is in a position and thus ignores
     * the fact that one is not present for mechanical reliability testing purposes.
     */
    @Override
    public void execute() {
        double fullRadiusMove;

        switch (operation) {
            case OP_STATE_START:
                String[] paramArray = getParamArray();
                dispenseNumber = Integer.parseInt(paramArray[0]);

                setState(CommandState.REL_ACQUIRE_PILL_CAMERA_PICTURE_DONE);
                break;

            case OP_STATE_RUN:
                switch (state) {

                    case REL_ACQUIRE_PILL_CAMERA_PICTURE_DONE:
                        // Use the coordinates returned from OpenCV pill detection algorithm to
                        // reposition the pickup head over the nearest/best pill. Reposition the
                        // r-axis first.
                        fullRadiusMove = PILL_LOCATION_R + AppConfig.getInstance().camera2TinyGRhoOffset;
                        String cmdReposR = String.format("x %f 2000", fullRadiusMove);

                        CommandManager.getInstance().callCommand(
                                DeviceCommand.COMMAND_MOTOR_MOVE, cmdReposR, null);

                        // Advance our state variable.
                        setState(CommandState.REL_ACQUIRE_PILL_WAITING_FOR_R_REPOSITION);

                        break;

                    case REL_ACQUIRE_PILL_WAITING_FOR_R_REPOSITION:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_MOTOR_MOVE)) {

                            // Copy mmz to pickup data to be used later in fast z move.
                            pickupData.mmz = PILL_LOCATION_MMZ;

                            // Read from the position sensor one more time to find our current
                            // absolute theta position.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_READ_POSITION, "", positionData);

                            // Advance our state variable.
                            setState(CommandState.REL_ACQUIRE_PILL_WAITING_FOR_READ_POSITION);
                        }
                        break;

                    case REL_ACQUIRE_PILL_WAITING_FOR_READ_POSITION:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_READ_POSITION)) {

                            // R-axmis has been repositioned, now reposition theta-axis to the
                            // target (absolute) position, which is the current absolute position
                            // minus the pill's theta offset + a camera-pickup nozzle offset.
                            double targetPosition = positionData.getAbsoluteDegrees(true)
                                    + PILL_LOCATION_THETA
                                    + AppConfig.getInstance().camera2TinyGThetaOffset;

                            String params = String.format("%f 0.3", targetPosition);

                            // Call the Rotate Carousel command to accurately position the pickup
                            // head to within 0.3 degrees of the pill's (absolute) theta location.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_ROTATE_CAROUSEL, params, null);

                            // Advance our state variable.
                            setState(CommandState.REL_ACQUIRE_PILL_WAITING_FOR_THETA_REPOSITION);
                        }
                        break;

                    case REL_ACQUIRE_PILL_WAITING_FOR_THETA_REPOSITION:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_ROTATE_CAROUSEL)) {
                            // Height must be assigned to pickupData
                            pickupData.mmz = PILL_LOCATION_MMZ;

                            // Both R and Theta has been repositioned. The pickup head is now
                            // supposed to be directly over a pill. Time to pick it up.
                            CommandManager.getInstance().callCommand(
                                    DeviceCommand.COMMAND_MOTOR_PICKUP, "", pickupData);

                            // Advance our state variable.
                            setState(CommandState.REL_ACQUIRE_PILL_MOVING_TO_PICKUP_PILL);
                        }
                        break;

                    case REL_ACQUIRE_PILL_MOVING_TO_PICKUP_PILL:
                        if (CommandManager.getInstance().isCommandDone(
                                DeviceCommand.COMMAND_MOTOR_PICKUP)) {

                            // There should not be a pill, assume none are present
                            setState(CommandState.COMPLETE_STATE);
                            IntentBroadcast.send(IntentTypeEnum.msg,
                                    TAG,
                                    IntentMsgEnum.acquired.toString(),
                                    IntentMsgEnum.mechanicalTestFinished.toString(),
                                    String.valueOf(dispenseNumber));
                        }
                        break;
                }
                break;

            default:
                break;
        }
    }
}
