package com.logicpd.papapill.enums;

public enum CameraIndexEnum {
    FRONT(0),
    BIN(1);

    public final int value;

    private CameraIndexEnum(int value) {
        this.value = value;
    }
}
