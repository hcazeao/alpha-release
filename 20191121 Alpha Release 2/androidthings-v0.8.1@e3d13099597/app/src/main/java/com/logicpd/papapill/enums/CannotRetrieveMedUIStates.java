package com.logicpd.papapill.enums;

public enum CannotRetrieveMedUIStates {
    DISABLED,
    ENABLED,
    MANUAL_ACCESS,
    RETRY
}
