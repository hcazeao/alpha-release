package com.logicpd.papapill.enums;

public enum  CarouselPosCmdEnum {
    home {
        public String toString() {return "CommandHomeAll";}
    },
    bin {
        public String toString() {return "CommandRotateCarousel";}
    }
}
