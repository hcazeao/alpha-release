package com.logicpd.papapill.enums;

public enum DispenseEventResultEnum {
    ACTIVE(0),
    EXPIRED(1),
    DISPENSING(2),
    DISPENSE_COMPLETE(3),
    DISPENSE_FAILED_MANUALLY_RETRIEVED(4);

    public final int value;

    private DispenseEventResultEnum(int value) {
        this.value = value;
    }
}
