package com.logicpd.papapill.enums;

/*
 * Table column: DispenseEvent.DispenseType values
 */
public enum DispenseEventTypeEnum {
    SCHEDULED(0),
    EARLY_DISPENSE(1),
    AS_NEEDED(2);

    public final int value;

    private DispenseEventTypeEnum(int value) {this.value = value;}
}
