package com.logicpd.papapill.enums;

public enum IntentCommandEnum {
    CommandPreDispense {
        public String toString() {return "CommandPreDispense";}
    },
    CommandHomeAll {
        public String toString() {return "CommandHomeAll";}
    },
    CommandFullDispense {
        public String toString() {return "CommandFullDispense";}
    },
    CommandRotateCarousel {
        public String toString() {return "CommandRotateCarousel";}
    },
    CommandReliabilityAcquirePill {
        public String toString() {return "CommandReliabilityAcquirePill";}
    },
    CommandReliabilityMechanicalAcquire {
        public String toString() {return "CommandReliabilityMechanicalAcquire";}
    },
    CommandReset {
        public String toString() {return "CommandReset";}
    },
    CommandManager {
        public String toString() {return "CommandManager";}
    },
    CommandUnknown {
        public String toString() {return "UnknownCommand";}
    }
}
