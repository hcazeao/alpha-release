package com.logicpd.papapill.enums;

public enum IntentMsgEnum {
    dispensed {
        public String toString() {return "dispensed";}
    },
    acquired {
        public String toString() {return "acquired";}
    },
    success {
        public String toString() {return "success";}
    },
    failed {
        public String toString() {return "failed";}
    },
    paramError {
        public String toString() {return "paramError";}
    },
    limitError {
        public String toString() {return "limitError";}
    },
    pillNotPresentError {
        public String toString() {return "pillNotPresentError";}
    },
    visionError {
        public String toString() {return "visionError";}
    },
    takePhoto {
        public String toString() {return "takePhoto";}
    },
    mechanicalTestFinished {
        public String toString() {return "mechanicalTestFinished";}
    },
    // Lock motor workflow:
    doorLocked {
        public String toString() {return "doorLocked";}
    },
    doorUnlocked {
        public String toString() {return "doorUnlocked";}
    },
    drawerLocked {
        public String toString() {return "drawerLocked";}
    },
    drawerUnlocked {
        public String toString() {return "drawerUnlocked";}
    },
    lockDriverError {
        public String toString() {return "lockDriverError";}
    },
}
