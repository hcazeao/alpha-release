package com.logicpd.papapill.enums;

public enum IntentParamsEnum {
    command {
        public String toString() {return "command";}
    },
    state {
        public String toString() {return "state";}
    },
    msg {
        public String toString() {return "msg";}
    },
    version {
        public String toString() {return "version";}
    }
}
