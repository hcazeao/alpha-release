package com.logicpd.papapill.enums;

public enum PreferenceEnum {
    BOOLEAN,
    STRING,
    INTEGER
}
