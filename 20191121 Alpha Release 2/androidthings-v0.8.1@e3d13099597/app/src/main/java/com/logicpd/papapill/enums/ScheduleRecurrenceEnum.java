package com.logicpd.papapill.enums;

public enum ScheduleRecurrenceEnum {
    NONE(0), DAILY(1), WEEKLY(2), MONTHLY(3);
    public final int value;

    private ScheduleRecurrenceEnum(int value) {
        this.value = value;
    }
}

