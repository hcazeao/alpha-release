package com.logicpd.papapill.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.interfaces.OnButtonClickListener;

public class BaseHomeFragment extends Fragment implements View.OnClickListener {
    protected LinearLayout backButton, homeButton;
    protected OnButtonClickListener mListener;
    protected TextView tvProgressPercent, tvProgressLabel;
    protected ProgressBar mProgress;

    static protected final String PROGRESS_SETUP_WIZARD = "Setup Wizard";
    static protected final String PROGRESS_ADD_USER = "Add User";
    static protected final String PROGRESS_ADD_MEDICATION = "Add Medication";
    static protected final String PROGRESS_SET_NOTIFICATIONS = "Set Notification";

    protected void setupViews(View view) {
        try {
            backButton = view.findViewById(R.id.button_back);
            backButton.setOnClickListener(this);
            homeButton = view.findViewById(R.id.button_home);
            homeButton.setOnClickListener(this);

            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.circular);

            tvProgressPercent = (TextView)view.findViewById(R.id.circularProgressbarPercent);
            tvProgressLabel = (TextView)view.findViewById(R.id.circularProgressbarLabel);
            mProgress = (ProgressBar) view.findViewById(R.id.circularProgressbar);
            mProgress.setProgressDrawable(drawable);
        }
        catch (Exception ex) {
            Log.v("BaseHomeFragment", "setupViews() failed to find back, home, or progress bar");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a = null;

        if (context instanceof Activity) {
            a = (Activity) context;
        }

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnButtonClickListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnButtonClickListener");
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        if (v == backButton) {
            bundle.putString("fragmentName", "Back");
        }
        if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
        }
        mListener.onButtonClicked(bundle);
    }

    public Boolean baseHomeOnClick(View v) {
        Bundle bundle = new Bundle();
        Boolean returnVal = false;

        if (v == backButton) {
            bundle.putString("fragmentName", "Back");
            returnVal = true;
        }

        if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
            returnVal = true;
        }

        mListener.onButtonClicked(bundle);
        return returnVal;
    }

    /**
     * Allows individual fragments to push updates to the left nav progress bar.
     * @param tag
     * @param current
     * @param max
     */
    public void setProgress(String tag, int current, int max) {
        int percent;
        // input check
        if (current >= max) {
            percent = 100;
        } else {
            percent = (int)(((double)current / (double)max) * 100);
        }
        mProgress.setProgress(percent);
        mProgress.setSecondaryProgress(100);
        mProgress.setMax(100);
        tvProgressLabel.setText(tag);
        tvProgressPercent.setText(percent + "%");
        mProgress.setVisibility(View.VISIBLE);
        tvProgressLabel.setVisibility(View.VISIBLE);
        tvProgressPercent.setVisibility(View.VISIBLE);
    }
}
