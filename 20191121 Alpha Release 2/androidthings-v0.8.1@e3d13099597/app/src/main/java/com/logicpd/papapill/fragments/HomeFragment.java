package com.logicpd.papapill.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextClock;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.gpio.PowerManager;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.misc.CloudManager;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.room.utils.TimeUtil;
import com.logicpd.papapill.utils.PreferenceUtils;
import com.logicpd.papapill.utils.TextUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;

/**
 * Fragment for Home screen
 *
 * @author alankilloren
 */
public class HomeFragment extends BaseHomeFragment {

    public static final String TAG = "HomeFragment";

    private LinearLayout contentLayout, usersLayout, userALayout, userBLayout;
    private Button btnMedication, btnUserSettings, btnSystemManager, btnHelp;
    private TextView btnHidden, tvFirstTimeSetup, tvUserAName, tvUserBName, tvUserAText, tvUserBText, tvBatteryPercent;
    private ImageView imgBattery;
    private RelativeLayout indicatorWifi, indicatorBluetooth, indicatorDrawer, indicatorDoor, indicatorBattery;
    private TextClock timeText, dateText;
    private int taps = 0;
    private List<UserEntity> userList;
    private WifiManager wifiManager;
    private WifiInfo wifiInfo;
    private BluetoothAdapter bluetoothAdapter;
    private NetworkInfo networkInfo;
    private ConnectivityManager connManager;
    private PowerManager powerManager;
    private PreferenceUtils prefs;
    private CloudManager cloudManager;
    private Timer updateUserTextTimer, updateStatusBarTimer, disconnectBatteryTimer;
    private TimerTask updateUserTextTask, updateStatusBarTask, disconnectBatteryTask;
    private Handler handler;
    private Sensor door, drawer;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        handler = new Handler();

        wifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            wifiInfo = wifiManager.getConnectionInfo();
        }
        connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager != null) {
            networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        }

        cloudManager = CloudManager.getInstance(getActivity());
        powerManager = PowerManager.getInstance();

        door = new Sensor(BoardDefaults.I2C_PORT,
                            BoardDefaults.I2C_MUX_DOOR_CHANNEL,
                            AppConfig.getInstance().getDoorThresholdHigh());
        drawer = new Sensor(BoardDefaults.I2C_PORT,
                            BoardDefaults.I2C_MUX_DRAWER_CHANNEL,
                            AppConfig.getInstance().getDrawerThresholdHigh());

        displayStatusBar();
        startStatusBarUpdateTimer();

        userList = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        if(null!= userList) {
            if (userList.size() == 0) {
                tvFirstTimeSetup.setVisibility(View.VISIBLE);
                usersLayout.setVisibility(View.GONE);
                TextUtils.disableButton(btnMedication);
                TextUtils.disableButton(btnUserSettings);
            } else { // userList.size() > 0)
                tvFirstTimeSetup.setVisibility(View.GONE);
                usersLayout.setVisibility(View.VISIBLE);
                startUpdateUserTextTimer();
            }

            List<?> list = (List<MedicationEntity>)(List<?>)new MedicationRepository().readAll();
            if (null == list || list.size() == 0) {
                TextUtils.disableButton(btnMedication);
            }
        }
    }

    private void startStatusBarUpdateTimer() {
        if (updateStatusBarTimer != null) {
            updateStatusBarTimer.cancel();
        }

        updateStatusBarTimer = new Timer();
        updateStatusBarTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        displayStatusBar();
                    }
                });
            }
        };

        // Update every second to more quickly show the switch from AC to Battery power
        updateStatusBarTimer.schedule(updateStatusBarTask, 0, 1000);
    }

    private void startUpdateUserTextTimer() {
        if (updateUserTextTimer != null) {
            updateUserTextTimer.cancel();
        }

        updateUserTextTimer = new Timer();
        updateUserTextTask = new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "Updating schedule...");
                // refresh view with updated schedule
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        getScheduleData();
                    }
                });
            }
        };
        updateUserTextTimer.schedule(updateUserTextTask, 0, 60000);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (updateUserTextTimer != null) {
            updateUserTextTimer.cancel();
        }
        if (updateStatusBarTimer != null) {
            updateStatusBarTimer.cancel();
        }
    }

    private void getScheduleData() {
        TextView tvUserText = tvUserAText;

        userList = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        if (userList.size() > 0) {
            //get first user
            UserEntity userA = userList.get(0);
            userALayout.setVisibility(View.VISIBLE);
            tvUserAName.setText(userA.getUserName());
            populateUserText(userA, tvUserAText);
        }

            // USER B
        if (userList.size() > 1) {
            //get second user
            UserEntity userB = userList.get(1);
            tvUserBName.setText(userB.getUserName());
            userBLayout.setVisibility(View.VISIBLE);
            populateUserText(userB, tvUserBText);
        }
    }

    private void populateUserText(UserEntity user, TextView tv) {
        String userSetText = this.getResources().getString(R.string.there_are_currently_no_scheduled_medications);

        List<JoinScheduleDispense> mSchedules;
        mSchedules = new ScheduleRepository().getJoinNextScheduledItems(user.getId(), new Date());
        if((null != mSchedules) && (mSchedules.size() > 0)) {
            Date nextDoseDate = mSchedules.get(0).getNextProcessDate();
            JoinScheduleDispense scheduleItem = mSchedules.get(0);

            userSetText = "NEXT DOSE:\n";

            if (TimeUtil.isDateToday(nextDoseDate)) {
                userSetText += "TODAY" + "\n";
            } else if (TimeUtil.isDateTomorrow(nextDoseDate)) {
                userSetText += "TOMORROW" + "\n";
            } else if (TimeUtil.isDateSixDaysOrLessAway(nextDoseDate)) {
                DateFormat df = new SimpleDateFormat("EEEE");
                userSetText += df.format(nextDoseDate).toUpperCase() + "\n";
            } else if (TimeUtil.isDateOneWeekAway(nextDoseDate)) {
                DateFormat df = new SimpleDateFormat("EEEE");
                userSetText += "NEXT " + df.format(nextDoseDate).toUpperCase() + "\n";
            } else if (TimeUtil.isDateLessThanAYearAway(nextDoseDate)){
                DateFormat df = new SimpleDateFormat("MMMM dd");
                userSetText += df.format(nextDoseDate).toUpperCase() + "\n";
            } else {
                DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
                userSetText += df.format(nextDoseDate).toUpperCase() + "\n";
            }

            userSetText +=  scheduleItem.getDispenseName() + " - " + scheduleItem.getDispenseTime();
        }

        tv.setText(userSetText);
    }

    private void setBluetoothIcon()
    {
        try {
            //TODO check bluetooth
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
                indicatorBluetooth.setVisibility(View.GONE);
            } else {
                indicatorBluetooth.setVisibility(View.VISIBLE);
            }
        }
        catch (Exception ex)
        {
            indicatorBluetooth.setVisibility(View.GONE);
            Log.e(AppConstants.TAG, "setBluetoothIcon() exception:"+ex.toString());
        }
    }

    private void displayStatusBar() {
        //check wifi
        if (networkInfo.isConnected()) {
            indicatorWifi.setVisibility(View.VISIBLE);
            indicatorWifi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextUtils.showToast(getActivity(), "Connected to " + wifiInfo.getSSID());
                }
            });
        } else {
            indicatorWifi.setVisibility(View.GONE);
        }

        setBluetoothIcon();

        indicatorBattery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder sb = new StringBuilder();
                sb.append("Power Source: ");

                if (powerManager.isAcPowered()) {
                    sb.append("AC Powered\n");
                } else {
                    sb.append("BATTERY POWERED\n");
                }

                TextUtils.showToast(getActivity(), sb.toString());
            }
        });

        if (powerManager.isAcPowered()) {
            imgBattery.setImageResource(R.drawable.ic_battery_charging);
            tvBatteryPercent.setText("AC POWER");
        } else if (powerManager.isBatteryDead()) {
            imgBattery.setImageResource(R.drawable.ic_battery_none);
            tvBatteryPercent.setText("DEAD BATTERY!");
            // Disconnect Battery to avoid damage to the battery
            Log.w(TAG, "DEAD BATTERY: DISCONNECTING!!!");
            disconnectBatteryInOneSecond();
        } else if (powerManager.isBatteryLow()) {
            imgBattery.setImageResource(R.drawable.ic_battery);
            tvBatteryPercent.setText("LOW BATTERY!");
            Log.w(TAG, "LOST AC -- LOW BATTERY!!!");
        } else {
            imgBattery.setImageResource(R.drawable.ic_battery_full);
            tvBatteryPercent.setText("BATTERY POWER");
            Log.w(TAG, "LOST AC -- BATTERY POWER");
        }

        //TODO Beta - create icon for a lock that is unlocked
        //TODO Beta - make sure to relock the drawer or door if found to be open
        if (!door.isSensorClosed()) {
            StringBuilder sb = new StringBuilder();
            String toShow = "DOOR IS NOT CLOSED\nPLEASE CLOSE THE DOOR";
            TextUtils.showToast(getActivity(), toShow);
            indicatorDoor.setVisibility(View.GONE);
        } else {
            indicatorDoor.setVisibility(View.VISIBLE);
        }

        if (!drawer.isSensorClosed()) {
            StringBuilder sb = new StringBuilder();
            String toShow = "DRAWER IS NOT CLOSED\nPLEASE CLOSE THE DRAWER";
            TextUtils.showToast(getActivity(), toShow);
            indicatorDrawer.setVisibility(View.GONE);
        } else {
            indicatorDrawer.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Waiting one second allows for debug messages to be sent and logging to finish
     */
    private void disconnectBatteryInOneSecond() {
        if (disconnectBatteryTimer != null) {
            disconnectBatteryTimer.cancel();
        }

        disconnectBatteryTimer = new Timer();
        disconnectBatteryTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        powerManager.disconnectBattery();
                    }
                });
            }
        };

        // One second should be enough to log and send out messages
        disconnectBatteryTimer.schedule(disconnectBatteryTask, 0, 1000);
    }
    /**
     * Uses reflection to draw the vertical thumb and track on the scrollbar
     *
     * @param scrollView     Passed in ScrollView object
     * @param declaredMethod String method to call (either "setVerticalTrackDrawable" or "setVerticalThumbDrawable")
     * @param drawable       Drawable to use
     */
    private void styleScrollbar(ScrollView scrollView, String declaredMethod, int drawable) {
        try {
            Field mScrollCacheField = View.class.getDeclaredField("mScrollCache");
            mScrollCacheField.setAccessible(true);
            Object mScrollCache = mScrollCacheField.get(scrollView);
            Field scrollBarField = mScrollCache.getClass().getDeclaredField("scrollBar");
            scrollBarField.setAccessible(true);
            Object scrollBar = scrollBarField.get(mScrollCache);
            Method method = scrollBar.getClass().getDeclaredMethod(declaredMethod, Drawable.class);
            method.setAccessible(true);
            method.invoke(scrollBar, ContextCompat.getDrawable(getActivity(), drawable));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        contentLayout = view.findViewById(R.id.layout_content);
        btnMedication = view.findViewById(R.id.button_my_medication);
        btnUserSettings = view.findViewById(R.id.button_user_settings);
        btnSystemManager = view.findViewById(R.id.button_system_manager);
        btnHelp = view.findViewById(R.id.button_help);
        btnHidden = view.findViewById(R.id.hiddenButton);
        timeText = view.findViewById(R.id.textclock_time);
        dateText = view.findViewById(R.id.textclock_date);
        /*timeText.setTimeZone("GMT-5:00");
        dateText.setTimeZone("GMT-5:00");*/

        btnMedication.setOnClickListener(this);
        btnUserSettings.setOnClickListener(this);
        btnSystemManager.setOnClickListener(this);
        btnHelp.setOnClickListener(this);
        btnHidden.setOnClickListener(this);

        tvFirstTimeSetup = view.findViewById(R.id.textview_first_time_setup);
        usersLayout = view.findViewById(R.id.layout_users);
        userALayout = view.findViewById(R.id.layout_userA);
        userBLayout = view.findViewById(R.id.layout_userB);
        tvUserAName = view.findViewById(R.id.textview_usernameA);
        tvUserBName = view.findViewById(R.id.textview_usernameB);
        tvUserAText = view.findViewById(R.id.textview_infotextA);
        tvUserBText = view.findViewById(R.id.textview_infotextB);

        indicatorWifi = view.findViewById(R.id.layout_wifi);
        indicatorBluetooth = view.findViewById(R.id.layout_bluetooth);
        indicatorDoor = view.findViewById(R.id.layout_door);
        indicatorDrawer = view.findViewById(R.id.layout_drawer);
        indicatorBattery = view.findViewById(R.id.layout_battery);

        tvBatteryPercent = view.findViewById(R.id.textview_battery_percentage);

        imgBattery = view.findViewById(R.id.symbol_battery);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        if (v == btnSystemManager) {
            //open system manager screen
            bundle.putString("fragmentName", "SystemManagerFragment");
            mListener.onButtonClicked(bundle);
        }

        //hidden exit button for dev
        if (v == btnHidden) {
            if (taps == 2) {
                taps = 0;
                bundle.putString("fragmentName", "Exit");
                mListener.onButtonClicked(bundle);
            } else {
                taps += 1;
            }
        }
        if (v == btnHelp) {
            bundle.putString("fragmentName", "HelpFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnUserSettings) {
            bundle.putString("fragmentName", "UserSettingsFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnMedication) {
            bundle.putString("fragmentName", "MyMedicationFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}
