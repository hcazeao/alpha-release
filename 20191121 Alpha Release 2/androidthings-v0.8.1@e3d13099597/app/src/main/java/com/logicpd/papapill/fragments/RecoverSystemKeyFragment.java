package com.logicpd.papapill.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;

/**
 * RecoverSystemKeyFragment
 *
 * @author alankilloren
 */
public class RecoverSystemKeyFragment extends BaseHomeFragment {

    public static final String TAG = "RecoverSystemKeyFragment";

    private Button btnCancel, btnReset;

    public RecoverSystemKeyFragment() {
        // Required empty public constructor
    }

    public static RecoverSystemKeyFragment newInstance() {
        return new RecoverSystemKeyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_key_recover, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnReset = view.findViewById(R.id.button_reset);
        btnReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnReset) {
            //TODO: this needs to go through a cloud-related process to recover the system key in Beta
            bundle.putString("fragmentName", "TempSystemKeyFragment");
            bundle.putString("authFragment", "ChangeSystemKeyFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}