package com.logicpd.papapill.fragments.help;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.enums.DeviceCommand;
import com.logicpd.papapill.device.gpio.LightringManager;
import com.logicpd.papapill.device.gpio.LockManager;
import com.logicpd.papapill.device.gpio.PowerManager;
import com.logicpd.papapill.device.i2c.CupLight;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.models.PillPresence;
import com.logicpd.papapill.device.models.PositionData;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.device.tinyg.CommandBuilder;
import com.logicpd.papapill.device.tinyg.CommandManager;
import com.logicpd.papapill.device.tinyg.CoordinateManager;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.AppConstants;

public class DeveloperFragment extends BaseHomeFragment {

    public static final String TAG = "DeveloperFragment";

    private TextView tvTitle, tvGotoBin,
            tvDistanceZ, tvFeedrateZ,
            tvDistanceR, tvFeedrateR,
            tvDistanceTheta, tvFeedrateTheta,
            tvReadDoor, tvDoorState,
            tvReadDrawer, tvDrawerState,
            tvReadCup, tvCupState,
            tvReadAbs,
            tvReadPsi, tvPsiState;

    private Button btnGotoBin,
            btnHomeZ, btnMovePosZ, btnMoveNegZ,
            btnHomeR, btnMovePosR, btnMoveNegR,
            btnHomeTheta, btnMovePosTheta, btnMoveNegTheta,
            btnLockDoor, btnUnlockDoor,
            btnLockDrawer, btnUnlockDrawer,
            btnReadDoor,
            btnReadDrawer,
            btnReadCup,
            btnReadAbs,
            btnReadPsi,
            btnCuplightOn, btnCuplightOff,
            btnLightringOn, btnLightringOff,
            btnVacOn, btnVacOff,
            btnDisconnectBattery, btnConnectBattery,
            btnConfigTinyG,
            btnResetTinyG;

    private int taps = 0;

    private TinyGDriver tg = TinyGDriver.getInstance();
    private PowerManager pm = PowerManager.getInstance();

    private Sensor drawer = new Sensor(BoardDefaults.I2C_PORT,
            BoardDefaults.I2C_MUX_DRAWER_CHANNEL,
            AppConfig.getInstance().getDrawerThresholdHigh());
    private Sensor door = new Sensor(BoardDefaults.I2C_PORT,
            BoardDefaults.I2C_MUX_DOOR_CHANNEL,
            AppConfig.getInstance().getDoorThresholdHigh());
    private Sensor cup = new Sensor(BoardDefaults.I2C_PORT,
            BoardDefaults.I2C_MUX_CUP_CHANNEL,
            AppConfig.getInstance().getCupThresholdHigh());
    private static CupLight cupLight = new CupLight(BoardDefaults.I2C_PORT, BoardDefaults.I2C_MUX_CUP_CHANNEL);

    PositionData positionData = new PositionData();
    PillPresence pillPresence= new PillPresence();

    private int DEFAULT_GOTO_BIN = 1;

    private double DEFAULT_DISTANCE_Z = 47.0;
    private double DEFAULT_DISTANCE_R = 36.5;
    private double DEFAULT_DISTANCE_THETA = 24.0;

    private int DEFAULT_FEEDRATE_Z = 1000;
    private int DEFAULT_FEEDRATE_R = 1000;
    private int DEFAULT_FEEDRATE_THETA = 1000;

    public DeveloperFragment() {
        // Required empty public constructor
    }

    public static DeveloperFragment newInstance() {
        return new DeveloperFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_developer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        //contentLayout = view.findViewById(R.id.layout_content);

        tvTitle = view.findViewById(R.id.textview_title);
        tvGotoBin = view.findViewById(R.id.input_text_goto_bin);
        tvDistanceZ = view.findViewById(R.id.input_text_distance_z);
        tvFeedrateZ = view.findViewById(R.id.input_text_feedrate_z);
        tvDistanceR = view.findViewById(R.id.input_text_distance_r);
        tvFeedrateR = view.findViewById(R.id.input_text_feedrate_r);
        tvDistanceTheta = view.findViewById(R.id.input_text_distance_theta);
        tvFeedrateTheta = view.findViewById(R.id.input_text_feedrate_theta);
        tvReadDoor = view.findViewById(R.id.result_text_read_door);
        tvDoorState = view.findViewById(R.id.result_text_door_state);
        tvReadDrawer = view.findViewById(R.id.result_text_read_drawer);
        tvDrawerState = view.findViewById(R.id.result_text_drawer_state);
        tvReadCup = view.findViewById(R.id.result_text_read_cup);
        tvCupState = view.findViewById(R.id.result_text_cup_state);
        tvReadAbs = view.findViewById(R.id.result_text_read_abs);
        tvReadPsi = view.findViewById(R.id.result_text_read_psi);
        tvPsiState = view.findViewById(R.id.result_text_psi_state);

        tvGotoBin.setText(Integer.toString(DEFAULT_GOTO_BIN));
        tvDistanceZ.setText(Double.toString(DEFAULT_DISTANCE_Z));
        tvFeedrateZ.setText(Integer.toString(DEFAULT_FEEDRATE_Z));
        tvDistanceR.setText(Double.toString(DEFAULT_DISTANCE_R));
        tvFeedrateR.setText(Integer.toString(DEFAULT_FEEDRATE_R));
        tvDistanceTheta.setText(Double.toString(DEFAULT_DISTANCE_THETA));
        tvFeedrateTheta.setText(Integer.toString(DEFAULT_FEEDRATE_THETA));

        btnGotoBin = view.findViewById(R.id.btn_goto_bin);
        btnGotoBin.setOnClickListener(this);
        btnHomeZ = view.findViewById(R.id.btn_home_z);
        btnHomeZ.setOnClickListener(this);
        btnMovePosZ = view.findViewById(R.id.btn_move_pos_z);
        btnMovePosZ.setOnClickListener(this);
        btnMoveNegZ = view.findViewById(R.id.btn_move_neg_z);
        btnMoveNegZ.setOnClickListener(this);
        btnHomeR = view.findViewById(R.id.btn_home_r);
        btnHomeR.setOnClickListener(this);
        btnMovePosR = view.findViewById(R.id.btn_move_pos_r);
        btnMovePosR.setOnClickListener(this);
        btnMoveNegR = view.findViewById(R.id.btn_move_neg_r);
        btnMoveNegR.setOnClickListener(this);
        btnHomeTheta = view.findViewById(R.id.btn_home_theta);
        btnHomeTheta.setOnClickListener(this);
        btnMovePosTheta = view.findViewById(R.id.btn_move_pos_theta);
        btnMovePosTheta.setOnClickListener(this);
        btnMoveNegTheta = view.findViewById(R.id.btn_move_neg_theta);
        btnMoveNegTheta.setOnClickListener(this);
        btnLockDoor = view.findViewById(R.id.btn_lock_door);
        btnLockDoor.setOnClickListener(this);
        btnUnlockDoor = view.findViewById(R.id.btn_unlock_door);
        btnUnlockDoor.setOnClickListener(this);
        btnLockDrawer = view.findViewById(R.id.btn_lock_drawer);
        btnLockDrawer.setOnClickListener(this);
        btnUnlockDrawer = view.findViewById(R.id.btn_unlock_drawer);
        btnUnlockDrawer.setOnClickListener(this);
        btnReadDoor = view.findViewById(R.id.btn_read_door);
        btnReadDoor.setOnClickListener(this);
        btnReadDrawer = view.findViewById(R.id.btn_read_drawer);
        btnReadDrawer.setOnClickListener(this);
        btnReadCup = view.findViewById(R.id.btn_read_cup);
        btnReadCup.setOnClickListener(this);
        btnReadAbs = view.findViewById(R.id.btn_read_abs);
        btnReadAbs.setOnClickListener(this);
        btnReadPsi = view.findViewById(R.id.btn_read_psi);
        btnReadPsi.setOnClickListener(this);
        btnCuplightOn = view.findViewById(R.id.btn_cuplight_on);
        btnCuplightOn.setOnClickListener(this);
        btnCuplightOff = view.findViewById(R.id.btn_cuplight_off);
        btnCuplightOff.setOnClickListener(this);
        btnLightringOn = view.findViewById(R.id.btn_lightring_on);
        btnLightringOn.setOnClickListener(this);
        btnLightringOff = view.findViewById(R.id.btn_lightring_off);
        btnLightringOff.setOnClickListener(this);
        btnVacOn = view.findViewById(R.id.btn_vac_on);
        btnVacOn.setOnClickListener(this);
        btnVacOff = view.findViewById(R.id.btn_vac_off);
        btnVacOff.setOnClickListener(this);
        btnDisconnectBattery = view.findViewById(R.id.btn_disconnect_battery);
        btnDisconnectBattery.setOnClickListener(this);
        btnConnectBattery = view.findViewById(R.id.btn_connect_battery);
        btnConnectBattery.setOnClickListener(this);
        btnConfigTinyG = view.findViewById(R.id.btn_config_tinyg);
        btnConfigTinyG.setOnClickListener(this);
        btnResetTinyG = view.findViewById(R.id.btn_reset_tinyg);
        btnResetTinyG.setOnClickListener(this);

        //TODO FOR DEVS ONLY - remove this in actual release
        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (taps == 2) {
                    taps = 0;
                    Bundle bundle = new Bundle();
                    bundle.putString("fragmentName", "DeveloperTestFragment");
                    mListener.onButtonClicked(bundle);
                } else {
                    taps += 1;
                    Log.i(AppConstants.TAG, "Tap: " + taps);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnGotoBin) {
            String s = tvGotoBin.getText().toString();
            int binId = Integer.parseInt(tvGotoBin.getText().toString());
            double binLocation = CoordinateManager.getBinLocationDegrees(binId);
        }

        if (v == btnHomeZ) {
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_HOME, "z", null);
        }

        if (v == btnMovePosZ) {
            String cmdParams = String.format("z %s %s", tvDistanceZ.getText().toString(), tvFeedrateZ.getText().toString());
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
        }

        if (v == btnMoveNegZ) {
            String cmdParams = String.format("z -%s %s", tvDistanceZ.getText().toString(), tvFeedrateZ.getText().toString());
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
        }

        if (v == btnHomeR) {
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_HOME, "x", null);
        }

        if (v == btnMovePosR) {
            String cmdParams = String.format("x %s %s", tvDistanceR.getText().toString(), tvFeedrateR.getText().toString());
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
        }

        if (v == btnMoveNegR) {
            String cmdParams = String.format("x -%s %s", tvDistanceR.getText().toString(), tvFeedrateR.getText().toString());
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
        }

        if (v == btnHomeTheta) {
            double targetPosition = CoordinateManager.getHomeBinOffset();
            String params = String.format("%f 0.3", targetPosition);
            tg.commandManager.callCommand(DeviceCommand.COMMAND_ROTATE_CAROUSEL, params, null);
        }

        if (v == btnMovePosTheta) {
            String cmdParams = String.format("a %s %s", tvDistanceTheta.getText().toString(), tvFeedrateTheta.getText().toString());
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
        }

        if (v == btnMoveNegTheta) {
            String cmdParams = String.format("a -%s %s", tvDistanceTheta.getText().toString(), tvFeedrateTheta.getText().toString());
            tg.commandManager.callCommand(DeviceCommand.COMMAND_MOTOR_MOVE, cmdParams, null);
        }

        if (v == btnLockDoor) {
            LockManager.getInstance().LockDoor();
        }

        if (v == btnUnlockDoor) {
            LockManager.getInstance().UnlockDoor();
        }

        if (v == btnLockDrawer) {
            LockManager.getInstance().LockDrawer();
        }

        if (v == btnUnlockDrawer) {
            LockManager.getInstance().UnlockDrawer();
        }

        if (v == btnReadDoor) {
            int result = door.getSensorValue();
            if (result == 0) {
                tvDoorState.setText("Error");
            } else if (result > AppConfig.getInstance().getDoorThresholdHigh()) {
                tvDoorState.setText("Closed");
            } else if (result < AppConfig.getInstance().getDoorThresholdLow()) {
                tvDoorState.setText("Opened");
            }
            tvReadDoor.setText(Integer.toString(result));
        }

        if (v == btnReadDrawer) {
            int result = drawer.getSensorValue();
            if (result == 0) {
                tvDrawerState.setText("Error");
            } else if (result > AppConfig.getInstance().getDrawerThresholdHigh()) {
                tvDrawerState.setText("Closed");
            } else if (result < AppConfig.getInstance().getDrawerThresholdLow()) {
                tvDrawerState.setText("Opened");
            }
            tvReadDrawer.setText(Integer.toString(result));
        }

        if (v == btnReadCup) {
            int result = cup.getSensorValue();
            if (result == 0) {
                tvCupState.setText("Error");
            } else if (result > AppConfig.getInstance().getCupThresholdHigh()) {
                tvCupState.setText("Present");
            } else if (result < AppConfig.getInstance().getCupThresholdLow()) {
                tvCupState.setText("Missing");
            }
            tvReadCup.setText(Integer.toString(result));
        }

        if (v == btnReadAbs) {
            tg.commandManager.callCommand(DeviceCommand
                    .COMMAND_READ_POSITION, "", positionData);
            while (!CommandManager.getInstance().isCommandDone(DeviceCommand.COMMAND_READ_POSITION)) {
                // Probably should have a software timer safety valve to break out in case
                // there is a bug in the command.
            }
            tvReadAbs.setText(String.format("%.3f°", positionData.getAbsoluteDegrees(true)));
        }

        if (v == btnReadPsi) {
            tg.commandManager.callCommand(DeviceCommand
                    .COMMAND_READ_IS_PILL_PRESENT, "", pillPresence);
            while (!CommandManager.getInstance().isCommandDone(DeviceCommand.COMMAND_READ_IS_PILL_PRESENT)) {
                // Probably should have a software timer safety valve to break out in case
                // there is a bug in the command.
            }
            tvReadPsi.setText(Boolean.toString(pillPresence.isPillPresent));
        }

        if (v == btnCuplightOn) {
            cupLight.setCupLightOn();
        }

        if (v == btnCuplightOff) {
            cupLight.setCupLightOff();
        }

        if (v == btnLightringOn) {
            LightringManager.getInstance().configureOutput(true);
        }

        if (v == btnLightringOff) {
            LightringManager.getInstance().configureOutput(false);
        }

        if (v == btnVacOn) {
            tg.write(CommandBuilder.CMD_COOLANT_ON);
        }

        if (v == btnVacOff) {
            tg.write(CommandBuilder.CMD_COOLANT_OFF);
        }

        if (v == btnDisconnectBattery) {
            pm.disconnectBattery();
        }

        if (v == btnConnectBattery) {
            pm.connectBattery();
        }

        if (v == btnConfigTinyG) {
            tg.doTinygInit(true);
        }

        if (v == btnResetTinyG) {
            tg.softwareReset();
        }
    }
}
