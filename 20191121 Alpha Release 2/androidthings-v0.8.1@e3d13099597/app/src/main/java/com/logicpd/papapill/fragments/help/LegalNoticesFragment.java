package com.logicpd.papapill.fragments.help;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;

/**
 * Fragment for Help...Legal Notices
 *
 * @author alankilloren
 */
public class LegalNoticesFragment extends BaseHomeFragment {

    public static final String TAG = "LegalNoticesFragment";

    private LinearLayout contentLayout;
    private Button btnDone;

    public LegalNoticesFragment() {
        // Required empty public constructor
    }

    public static LegalNoticesFragment newInstance() {
        return new LegalNoticesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_help_legal_notices, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
    }


    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            backButton.performClick();
        }
    }
}
