package com.logicpd.papapill.fragments.my_medication;

import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.data.Notification;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.NotificationTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.CloudManager;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.DispenseEventRepository;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.NotificationSettingRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DispenseStrategy;
import com.logicpd.papapill.databinding.FragmentMyMedsAlarmReceivedBinding;
import com.logicpd.papapill.wireframes.workflows.GetAsNeededMedication;
import com.logicpd.papapill.wireframes.IWireframe;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

import java.util.Date;
import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class AlarmReceivedFragment extends BaseHomeFragment  {

    public static final String TAG = "AlarmReceivedFragment";
    private FragmentMyMedsAlarmReceivedBinding mBinding;
    private List<UserEntity> users;

    private MediaPlayer mp;
    private int activeUserIndex;
    public String title;
    public String usernameA, usernameB;
    public boolean enabledUserA = true;
    public boolean enabledUserB = true;
    public int visibleUserA = View.VISIBLE;
    public int visibleUserB = View.VISIBLE;

    public AlarmReceivedFragment() {
        // Required empty public constructor
    }

    public static AlarmReceivedFragment newInstance() {
        return new AlarmReceivedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_meds_alarm_received, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model=null;

        getUsernames();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            JoinScheduleDispense scheduleItem = (JoinScheduleDispense) bundle.getSerializable("scheduleItem");
            model = (BaseWireframe) ((MainActivity)this.getActivity()).getFeatureModel();
            DispenseTimeEntity dispenseTime = new DispenseTimeRepository().read(scheduleItem.getDispenseTimeId());
            UserEntity user = setActiveUser();
            if(null!=user) {
                // update layout
                title = this.getContext().getString(R.string.its_time_for_your) + "\n" + dispenseTime.getDispenseName().toUpperCase() + "\n" + this.getContext().getString(R.string.medications);
                sendCloudNotification(user);

                /*
                 * Less thrills for the EARLY_DOSE or AS_NEEDED users
                 * - no alarm
                 * - go straight to next fragment
                 */
                switch(getTAG(model)) {
                    case SingleDoseEarly.TAG:
                    case GetAsNeededMedication.TAG:
                        Log.d(TAG, "straight to dispense");
                        goDispense(user, false);
                        break;

                    default:
                        Log.d(TAG, "play alarm sound");
                        mp = MediaPlayer.create(getActivity(), R.raw.emn_13_l_n);
                        mp.start();
                        break;
                }
            }
        }
        mBinding.invalidateAll();
    }

    private String getTAG(BaseWireframe model) {
        return (null!=model)?
                model.getClass().getSimpleName():
                "null";
    }

    private void getUsernames() {
        users = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);

        if(null!=users) {
            usernameA = users.get(0).getUserName();

            if (users.size() > 1)
                usernameB = users.get(1).getUserName();
        }
    }

    private void sendCloudNotification(UserEntity user) {
        //send notification to user contacts
        Notification notification = new Notification();
        notification.setUser(user);
        notification.setSubject("It's time for your medication"); //TODO Need to change according to the workflow
        notification.setContent(title); //TODO Need to change according to the workflow
        // Strings.xml -> notification_settings.itemId
        CloudManager.getInstance(getActivity()).sendNotification(notification, NotificationTypeEnum.TIME_TO_TAKE_MEDS);
    }

    /*
     * Find the active user
     * - should separate user for UI configuration
     */
    private UserEntity setActiveUser() {
        // Enable buttons
        List<Integer> userIds = new DispenseEventRepository().getUserIdsFromJoinActive();
        if(null!=userIds) {
            switch (userIds.size()) {
                case 0:
                    // workflow does not allow it !
                    enabledUserA = enabledUserB = false;
                    visibleUserA = visibleUserB = View.GONE;
                    break;

                case 1:
                    activeUserIndex = (userIds.get(0) == users.get(0).getId()) ? 0 : 1;
                    enabledUserA = (0 == activeUserIndex) ? true : false;
                    enabledUserB = (1 == activeUserIndex) ? true : false;
                    visibleUserA = (enabledUserA)?View.VISIBLE:View.GONE;
                    visibleUserB = (enabledUserB)?View.VISIBLE:View.GONE;
                    return users.get(activeUserIndex);

                case 2:
                    enabledUserA = enabledUserB = true;
                    visibleUserA = visibleUserB = View.VISIBLE;
                    return users.get(0);
            }
        }
        return null;
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (mp != null) {
                if (mp.isPlaying()) {
                    mp.stop();
                    mp.release();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Snooze 10 minutes !
     */
    public void onClickSnooze() {
        /*
         * currently disabled until Beta
         */
    }

    /*
     * Click on UserA or user 0th
     * - buttons are enabled if there is a dispenseEvent available
     */
    public void onClickUserA() {
        goDispense(users.get(0), true);
    }

    /*
     * Click on UserB or user 1st
     * - buttons are enabled if there is a dispenseEvent available
     */
    public void onClickUserB() {

        goDispense(users.get(1), true);
    }

    protected void goDispense(UserEntity user, boolean requireAuth) {
        Bundle bundle = this.getArguments();

        // Retrieve current active dispense events from the database.
        List<JoinDispenseEventScheduleMedication> dispenseEventItems = new ScheduleRepository().getJoinActiveByUserId(user.getId());

        DispenseEventsModel dispenseEventsModel = new DispenseEventsModel();
        dispenseEventsModel.dispenseEventItems = dispenseEventItems;
        dispenseEventsModel.user = user;

        // Snapshot this information and send it to the dispensing meds fragment.
        bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
        MedicationEntity medication = dispenseEventsModel.getCurrent().getMedicationEntity();
        bundle.putSerializable("medication", medication);
        String name = DispenseStrategy.getFragmentName(medication.getMedicationLocation());

        // If we require user pin authentication (dispensing a scheduled medication), then proceed
        // to the user pin fragment. For get scheduled med early or get as needed, we were already
        // authenticated from an earlier workflow, so proceed directly to dispensing workflow.
        if (requireAuth) {
            bundle.putString("authFragment", name);
            bundle.putString("fragmentName", "UserPinFragment");
            bundle.putBoolean("isFromAlarmReceivedFragment", true);
        } else {
            bundle.putString("fragmentName", name);
        }

        mListener.onButtonClicked(bundle);
    }
}