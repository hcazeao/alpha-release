package com.logicpd.papapill.fragments.my_medication;

import android.os.Bundle;
import android.util.Log;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DispenseStrategy;

public class BaseDispenseMedsFragment extends BaseHomeFragment {
    public static String TAG = "BaseDispenseMedsFragment";
    public DispenseEventsModel dispenseEventsModel;
    public JoinDispenseEventScheduleMedication currentDispenseEvent;

    /*
     * Go dispense the next item
     */
    protected void gotoDispense() {
        int location = dispenseEventsModel.getCurrent().getMedicationEntity().getMedicationLocation();
        String fragmentName = DispenseStrategy.getFragmentName(location);
        Bundle bundle = createBundle(fragmentName);
        mListener.onButtonClicked(bundle);
    }

    protected void gotoHowUFeelingFragment() {
        String fragmentName = HowUFeelingFragment.class.getSimpleName();

        if (null != currentDispenseEvent) {
            Bundle bundle = createBundle(fragmentName);
            mListener.onButtonClicked(bundle);
        }
    }

    protected void gotoTakeMedsFragment() {
    /*
     * Duplicate code in ReturnCupFragment -- should go away when we switch to models
     */
        String fragmentName = ScheduledMedicationTakenFragment.class.getSimpleName();

        if(null!=currentDispenseEvent) {
            int i = currentDispenseEvent.getDispenseType();
            DispenseEventTypeEnum type = DispenseEventTypeEnum.values()[i];
            Log.d("BaseDispenseMedsFragment", "type:"+type);

            switch (type) {
                case EARLY_DISPENSE:
                    fragmentName = MedicationTakenEarlyFragment.class.getSimpleName();
                    break;

                case AS_NEEDED:
                    fragmentName = MedicationTakenFragment.class.getSimpleName();
                    break;

                default:
                case SCHEDULED:
                    // already set above
            }
        }

        Bundle bundle = createBundle(fragmentName);
        mListener.onButtonClicked(bundle);
    }

    protected Bundle createBundle(String fragmentName) {
        Bundle bundle = this.getArguments();
        bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
        bundle.putString("fragmentName", fragmentName);
        return bundle;
    }

    protected boolean handleDispensed(int pillsRemain) {
        // The dispense was a success so we can decrement our pills remaining for this
        // dispense event and update the db.
        Log.d(TAG, "Handle Intent: Dispensed");
        Log.d(TAG, "Updated Pills Remaining: " + currentDispenseEvent.getPillsRemaining());

        // While we're at it, also update the medication quantity in the db.
        this.dispenseEventsModel.decrementMedicationQuantity();

        Log.d(TAG, "Updated Medication Quantity: " + currentDispenseEvent.getMedicationQuantity());

        // If we are at 0 pills remaining, this dispense event is done.
        if (0 == pillsRemain) {

            this.dispenseEventsModel.gotoCompleteState();

            Log.d(TAG, "Done with current dispense event. Updated dispense event index: "
                    + this.dispenseEventsModel.getIndex());

            // TODO: This may be a good place to push the results to the cloud.
            //currentDispenseEvent.setDispenseStopDate(new Date());
            //dispenseEventEntity = currentDispenseEvent.getDispenseEventEntity();
            //db.updateDispenseEvent(dispenseEventEntity);

            // Now check if we are done with ALL dispense events.
            if (this.dispenseEventsModel.isLastDispense()) {

                Log.d(TAG, "Done with ALL dispense events! Going to Take Meds screen.");

                // clean up if we are done
                BaseWireframe model = ((MainActivity)(this.getActivity())).getFeatureModel();
                if(null!=model)
                    model.cleanup();

                // We are all done, go to the feeling screen.
                gotoHowUFeelingFragment();
            } else {
                dispenseEventsModel.incrementIndexIfNotLastDispense();

                // We have more dispense events to handle. Go back to the Dispense Meds screen.
                gotoDispense();
            }
        } else {
            Log.d(TAG, "Current dispense event still in progress. Going back to Dispense Meds screen.");
            // This dispense event is still in progress. Go back to the Dispense Meds screen.
            gotoDispense();
        }

        return true;
    }
}
