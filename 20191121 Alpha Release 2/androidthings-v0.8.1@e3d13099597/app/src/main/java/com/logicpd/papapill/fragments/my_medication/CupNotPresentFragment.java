package com.logicpd.papapill.fragments.my_medication;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.utils.TimerUtil;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

public class CupNotPresentFragment extends BaseDispenseMedsFragment implements OnTimerListener {

    public static final String TAG = "CupNotPresentFragment";
    private Button btnCancel;
    private Button btnDispenseWithoutCup;

    private TimerUtil mTimer;
    private Sensor cup;

    public CupNotPresentFragment() {
        // Required empty public constructor
    }

    public static CupNotPresentFragment newInstance() {
        return new CupNotPresentFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cup_not_present, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            if (dispenseEventsModel != null) {
                currentDispenseEvent = dispenseEventsModel.getCurrent();

                // Populate the left side user name textview
                TextView textView = view.findViewById(R.id.textview_username);
                textView.setText(dispenseEventsModel.user.getUserName());

                // Establish connection to the cup sensor.
                cup = new Sensor(BoardDefaults.I2C_PORT,
                                    BoardDefaults.I2C_MUX_CUP_CHANNEL,
                                    AppConfig.getInstance().getCupThresholdHigh());

                // Right off the bat, perform a check if the cup is already present. If so, then we
                // do not need to be here anymore.
                int cupProximity = cup.getSensorValue();
                if (cupProximity != PROXIMITY_ERROR && cupProximity > AppConfig.getInstance().getCupThresholdHigh()) {
                    // The cup is present, proceed immediately to dispense meds.
                    bundle.putString("fragmentName", "DispenseMedsFragment");
                    bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
                    mListener.onButtonClicked(bundle);
                } else {
                    // Too bad, the cup is not present. We have to wait here until it is, or the user
                    // dispenses without the cup.
                    if(null==mTimer) {
                        mTimer = new TimerUtil(this);
                    }
                    mTimer.start(PROXIMITY_READ_TIMER_MS);
                }
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        TextUtils.disableButton(btnCancel);
        btnDispenseWithoutCup = view.findViewById(R.id.button_dispense_without_cup);
        btnDispenseWithoutCup.setOnClickListener(this);
        TextUtils.enableButton(btnDispenseWithoutCup);
    }

    @Override
    public void onTimerFinished() {
        // Read the door proximity sensor.
        int cupProximity = cup.getSensorValue();
        Log.d(TAG, "Cup Proximity Read: " + cupProximity);

        if (cupProximity != PROXIMITY_ERROR && cupProximity > AppConfig.getInstance().getCupThresholdHigh()) {
            Log.d(TAG, "Cup is present!");

            // User inserted the cup, start dispensing.
            Bundle bundle = this.getArguments();
            bundle.putString("fragmentName", "DispenseMedsFragment");
            bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
            mListener.onButtonClicked(bundle);
        } else {
            // Else check again after some time.
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = this.getArguments();
        if (v == btnCancel) {

        }
        if (v == btnDispenseWithoutCup) {
            bundle.putString("fragmentName", "DispenseMedsFragment");
            bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
            mListener.onButtonClicked(bundle);
        }
    }
}