package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.services.ScheduleProcessor;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DispenseStrategy;
import com.logicpd.papapill.wireframes.workflows.GetAsNeededMedication;
import com.logicpd.papapill.wireframes.IWireframe;

import java.util.Calendar;
import java.util.Date;

/**
 * GetAsNeededDispenseAmountFragment
 *
 * @author alankilloren
 */
public class GetAsNeededDispenseAmountFragment extends BaseHomeFragment {
    public static final String TAG = "GetAsNeededDispenseAmountFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private TextView tvMedication;
    private ImageView btnIncrease, btnDecrease;
    private Button btnNext;
    private EditText etDispenseAmount;
    private int mPillsNeeded;

    private static int MIN_DISPENSE_AMOUNT = 1;

    public GetAsNeededDispenseAmountFragment() {
        // Required empty public constructor
    }

    public static GetAsNeededDispenseAmountFragment newInstance() {
        return new GetAsNeededDispenseAmountFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_amount, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (medication != null) {
                String s = medication.getMedicationName() + " " + medication.getStrengthMeasurement();
                tvMedication.setText(s);
            }
        }
        etDispenseAmount.setText(String.valueOf(MIN_DISPENSE_AMOUNT));
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        tvMedication = view.findViewById(R.id.textview_medicationName);
        btnDecrease = view.findViewById(R.id.button_decrease);
        btnDecrease.setOnClickListener(this);
        btnIncrease = view.findViewById(R.id.button_increase);
        btnIncrease.setOnClickListener(this);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etDispenseAmount = view.findViewById(R.id.edittext_dispense_amount);
    }

    protected void updatePillsNeeded() {
        mPillsNeeded = Integer.parseInt(etDispenseAmount.getText().toString());
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnIncrease) {
            //TODO should this max out to be what's currently on-hand for this medication?
            updatePillsNeeded();
            if (mPillsNeeded >= MIN_DISPENSE_AMOUNT
                    && mPillsNeeded < medication.getMedicationQuantity()) {
                updatePillsNeeded();
                etDispenseAmount.setText(String.valueOf(++mPillsNeeded));
            }
        }
        if (v == btnDecrease) {
            if (mPillsNeeded > MIN_DISPENSE_AMOUNT) {
                updatePillsNeeded();
                etDispenseAmount.setText(String.valueOf(--mPillsNeeded));
            }
        }

        if (v == btnNext) {
            createDispenseEvent();
        }
    }

    /*
     * Use MedicationScheduleService functionality to update DispenseEvents and invoke dispense.
     */
    private void createDispenseEvent() {
        /*
         * TODO some additional busines logic needed here to handle exceptions based on # pills entered
         * TODO: MedicationScheduleService should be driven by Application state not event.
         */
        ((MainActivity)this.getActivity()).stopMedicationScheduleService();
        // create a dispenseEvent -- push to db
        ScheduleProcessor mScheduleProcessor = new ScheduleProcessor();
        BaseWireframe model = ((MainActivity) this.getActivity()).getFeatureModel();
        GetAsNeededMedication getAsNeeded = (GetAsNeededMedication)model;
        getAsNeeded.createScheduleDispenseTime(user.getId(), medication, mPillsNeeded);

        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(new Date());
        // process only the scheduled items
        ScheduleEntity schedule = getAsNeeded.getSchedule();
        if(schedule.getRecurrence()== ScheduleRecurrenceEnum.NONE.value) {
            mScheduleProcessor.createDispenseEvent(schedule.getId(),
                    getAsNeeded.getDispenseTime().getDispenseAmount(),
                    currentDate,
                    currentDate,
                    DispenseEventTypeEnum.AS_NEEDED);

            mScheduleProcessor.sendIntent(schedule.getId());
        }
    }
}