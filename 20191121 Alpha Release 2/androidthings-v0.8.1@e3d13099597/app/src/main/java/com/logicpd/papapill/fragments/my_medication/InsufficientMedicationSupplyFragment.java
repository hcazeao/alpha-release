package com.logicpd.papapill.fragments.my_medication;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentManageMedsInsufficientMedsBinding;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.DispenseStrategy;

public class InsufficientMedicationSupplyFragment extends BaseHomeFragment {
    public static final String TAG = "GetSingleDoseEarlyFragment";
    private FragmentManageMedsInsufficientMedsBinding mBinding;
    private UserEntity user;
    private MedicationEntity medication;
    private JoinScheduleDispense scheduleItem;

    public InsufficientMedicationSupplyFragment() {
        // Required empty public constructor
    }

    public static InsufficientMedicationSupplyFragment newInstance() {
        return new InsufficientMedicationSupplyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_meds_insufficient_meds, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            scheduleItem = (JoinScheduleDispense) bundle.getSerializable("scheduleItem");
        }
    }

    public void onClickCancel()
    {
        backButton.performClick();
    }


    /*
     * Dispense whatever is available in the location (bin, drawer, etc)
     */
    public void onClickDispenseAvailable()
    {
        Bundle bundle = this.getArguments();
        String fragmentName = DispenseStrategy.getFragmentName(medication.getMedicationLocation());   // destination by dispense location
        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}
