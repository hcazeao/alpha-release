package com.logicpd.papapill.fragments.my_medication;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.databinding.FragmentMyMedsMedicationTakenEarlyBinding;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;

public class MedicationTakenEarlyFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationTakenEarlyFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private JoinScheduleDispense scheduleItem;
    public FragmentMyMedsMedicationTakenEarlyBinding mBinding;
    public String mSubTitle;
    private BaseWireframe mModel;

    public MedicationTakenEarlyFragment() {
        // Required empty public constructor
    }

    public static MedicationTakenEarlyFragment newInstance() {
        return new MedicationTakenEarlyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_meds_medication_taken_early, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mModel = (BaseWireframe) ((MainActivity)this.getActivity()).getFeatureModel();
        user = (UserEntity) mModel.getMapItem("user");
        medication = (MedicationEntity) mModel.getMapItem("medication");
        scheduleItem = (JoinScheduleDispense) mModel.getMapItem("scheduleItem");

        if(null!=scheduleItem) {
            mSubTitle = this.getString(R.string.remember_to_take_this_med) +
                    " " + scheduleItem.getDispenseTime() +
                    " WITH " + scheduleItem.getDispenseName();
        }
    }

    public void onClickDispenseAnotherDose()
    {
        Bundle bundle = this.getArguments();
        bundle.putSerializable("user", user);
        bundle.putSerializable("scheduleItem", scheduleItem);
        bundle.putString("fragmentName", "GetSingleDoseEarlyFragment");
        mListener.onButtonClicked(bundle);
    }

    public void onClickDone()
    {
        Bundle bundle = new Bundle();

        bundle.putString("fragmentName", "MyMedicationFragment");
        mListener.onButtonClicked(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((MainActivity)this.getActivity()).exitFeature();
    }
}