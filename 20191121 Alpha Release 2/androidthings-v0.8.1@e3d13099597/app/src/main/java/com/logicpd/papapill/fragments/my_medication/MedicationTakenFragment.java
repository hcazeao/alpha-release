package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class MedicationTakenFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationTakenFragment";

    private LinearLayout contentLayout;
    private DispenseEventsModel dispenseEventsModel;
    private Button btnGetMedication, btnDone;
    private TextView tvTitle, tvMedInfo;

    public MedicationTakenFragment() {
        // Required empty public constructor
    }

    public static MedicationTakenFragment newInstance() {
        return new MedicationTakenFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_taken, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);

        btnGetMedication = view.findViewById(R.id.button_get_as_needed_med);
        btnGetMedication.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvTitle = view.findViewById(R.id.textview_title);
        tvMedInfo = view.findViewById(R.id.textview_med_info);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnGetMedication) {
            //do it again
            bundle.putSerializable("user", dispenseEventsModel.user);
            bundle.putString("fragmentName", "GetAsNeededMedicationFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnDone) {
            bundle.putSerializable("user", dispenseEventsModel.user);
            bundle.putString("fragmentName", "MyMedicationFragment");
            mListener.onButtonClicked(bundle);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ((MainActivity)this.getActivity()).exitFeature();
    }
}