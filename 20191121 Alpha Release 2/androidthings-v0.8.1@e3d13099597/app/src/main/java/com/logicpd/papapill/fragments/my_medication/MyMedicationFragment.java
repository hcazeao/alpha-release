package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.CheckFillStatus;
import com.logicpd.papapill.wireframes.workflows.GetAsNeededMedication;
import com.logicpd.papapill.wireframes.workflows.PauseMedication;
import com.logicpd.papapill.wireframes.workflows.ReviewMedicationSchedule;
import com.logicpd.papapill.wireframes.workflows.SingleDoseEarly;

/**
 * Fragment for My Medication
 *
 * @author alankilloren
 */
public class MyMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "MyMedicationFragment";

    private Button btnScheduled, btnReview, btnAsNeeded, btnFillStatus, btnPause, btnTracker;
    private UserEntity user;

    public MyMedicationFragment() {
        // Required empty public constructor
    }

    public static MyMedicationFragment newInstance() {
        return new MyMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnAsNeeded = view.findViewById(R.id.button_get_as_needed_med);
        btnAsNeeded.setOnClickListener(this);
        btnPause = view.findViewById(R.id.button_pause_med);
        btnPause.setOnClickListener(this);
        btnTracker = view.findViewById(R.id.button_med_tracker);
        btnTracker.setOnClickListener(this);
        btnFillStatus = view.findViewById(R.id.button_check_fill_status);
        btnFillStatus.setOnClickListener(this);
        btnScheduled = view.findViewById(R.id.button_get_scheduled_med);
        btnScheduled.setOnClickListener(this);
        btnReview = view.findViewById(R.id.button_review_med_sched);
        btnReview.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        String featureName = null;

        if (v == btnScheduled) {
            featureName = SingleDoseEarly.TAG;
            bundle.putSerializable("user", user);
            bundle.putString("authFragment", SingleDoseEarly.SingleDoseEarlyEnum.GetScheduledMedEarly.toString());
            bundle.putString("fragmentName", SingleDoseEarly.SingleDoseEarlyEnum.SelectUserForGetMeds.toString());
        }
        if (v == btnAsNeeded) {
            featureName = GetAsNeededMedication.TAG;
            bundle.putSerializable("user", user);
            bundle.putString("authFragment", "GetAsNeededMedicationFragment");
            bundle.putString("fragmentName", "SelectUserForGetMedsFragment");
        }
        if (v == btnPause) {
            featureName = PauseMedication.TAG;
            bundle.putSerializable("user", user);
            bundle.putString("authFragment", "SelectPauseMedicationFragment");
            bundle.putString("fragmentName", "SelectUserForGetMedsFragment");
        }
        if (v == btnTracker) {
            bundle.putSerializable("user", user);
            bundle.putString("authFragment", "MedicationTrackerFragment");
            bundle.putString("fragmentName", "SelectUserForGetMedsFragment");
        }
        if (v == btnFillStatus) {
            featureName = CheckFillStatus.TAG;
            bundle.putSerializable("user", user);
            bundle.putString("authFragment", "CheckFillStatusFragment");
            bundle.putString("fragmentName", "SelectUserForGetMedsFragment");
        }
        if (v == btnReview) {
            featureName = ReviewMedicationSchedule.TAG;
            bundle.putSerializable("user", user);
            bundle.putString("authFragment", "SelectReviewMedSchedFragment");
            bundle.putString("fragmentName", "SelectUserForGetMedsFragment");
        }

        /*
         * 1. Create model
         * 2. change application state
         * 3. store user
         */
        if(null!=featureName) {
            ((MainActivity) getActivity()).enterFeature(featureName);
            BaseWireframe model = ((MainActivity) getActivity()).getFeatureModel();
            model.addMapItem("user", user);
        }
        mListener.onButtonClicked(bundle);
    }
}
