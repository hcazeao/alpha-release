package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class ScheduledMedicationTakenFragment extends BaseHomeFragment {

    public static final String TAG = "ScheduledMedicationTakenFragment";

    private LinearLayout contentLayout;
    private UserEntity user;
    private MedicationEntity medication;
    private Button btnOk;
    private TextView tvTitle, tvMedInfo, txtUserName;

    private DispenseEventsModel dispenseEventsModel;

    public ScheduledMedicationTakenFragment() {
        // Required empty public constructor
    }

    public static ScheduledMedicationTakenFragment newInstance() {
        return new ScheduledMedicationTakenFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_scheduled_medication_taken, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            if (dispenseEventsModel != null) {
                tvTitle.setText(dispenseEventsModel.user.getUserName());
                txtUserName.setText(dispenseEventsModel.user.getUserName());
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
        txtUserName = view.findViewById(R.id.textview_username);
        btnOk = view.findViewById(R.id.button_ok);
        btnOk.setOnClickListener(this);
        tvTitle = view.findViewById(R.id.textview_title);
        tvMedInfo = view.findViewById(R.id.textview_med_info);
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();

        if (v == btnOk) {
            bundle.putBoolean("updateSchedule", true);
            bundle.putString("fragmentName", "Home");
        }
        mListener.onButtonClicked(bundle);
    }

/*
    use this when we turn on model feature for this.
    @Override
    public void onPause() {
        super.onPause();
        ((MainActivity)this.getActivity()).exitFeature();
    }*/
}