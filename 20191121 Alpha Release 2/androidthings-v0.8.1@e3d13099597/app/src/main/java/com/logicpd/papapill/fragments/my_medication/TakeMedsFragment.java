package com.logicpd.papapill.fragments.my_medication;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.device.i2c.CupLight;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.utils.TimerUtil;
import com.logicpd.papapill.wireframes.BundleFactory;
import com.logicpd.papapill.wireframes.workflows.DispenseStrategy;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class TakeMedsFragment extends BaseHomeFragment implements OnTimerListener {

    public static final String TAG = "TakeMedsFragment";

    private LinearLayout contentLayout;
    private TextView tvMedInfo, txtUserName;

    private DispenseEventsModel dispenseEventsModel;
    private JoinDispenseEventScheduleMedication currentDispenseEvent;

    private Sensor cup;
    private static CupLight cupLight = new CupLight(BoardDefaults.I2C_PORT, BoardDefaults.I2C_MUX_CUP_CHANNEL);

    private TimerUtil mTimer;

    public TakeMedsFragment() {
        // Required empty public constructor
    }

    public static TakeMedsFragment newInstance() {
        return new TakeMedsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_take_meds, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
            txtUserName.setText(dispenseEventsModel.user.getUserName());

            // Set mux to read from cup sensor.
            cup = new Sensor(BoardDefaults.I2C_PORT,
                                BoardDefaults.I2C_MUX_CUP_CHANNEL,
                                AppConfig.getInstance().getCupThresholdHigh());

            // Turn on the cup light.
            // TODO Beta: This needs to be a pulsing light.
            cupLight.setCupLightOn();

            // Start a timer to poll cup sensor to check if cup has been removed.
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
        tvMedInfo = view.findViewById(R.id.textview_med_info);
        txtUserName = view.findViewById(R.id.textview_username);
    }

    @Override
    public void onTimerFinished() {
        // Read the cup proximity sensor.
        int cupProximity = cup.getSensorValue();
        Log.d(TAG, "Cup Proximity Read: " + cupProximity);

        // Check if cup is removed. If so, advance to the next fragment, otherwise, check again
        // after another timer event.
        if (cupProximity != PROXIMITY_ERROR && cupProximity < AppConfig.getInstance().getCupThresholdLow()) {
            Log.d(TAG, "Cup has been removed, going to next screen.");

            Bundle bundle = this.getArguments();
            bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
            if (null == dispenseEventsModel ||               // give myself a way out
                    dispenseEventsModel.isLastDispense()) {
                bundle.putString("fragmentName", "ReturnCupFragment");
            } else {
                //dispenseEventsModel.incrementIndexIfNotLastDispense();
                int location = dispenseEventsModel.getCurrent().getMedicationEntity().getMedicationLocation();
                String fragmentName = DispenseStrategy.getFragmentName(location);
                bundle.putString("fragmentName", fragmentName);
            }
            mListener.onButtonClicked(bundle);

        } else {
            // Cup has not been removed, check again after the next timer elapses.
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    public void onClick(View v) {

    }
}