package com.logicpd.papapill.fragments.power_on;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;

public class ConnectToWirelessNetworkFragment extends BaseHomeFragment {
    public static final String TAG = "ConnectToWirelessNetworkFragment";
    private Button btnSkip;
    private Button btnConnectWifi;
    private Button btnConnectCellular;
    private boolean isFromSetup;

    public ConnectToWirelessNetworkFragment() {
        // Required empty public constructor
    }

    public static ConnectToWirelessNetworkFragment newInstance() {
        return new ConnectToWirelessNetworkFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_power_on_connect_to_wireless_network, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }

        setProgress(PROGRESS_SETUP_WIZARD,
                WorkflowProgress.SetupWizard.CONNECT_TO_WIRELESS_NETWORK.value,
                WorkflowProgress.SetupWizard.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        homeButton.setVisibility(View.GONE);

        btnSkip = view.findViewById(R.id.button_skip);
        btnSkip.setOnClickListener(this);
        btnConnectWifi = view.findViewById(R.id.button_connect_wifi);
        btnConnectWifi.setOnClickListener(this);
        btnConnectCellular = view.findViewById(R.id.button_connect_cellular);
        btnConnectCellular.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromSetup", isFromSetup);

        if (v == btnSkip) {
            // Skip to set date and time fragment.
            bundle.putString("fragmentName", "SetSystemDateTimeFragment");
        }

        if (v == btnConnectWifi) {
            bundle.putString("fragmentName", "SelectWifiNetworkFragment");
        }

        if (v == btnConnectCellular) {
            // Not implemented yet.
        }

        mListener.onButtonClicked(bundle);
    }
}