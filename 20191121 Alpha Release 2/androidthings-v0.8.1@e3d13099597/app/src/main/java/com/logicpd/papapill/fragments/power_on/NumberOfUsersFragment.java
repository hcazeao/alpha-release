package com.logicpd.papapill.fragments.power_on;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.utils.PreferenceUtils;

public class NumberOfUsersFragment extends BaseHomeFragment {

    public static final String TAG = "NumberOfUsersFragment";

    private Button btnOne, btnTwo, btnNext;
    private boolean isOneSelected, isTwoSelected;
    private boolean isFromSetup = false;
    private PreferenceUtils prefs;

    public NumberOfUsersFragment() {
        // Required empty public constructor
    }

    public static NumberOfUsersFragment newInstance() {
        return new NumberOfUsersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_power_on_number_of_users, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }

            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.NUMBER_OF_USERS.value,
                    WorkflowProgress.SetupWizard.values().length);

            btnOne.setBackgroundResource(R.drawable.button_selector);
            isOneSelected = false;
            btnTwo.setBackgroundResource(R.drawable.button_selector);
            isTwoSelected = false;
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        homeButton.setVisibility(View.GONE);

        btnOne = view.findViewById(R.id.button_1_user);
        btnOne.setOnClickListener(this);
        btnTwo = view.findViewById(R.id.button_2_users);
        btnTwo.setOnClickListener(this);

        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOne) {
            if (!isOneSelected) {
                btnOne.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isOneSelected = true;
                btnTwo.setBackgroundResource(R.drawable.button_selector);
                isTwoSelected = false;
            }
        }
        if (v == btnTwo) {
            if (!isTwoSelected) {
                btnTwo.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isTwoSelected = true;
                btnOne.setBackgroundResource(R.drawable.button_selector);
                isOneSelected = false;
            }
        }
        if (v == btnNext) {
            if (isOneSelected) {
                prefs.setFirstTimeUserCount(1);
                bundle.putString("fragmentName", "SetupUserFragment");
                bundle.putBoolean("isFromSetup", true);
                mListener.onButtonClicked(bundle);
            } else if (isTwoSelected) {
                prefs.setFirstTimeUserCount(2);
                bundle.putString("fragmentName", "SetupUserFragment");
                bundle.putBoolean("isFromSetup", true);
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please select an option first");
            }
        }
    }
}
