package com.logicpd.papapill.fragments.power_on;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.adapters.ContactsAdapter;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;

import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SetSystemContactFragment extends BaseHomeFragment {

    public static final String TAG = "SetSystemContactFragment";

    private Button btnNewContact, btnEdit;
    private RecyclerView recyclerView;
    private List<ContactEntity> contactList;
    private List<ContactEntity> selectedContactList;

    private TextView emptyText, tvTitle;
    private ContactsAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean isFromSetup;
    private String systemKey;

    public SetSystemContactFragment() {
        // Required empty public constructor
    }

    public static SetSystemContactFragment newInstance() {
        return new SetSystemContactFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_power_on_set_system_contact, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
                systemKey = bundle.getString("system_key");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.SET_SYSTEM_CONTACT.value,
                    WorkflowProgress.SetupWizard.values().length);
        }

        tvTitle.setText("SELECT A CONTACT FOR KEY RECOVERY");

        contactList = (List<ContactEntity>)new ContactRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        adapter = new ContactsAdapter(getActivity(), contactList, false, false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);
        if (contactList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText("Press NEW CONTACT to add a contact");
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
        }

        adapter.setOnItemClickListener(new ContactsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if(position < 0)
                {
                    Log.e(TAG, "invalid position");
                    return;
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable("contact", contactList.get(position));
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("system_key", systemKey);
                bundle.putString("fragmentName", "VerifySystemKeyFragment");
                mListener.onButtonClicked(bundle);
            }
        });
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnNewContact = view.findViewById(R.id.button_new_contact);
        btnNewContact.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_view_edit);
        btnEdit.setOnClickListener(this);

        // TODO: View/Edit button currently needs to be implemented.
        btnEdit.setEnabled(false);
        btnEdit.setBackgroundResource(R.drawable.rounded_rectangle_pressed);

        recyclerView = view.findViewById(R.id.recyclerview_contact_list);
        emptyText = view.findViewById(R.id.textview_add_contact);

        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNewContact) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            //bundle.putBoolean("isSystemContact", true); // Not required since presence of system key conveys the same info.
            bundle.putString("system_key", systemKey);
            bundle.putString("fragmentName", "ContactNameFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}