package com.logicpd.papapill.fragments.power_on;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.utils.TextUtils;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SetSystemKeyFragment extends BaseHomeFragment {

    public static final String TAG = "SetSystemKeyFragment";

    LinearLayout contentLayout;
    Button btnOk;
    EditText etSystemKey;
    TextView tvTitle;

    public SetSystemKeyFragment() {
        // Required empty public constructor
    }

    public static SetSystemKeyFragment newInstance() {
        return new SetSystemKeyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_key, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //TODO - handle passed in bundle
        }

        tvTitle.setText("SET SYSTEM KEY");
        setProgress(PROGRESS_SETUP_WIZARD,
                WorkflowProgress.SetupWizard.SET_SYSTEM_KEY.value,
                WorkflowProgress.SetupWizard.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
        homeButton.setVisibility(View.GONE);
        btnOk = view.findViewById(R.id.button_ok);
        btnOk.setOnClickListener(this);
        //btnOk.setVisibility(View.GONE);
        etSystemKey = view.findViewById(R.id.edittext_system_key);
        etSystemKey.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etSystemKey.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnOk.performClick();
                    handled = true;
                }
                return handled;
            }
        });

        TextUtils.showKeyboard(getActivity(), etSystemKey);

        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOk) {
            if (etSystemKey.getText().toString().length() > 3) {
                bundle.putString("system_key", etSystemKey.getText().toString());
                bundle.putBoolean("isFromSetup", true);
                bundle.putString("fragmentName", "SetSystemContactFragment");
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please enter a valid system key");
            }
        }
    }
}