package com.logicpd.papapill.fragments.power_on;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;

public class SetupCompleteFragment extends BaseHomeFragment {
    public static final String TAG = "SetupCompleteFragment";
    private Button btnNext;
    private boolean isFromSetup;

    public SetupCompleteFragment() {
        // Required empty public constructor
    }

    public static SetupCompleteFragment newInstance() {
        return new SetupCompleteFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_power_on_setup_complete, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }

        setProgress(PROGRESS_SETUP_WIZARD,
                WorkflowProgress.SetupWizard.SETUP_COMPLETE.value,
                WorkflowProgress.SetupWizard.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        homeButton.setVisibility(View.GONE);

        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            bundle.putString("fragmentName", "Home");
        }
        mListener.onButtonClicked(bundle);
    }
}