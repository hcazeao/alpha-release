package com.logicpd.papapill.fragments.power_on;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.utils.PreferenceUtils;

import java.util.List;

public class SetupMedicationFragment extends BaseHomeFragment {
    public static final String TAG = "SetupMedicationFragment";
    private TextView tvUserName;
    private Button btnSkip;
    private Button btnAddMedication;
    private boolean isFromSetup;
    private UserEntity user;
    private PreferenceUtils prefs;

    public SetupMedicationFragment() {
        // Required empty public constructor
    }

    public static SetupMedicationFragment newInstance() {
        return new SetupMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_power_on_setup_medication, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
                user = (UserEntity)bundle.getSerializable("user");
                if (user != null) {
                    tvUserName.setText(user.getUserName());
                }
            }
        }

        setProgress(PROGRESS_SETUP_WIZARD,
                WorkflowProgress.SetupWizard.SETUP_MEDICATION.value,
                WorkflowProgress.SetupWizard.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        homeButton.setVisibility(View.GONE);

        tvUserName = view.findViewById(R.id.setup_medication_subtitle);
        btnSkip = view.findViewById(R.id.button_skip);
        btnSkip.setOnClickListener(this);
        btnAddMedication = view.findViewById(R.id.button_add_medication);
        btnAddMedication.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromSetup", true);

        if (v == btnSkip) {
            int usersToBeAdded = prefs.getFirstTimeUserCount();
            List<UserEntity> userList = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);

            Log.d(TAG, "Users to be added: " + usersToBeAdded);
            Log.d(TAG, "UserList.size(): " + userList.size());

            if (usersToBeAdded == 2 && userList.size() < usersToBeAdded) {
                // We have one more user to go.
                bundle.putString("fragmentName", "SetupUserFragment");
            } else {
                // For a single user case, we are done with setup.
                bundle.putString("fragmentName", "SetupCompleteFragment");
            }
        }
        if (v == btnAddMedication) {
            bundle.putString("fragmentName", "ImportantMessageFragment");
            bundle.putSerializable("user", user);
        }
        mListener.onButtonClicked(bundle);
    }
}