package com.logicpd.papapill.fragments.power_on;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class SetupUserFragment extends BaseHomeFragment {
    public static final String TAG = "SetupUserFragment";
    private TextView tvTitle;
    private Button btnSkip;
    private Button btnAddUser;
    private boolean isFromSetup;
    private List<UserEntity> userList = new ArrayList<UserEntity>();

    public SetupUserFragment() {
        // Required empty public constructor
    }

    public static SetupUserFragment newInstance() {
        return new SetupUserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_power_on_setup_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }

            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.SETUP_USER.value,
                    WorkflowProgress.SetupWizard.values().length);

            // The very first time the setup wizard is run, userList should be null (no users created).
            // If 2 Users was selected, we will eventually re-enter this screen, at which userList
            // should be size 1.
            userList = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);

            if (userList.size() > 0) {
                tvTitle.setText("WOULD YOU LIKE TO ADD A SECOND USER NOW?");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        homeButton.setVisibility(View.GONE);

        tvTitle = view.findViewById(R.id.setup_user_title);
        btnSkip = view.findViewById(R.id.button_skip);
        btnSkip.setOnClickListener(this);
        btnAddUser = view.findViewById(R.id.button_add_user);
        btnAddUser.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromSetup", true);

        if (v == btnSkip) {
            bundle.putString("fragmentName", "SetupCompleteFragment");
        }
        if (v == btnAddUser) {
            bundle.putString("fragmentName", "AddUserFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}