package com.logicpd.papapill.fragments.reliability_testing;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.utils.StatsUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Fragment for the Reliability Home screen
 *
 * @author matttreiber
 */
public class ReliabilityHomeFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = "ReliabilityHomeFragment";

    protected OnButtonClickListener mListener;

    private Button btnVisionTest, btnMechanicalTest;
    private boolean isVisionTest;
    private int homePillCount = 1;      // Reliability VOR starts with single pill testing
    TextView tvNetworkInfo;
    TextView tvTestInstructions;
    TextView numboxPillCountTextView;

    public ReliabilityHomeFragment() {
        // Required empty public constructor
    }

    public static ReliabilityHomeFragment newInstance() {
        return new ReliabilityHomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reliability_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnVisionTest = view.findViewById(R.id.btn_vision_test);
        btnVisionTest.setOnClickListener(this);
        btnMechanicalTest = view.findViewById(R.id.btn_mechanical_test);
        btnMechanicalTest.setOnClickListener(this);
        isVisionTest = true;

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            homePillCount = bundle.getInt("homePillCount");
            if (homePillCount <= 0) {
                homePillCount = 1;
            }
        }

        numboxPillCountTextView = view.findViewById(R.id.id_pill_count);
        numboxPillCountTextView.setText(Integer.toString(homePillCount));
        fillTestInstructions(view);
        fillNetworkInfo(view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    protected void fillTestInstructions(View view) {
        tvTestInstructions = view.findViewById(R.id.id_test_instructions);
        tvTestInstructions.setText("This test will move a number (min 1) of pills ");
        tvTestInstructions.append("the following locations\n\n");
        tvTestInstructions.append("* bin 1 to bin 2\n");
        tvTestInstructions.append("* bin 2 to bin 14\n");
        tvTestInstructions.append("* bin 14 to bin 7\n");
        tvTestInstructions.append("* bin 7 to bin 3\n");
        tvTestInstructions.append("* bin 3 to bin 11");
    }

    protected void fillNetworkInfo(View view) {
        String line;
        String words[];
        tvNetworkInfo = view.findViewById(R.id.id_network_info);
        try {
            // Execute the ifconfig command
            Process p = Runtime.getRuntime().exec("ifconfig");

            // Read the response from the input stream.
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

            tvNetworkInfo.setText("IP Address Information:" + "\n");
            while ((line = input.readLine()) != null) {
                // Just print out all lines that have "useful information" in case we need it
                // for debugging.
                if (line.contains("eth0")) {
                    tvNetworkInfo.append("eth0:\n");
                }
                if (line.contains("wlan0")) {
                    tvNetworkInfo.append("wlan0:\n");
                }
                if (line.contains("lo")) {
                    tvNetworkInfo.append("lo:\n");
                }
                if (line.contains("Link encap") || line.contains("inet addr")) {
                    words = line.split(" ");
                    for (String word : words) {
                        if (word.contains(":")) {
                            if (word.contains("addr") && !word.contains("127")) {
                                // Save the IP Address for sending out emails
                                StatsUtil.getInstance().setIpAddress(word.substring(word.indexOf(':') + 1));
                            }
                            tvNetworkInfo.append(word + "\n");
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Bad: " + e.getMessage());
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "Attempting to Attach");
        super.onAttach(context);

        Activity a = null;

        if (context instanceof Activity) {
            a = (Activity) context;
        }

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnButtonClickListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnButtonClickListener");
        }
    }

    @Override
    public void onClick(View v) {
        if ((v == btnVisionTest) || (v == btnMechanicalTest)) {

            Bundle bundle = new Bundle();
            if (homePillCount <= 0) {
                homePillCount = 1;
            }

            String s = numboxPillCountTextView.getText().toString();
            homePillCount = Integer.parseInt(s);

            if (v == btnVisionTest) {
                Log.d(TAG, "Vision Test was started!");
                isVisionTest = true;
            } else {
                Log.d(TAG, "Mechanical Test was started");
                isVisionTest = false;
            }

            bundle.putString("fragmentName", ReliabilityTesterFragment.TAG);
            bundle.putInt("homePillCount", homePillCount);
            bundle.putBoolean("isVisionTest", isVisionTest);

            mListener.onButtonClicked(bundle);
        }
    }
}
