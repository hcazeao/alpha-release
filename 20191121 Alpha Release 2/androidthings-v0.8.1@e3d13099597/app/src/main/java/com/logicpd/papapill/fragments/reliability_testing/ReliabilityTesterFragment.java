package com.logicpd.papapill.fragments.reliability_testing;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Environment;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.computervision.BinCamera;
import com.logicpd.papapill.computervision.detection.DetectorException;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.tinyg.CommandBuilder;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.enums.IntentMsgEnum;
import com.logicpd.papapill.enums.IntentCommandEnum;
import com.logicpd.papapill.interfaces.IntentMsgInterface;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.misc.CloudManager;
import com.logicpd.papapill.utils.StatsUtil;
import com.logicpd.papapill.utils.TimerUtil;

import java.io.File;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReliabilityTesterFragment extends Fragment implements View.OnClickListener, IntentMsgInterface, OnTimerListener {
    public static final String TAG = "ReliabilityTesterFragment";

    protected OnButtonClickListener mListener;
    private Button btnStopTesting;
    private TextView tvTestDescription, tvTestStatus, tvSuccessCount, tvFailureCount;
    private TimerUtil timerTinyG;
    private CountDownTimer failureWatchDogTimer = null;
    private final Integer FAILURE_WATCHDOG_TIMER_5MIN = 300000;   // in ms

    private BinCamera mCamera = null;
    private Handler mCameraHandler;
    private HandlerThread mCameraThread;
    private Image image;
    private Integer currentAcquireBin;
    private Integer currentDestinationBin;
    private Integer pillCount = 1;      // 1 pill is the default for testing
    private Integer homePillCount;
    private boolean isVisionTest;
    private Integer failures;

    private enum TesterState {
        start,
        takePhoto,
        acquire,
        acquired,
        dispensing,
        stopped
    }

    // As described in task PAPA1-775
    // https://jira.logicpd.com/browse/PAPA1-775
    private enum BinProgressionState {
        bin1toBin2,
        bin2toBin14,
        bin14toBin7,
        bin7toBin3,
        bin3toBin11,
        bin11toBin9,
        bin9toBin1
    }

    private TesterState currentState;
    private BinProgressionState currentBins;

    Logger fileLogger = LoggerFactory.getLogger(ReliabilityHomeFragment.class);


    public ReliabilityTesterFragment() {
        // Required empty public constructor
    }

    public static ReliabilityTesterFragment newInstance() {
        return new ReliabilityTesterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reliability_tester, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            homePillCount = bundle.getInt("homePillCount");
            isVisionTest = bundle.getBoolean("isVisionTest");
        } else {
            homePillCount = 1;
            isVisionTest = true;
        }

        if (homePillCount <= 0) {
            pillCount = 1;
            homePillCount = 1;
        } else {
            pillCount = homePillCount;
        }

        Log.d(TAG, "Starting pillCount = " + pillCount.toString());


        btnStopTesting = view.findViewById(R.id.btn_stop_testing);
        btnStopTesting.setOnClickListener(this);
        if (isVisionTest) {
            btnStopTesting.setText("Stop Vision Test");
            Log.d(TAG, "Starting Test with Vision");
        } else {
            btnStopTesting.setText("Stop Mechanical Test");
            Log.d(TAG, "Starting Mechanical Test");
        }


        tvTestDescription = view.findViewById(R.id.tv_test_description);
        tvTestDescription.setText("Test will work on ");
        tvTestDescription.append(Integer.toString(pillCount));
        tvTestDescription.append(" pill(s) at a time");

        failures = 0;
        // Time can be save by going straight to Bin 1
        if (isVisionTest) {
            currentState = TesterState.takePhoto;
        } else {
            currentState = TesterState.acquire;
        }

        currentBins = BinProgressionState.bin1toBin2;
        currentAcquireBin = 1;
        currentDestinationBin = 2;
        rotateToBin(currentAcquireBin);

        StatsUtil.getInstance().resetStats();
        fileLogger.info("Begin New Test");

        updateStatsView(view);

        // Inflate the layout for this fragment
        createCameraThread();

        startWDTimer();
    }

    public void updateStatsView(View view) {
        tvTestStatus = view.findViewById(R.id.id_status_update_view);
        tvSuccessCount = view.findViewById(R.id.id_successes_update_view);
        tvFailureCount = view.findViewById(R.id.id_failures_update_view);

        if (StatsUtil.getInstance().getIsRunningStatus()) {
            tvTestStatus.setText("Running");
        } else {
            tvTestStatus.setText("Stopped");
        }
        tvSuccessCount.setText(StatsUtil.getInstance().getSuccesses().toString());
        tvFailureCount.setText(StatsUtil.getInstance().getFailures().toString());
    }

    private void startWDTimer() {
        failureWatchDogTimer = new CountDownTimer(FAILURE_WATCHDOG_TIMER_5MIN, 1) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                currentState = TesterState.stopped;
                StatsUtil.getInstance().setIsRunningStatus(false);
                finalizeTest("Watchdog Timeout");
                Intent updateUI = new Intent("updateUI");
                getContext().sendBroadcast(updateUI);            }
        };
        Log.d(TAG, "Started Failure WD Timer");
        failureWatchDogTimer.start();
    }

    private void kickWDTimer() {
        Log.d(TAG, "Kicked the Failure WD Timer");
        failureWatchDogTimer.start();
    }

    private void testerStateMachine() {
        switch (currentState) {
            case start:
                rotateToBin(currentAcquireBin);
                if (isVisionTest) {
                    currentState = TesterState.takePhoto;
                } else {
                    // Don't need an image for mechanical only testing
                    currentState = TesterState.acquire;
                }
                break;

            case takePhoto:
                // Take image if prepDispense is complete! OnImageAvailableListener calls for the
                // dispense after the image is acquired.
                Log.d(TAG, "Let's take a picture and find meds");
                takePhoto();
                currentState = TesterState.acquire;
                break;

            case acquire:
                if (!isVisionTest) {
                    TinyGDriver.getInstance().doReliabilityMechanicalAcquire(pillCount);
                    currentState = TesterState.acquired;
                    break;
                }

                Log.d(TAG, "Acquired Latest Image");
                //get the bytes
                ByteBuffer imageBuf = image.getPlanes()[0].getBuffer();
                final byte[] imageBytes = new byte[imageBuf.remaining()];
                imageBuf.get(imageBytes);
                image.close();
                TinyGDriver.getInstance().doReliabilityAcquirePill(imageBytes, pillCount);
                currentState = TesterState.acquired;
                break;

            case acquired:
                if (isVisionTest) {
                    Log.d(TAG, "Pill Acquired");
                } else {
                    Log.d(TAG, "Mechanical Test Success");
                }
                StatsUtil.getInstance().addSuccesses();
                debugLogStatus();
                kickWDTimer();
                Intent updateUI = new Intent("updateUI");
                getContext().sendBroadcast(updateUI);

                // Now drop the pill in the next bin
                rotateToBin(currentDestinationBin);
                currentState = TesterState.dispensing;
                break;

            case dispensing:
                Log.d(TAG, "Dispensing pill in bin:" + currentDestinationBin.toString());
                TinyGDriver.getInstance().write(CommandBuilder.CMD_COOLANT_OFF);
                pillCount--;
                Log.d(TAG, "pillCount = " + pillCount.toString());

                // Really should set the state machine to start, but then it would be a recursive
                // call to switch states since turning off the vacuum pump happens immediately and
                // there is no outside process that can call the state machine. Without which,
                // this function won't get called again.
                if (pillCount <= 0) {
                    // reset the pillCount
                    pillCount = homePillCount;
                    // Time to switch bins
                    updateBins();
                }

                rotateToBin(currentAcquireBin);
                if (isVisionTest) {
                    currentState = TesterState.takePhoto;
                } else {
                    // Don't need an image for mechanical only testing
                    currentState = TesterState.acquire;
                }
                break;

            case stopped:
                // Lie in mourning
                break;
        }
    }

    private void debugLogStatus() {
        String debugString = "isRunningStatus: " + StatsUtil.getInstance().getIsRunningStatus();
        debugString += " Sucessess: " + StatsUtil.getInstance().getSuccesses();
        debugString += " Failures: " + StatsUtil.getInstance().getFailures();
        Log.d(TAG, debugString);
    }

    private void updateBins() {
        switch (currentBins) {
            case bin1toBin2:
                currentBins = BinProgressionState.bin2toBin14;
                currentAcquireBin = 2;
                currentDestinationBin = 14;
                break;

            case bin2toBin14:
                currentBins = BinProgressionState.bin14toBin7;
                currentAcquireBin = 14;
                currentDestinationBin = 7;
                break;

            case bin14toBin7:
                currentBins = BinProgressionState.bin7toBin3;
                currentAcquireBin = 7;
                currentDestinationBin = 3;
                break;

            case bin7toBin3:
                currentBins = BinProgressionState.bin3toBin11;
                currentAcquireBin = 3;
                currentDestinationBin = 11;
                break;

            case bin3toBin11:
                currentBins = BinProgressionState.bin11toBin9;
                currentAcquireBin = 11;
                currentDestinationBin = 9;
                break;

            case bin11toBin9:
                currentBins = BinProgressionState.bin9toBin1;
                currentAcquireBin = 9;
                currentDestinationBin = 1;
                break;

            case bin9toBin1:
                currentBins = BinProgressionState.bin1toBin2;
                currentAcquireBin = 1;
                currentDestinationBin = 2;
                break;

            default:
                currentBins = BinProgressionState.bin1toBin2;
                currentAcquireBin = 1;
                currentDestinationBin = 2;
                break;
        }
        Log.d(TAG, "Updated bin sequence to " + currentBins.toString());
    }

    private void updateFailures(IntentMsgEnum msg) {
        failures++;
        StatsUtil.getInstance().addFailures();
        fileLogger.error("Failure (msg): " + msg.toString());
        debugLogStatus();

        if (msg != IntentMsgEnum.limitError) {
            currentState = TesterState.start;
            testerStateMachine();
        } else {
            // If there is a limitError, restart after the HOME_ALL command to ensure only one
            // rotateToBin() command is issued
        }

        preserveVisionDebug();

        Intent updateUI = new Intent("updateUI");
        getContext().sendBroadcast(updateUI);
    }

    private void preserveVisionDebug() {
        String fromPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        fromPath += "/Pictures/6-Final.jpg";
        File from = new File(fromPath);
        String toPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        toPath += "/Logs/" +"failure-" + failures.toString() + "-6-Final.jpg";
        File to = new File(toPath);
        from.renameTo(to);
        fromPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        fromPath += "/Pictures/1a-Original.jpg";
        from = new File(fromPath);
        toPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        toPath += "/Logs/" +"failure-" + failures.toString() + "-1a-Original.jpg";
        to = new File(toPath);
        from.renameTo(to);
    }

    private void finalizeTest(String testResultSeed) {
        if((!TinyGDriver.getInstance().isStateMachineIdle()) &&
                (!testResultSeed.equals("Test stopped manually "))) {
            fileLogger.error("Timeout Failure: TinyG is still busy");
        }

        testResultSeed += ": Successes = " + StatsUtil.getInstance().getSuccesses();
        testResultSeed += "; Failures = " + StatsUtil.getInstance().getFailures();
        fileLogger.info(testResultSeed);

        Log.d(TAG, "Trying to send notification");

        String toEmailMatt = "matt.treiber@logicpd.com";
        String toEmailRick = "rick.kinnunen@logicpd.com";
        String subject = "Reliability Test Ended";
        String body = "Serial Number " + AppConfig.getInstance().getSerialNumber();
        body += "IP Address: " + StatsUtil.getInstance().getIpAddress() + "\n";
        body += "Results:\n";
        body += testResultSeed;
        CloudManager.getInstance(getActivity()).sendEmailMessage(toEmailMatt, subject, body);

        // Reset TinyG since a test is currently running
        TinyGDriver.getInstance().reset();
        failureWatchDogTimer.cancel();
    }

    private void rotateToBin(Integer bin) {
        if (AppConfig.getInstance().isTinyGAvailable) {
            // check if tinyG is ready ?
            TinyGDriver tinyG =  TinyGDriver.getInstance();
            if(!tinyG.isStateMachineIdle()) {
                Log.d(TAG, "TinyG is not ready, wait 500ms");
                // start a timer event
                if(timerTinyG == null) {
                    timerTinyG = new TimerUtil(this);
                }
                // Limit error generates crash without Looper
                timerTinyG.startTimerWithLooper();
            }
            else {
                Log.v(TAG, "rotateToBin(" + bin.toString() + ")");
                tinyG.doPrepDispense(bin, pillCount);
            }
        }
    }

    /*
     * Time is up, try to run TinyG again
     */
    public void onTimerFinished() {
        Log.v(TAG, "onTimerFinished() 500ms");
        rotateToBin(currentAcquireBin);
    }

    @Override
    public boolean handleIntent(String command,
                                String state,
                                String message,
                                String version) {   // idempotent

        IntentMsgEnum msg = IntentMsgEnum.valueOf(message);
        IntentCommandEnum cmd = IntentCommandEnum.CommandUnknown;

        try {
            cmd = IntentCommandEnum.valueOf(command);
        } catch (Exception e) {
            Log.e(TAG, "Intent Command Not Found: " + command, e);
        }

        Log.d(TAG, String.format("handleIntent() command: %s state: %s msg: %s", command, state, msg));

        switch (cmd) {
            case CommandHomeAll:
                if (msg == IntentMsgEnum.success) {
                    Log.d(TAG, "Somthing bad happened");
                    // Possible to get here after a limit switch error
                    if (currentState != TesterState.stopped) {
                        currentState = TesterState.start;
                        testerStateMachine();
                    }
                }
                return true;

            case CommandPreDispense:
                return processPreDispense(msg, state, version);

            case CommandReliabilityAcquirePill:
                return processReliabilityAcquirePill(msg);

            case CommandFullDispense:
                // Ignore
                return true;

            case CommandRotateCarousel:
                // Ignore
                return true;

            case CommandReset:
                // Ignore, only ReliabilityHomeFragment issues a CommandReset and at the creation
                // of this fragment.
                return true;

            case CommandManager:
                if (msg == IntentMsgEnum.limitError) {
                    updateFailures(msg);
                    Log.e(TAG, "TinyG Hit A Limit Switch");
                }
                return true;

            case CommandReliabilityMechanicalAcquire:
                Log.d(TAG, "CommandReliabilityMechanicalAcquire finished, state = " +
                        currentState.toString());
                // Finished, pill not acquired, but check anyway. There are no errors to process
                // unless there is a watchdog timeout
                if (msg == IntentMsgEnum.mechanicalTestFinished) {
                    testerStateMachine();
                }
                return true;

            default:
                Log.e(TAG, String.format("Did not recognize the Intent Broadcast command: " + command));
                return false;
        }
    }

    // Returning true means the message was handled.
    private boolean processPreDispense(IntentMsgEnum msg, String state, String version) {
        switch (msg) {
            case success:
                testerStateMachine();
                return true;

            default:
                Log.e(TAG, "CommandPreDispense Failed!");
                return false;
        }
    }

    // Returning true means the message was handled.
    private boolean processReliabilityAcquirePill(IntentMsgEnum msg) {
        switch (msg) {
            case acquired:
                testerStateMachine();
                return true;

            case pillNotPresentError:
                // The TinyG says the pill is not attached. If the pill is dropped, or the vision
                // system mistakes the center of a pill. If the vacuum head makes clean contact with
                // the bottom of the bin, the TinyG will think it acquired a pill and start homing
                // the z-stage. At the homing location, the TinyG is asked if the pill is attached.
                // If not, this error is generated. Save the final image to determine if it was a
                // vision error.
                updateFailures(msg);
                Log.e(TAG, "Pill Not Present Error");
                return true;

            case visionError:
                updateFailures(msg);
                Log.e(TAG, "Vision error occurred");
                return true;

            default:
                return false;
        }
    }

    /*
     * The listener for when the camera has taken a picture.  Once the picture is taken, it will send
     * the image to the processor to identify meds and their locations.
     */
    private ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            image = reader.acquireLatestImage();
            testerStateMachine();
        }
    };

    private void createCameraThread()
    {
        //Setup Camera Thread
        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            mCameraThread = new HandlerThread("CameraBackground");
            mCameraThread.start();
            mCameraHandler = new Handler(mCameraThread.getLooper());
        }
    }

    private void initCamera()
    {
        //check to see if this build has camera enabled
        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            try {
                mCamera = new BinCamera();
                mCamera.initializeCamera(getActivity(), mCameraHandler, mOnImageAvailableListener, null);
            } catch (DetectorException de) {
                Log.e(TAG, "Error in Camera Initialization", de);
            }
        }
    }

    private void takePhoto()
    {
        if (mCamera != null) {
            mCamera.takePicture();
            Log.d(TAG, "takePhoto() done");
            return;
        }
        Log.e(TAG, "takePhoto() camera not available");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        if (isVisionTest) {
            initCamera();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCamera != null) {
            mCamera.shutDown();
        }

        if (mCameraThread != null) {
            mCameraThread.quit();
            mCameraHandler.removeCallbacksAndMessages(null);
            mCameraHandler = null;
            mCameraThread = null;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a = null;

        if (context instanceof Activity) {
            a = (Activity) context;
        }

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mListener = (OnButtonClickListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnButtonClickListener");
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();

        if (currentState != TesterState.stopped) {
            // Assume the test has stopped from manual intervention.
            finalizeTest("Test stopped manually ");
        }

        if (v == btnStopTesting) {
            bundle.putString("fragmentName", ReliabilityHomeFragment.TAG);
        }

        bundle.putInt("homePillCount", homePillCount);
        mListener.onButtonClicked(bundle);
    }
}
