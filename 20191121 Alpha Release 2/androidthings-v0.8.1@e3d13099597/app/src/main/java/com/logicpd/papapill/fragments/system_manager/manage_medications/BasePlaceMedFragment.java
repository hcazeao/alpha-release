package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.AddNewMedication;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

public class BasePlaceMedFragment extends BaseHomeFragment {
    public static final String TAG = "BasePlaceMedFragment";
    protected UserEntity user;
    protected MedicationEntity medication;
    protected boolean isFromSchedule, isFromSetup;
    protected BaseWireframe mModel;
    protected Button btnCancel;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.PLACE_MED_X.value,
                WorkflowProgress.AddMedication.values().length);

        mModel = ((MainActivity)getActivity()).getFeatureModel();
        if(null!=mModel) {
            if(null==user) {
                user = mModel.getUser();
            }
            if(null==medication) {
                medication = mModel.getMedication();
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnCancel) {
            backButton.performClick();
        }
    }

    protected void gotoHappyPath() {
        Bundle bundle = new Bundle();

        //This code can be used in the actual hardware callback to save the med to the db
        //For now, this button is being used to advance the workflow until hardware is present
        boolean returnVal = (0==medication.getId()) ?
                // ok to re-insert if 0 is the correct id
                new MedicationRepository().insert(medication):
                new MedicationRepository().update(medication);

        if (returnVal) {

            String fragmentName;
            String modelName = getModelName();
            switch (modelName){
                case RefillMedication.TAG:
                    fragmentName = mModel.getNextFragmentName(TAG);
                    break;

                default:
                case AddNewMedication.TAG:
                    fragmentName = "MedicationAddedFragment";
                    break;
            }

            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", fragmentName);
            mListener.onButtonClicked(bundle);

        } else {
            //TODO do we need an actual screen for this instead in case something goes wrong?
            TextUtils.showToast(getActivity(), "Problem saving medication");
        }
    }

    private String getModelName() {
        if(null==mModel)
            return "";

        return mModel.getClass().getSimpleName();
    }
}
