package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;

/**
 * ConfirmDeleteDispenseTimeFragment
 *
 * @author alankilloren
 */
public class ConfirmDeleteDispenseTimeFragment extends BaseHomeFragment {

    public static final String TAG = "ConfirmDeleteDispenseTimeFragment";

    private UserEntity user;
    private DispenseTimeEntity dispenseTime;
    private TextView tvDeleteText;
    private Button btnCancel, btnRemove;
    private boolean isFromSchedule;

    public ConfirmDeleteDispenseTimeFragment() {
        // Required empty public constructor
    }

    public static ConfirmDeleteDispenseTimeFragment newInstance() {
        return new ConfirmDeleteDispenseTimeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_confirm_delete_dispense_time, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            dispenseTime = (DispenseTimeEntity) bundle.getSerializable("dispensetime");

            if (dispenseTime != null) {
                String s = "DELETE " + dispenseTime.getDispenseName() + " " + dispenseTime.getDispenseTime() + " DISPENSING TIME FROM SYSTEM?";
                tvDeleteText.setText(s);
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvDeleteText = view.findViewById(R.id.textview_delete_dispenseTime_text);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnRemove = view.findViewById(R.id.button_delete_dispenseTime);
        btnRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnRemove) {
            // remove dispense time from db and proceed to DispenseTimeDeletedFragment
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            int returnVal = model.deleteDispenseTime(dispenseTime);
            if (returnVal > 0) {
                bundle.putBoolean("isFromSchedule", isFromSchedule);
                bundle.putString("fragmentName", "DispenseTimeDeletedFragment");
                bundle.putSerializable("user", user);
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Problem deleting dispense time");
            }
        }
    }
}