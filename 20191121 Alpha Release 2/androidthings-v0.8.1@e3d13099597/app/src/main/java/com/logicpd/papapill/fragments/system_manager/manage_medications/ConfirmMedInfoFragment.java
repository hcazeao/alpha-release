package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

public class ConfirmMedInfoFragment extends BaseHomeFragment {
    public static final String TAG = "ConfirmMedInfoFragment";

    private Button btnEdit, btnOK;
    private TextView tvMedInfo, tvTitle;
    private UserEntity user;
    private MedicationEntity medication;
    private boolean isFromAddNewUser;
    private boolean isFromSetup;

    public ConfirmMedInfoFragment() {
        // Required empty public constructor
    }

    public static ConfirmMedInfoFragment newInstance() {
        return new ConfirmMedInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_verify_med_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        getBundle();
        getModel();

        if (user != null) {
            tvTitle.setText("NEW MED FOR " + user.getPatientName());
        }

        String sb = null;
        if (medication != null) {
            sb = "MED NAME: " + medication.getMedicationName() + " " + medication.getStrengthMeasurement() + "\n"
                    + "MED NICKNAME: " + medication.getMedicationNickname() + "\n"
                    + "QUANTITY: " + medication.getMedicationQuantity() + "\n"
                    + "DIRECTIONS FOR USE: " + medication.getDosageInstructions() + "\n"
                    + "USE-BY: " + medication.getUseByDate();
        }
        tvMedInfo.setText(sb);

    }

    private void getBundle() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            medication = (MedicationEntity) bundle.getSerializable("medication");
            user = (UserEntity) bundle.getSerializable("user");

            if (bundle.containsKey("isFromAddNewUser")) {
                isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.CONFIRM_MED_INFO.value,
                WorkflowProgress.AddMedication.values().length);
    }

    private void getModel() {
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        if(null!=model){
            if(null==medication) {
                medication = model.getMedication();
            }
            if(null==user) {
                user = model.getUser();
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_edit);
        btnEdit.setOnClickListener(this);
        tvMedInfo = view.findViewById(R.id.textview_verify_med_info);
        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromSetup", isFromSetup);
        bundle.putSerializable("user", user);
        bundle.putSerializable("medication", medication);

        if (v == btnOK) {
            bundle.putString("fragmentName", SelectMedScheduleFragment.TAG);
        }
        if (v == btnEdit) {
            //TODO since popbackstack doesn't retain the bundle, we may have to temp save medication/user
            Gson gson = new Gson();
            String medJSON = gson.toJson(medication);
            String userJSON = gson.toJson(user);
            bundle.putString("removeAllFragmentsUpToCurrent", PatientNameFragment.TAG);
        }
        mListener.onButtonClicked(bundle);
    }
}