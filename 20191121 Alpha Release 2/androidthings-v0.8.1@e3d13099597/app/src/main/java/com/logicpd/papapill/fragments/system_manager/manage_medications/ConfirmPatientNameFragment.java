package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentManageMedsConfirmPatientNameBinding;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

public class ConfirmPatientNameFragment extends BaseHomeFragment {
    public static final String TAG = "ConfirmPatientNameFragment";
    private FragmentManageMedsConfirmPatientNameBinding mBinding;
    public String mMed4PatientName;
    public ConfirmPatientNameFragment() {

    }

    public static ConfirmPatientNameFragment newInstance() { return new ConfirmPatientNameFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_meds_confirm_patient_name, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        RefillMedication model = (RefillMedication)((MainActivity)getActivity()).getFeatureModel();
        mMed4PatientName = this.getString(R.string.medication_is_for) + ((RefillMedication)model).getUserName();
        mBinding.invalidateAll();
    }

    public void onClickCorrect() {
        gotoFragment(RefillMedication.RefillMedicationEnum.ConfirmRefillMedication.toString());
    }

    public void onClickIncorrect() {
        gotoFragment(RefillMedication.RefillMedicationEnum.IncorrectPatientName.toString());
    }

    private void gotoFragment(String fragmentName) {
        Bundle bundle = new Bundle();
        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}
