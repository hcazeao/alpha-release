package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class ConfirmRemoveMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "ConfirmRemoveMedicationFragmente";

    private UserEntity user;
    private MedicationEntity medication;
    private TextView tvMedication, tvFromSched;
    private Button btnCancel, btnRemove;
    private boolean isFromUserDelete;

    public ConfirmRemoveMedicationFragment() {
        // Required empty public constructor
    }

    public static ConfirmRemoveMedicationFragment newInstance() {
        return new ConfirmRemoveMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_confirm_remove_med, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (medication != null) {
                String s = medication.getMedicationName() + " " + medication.getStrengthMeasurement();
                tvMedication.setText(s);
            }
            if (user != null) {
                String s = "FROM " + user.getUserName() + "'S SCHEDULE AND THE DEVICE?";
                tvFromSched.setText(s);
            }
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvMedication = view.findViewById(R.id.textview_medicationName);
        tvFromSched = view.findViewById(R.id.textview_from_device_schedule);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnRemove = view.findViewById(R.id.button_remove);
        btnRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnRemove) {
            bundle.putBoolean("isRemoveMedication", true);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putBoolean("isFromUserDelete", isFromUserDelete);
            int medLocation = medication.getMedicationLocation();
            if (medLocation > 0 && medLocation < 15) {
                //this is a bin - goto RemoveBinFragment
                bundle.putString("fragmentName", "RemoveBinFragment");
            }
            if (medLocation == getActivity().getResources().getInteger(R.integer.drawerLocation)) {
                //this is a drawer
                bundle.putString("fragmentName", "RemoveDrawerFragment");
            }
            if (medLocation == getActivity().getResources().getInteger(R.integer.refridgeratorLocation)) {
                //this is the fridge
                bundle.putString("fragmentName", "RemoveFridgeFragment");
            }
            if (medLocation == getActivity().getResources().getInteger(R.integer.otherLocation)) {
                //other location
                bundle.putString("fragmentName", "RemoveOtherFragment");
            }
            mListener.onButtonClicked(bundle);
        }
    }
}