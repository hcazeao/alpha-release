package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.ChangeMedSchedule;

/**
 * DispenseTimeSummaryFragment
 *
 * @author alankilloren
 */
public class DispenseTimeSummaryFragment extends BaseHomeFragment {

    public static final String TAG = "DispenseTimeSummaryFragment";

    private TextView tvSummary, tvTitle;
    private Button btnEdit, btnNext;
    private boolean isEditMode, isFromSchedule;
    private UserEntity user;
    private DispenseTimeEntity dispenseTime;
    private boolean isFromSetup;

    public DispenseTimeSummaryFragment() {
        // Required empty public constructor
    }

    public static DispenseTimeSummaryFragment newInstance() {
        return new DispenseTimeSummaryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_dispense_time_summary, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            if (bundle.containsKey("isEditMode")) {
                isEditMode = bundle.getBoolean("isEditMode");
                if (isEditMode) {
                    String s = "UPDATED NAMED DISPENSING TIME";
                    tvTitle.setText(s);
                }
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
            user = (UserEntity) bundle.getSerializable("user");
            dispenseTime = (DispenseTimeEntity) bundle.getSerializable("dispensetime");
            if (dispenseTime != null) {
                String s = dispenseTime.getDispenseName() + "\n" + dispenseTime.getDispenseTime();
                tvSummary.setText(s);
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            // TODO: Edit feature currently leads to a potential crash. Disabling this button
            // while investigating the issue.
            btnEdit.setEnabled(false);
            btnEdit.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.DISPENSE_TIME_SUMMARY.value,
                WorkflowProgress.AddMedication.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnEdit = view.findViewById(R.id.button_edit_dispenseTime);
        btnEdit.setOnClickListener(this);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        tvSummary = view.findViewById(R.id.textview_dispenseTime_summary);
        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        bundle.putBoolean("isFromSetup", isFromSetup);

        if (v == btnNext) {
            // add/update db
            if (isEditMode) {
                //edit

                int returnVal = model.updateDispenseTime(dispenseTime);
                if (returnVal > 0) {

                    bundle.putBoolean("isEditMode", isEditMode);
                    bundle.putBoolean("isFromSchedule", isFromSchedule);
                    bundle.putSerializable("user", user);
                    bundle.putBoolean("updateSchedule", true);//update schedule to reflect any time changes, etc.
                    if (isWorkflowChangeMedSchedule()) {
                        bundle.putString("removeAllFragmentsUpToCurrent", "SelectDispensingTimesFragment");
                    }
                    else {
                        bundle.putString("removeAllFragmentsUpToCurrent", ManageMedsFragment.class.getSimpleName());
                    }
                    mListener.onButtonClicked(bundle);
                }
                else {
                    TextUtils.showToast(getActivity(), "Problem saving info");
                }
            } else {
                //add
                int returnVal = model.addDispenseTime(dispenseTime);
                if (returnVal > 0) {
                    bundle.putBoolean("isEditMode", isEditMode);
                    bundle.putBoolean("isFromSchedule", isFromSchedule);
                    bundle.putSerializable("user", user);

                    if(isFromSchedule || isWorkflowChangeMedSchedule()) {
                        bundle.putString("removeAllFragmentsUpToCurrent", "SelectDispensingTimesFragment");
                    }
                    else {
                        bundle.putString("removeAllFragmentsUpToCurrent", ManageMedsFragment.class.getSimpleName());
                    }
                    mListener.onButtonClicked(bundle);
                } else {
                    TextUtils.showToast(getActivity(), "Problem saving info");
                }
            }
        }
        if (v == btnEdit) {
            bundle.putBoolean("isEditMode", isEditMode);
            bundle.putSerializable("user", user);
            bundle.putSerializable("dispensetime", dispenseTime);
            String fragmentName = (isWorkflowChangeMedSchedule()?
                                    "SelectDispensingTimesFragment":
                                    ManageMedsFragment.class.getSimpleName());
            bundle.putString("fragmentName", fragmentName);
            mListener.onButtonClicked(bundle);
        }
    }

    private boolean isWorkflowChangeMedSchedule() {
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        return model.getClass().getSimpleName().equals(ChangeMedSchedule.TAG)?true:false;
    }
}
