package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

import org.w3c.dom.Text;

/**
 * DuplicateMedicationFragment
 *
 * @author alankilloren
 */
public class DuplicateMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "DuplicateMedicationFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private TextView tvMedName;
    private Button btnRefill, btnRemove;

    public DuplicateMedicationFragment() {
        // Required empty public constructor
    }

    public static DuplicateMedicationFragment newInstance() {
        return new DuplicateMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_duplicate_medication, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (medication != null) {
                tvMedName.setText(medication.getMedicationName() + " " + medication.getStrengthMeasurement());
            }
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvMedName = view.findViewById(R.id.textview_medicationName);
        btnRefill = view.findViewById(R.id.button_refill);
        btnRemove = view.findViewById(R.id.button_remove);
        TextUtils.disableButton(btnRefill);
        TextUtils.disableButton(btnRemove);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();

        //TODO - handle other button clicks here
    }
}