package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.DispenseTimesQuantityAdapter;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.utils.SimpleTimeConverter;
import com.logicpd.papapill.utils.CalendarUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.ChangeMedSchedule;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class EnterDailyQuantityPerDoseFragment extends BaseHomeFragment {

    public static final String TAG = "EnterDailyQuantityPerDoseFragment";

    private UserEntity user;
    private TextView tvTitle, tvEmpty;
    private DispenseTimesQuantityAdapter adapter;
    private List<DispenseTimeEntity> dispenseTimeList;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private Button btnWeek, btnMonth, btnDone;
    private MedicationEntity medication;
    private ProgressBar progressBar;
    private boolean isFromSchedule, isEditMode;
    private boolean isFromSetup;

    public EnterDailyQuantityPerDoseFragment() {
        // Required empty public constructor
    }

    public static EnterDailyQuantityPerDoseFragment newInstance() {
        return new EnterDailyQuantityPerDoseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_enter_qty_per_dose, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
            if (bundle.containsKey("isEditMode")) {
                isEditMode = bundle.getBoolean("isEditMode");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.ENTER_DAILY_QUANTITY_PER_DOSE.value,
                WorkflowProgress.AddMedication.values().length);

        dispenseTimeList = model.getDispenseTimes(true);
        adapter = new DispenseTimesQuantityAdapter(getActivity(), dispenseTimeList);

        if (isEditMode) {
            // set the saved quantity for each dispense time
            for (int i = 0; i < dispenseTimeList.size(); i++) {
                DispenseTimeEntity dispenseTime = dispenseTimeList.get(i);
                int id = dispenseTime.getId();
                JoinScheduleDispense scheduleItem = model.getScheduleItemByDispenseTimeId(id);
                if (scheduleItem != null) {
                    int dispenseAmount = scheduleItem.getDispenseAmount();
                    adapter.setDispenseAmount(i, dispenseAmount);
                }
            }
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvTitle = view.findViewById(R.id.textview_title);
        btnWeek = view.findViewById(R.id.button_week_view);
        btnWeek.setOnClickListener(this);
        btnMonth = view.findViewById(R.id.button_month_view);
        btnMonth.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.recyclerview_dispenseTimes_list);
        tvEmpty = view.findViewById(R.id.textview_add_dispenseTime);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnWeek) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", "EnterWeeklyQuantityPerDoseFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnMonth) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putString("fragmentName", "EnterMonthlyQuantityPerDoseFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnDone) {
            new dbTask().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class dbTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            //get latest list (with changes) from adapter
            List<DispenseTimeEntity> dispenseTimes = adapter.getListFromAdapter();
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();

            //delete all scheduled items related to this user->medication
            if (isEditMode) {
                model.deleteScheduleItems(user, medication);
            }

            // create new scheduled items and insert
            for (DispenseTimeEntity dispenseTime : dispenseTimes)
            {
                ScheduleEntity schedule = new ScheduleEntity(user.getId(), medication.getId(), dispenseTime.getId());
                schedule.setDispenseAmount(dispenseTime.getDispenseAmount());

                // Do not commit to database if the dispense amount is 0.
                if (schedule.getDispenseAmount() > 0) {
                    //schedule.setScheduleDate("NULL");
                    //schedule.setScheduleDay("NULL");
                    schedule.setRecurrence(ScheduleRecurrenceEnum.DAILY.value);//daily

                    // First we need to parse and extract out the hours and the minutes values from
                    // dispense time.
                    Date d = SimpleTimeConverter.fromTimestamp(dispenseTime.getDispenseTime());
                    Calendar dt = Calendar.getInstance();
                    dt.setTime(d);

                    // We care about the hours and the minutes so isolate those 2 fields from calendar.
                    int dtHrs = dt.get(Calendar.HOUR_OF_DAY);
                    int dtMin = dt.get(Calendar.MINUTE);

                    // Set the next process date.
                    Calendar nextProcessDate = Calendar.getInstance();
                    CalendarUtils.setCalendarToNextDispenseTime(nextProcessDate, dtHrs, dtMin);

                    schedule.setNextProcessDate(nextProcessDate.getTime());
                    model.addScheduleItem(schedule);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            Bundle bundle = new Bundle();

            // advance to enter medication quantity

            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putBoolean("isEditMode", isEditMode);
            String fragmentName;

            if (isEditMode) {
                if(isWorkflowChangeMedSchedule()) {
                    fragmentName = ChangeMedSchedule.ChangeMedScheduleEnum.ManageMeds.toString();
                }
                else {
                    bundle.putBoolean("updateSchedule", true);
                    fragmentName = "SelectMedLocationFragment";
                }
            }
            else {
                fragmentName = "MedicationQuantityMessageFragment";
            }
            bundle.putString("fragmentName", fragmentName);
            mListener.onButtonClicked(bundle);
        }

        private boolean isWorkflowChangeMedSchedule() {
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            return model.getClass().getSimpleName().equals(ChangeMedSchedule.TAG)?true:false;
        }
    }
}
