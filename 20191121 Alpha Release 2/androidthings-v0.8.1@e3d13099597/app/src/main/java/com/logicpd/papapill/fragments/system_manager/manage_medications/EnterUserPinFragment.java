package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

public class EnterUserPinFragment extends BaseHomeFragment {

    public static final String TAG = "EnterUserPinFragment";

    private LinearLayout contentLayout;
    private Button btnNext;
    private EditText etUserPIN;
    private UserEntity user;

    public EnterUserPinFragment() {
        // Required empty public constructor
    }

    public static EnterUserPinFragment newInstance() {
        return new EnterUserPinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_user_pin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            //etUserPIN.setText(user.getPin());
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etUserPIN = view.findViewById(R.id.edittext_enter_user_pin);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etUserPIN.getText().toString().length() == 4) {
                if (etUserPIN.getText().toString().equals(user.getPin())) {
                    bundle.putSerializable("user", user);
                    bundle.putString("fragmentName", "ImportantMessageFragment");
                    mListener.onButtonClicked(bundle);
                } else {
                    TextUtils.showToast(getActivity(), "Invalid PIN entered");
                }

            } else {
                TextUtils.showToast(getActivity(), "PIN must be 4 numbers");
            }
        }
    }
}
