package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * ImportantMessageFragment
 *
 * @author alankilloren
 */
public class ImportantMessageFragment extends BaseHomeFragment {

    public static final String TAG = "ImportantMessageFragment";

    LinearLayout contentLayout;
    TextView tvMessage;
    Button btnNext;
    UserEntity user;
    private boolean isFromSetup;

    public ImportantMessageFragment() {
        // Required empty public constructor
    }

    public static ImportantMessageFragment newInstance() {
        return new ImportantMessageFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_important_message, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }
        tvMessage.setText("IT IS IMPORTANT THAT INFORMATION IS ENTERED EXACTLY AS IT APPEARS ON THE PRESCRIPTION BOTTLE LABEL." +
                "\n\nENTERING INCORRECT OR INCOMPLETE INFORMATION CAN BE DANGEROUS.");

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.IMPORTANT_MESSAGE.value,
                WorkflowProgress.AddMedication.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
        tvMessage = view.findViewById(R.id.textview_important_message);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putString("fragmentName", "PatientNameFragment");
            bundle.putSerializable("user", user);
        }
        mListener.onButtonClicked(bundle);
    }
}
