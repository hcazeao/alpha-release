package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentManageMedsIncorrectPatientNameBinding;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

public class IncorrectPatientNameFragment extends BaseHomeFragment {
    public static String TAG = "IncorrectPatientNameFragment";
    private FragmentManageMedsIncorrectPatientNameBinding mBinding;
    private RefillMedication mModel;
    public boolean isBtnEnabledSwitchUser = true;

    public IncorrectPatientNameFragment() {

    }

    public static IncorrectPatientNameFragment newInstance() {
        return new IncorrectPatientNameFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_meds_incorrect_patient_name, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        mModel = (RefillMedication) ((MainActivity)getActivity()).getFeatureModel();
        isBtnEnabledSwitchUser = (mModel.numOfUsers()>=2) ? true:false;
        mBinding.invalidateAll();
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
    }

    /*
     * Button switch user is enabled if there are 2 users
     */
    public void onClickSwitchUser() {
        /*
          use this if we want to jump straight to medication list
            UserEntity user = mModel.getOtherUser();
            mModel.addMapItem("user", user);
            gotoFragment(RefillMedication.RefillMedicationEnum.SelectRefillMedication.toString());
        */
        gotoFragment(RefillMedication.RefillMedicationEnum.SelectUserForRefill.toString());
    }

    public void onClickUseCurrentName() {
        gotoFragment(RefillMedication.RefillMedicationEnum.ConfirmRefillMedication.toString());
    }

    public void onClickCancelRefill() {
        gotoFragment(RefillMedication.RefillMedicationEnum.ManageMeds.toString());
    }

    private void gotoFragment(String fragmentName) {
        Bundle bundle = new Bundle();
        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}
