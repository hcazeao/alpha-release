package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.gpio.LockManager;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TimerUtil;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

/**
 * LoadMedicationFragment
 *
 * @author alankilloren
 */
public class LoadMedicationFragment extends BaseHomeFragment implements OnTimerListener {

    public static final String TAG = "LoadMedicationFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private boolean isFromRefill, isFromSchedule;
    private boolean isFromSetup;

    private TimerUtil mTimer;
    Sensor door;

    public LoadMedicationFragment() {
        // Required empty public constructor
    }

    public static LoadMedicationFragment newInstance() {
        return new LoadMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_load_medication, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey("isFromRefill")) {
                isFromRefill = bundle.getBoolean("isFromRefill");
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }

            // Set the mux to the door sensor to begin polling for proximity
            door = new Sensor(BoardDefaults.I2C_PORT,
                    BoardDefaults.I2C_MUX_DOOR_CHANNEL,
                    AppConfig.getInstance().getDoorThresholdHigh());

            // Start a one shot timer to poll door sensor to check if door is closed
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display progress bar according to workflow
        if (isFromSchedule) {
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.LOAD_MEDICATION.value,
                    WorkflowProgress.AddMedication.values().length);
        } else if (isFromRefill) {
            // TBD
        } else {
            // Got here from as-needed, instead of scheduled, however, the progress bar is unchanged.
            // May be able to simplify this in the future if no other workflows are required.
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.LOAD_MEDICATION.value,
                    WorkflowProgress.AddMedication.values().length);
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
    }

    @Override
    public void onTimerFinished() {
        // Read the door proximity sensor.
        int doorProximity = door.getSensorValue();
        Log.d(TAG, "Door Proximity Read: " + doorProximity);

        // Check if door is closed. If so, advance to the next fragment, otherwise, check again
        // after another timer event.
        if (doorProximity != PROXIMITY_ERROR && doorProximity > AppConfig.getInstance().getDoorThresholdHigh()) {
            Log.d(TAG, "Door is closed, going to next screen.");

            // Lock the door immediately.
            LockManager.getInstance().LockDoor();

            Bundle bundle = new Bundle();
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putBoolean("isFromRefill", isFromRefill);
            bundle.putSerializable("medication", medication);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putString("fragmentName", "MedicationFitInstructionsFragment");
            mListener.onButtonClicked(bundle);

            // restart the service
            ((MainActivity)getActivity()).startCarouselPosService(CarouselPosCmdEnum.home,0);
        } else {
            // Door is still open, check again after the next timer elapses.
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
    }
}