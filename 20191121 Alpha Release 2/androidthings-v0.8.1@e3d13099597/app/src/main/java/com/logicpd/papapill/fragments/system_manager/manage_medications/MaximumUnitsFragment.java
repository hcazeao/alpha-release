package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

/**
 * MaximumUnitsFragment
 *
 * @author alankilloren
 */
public class MaximumUnitsFragment extends BaseHomeFragment {

    public static final String TAG = "MaximumUnitsFragment";

    private UserEntity user;
    private Button btnNext;
    private MedicationEntity medication;
    private ImageView btnIncreaseUnitsPerDose, btnDecreaseUnitsPerDose,
            btnIncreaseUnitsPerDay, btnDecreaseUnitsPerDay,
            btnIncreaseHrsBetweenDoses, btnDecreaseHrsBetweenDoses;
    private EditText etMaxUnitsPerDose, etMaxUnitsPerDay, etHrsBetweenDoses;
    private boolean isFromSetup;

    private int currentUnitsPerDose = 0, currentUnitsPerDay = 0, currentHrsBetweenDoses = 0;
    private String mNextFragment = "SelectMedLocationFragment";

    public MaximumUnitsFragment() {
        // Required empty public constructor
    }

    public static MaximumUnitsFragment newInstance() {
        return new MaximumUnitsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_max_units, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable(BundleEnums.user.toString());
            medication = (MedicationEntity) bundle.getSerializable(BundleEnums.medication.toString());

            if(bundle.containsKey(BundleEnums.nextFragmentName.toString()))
                mNextFragment = bundle.getString(BundleEnums.nextFragmentName.toString());
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.MAXIMUM_UNITS.value,
                WorkflowProgress.AddMedication.values().length);

        etMaxUnitsPerDose.setText("0");
        etMaxUnitsPerDay.setText("0");
        etHrsBetweenDoses.setText("0");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etMaxUnitsPerDose = view.findViewById(R.id.edittext_max_units_per_dose);
        etMaxUnitsPerDay = view.findViewById(R.id.edittext_max_units_per_day);
        etHrsBetweenDoses = view.findViewById(R.id.edittext_min_hours_between_doses);
        btnIncreaseUnitsPerDose = view.findViewById(R.id.button_increase_units_per_dose);
        btnIncreaseUnitsPerDose.setOnClickListener(this);
        btnDecreaseUnitsPerDose = view.findViewById(R.id.button_decrease_units_per_dose);
        btnDecreaseUnitsPerDose.setOnClickListener(this);

        btnIncreaseUnitsPerDay = view.findViewById(R.id.button_increase_units_per_day);
        btnIncreaseUnitsPerDay.setOnClickListener(this);
        btnDecreaseUnitsPerDay = view.findViewById(R.id.button_decrease_units_per_day);
        btnDecreaseUnitsPerDay.setOnClickListener(this);

        btnIncreaseHrsBetweenDoses = view.findViewById(R.id.button_increase_hours_between_doses);
        btnIncreaseHrsBetweenDoses.setOnClickListener(this);
        btnDecreaseHrsBetweenDoses = view.findViewById(R.id.button_decrease_hours_between_doses);
        btnDecreaseHrsBetweenDoses.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnIncreaseUnitsPerDose) {
            if (Integer.parseInt(etMaxUnitsPerDose.getText().toString()) >= 0) {
                currentUnitsPerDose = Integer.parseInt(etMaxUnitsPerDose.getText().toString());
                currentUnitsPerDose += 1;
                etMaxUnitsPerDose.setText(String.valueOf(currentUnitsPerDose));
            }
        }
        if (v == btnDecreaseUnitsPerDose) {
            if (Integer.parseInt(etMaxUnitsPerDose.getText().toString()) > 0) {
                currentUnitsPerDose = Integer.parseInt(etMaxUnitsPerDose.getText().toString());
                currentUnitsPerDose -= 1;
                etMaxUnitsPerDose.setText(String.valueOf(currentUnitsPerDose));
            }
        }
        if (v == btnIncreaseUnitsPerDay) {
            if (Integer.parseInt(etMaxUnitsPerDay.getText().toString()) >= 0) {
                currentUnitsPerDay = Integer.parseInt(etMaxUnitsPerDay.getText().toString());
                currentUnitsPerDay += 1;
                etMaxUnitsPerDay.setText(String.valueOf(currentUnitsPerDay));
            }
        }
        if (v == btnDecreaseUnitsPerDay) {
            if (Integer.parseInt(etMaxUnitsPerDay.getText().toString()) > 0) {
                currentUnitsPerDay = Integer.parseInt(etMaxUnitsPerDay.getText().toString());
                currentUnitsPerDay -= 1;
                etMaxUnitsPerDay.setText(String.valueOf(currentUnitsPerDay));
            }
        }
        if (v == btnIncreaseHrsBetweenDoses) {
            if (Integer.parseInt(etHrsBetweenDoses.getText().toString()) >= 0) {
                currentHrsBetweenDoses = Integer.parseInt(etHrsBetweenDoses.getText().toString());
                currentHrsBetweenDoses += 1;
                etHrsBetweenDoses.setText(String.valueOf(currentHrsBetweenDoses));
            }
        }
        if (v == btnDecreaseHrsBetweenDoses) {
            if (Integer.parseInt(etHrsBetweenDoses.getText().toString()) > 0) {
                currentHrsBetweenDoses = Integer.parseInt(etHrsBetweenDoses.getText().toString());
                currentHrsBetweenDoses -= 1;
                etHrsBetweenDoses.setText(String.valueOf(currentHrsBetweenDoses));
            }
        }
        if (v == btnNext) {
            if (Integer.parseInt(etMaxUnitsPerDose.getText().toString()) > 0
                    && Integer.parseInt(etMaxUnitsPerDay.getText().toString()) > 0
                    && Integer.parseInt(etHrsBetweenDoses.getText().toString()) > 0) {

                medication.setMaxUnitsPerDay(Integer.parseInt(etMaxUnitsPerDay.getText().toString()));
                medication.setMaxNumberPerDose(Integer.parseInt(etMaxUnitsPerDose.getText().toString()));
                medication.setTimeBetweenDoses(Integer.parseInt(etHrsBetweenDoses.getText().toString()));

                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putSerializable(BundleEnums.user.toString(), user);
                bundle.putSerializable(BundleEnums.medication.toString(), medication);
                bundle.putString(BundleEnums.fragmentName.toString(), mNextFragment);
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please enter valid values for all items");
            }
        }
    }
}