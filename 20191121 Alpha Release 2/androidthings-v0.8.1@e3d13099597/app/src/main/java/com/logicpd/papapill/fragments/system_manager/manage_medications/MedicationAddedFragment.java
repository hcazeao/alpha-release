package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.utils.PreferenceUtils;

import java.util.List;

/**
 * MedicationAddedFragment
 *
 * @author alankilloren
 */
public class MedicationAddedFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationAddedFragment";

    private Button btnAddMed, btnDone;
    private UserEntity user;
    private MedicationEntity medication;
    private TextView tvMedication;
    private boolean isFromSchedule;
    private boolean isFromSetup;
    private PreferenceUtils prefs;

    public MedicationAddedFragment() {
        // Required empty public constructor
    }

    public static MedicationAddedFragment newInstance() {
        return new MedicationAddedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_medication_added, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            tvMedication.setText(medication.getMedicationName() + " " + medication.getStrengthMeasurement());
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.MEDICATION_ADDED.value,
                WorkflowProgress.AddMedication.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnAddMed = view.findViewById(R.id.button_add_med);
        btnAddMed.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvMedication = view.findViewById(R.id.textview_medicationName);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            if (isFromSchedule) {
                // send something back to MainActivity, telling it we entered a new medication and to update the schedule
                bundle.putBoolean("updateSchedule", true);
            }
            if (isFromSetup) {
                int usersToBeAdded = prefs.getFirstTimeUserCount();
                List<UserEntity> userList = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);

                Log.d(TAG, "Users to be added: " + usersToBeAdded);
                Log.d(TAG, "UserList.size(): " + userList.size());

                if (usersToBeAdded == 2 && userList.size() < usersToBeAdded) {
                    // We have one more user to go.
                    bundle.putString("fragmentName", "SetupUserFragment");
                } else {
                    // For a single user case, we are done with setup.
                    bundle.putString("fragmentName", "SetupCompleteFragment");
                }
            } else {
                bundle.putString("fragmentName", "Home");
            }
            mListener.onButtonClicked(bundle);
        }
        if (v == btnAddMed) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putString("fragmentName", "ImportantMessageFragment");
            bundle.putSerializable("user", user);
            mListener.onButtonClicked(bundle);
        }
    }
}