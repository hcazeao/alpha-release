package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.misc.AppConstants;

/**
 * MedicationFitInstructionsFragment
 *
 * @author alankilloren
 */
public class MedicationFitInstructionsFragment extends BasePlaceMedFragment {

    public static final String TAG = "MedicationFitInstructionsFragment";

    private Button btnNext;
    private boolean isFromRefill, isFromSchedule;
    private boolean isFromSetup;

    public MedicationFitInstructionsFragment() {
        // Required empty public constructor
    }

    public static MedicationFitInstructionsFragment newInstance() {
        return new MedicationFitInstructionsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_medication_fit, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            if (bundle.containsKey("isFromRefill")) {
                isFromRefill = bundle.getBoolean("isFromRefill");
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display progress bar according to workflow
        if (isFromSchedule) {
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.MEDICATION_FIT_INSTRUCTIONS.value,
                    WorkflowProgress.AddMedication.values().length);
        } else if (isFromRefill) {
            // TBD
        } else {
            // Got here from as-needed, instead of scheduled, however, the progress bar is unchanged.
            // May be able to simplify this in the future if no other workflows are required.
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.MEDICATION_FIT_INSTRUCTIONS.value,
                    WorkflowProgress.AddMedication.values().length);
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();

        if (v == btnNext) {
            gotoHappyPath();
        }
    }
}