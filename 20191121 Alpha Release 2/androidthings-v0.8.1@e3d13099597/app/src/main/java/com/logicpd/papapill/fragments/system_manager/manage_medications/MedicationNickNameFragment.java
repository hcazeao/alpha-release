package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

public class MedicationNickNameFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationNickNameFragment";

    private Button btnNext;
    private EditText etMedicationNickName;
    private UserEntity user;
    private MedicationEntity medication;
    private boolean isFromSetup;

    public MedicationNickNameFragment() {
        // Required empty public constructor
    }

    public static MedicationNickNameFragment newInstance() {
        return new MedicationNickNameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_medication_nickname, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (medication!=null){
                etMedicationNickName.setText(medication.getMedicationNickname());
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.MEDICATION_NICKNAME.value,
                WorkflowProgress.AddMedication.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etMedicationNickName = view.findViewById(R.id.edittext_medication_nickname);
        etMedicationNickName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etMedicationNickName.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etMedicationNickName.getText().toString().length() <= 3) {
                TextUtils.showToast(getActivity(), "Please enter a valid medication name");
            } else {
                medication.setUserId(user.getId());
                medication.setMedicationNickname(etMedicationNickName.getText().toString());
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putSerializable("medication", medication);
                bundle.putString("fragmentName", "MedicationStrengthFragment");
                bundle.putSerializable("user", user);
                mListener.onButtonClicked(bundle);
            }
        }
    }
}
