package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.AddNewMedication;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

/**
 * MedicationQuantityFragment
 *
 * @author alankilloren
 */
public class MedicationQuantityFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationQuantityFragment";

    private Button btnNext;
    private TextView tvMedication;
    private EditText etQuantity;
    private MedicationEntity medication;
    private UserEntity user;
    private boolean isFromSchedule;
    private BaseWireframe mModel;
    private boolean isFromSetup;

    public MedicationQuantityFragment() {
        // Required empty public constructor
    }

    public static MedicationQuantityFragment newInstance() {
        return new MedicationQuantityFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_enter_med_quantity, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            // TODO: Update all workflow with models
            medication = (MedicationEntity) bundle.getSerializable("medication");
            user = (UserEntity) bundle.getSerializable("user");

            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display different progress bar depending on workflow
        if (isFromSchedule) {
            // TBD
        } else {
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.MEDICATION_QUANTITY.value,
                    WorkflowProgress.AddMedication.values().length);
        }

        mModel = ((MainActivity)getActivity()).getFeatureModel();
        if(null!=mModel){
            if(null==medication) {
                medication = mModel.getMedication();
            }

            if(null==user) {
                user = mModel.getUser();
            }
        }

        if (medication != null) {
            tvMedication.setText(medication.getMedicationName() + " " + " " + medication.getStrengthMeasurement());

            String hint = String.valueOf(medication.getMedicationQuantity());
            etQuantity.setHint(hint);
            etQuantity.setText("");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        tvMedication = view.findViewById(R.id.textview_medication_dosage);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etQuantity = view.findViewById(R.id.edittext_medication_qty);
        etQuantity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etQuantity.getText().length() > 0 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etQuantity.getText().toString().length() > 0 && Integer.parseInt(etQuantity.getText().toString()) > 0) {
                medication.setMedicationQuantity(Integer.parseInt(etQuantity.getText().toString()));

                String modelName = getModelName();
                switch(modelName) {
                    case RefillMedication.TAG:
                        // Refill is the only one for now
                        String fragmentName = mModel.getNextFragmentName(this.TAG); // TODO: Complete this path setup wizard
                        bundle.putString("fragmentName", fragmentName);
                        break;

                    default:
                    case AddNewMedication.TAG:
                    bundle.putSerializable("user", user);
                    bundle.putSerializable("medication", medication);
                    bundle.putString("fragmentName", SelectUseByDateFragment.TAG);
                    break;
                }

                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putBoolean("isFromSchedule", isFromSchedule);
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please enter a valid quantity");
            }
        }
    }

    private String getModelName() {
        if(null==mModel)
            return "";

        return mModel.getClass().getSimpleName();
    }
}