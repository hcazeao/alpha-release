package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class MedicationQuantityMessageFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationQuantityMessageFragment";

    private TextView tvMedication;
    private Button btnNext;
    private boolean isFromRefill, isFromSchedule;
    private MedicationEntity medication;
    private UserEntity user;
    private boolean isFromSetup;

    public MedicationQuantityMessageFragment() {
        // Required empty public constructor
    }

    public static MedicationQuantityMessageFragment newInstance() {
        return new MedicationQuantityMessageFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_enter_med_quantity_message, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            medication = (MedicationEntity) bundle.getSerializable("medication");
            user = (UserEntity)bundle.getSerializable("user");

            if (bundle.containsKey("isFromRefill")) {
                isFromRefill = bundle.getBoolean("isFromRefill");
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display different progress bar depending on workflow
        if (isFromSchedule) {
            // TBD
        } else if (isFromRefill) {
            // TBD
        } else {
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.MEDICATION_QUANTITY_MESSAGE.value,
                    WorkflowProgress.AddMedication.values().length);
        }

        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        if(null!=model) {
            if(null==medication) {
                medication = model.getMedication();
            }
            if(null==user) {
                user = model.getUser();
            }
        }

        tvMedication.setText("ABOUT HOW MANY " + medication.getMedicationName() + " "
                + medication.getStrengthMeasurement()
                + " DO YOU CURRENTLY HAVE?");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvMedication = view.findViewById(R.id.textview_medication_dosage);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        if (v == btnNext) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putBoolean("isFromRefill", isFromRefill);
            bundle.putBoolean("isFromSchedule", isFromSchedule);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", "MedicationQuantityFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}