package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.RemoveMedication;

/**
 * MedicationRemovedFragment
 *
 * @author alankilloren
 */
public class MedicationRemovedFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationRemovedFragment";

    private Button btnRemove, btnDone;
    private UserEntity user;
    private TextView tvMedication, tvFromSched;
    private Boolean isFromUserDelete;

    public MedicationRemovedFragment() {
        // Required empty public constructor
    }

    public static MedicationRemovedFragment newInstance() {
        return new MedicationRemovedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_medication_removed, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            MedicationEntity medication = (MedicationEntity) bundle.getSerializable("medication");
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
            if (medication != null) {
                String s = medication.getMedicationName() + " " + medication.getStrengthMeasurement();
                tvMedication.setText(s);
            }
            if (user != null) {
                String s = "FROM " + user.getUserName() + "'S SCHEDULE AND THE DEVICE";
                tvFromSched.setText(s);
            }
        }
        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
       super.setupViews(view);

        btnRemove = view.findViewById(R.id.button_remove);
        btnRemove.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvMedication = view.findViewById(R.id.textview_medicationName);
        tvFromSched = view.findViewById(R.id.textview_from_device_schedule);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v == btnDone) {
            onClickDone();
        }
        else if (v == btnRemove) {
            onClickRemove();
        }
    }

    public void onClickDone() {
        Bundle bundle = new Bundle();

        if(isFromUserDelete){
            bundle.putString("fragmentName", "SelectDeleteUserFragment");
        }
        else{
            RemoveMedication model = (RemoveMedication)((MainActivity)getActivity()).getFeatureModel();
            String fragmentName = model.getNextFragmentName(this.TAG);
            bundle.putString("removeAllFragmentsUpToCurrent", fragmentName);
            ((MainActivity)getActivity()).exitFeature();
        }
        mListener.onButtonClicked(bundle);
    }

    public void onClickRemove() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);

        if(isFromUserDelete){
            bundle.putBoolean("isFromUserDelete", isFromUserDelete);
            bundle.putString("removeAllFragmentsUpToCurrent", "SelectRemoveMedicationFragment");
        }
        else{
            bundle.putString("removeAllFragmentsUpToCurrent", "SelectUserForRemoveMedFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}