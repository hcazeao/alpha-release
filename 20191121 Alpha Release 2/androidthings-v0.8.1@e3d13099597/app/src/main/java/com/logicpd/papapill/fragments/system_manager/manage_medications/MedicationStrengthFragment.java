package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.utils.TextUtils;

/**
 * MedicationStrengthFragment
 *
 * @author alankilloren
 */
public class MedicationStrengthFragment extends BaseHomeFragment {

    public static final String TAG = "MedicationStrengthFragment";

    private Button btnNext;
    private UserEntity user;
    private MedicationEntity medication;
    private EditText etStrength;
    private TextView tvMedicationName;
    private boolean isFromSetup;

    public MedicationStrengthFragment() {
        // Required empty public constructor
    }

    public static MedicationStrengthFragment newInstance() {
        return new MedicationStrengthFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_enter_strength, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (medication != null) {
                tvMedicationName.setText(medication.getMedicationName());
                etStrength.setText(medication.getStrengthMeasurement());
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.MEDICATION_STRENGTH.value,
                WorkflowProgress.AddMedication.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvMedicationName = view.findViewById(R.id.textview_medicationName);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etStrength = view.findViewById(R.id.edittext_medication_strength);
        etStrength.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etStrength.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etStrength.getText().toString().length() <= 1) {
                TextUtils.showToast(getActivity(), "Please enter a medication strength");
            } else {
                medication.setStrengthMeasurement(etStrength.getText().toString());
                if (isDuplicateMedicationFound(medication)) {
                    // found duplicate medication, show exception screen
                    bundle.putBoolean("isFromSetup", isFromSetup);
                    bundle.putSerializable("user", user);
                    bundle.putSerializable("medication", medication);
                    bundle.putString("fragmentName", "DuplicateMedicationFragment"); // TODO: Fix this path for setup wizard
                    mListener.onButtonClicked(bundle);
                } else {
                    bundle.putBoolean("isFromSetup", isFromSetup);
                    bundle.putSerializable("user", user);
                    bundle.putSerializable("medication", medication);
                    bundle.putString("fragmentName", "PictureOrManualEntryFragment");
                    mListener.onButtonClicked(bundle);
                }
            }
        }
    }

    private boolean isDuplicateMedicationFound(MedicationEntity medication) {
        return new MedicationRepository().isDuplicateMedicationFound(medication);
    }
}