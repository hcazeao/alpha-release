package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

/**
 * OtherLocationNameFragment
 *
 * @author alankilloren
 */
public class OtherLocationNameFragment extends BaseHomeFragment {

    public static final String TAG = "OtherLocationNameFragment";

    private EditText etName;
    private Button btnOk;
    private MedicationEntity medication;
    private UserEntity user;
    private boolean isFromSchedule, isFromRefill;
    private boolean isFromSetup;

    public OtherLocationNameFragment() {
        // Required empty public constructor
    }

    public static OtherLocationNameFragment newInstance() {
        return new OtherLocationNameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_location_name, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey(BundleEnums.isFromSchedule.toString())) {
                isFromSchedule = bundle.getBoolean(BundleEnums.isFromSchedule.toString());
            }
            if (bundle.containsKey(BundleEnums.isFromRefill.toString())) {
                isFromRefill = bundle.getBoolean(BundleEnums.isFromRefill.toString());
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display progress bar according to workflow
        if (isFromSchedule) {
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.PLACE_MED_X.value,
                    WorkflowProgress.AddMedication.values().length);
        } else if (isFromRefill) {
            // TBD
        } else {
            // Got here from as-needed, instead of scheduled.
            setProgress(PROGRESS_ADD_MEDICATION,
                    WorkflowProgress.AddMedication.PLACE_MED_X.value,
                    WorkflowProgress.AddMedication.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnOk = view.findViewById(R.id.button_ok);
        btnOk.setOnClickListener(this);
        etName = view.findViewById(R.id.edittext_location_name);
        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etName.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnOk.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOk) {
            if (etName.getText().toString().length() > 0) {
                medication.setMedicationLocationName(etName.getText().toString());
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putSerializable("user", user);
                bundle.putSerializable("medication", medication);
                bundle.putString("fragmentName", "PlaceMedOtherFragment");
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please enter a location name");
            }
        }
    }
}