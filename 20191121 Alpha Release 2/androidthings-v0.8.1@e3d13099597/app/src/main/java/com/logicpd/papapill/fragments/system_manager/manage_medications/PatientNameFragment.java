package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.UserRepository;

import java.util.List;

public class PatientNameFragment extends BaseHomeFragment {

    public static final String TAG = "PatientNameFragment";

    private Button btnNext;
    private AutoCompleteTextView etPatientName;
    private UserEntity user;
    private boolean isFromSetup;

    public PatientNameFragment() {
        // Required empty public constructor
    }

    public static PatientNameFragment newInstance() {
        return new PatientNameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_patient_name, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            if (user != null) {
                etPatientName.setText(user.getPatientName());
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.PATIENT_NAME.value,
                WorkflowProgress.AddMedication.values().length);

        //form a list of all patient names in the system for this user
        List<String> patientNames = new MedicationRepository().getPatientNames(user.getId());
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, patientNames);
        etPatientName.setThreshold(1);
        etPatientName.setAdapter(arrayAdapter);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        etPatientName = view.findViewById(R.id.edittext_patientName);
        etPatientName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etPatientName.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etPatientName.getText().toString().length() > 2) {
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("fragmentName", "MedicationNameFragment");
                user.setPatientName(etPatientName.getText().toString());
                MedicationEntity medication = new MedicationEntity(user.getId());
                medication.setPatientName(etPatientName.getText().toString());
                bundle.putSerializable("medication", medication);
                bundle.putSerializable("user", user);
                new UserRepository().update(user);//TODO should I update the db now, or wait until med is added?
                mListener.onButtonClicked(bundle);
            }
        }
    }
}
