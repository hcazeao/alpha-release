package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

import org.w3c.dom.Text;

/**
 * DuplicateMedicationFragment
 *
 * @author alankilloren
 */
public class PictureOrManualEntryFragment extends BaseHomeFragment {

    public static final String TAG = "PictureOrManualEntryFragment";

    private UserEntity user;
    private MedicationEntity medication;
    private Button btnUseCamera, btnKeyManually;
    private boolean isFromSetup;

    public PictureOrManualEntryFragment() {
        // Required empty public constructor
    }

    public static PictureOrManualEntryFragment newInstance() {
        return new PictureOrManualEntryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_picture_or_manual_entry, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.PICTURE_OR_MANUAL_ENTRY.value,
                WorkflowProgress.AddMedication.values().length);

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnUseCamera = view.findViewById(R.id.button_use_camera);
        btnUseCamera.setOnClickListener(this);
        btnKeyManually = view.findViewById(R.id.button_key_manually);
        btnKeyManually.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        bundle.putBoolean("isFromSetup", isFromSetup);
        bundle.putSerializable("user", user);
        bundle.putSerializable("medication", medication);

        if (v == btnUseCamera) {
            bundle.putString("fragmentName", "PictureInstructionsFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnKeyManually) {
            bundle.putString("fragmentName", "MedicationDosageFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}