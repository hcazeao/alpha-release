package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.gpio.LockManager;
import com.logicpd.papapill.device.i2c.Sensor;
import com.logicpd.papapill.device.tinyg.BoardDefaults;
import com.logicpd.papapill.interfaces.OnTimerListener;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.utils.TimerUtil;

import java.util.Timer;

import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_ERROR;
import static com.logicpd.papapill.misc.AppConstants.PROXIMITY_READ_TIMER_MS;

/**
 * PlaceMedDrawerFragment
 *
 * @author alankilloren
 */

public class PlaceMedDrawerFragment extends BasePlaceMedFragment implements OnTimerListener {

    public static final String TAG = "PlaceMedDrawerFragment";

    private Button btnHelp, btnDone;
    private TextView tvTextInfo;

    private boolean isDrawerOpen;
    private Timer drawerTimer;
    private int drawerIsOpenTimerLength = 120000; //ms
    private int drawerIsClosedTimerLength = 60000; //ms

    private TimerUtil mTimer;

    Sensor drawer;

    public PlaceMedDrawerFragment() {
        // Required empty public constructor
    }

    public static PlaceMedDrawerFragment newInstance() {
        return new PlaceMedDrawerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_place_med_drawer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Unlock the drawer lock
        LockManager.getInstance().UnlockDrawer();

        // Set mux to read from drawer sensor.
        drawer = new Sensor(BoardDefaults.I2C_PORT,
                BoardDefaults.I2C_MUX_DRAWER_CHANNEL,
                AppConfig.getInstance().getDrawerThresholdHigh());

        // Start the timer to poll drawer sensor to check if drawer is opened
        if(null==mTimer) {
            mTimer = new TimerUtil(this);
        }
        mTimer.start(PROXIMITY_READ_TIMER_MS);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnHelp = view.findViewById(R.id.button_help);
        btnHelp.setOnClickListener(this);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        TextUtils.disableButton(btnDone);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);

        tvTextInfo = view.findViewById(R.id.text_info);
    }

    @Override
    public void onTimerFinished() {
        // Read the drawer proximity sensor.
        int drawerProximity = drawer.getSensorValue();
        Log.d(TAG, "Drawer Proximity Read: " + drawerProximity);

        // Check if drawer is open.
        if (drawerProximity != PROXIMITY_ERROR && drawerProximity < AppConfig.getInstance().getDrawerThresholdLow()) {
            Log.d(TAG, "Drawer is open, wait for it to close again.");

            // Turn the drawer lock immediately, this will cause it to snap into locked mode
            // when pushed in by the user.
            LockManager.getInstance().LockDrawer();

            TextUtils.enableButton(btnDone);
            tvTextInfo.setText("CLOSE DRAWER WHEN FINISHED");

        } else {
            if(null==mTimer) {
                mTimer = new TimerUtil(this);
            }
            mTimer.start(PROXIMITY_READ_TIMER_MS);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnHelp) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putString("fragmentName", "PlaceMedDrawerHelpFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnDone) {

            int drawerProximity = drawer.getSensorValue();
            Log.d(TAG, "User clicked done. Drawer Proximity Read: " + drawerProximity);

            if (drawerProximity != PROXIMITY_ERROR && drawerProximity > AppConfig.getInstance().getDrawerThresholdHigh()) {

                gotoHappyPath();
            }
        }
    }
}