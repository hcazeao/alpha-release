package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

/**
 * PlaceMedOtherFragment
 *
 * @author alankilloren
 */
public class PlaceMedOtherFragment extends BasePlaceMedFragment {

    public static final String TAG = "PlaceMedOtherFragment";

    private Button btnDone;
    private TextView tvTitle, tvPlaceMed;

    public PlaceMedOtherFragment() {
        // Required empty public constructor
    }

    public static PlaceMedOtherFragment newInstance() {
        return new PlaceMedOtherFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_place_med_other, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvTitle.setText("PLACE MEDICATION IN " + medication.getMedicationLocationName());
        tvPlaceMed.setText("PLACE YOUR MEDICATION IN " + medication.getMedicationLocationName() + " NOW");
    }

    @Override
    protected void setupViews(View view) {
       super.setupViews(view);

        tvTitle = view.findViewById(R.id.textview_title);
        tvPlaceMed = view.findViewById(R.id.textview_place_other_location);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Bundle bundle = new Bundle();
        if (v == btnDone) {
            gotoHappyPath();
        }
    }
}