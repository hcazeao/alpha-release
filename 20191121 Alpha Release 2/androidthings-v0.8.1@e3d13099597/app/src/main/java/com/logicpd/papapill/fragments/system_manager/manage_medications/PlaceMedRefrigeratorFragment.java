package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

/**
 * PlaceMedRefrigeratorFragment
 *
 * @author alankilloren
 */
public class PlaceMedRefrigeratorFragment extends BasePlaceMedFragment {

    public static final String TAG = "PlaceMedRefrigeratorFragment";

    private Button btnDone;

    public PlaceMedRefrigeratorFragment() {
        // Required empty public constructor
    }

    public static PlaceMedRefrigeratorFragment newInstance() {
        return new PlaceMedRefrigeratorFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_place_med_fridge, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnDone) {
            //add medication to db, etc.
           gotoHappyPath();
        }
    }
}