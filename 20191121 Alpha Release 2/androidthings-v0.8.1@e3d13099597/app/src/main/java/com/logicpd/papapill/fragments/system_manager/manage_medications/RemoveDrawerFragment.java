package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

/**
 * RemoveDrawerFragment
 *
 * @author alankilloren
 */
public class RemoveDrawerFragment extends BaseHomeFragment {

    public static final String TAG = "RemoveDrawerFragment";

    private Button btnHelp, btnDeveloper, btnCancel;
    private UserEntity user;
    private MedicationEntity medication;
    private boolean isRemoveMedication, isFromSchedule, isFromUserDelete;
    private boolean isFromSetup;

    public RemoveDrawerFragment() {
        // Required empty public constructor
    }

    public static RemoveDrawerFragment newInstance() {
        return new RemoveDrawerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_remove_drawer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey("isRemoveMedication")) {
                isRemoveMedication = bundle.getBoolean("isRemoveMedication");
            }
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnHelp = view.findViewById(R.id.button_help);
        btnHelp.setOnClickListener(this);
        btnDeveloper = view.findViewById(R.id.button_developer);
        btnDeveloper.setOnClickListener(this);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnHelp) {
            // display help/video for remove drawer
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putBoolean("isRemoveMedication", isRemoveMedication);
            bundle.putString("fragmentName", "RemoveDrawerHelpFragment");
            mListener.onButtonClicked(bundle);
        }
        //TODO this fragment needs to be able to detect the drawer open/close so it can advance to next fragment
        if (v == btnDeveloper) {
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            //remove med from db
            model.removeMedication(medication);
            model.removeMedicationFromSchedule(user, medication);

            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", "MedicationRemovedFragment");
            if (isFromSchedule) {
                bundle.putBoolean("updateSchedule", true);
            }
            bundle.putBoolean("isFromUserDelete", isFromUserDelete);
            mListener.onButtonClicked(bundle);
        }
    }
}