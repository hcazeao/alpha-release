package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

/**
 * RemoveFridgeFragment
 *
 * @author alankilloren
 */
public class RemoveFridgeFragment extends BaseHomeFragment {

    public static final String TAG = "RemoveFridgeFragment";

    private Button btnDone;
    private UserEntity user;
    private MedicationEntity medication;
    private boolean isFromSchedule, isFromUserDelete;

    public RemoveFridgeFragment() {
        // Required empty public constructor
    }

    public static RemoveFridgeFragment newInstance() {
        return new RemoveFridgeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_remove_fridge, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            //remove med from db
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            model.removeMedication(medication);
            model.removeMedicationFromSchedule(user, medication);

            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", "MedicationRemovedFragment");
            if (isFromSchedule) {
                bundle.putBoolean("updateSchedule", true);
            }
            bundle.putBoolean("isFromUserDelete", isFromUserDelete);
            mListener.onButtonClicked(bundle);
        }
    }
}