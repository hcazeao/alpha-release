package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

/**
 * RemoveMedicationFragment
 *
 * @author alankilloren
 */
public class RemoveMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "RemoveMedicationFragment";

    private Button btnDeveloper;
    private UserEntity user;
    private MedicationEntity medication;
    private boolean isFromUserDelete;

    public RemoveMedicationFragment() {
        // Required empty public constructor
    }

    public static RemoveMedicationFragment newInstance() {
        return new RemoveMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_remove_medication, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            medication = (MedicationEntity) bundle.getSerializable("medication");
            isFromUserDelete = bundle.getBoolean("isFromUserDelete");
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDeveloper = view.findViewById(R.id.button_developer);
        btnDeveloper.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        //TODO this fragment needs to be able to detect the door close so it can advance to next fragment
        if (v == btnDeveloper) {
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            model.removeMedication(medication);
            model.removeMedicationFromSchedule(user, medication);

            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", "MedicationRemovedFragment");
            bundle.putBoolean("updateSchedule", true);
            bundle.putBoolean("isFromUserDelete", isFromUserDelete);

            mListener.onButtonClicked(bundle);

            // restart the service
            ((MainActivity)getActivity()).startCarouselPosService(CarouselPosCmdEnum.home,0);
        }
    }
}