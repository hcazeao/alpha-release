package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.MedicationsAdapter;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectChangeMedScheduleFragment extends BaseHomeFragment {

    public static final String TAG = "SelectChangeMedScheduleFragment";

    private Button btnNext;
    private TextView tvTitle, tvEmpty;
    private MedicationsAdapter adapter;
    private List<MedicationEntity> medicationList;
    private UserEntity user;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView recyclerView;

    public SelectChangeMedScheduleFragment() {
        // Required empty public constructor
    }

    public static SelectChangeMedScheduleFragment newInstance() {
        return new SelectChangeMedScheduleFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_list, container, false);
    }

    private MedicationEntity selectedMedication = null;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }
        tvTitle.setText("SELECT A MED SCHEDULE TO CHANGE");

        medicationList = model.getScheduledMedicationsForUser(user, MedPausedEnum.NOT_PAUSED);

        // sort by bin #
        Collections.sort(medicationList, new Comparator<MedicationEntity>() {

            @Override
            public int compare(MedicationEntity m1, MedicationEntity m2) {
                try {
                    return m1.getMedicationLocation() - m2.getMedicationLocation();
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });

        adapter = new MedicationsAdapter(getActivity(), medicationList, true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new MedicationsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                selectedMedication = medicationList.get(position);
                btnNext.setEnabled(true);
            }
        });

        if (medicationList.size() > 0) {
            tvEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.GONE);
            tvEmpty.setText("NO SCHEDULED MEDICATIONS FOUND");
            tvEmpty.setVisibility(View.VISIBLE);
            btnNext.setText("OK");
        }
        btnNext.setVisibility(View.VISIBLE);
        btnNext.setEnabled(false);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        btnNext.setVisibility(View.GONE);
        recyclerView = view.findViewById(R.id.recyclerview_list);
        tvTitle = view.findViewById(R.id.textview_title);
        tvEmpty = view.findViewById(R.id.textview_empty);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            bundle.putBoolean("isEditMode", true);
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            List<ScheduleEntity> scheduleItems = model.getScheduleItemsByMedication(selectedMedication);
            if (scheduleItems.size() >= 1) {//this is a daily schedule
                //TODO
                bundle.putString("fragmentName", "SelectDispensingTimesFragment");
            }

            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", selectedMedication);
            mListener.onButtonClicked(bundle);
        }
    }
}
