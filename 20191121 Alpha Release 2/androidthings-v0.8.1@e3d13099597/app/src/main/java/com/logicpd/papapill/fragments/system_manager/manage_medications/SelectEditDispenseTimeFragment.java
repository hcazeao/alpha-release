package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.DispenseTimesAdapter;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.BundleFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * SelectEditDispenseTimeFragment
 *
 * @author alankilloren
 */
public class SelectEditDispenseTimeFragment extends BaseHomeFragment {

    public static final String TAG = "SelectEditDispenseTimeFragment";

    private UserEntity user;
    private List<DispenseTimeEntity> dispenseTimeList;
    private RecyclerView recyclerView;
    private boolean isFromSchedule;

    public SelectEditDispenseTimeFragment() {
        // Required empty public constructor
    }

    public static SelectEditDispenseTimeFragment newInstance() {
        return new SelectEditDispenseTimeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_select_edit_dispense_time, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            if (bundle.containsKey("isFromSchedule")) {
                isFromSchedule = bundle.getBoolean("isFromSchedule");
            }
        }

        dispenseTimeList = model.getDispenseTimes(false);
        //sort list by time
        Collections.sort(dispenseTimeList, new Comparator<DispenseTimeEntity>() {
            DateFormat f = new SimpleDateFormat("h:mm a", Locale.getDefault());

            @Override
            public int compare(DispenseTimeEntity o1, DispenseTimeEntity o2) {
                try {
                    return f.parse(o1.getDispenseTime()).compareTo(f.parse(o2.getDispenseTime()));
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        DispenseTimesAdapter adapter = new DispenseTimesAdapter(getActivity(), dispenseTimeList, false, false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new DispenseTimesAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                DispenseTimeEntity dispenseTime = dispenseTimeList.get(position);
                Bundle bundle = new Bundle();

                bundle.putSerializable("user", user);
                bundle.putSerializable("dispensetime", dispenseTime);
                bundle.putString("fragmentName", "DispenseTimeNameFragment");
                bundle.putBoolean("isEditMode", true);
                bundle.putBoolean("isFromSchedule", isFromSchedule);
                mListener.onButtonClicked(bundle);
            }
        });
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        Button btnNext = view.findViewById(R.id.button_next);
        btnNext.setVisibility(View.GONE);
        recyclerView = view.findViewById(R.id.recyclerview_dispenseTimes_list);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

    }
}
