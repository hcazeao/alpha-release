package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.MedicationsAdapter;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.utils.MonthDayYearConverter;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.room.utils.TimeUtil;
import com.logicpd.papapill.wireframes.workflows.RefillMedication;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectRefillMedicationFragment extends BaseHomeFragment {

    public static final String TAG = "SelectRefillMedicationFragment";

    private TextView tvTitle, tvEmpty;
    private Button btnNext;
    private MedicationsAdapter adapter;
    private List<MedicationEntity> medicationList;
    private RefillMedication mModel;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private final int INIT_POSITION = -1;
    private int lastPosition = INIT_POSITION;

    public SelectRefillMedicationFragment() {
        // Required empty public constructor
    }

    public static SelectRefillMedicationFragment newInstance() {
        return new SelectRefillMedicationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_meds_medication_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        tvTitle.setText("SELECT MEDICATION TO REFILL");

        mModel = (RefillMedication)((MainActivity)getActivity()).getFeatureModel();
        medicationList = mModel.getMedications4User();

        adapter = new MedicationsAdapter(getActivity(), medicationList, true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);

        TextUtils.disableButton(btnNext);

        adapter.setOnItemClickListener(new MedicationsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                /*
                 * TODO: clunky, should de-select inside adapter code (for now, least intrusive change)
                 * 1) de-select last
                 * 2) store new medication selected in model
                 * 3) enable button 'next'
                 */
                if(lastPosition != INIT_POSITION){
                    adapter.setCheck(lastPosition, false);
                }
                MedicationEntity med = medicationList.get(position);
                mModel.addMapItem("medication", med);

                TextUtils.enableButton(btnNext);
                lastPosition = position;
            }
        });

        if (medicationList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvTitle = view.findViewById(R.id.textview_title);
        recyclerView = view.findViewById(R.id.recyclerview_list);
        tvEmpty = view.findViewById(R.id.textview_empty);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if(v==btnNext)
            onClickNext();
    }

    /*        Software can deny a refill request if either of these two conditions are present:
            •	Refill Service Pause - Monthly Service Payment Not Received
            •	Existing Bin Still has Pills - Less than 90 days from Expiring

        These conditions are refined below.
        6.5.1.4.2.3.1 Refill Service Paused – Monthly Service Payment Not Received
        PAPA-992 - Refill Service Paused – Monthly Service Payment Not Received
                [Must Have]
        The Refill Medication software shall deny a refill from being entered if the device does not have a valid certificate. Software shall display a pop-up that tells the user they must pay the monthly service charge before any refills will be allowed into the device.
        This feature still allows existing medications to be dispensed, but won’t allow any refills or new medications until the service plan is paid up.
        6.5.1.4.2.3.2 Existing Bin Still has Pills - Less than 90 days from Expiring
        PAPA-990 - Existing Bin Still has Pills – Less than 90 days from Expiring
                [Must Have]
        The Refill Medication software shall deny a refill from being entered if targeted storage bin is not empty and the medication in the bin is less than 90 days from expiring. Software shall display a pop-up that tells the user they must use up the existing medication and empty the bin before the bin can be refilled.
                This feature protects from the operator dumping a 90-day supply of pills into a bin that already has pills in it that will expire in less than 90 days. The system has no way to guarantee the older pills will be used first.
        */
    public void onClickNext() {
        Bundle bundle = new Bundle();
        /*
         * check for certificate after Alpha
         */
        String fragmentName = isExpiredIn90Days() ?
                                SelectBinNotRefillMedFragment.class.getSimpleName():
                                ConfirmPatientNameFragment.class.getSimpleName();

        bundle.putString("fragmentName", fragmentName);
        mListener.onButtonClicked(bundle);
    }

    /*
     * Is the medication useByDate expiring in 90 days ?
     * Return: worst case, return false and let user do as they wish.
     */
    private boolean isExpiredIn90Days() {
        final int NINETY_DAYS = this.getResources().getInteger(R.integer.ninetyDays);

        try {

            String useByDate = mModel.getMedication().getUseByDate();
            Date dateUseBy = MonthDayYearConverter.fromString(useByDate);
            Calendar calendarUseBy = Calendar.getInstance();
            calendarUseBy.setTime(dateUseBy);

            Calendar calendar90Days = Calendar.getInstance();
            calendar90Days.setTime(new Date());
            calendar90Days.add(Calendar.DAY_OF_YEAR, NINETY_DAYS);

            return calendar90Days.compareTo(calendarUseBy) > 0 ? true : false;
        }
        catch (Exception ex) {
            return false;
        }
    }
}
