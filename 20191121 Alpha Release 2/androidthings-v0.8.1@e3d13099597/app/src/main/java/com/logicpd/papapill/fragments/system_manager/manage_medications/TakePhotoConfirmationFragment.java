package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.computervision.ImageUtility;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

public class TakePhotoConfirmationFragment extends BaseHomeFragment {

    public static final String TAG = "TakePhotoConfirmationFragment";

    protected LinearLayout contentLayout;
    protected Button btnKeepPhoto, btnRetakePhoto;
    protected TextView tvTitle, tvMedication, txtUserName;

    private ImageView mImageView;

    private MedicationEntity medication;
    private UserEntity user;
    private boolean isFromSetup;

    private byte[] imageBytes;
    Handler h;

    public TakePhotoConfirmationFragment() {
    }

    public static TakePhotoConfirmationFragment newInstance() {
        return new TakePhotoConfirmationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_meds_take_photo_confirmation, container, false);
    }

    protected void setupViews(View view) {
        super.setupViews(view);

        mImageView = view.findViewById(R.id.camera_photo);
        txtUserName = view.findViewById(R.id.textview_username);

        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            mImageView.getLayoutParams().height = this.getResources().getInteger(R.integer.imageHeight);
            mImageView.getLayoutParams().width = this.getResources().getInteger(R.integer.imageWidth);
        }

        btnKeepPhoto = view.findViewById(R.id.button_keep_photo);
        btnKeepPhoto.setOnClickListener(this);
        btnRetakePhoto = view.findViewById(R.id.button_retake_photo);
        btnRetakePhoto.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        h = new Handler();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            medication = (MedicationEntity) bundle.getSerializable("medication");
            user = (UserEntity)bundle.getSerializable("user");
            imageBytes = bundle.getByteArray("imageBytes");

            if (imageBytes != null) {
                // Display the photo as image view.
                final Bitmap b = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                mImageView.setImageBitmap(b);
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.TAKE_PHOTO_CONFIRMATION.value,
                WorkflowProgress.AddMedication.values().length);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnKeepPhoto) {
            // Save the image to the SD card and add to medication.
            Bitmap bmapImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

            String labelFileName = "Labels/" + medication.getId() + "_" + user.getId() + "_" + System.currentTimeMillis();

            ImageUtility.initializeLabelDirectory();
            ImageUtility.storeImage(bmapImage, labelFileName);
            medication.setMedicationLabel(labelFileName);

            // Advance to the next screen.
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putString("fragmentName", "MedicationDosageFragment");
            mListener.onButtonClicked(bundle);
        }

        if (v == btnRetakePhoto) {
            backButton.performClick();
        }
    }
}
