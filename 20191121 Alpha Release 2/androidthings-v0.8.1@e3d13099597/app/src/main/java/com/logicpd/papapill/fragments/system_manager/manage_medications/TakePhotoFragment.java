package com.logicpd.papapill.fragments.system_manager.manage_medications;

import android.graphics.SurfaceTexture;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.computervision.FrontCamera;
import com.logicpd.papapill.computervision.detection.DetectorException;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.nio.ByteBuffer;

public class TakePhotoFragment extends BaseHomeFragment {

    public static final String TAG = "TakePhotoFragment";

    protected LinearLayout contentLayout;
    protected Button btnTakePhoto;
    protected TextView tvTitle, tvMedication, txtUserName;

    private FrontCamera mCamera = null;
    private Handler mCameraHandler;
    private HandlerThread mCameraThread;
    private TextureView mTextureView;

    private MedicationEntity medication;
    private UserEntity user;
    private boolean isFromSetup;

    private byte[] imageBytes;
    Handler h;

    public TakePhotoFragment() {
    }

    public static TakePhotoFragment newInstance() {
        return new TakePhotoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        createCameraThread();
        return inflater.inflate(R.layout.fragment_manage_meds_take_photo, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
        initCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView()");
        if(null!=mCamera) {
            mCamera.shutDown();
        }

        if(null!=mCameraThread) {
            mCameraThread.quit();
            mCameraHandler.removeCallbacksAndMessages(null);
            mCameraHandler = null;
            mCameraThread = null;
        }
    }

    private void createCameraThread()
    {
        //Setup Camera Thread
        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            mCameraThread = new HandlerThread("CameraBackground");
            mCameraThread.start();
            mCameraHandler = new Handler(mCameraThread.getLooper());
        }
    }

    private void initCamera()
    {
        //check to see if this build has camera enabled
        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            try {
                mCamera = new FrontCamera();
                mCamera.initializeCamera(getActivity(), mCameraHandler, mOnImageAvailableListener, mTextureView);
            } catch (DetectorException de) {
                Log.e(TAG, "Error in Camera Initialization", de);
            }
        }
    }

    private void takePhoto()
    {
        if(null!=mCamera) {
            mCamera.takePicture();
            Log.d(TAG, "takePhoto() done");
            return;
        }
        Log.e(TAG, "takePhoto() camera not available");
    }

    protected void setupViews(View view) {
        super.setupViews(view);

        mTextureView = view.findViewById(R.id.camera_preview);
        txtUserName = view.findViewById(R.id.textview_username);

        if (com.logicpd.papapill.data.AppConfig.getInstance().isCameraAvailable) {
            mTextureView.setSurfaceTextureListener(surfaceTextureListener);
            mTextureView.getLayoutParams().height = this.getResources().getInteger(R.integer.imageHeight);
            mTextureView.getLayoutParams().width = this.getResources().getInteger(R.integer.imageWidth);
        }

        btnTakePhoto = view.findViewById(R.id.button_take_photo);
        btnTakePhoto.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        h = new Handler();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            medication = (MedicationEntity) bundle.getSerializable("medication");
            user = (UserEntity)bundle.getSerializable("user");
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_MEDICATION,
                WorkflowProgress.AddMedication.TAKE_PHOTO.value,
                WorkflowProgress.AddMedication.values().length);
    }

    /*
     * The listener for when the camera has taken a picture.  Once the picture is taken, it will send
     * the image to the processor to identify meds and their locations.
     */
    private ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image image = reader.acquireLatestImage();
            Log.d("TAG", "acquireLatestImage()");

            //get the bytes
            ByteBuffer imageBuf = image.getPlanes()[0].getBuffer();
            imageBytes = new byte[imageBuf.remaining()];
            imageBuf.get(imageBytes);
            image.close();
            Log.d("TAG", "got the bytes()");

            // Advance to the next screen.
            Bundle bundle = new Bundle();
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putSerializable("medication", medication);
            bundle.putByteArray("imageBytes", imageBytes);
            bundle.putString("fragmentName", "TakePhotoConfirmationFragment");
            mListener.onButtonClicked(bundle);
        }
    };

    private TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {

        /*The surface texture is available, so this is where we will create and open the camera, as
        well as create the request to start the camera preview.
         */
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            mCamera.setSurface(surface);
            mCamera.startVideo(mTextureView);
            btnTakePhoto.setVisibility(View.VISIBLE);
            Log.d(TAG, "onSurfaceTextureAvailable");
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            Log.d(TAG, "onSurfaceTextureSizeChanged");
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            Log.d(TAG, "onSurfaceTextureDestroyed()");
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            Log.d(TAG, "onSurfaceTextureUpdated");
        }
    };

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnTakePhoto) {
            btnTakePhoto.setEnabled(false); // prevent double clicking
            btnTakePhoto.setText("PLEASE HOLD LABEL STEADY...");
            btnTakePhoto.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
            takePhoto();
        }
    }
}
