package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

public class AddUserPinFragment extends BaseHomeFragment {

    public static final String TAG = "AddUserPinFragment";

    private Button btnNext;
    private EditText etUserPIN;
    private UserEntity user;
    private boolean isFromAddNewUser;
    private boolean isFromSetup;

    public AddUserPinFragment() {
        // Required empty public constructor
    }

    public static AddUserPinFragment newInstance() {
        return new AddUserPinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_add_user_pin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            etUserPIN.setText(user.getPin());
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            if (bundle.containsKey("isFromAddNewUser")) {
                isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            }
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        setProgress(PROGRESS_ADD_USER,
                WorkflowProgress.AddUser.USER_PIN.value,
                WorkflowProgress.AddUser.values().length);
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        TextUtils.disableButton(btnNext);
        etUserPIN = view.findViewById(R.id.edittext_add_user_pin);
        etUserPIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.d(AppConstants.TAG, "in beforeTextChanged()");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.i(AppConstants.TAG, "in onTextChanged() - " + start);
                if (start > 2) {
                    TextUtils.enableButton(btnNext);
                } else {
                    TextUtils.disableButton(btnNext);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.d(AppConstants.TAG, "in afterTextChanged()");
            }
        });

        etUserPIN.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etUserPIN.getText().length() > 3 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etUserPIN.getText().toString().length() == 4) {
                user.setPin(etUserPIN.getText().toString());
                bundle.putSerializable("user", user);
                bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
                bundle.putString("fragmentName", "SelectContactPinRecoveryFragment");
                bundle.putBoolean("isFromSetup", isFromSetup);
                mListener.onButtonClicked(bundle);
            }
            etUserPIN.setText("");
        }
    }
}
