package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.UserRepository;

import java.util.List;

public class BaseSelectEditUser extends BaseHomeFragment {
    protected LinearLayout contentLayout;
    protected Button btnUserA, btnUserB;
    protected List<UserEntity> userList;

    protected void enableUserSelection() {

        userList = (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);
        if (userList.size() > 0) {
            btnUserA.setText(userList.get(0).getUserName());
            if (userList.size() == 2) {
                btnUserB.setText(userList.get(1).getUserName());
            }
        }

        if (userList.size() == 1) {
            btnUserA.setVisibility(View.VISIBLE);
            btnUserB.setVisibility(View.GONE);
        } else if (userList.size() == 2) {
            btnUserA.setVisibility(View.VISIBLE);
            btnUserB.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        contentLayout = view.findViewById(R.id.layout_content);

        btnUserA = view.findViewById(R.id.button_select_user_a);
        btnUserA.setOnClickListener(this);
        btnUserB = view.findViewById(R.id.button_select_user_b);
        btnUserB.setOnClickListener(this);
    }
}
