package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentManageUsersCannotDeleteContactBinding;
import com.logicpd.papapill.wireframes.workflows.DeleteContact;

public class CannotDeleteContactFragment extends BaseHomeFragment {
    public static final String TAG = "CannotDeleteContactFragment";
    private FragmentManageUsersCannotDeleteContactBinding mBinding;

    public CannotDeleteContactFragment() {}

    public CannotDeleteContactFragment newInstance() {
        return new CannotDeleteContactFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_users_cannot_delete_contact, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
    }

    public void onClickCancel() {
        backButton.performClick();
    }

    public void onClickEdit() {
        Bundle bundle = this.getArguments();
        bundle.putString("fragmentName", DeleteContact.DeleteContactEnum.SelectUserEditNotifications.toString());
        mListener.onButtonClicked(bundle);
    }
}
