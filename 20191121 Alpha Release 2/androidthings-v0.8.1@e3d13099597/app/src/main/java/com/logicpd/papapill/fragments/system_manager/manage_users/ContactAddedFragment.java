package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;

public class ContactAddedFragment extends BaseHomeFragment {
    public static String TAG = "ContactAddedFragment";

    private LinearLayout contentLayout;
    private TextView tvContactName;
    private Button btnDone;
    private ContactEntity contact;
    private boolean isEditMode;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private boolean isFromChangePIN = false;
    private boolean isFromSetup = false;
    private String systemKey;
    private UserEntity user;

    public ContactAddedFragment() {
        // Required empty public constructor
    }

    public static ContactAddedFragment newInstance() {
        return new ContactAddedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_added, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        backButton.setVisibility(View.GONE);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
                systemKey = bundle.getString("system_key");
            }
            if (bundle.containsKey("isEditMode")) {
                if (bundle.getBoolean("isEditMode")) {
                    isEditMode = true;
                }
            }
            if (bundle.containsKey("isFromAddNewUser")) {
                if (bundle.getBoolean("isFromAddNewUser")) {
                    isFromAddNewUser = true;
                }
            }
            isFromNotifications = bundle.getBoolean("isFromNotifications");
            isFromChangePIN = bundle.getBoolean("isFromChangePIN");
            user = (UserEntity) bundle.getSerializable("user");
            contact = (ContactEntity) bundle.getSerializable("contact");
            tvContactName.setText(contact.getName());
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display a different progress bar depending on the workflow
        if (isFromAddNewUser && !isFromNotifications) {
            setProgress(PROGRESS_ADD_USER,
                    WorkflowProgress.AddUser.CONTACT_ADDED.value,
                    WorkflowProgress.AddUser.values().length);
        } else if (isFromChangePIN) {
            // TBD
        } else if (isFromNotifications) {
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.CONTACT_ADDED.value,
                    WorkflowProgress.SetNotifications.values().length);
        } else if (isFromSetup) {
            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.CONTACT_ADDED.value,
                    WorkflowProgress.SetupWizard.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        tvContactName = view.findViewById(R.id.textview_contact_name);
        btnDone = view.findViewById(R.id.button_ok);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);

            if (isFromAddNewUser && !isFromNotifications || isFromChangePIN) {
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("fragmentName", "SelectContactPinRecoveryFragment");
            } else if (isFromNotifications) {
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("fragmentName", "NotificationContactsFragment");
            } else {
                if (isFromSetup) {
                    bundle.putBoolean("isFromSetup", true);
                    bundle.putSerializable("contact", contact);
                    bundle.putString("system_key", systemKey);
                    bundle.putString("fragmentName", "VerifySystemKeyFragment");
                } else {
                    bundle.putString("fragmentName", "ContactListFragment");
                }
            }
        }
        mListener.onButtonClicked(bundle);
    }
}
