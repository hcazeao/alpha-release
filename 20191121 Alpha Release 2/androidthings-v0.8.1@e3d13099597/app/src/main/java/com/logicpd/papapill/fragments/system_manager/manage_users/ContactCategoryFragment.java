package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class ContactCategoryFragment extends BaseHomeFragment {

    public static final String TAG = "ContactCategoryFragment";

    private Button btnMyself, btnFamily, btnCaregiver, btnNext;
    private UserEntity user;
    private TextView tvContactName;
    private ContactEntity contact;
    private boolean isFromAddNewUser;
    private boolean isFromSetup;

    public ContactCategoryFragment() {
        // Required empty public constructor
    }

    public static ContactCategoryFragment newInstance() {
        return new ContactCategoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_category, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            contact = (ContactEntity) bundle.getSerializable("contact");
            switch (contact.getRelationship()) {
                case 1://myself
                    btnMyself.setSelected(true);
                    btnCaregiver.setSelected(false);
                    btnFamily.setSelected(false);
                    break;
                case 2://family member
                    btnMyself.setSelected(false);
                    btnCaregiver.setSelected(false);
                    btnFamily.setSelected(true);
                    break;
                case 3://caregiver
                    btnMyself.setSelected(false);
                    btnCaregiver.setSelected(true);
                    btnFamily.setSelected(false);
                    break;
            }
            tvContactName.setText(contact.getName());
            if (bundle.containsKey("isFromAddNewUser")) {
                if (bundle.getBoolean("isFromAddNewUser")) {
                    isFromAddNewUser = true;
                }
            }
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.CONTACT_CATEGORY.value,
                    WorkflowProgress.SetNotifications.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvContactName = view.findViewById(R.id.textview_contact_name);
        btnMyself = view.findViewById(R.id.button_myself);
        btnMyself.setOnClickListener(this);
        btnFamily = view.findViewById(R.id.button_family);
        btnFamily.setOnClickListener(this);
        btnCaregiver = view.findViewById(R.id.button_caregiver);
        btnCaregiver.setOnClickListener(this);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnMyself) {
            contact.setRelationship(1);
            btnMyself.setSelected(true);
            btnCaregiver.setSelected(false);
            btnFamily.setSelected(false);
        }
        if (v == btnFamily) {
            contact.setRelationship(2);
            btnMyself.setSelected(false);
            btnCaregiver.setSelected(false);
            btnFamily.setSelected(true);
        }
        if (v == btnCaregiver) {
            contact.setRelationship(3);
            btnMyself.setSelected(false);
            btnCaregiver.setSelected(true);
            btnFamily.setSelected(false);
        }
        if (v == btnNext) {
            if (contact.getRelationship() > 0) {
                bundle.putSerializable("contact", contact);
                bundle.putSerializable("user", user);
                bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("fragmentName", "NotificationSettingsMessageFragment");
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please select a category");
            }
        }
    }
}
