package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.UserRepository;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DeleteContact;
import com.logicpd.papapill.wireframes.workflows.EditContact;
import com.logicpd.papapill.wireframes.workflows.ManageContacts;

import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class ContactInfoFragment extends BaseHomeFragment {

    public static final String TAG = "ContactInfoFragment";

    private TextView tvSummary, tvTitle;
    private Button btnEdit, btnNext, btnDelete;
    private ContactEntity contact;

    private boolean isEditMode;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private ManageContacts mModel;

    public ContactInfoFragment() {
        // Required empty public constructor
    }

    public static ContactInfoFragment newInstance() {
        return new ContactInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_verify_contact_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isEditMode")) {
                if (bundle.getBoolean("isEditMode")) {
                    isEditMode = true;
                }
            }
            isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            isFromNotifications = bundle.getBoolean("isFromNotifications");
            contact = (ContactEntity) bundle.getSerializable("contact");
            StringBuilder sb = new StringBuilder();
            sb.append(getString(R.string.name)+" ").append(contact.getName());
            if (contact.getTextNumber() != null && !contact.getTextNumber().isEmpty()) {
                sb.append(getString(R.string.text_message)+" ").append(contact.getTextNumber());
            }
            if (contact.getVoiceNumber() != null && !contact.getVoiceNumber().isEmpty()) {
                sb.append(getString(R.string.voice_message)+" ").append(contact.getVoiceNumber());
            }
            if (contact.getEmail() != null && !contact.getEmail().isEmpty()) {
                sb.append(getString(R.string.email)+" ").append(contact.getEmail());
            }
            tvSummary.setText(sb.toString());

            mModel = (ManageContacts)((MainActivity)getActivity()).getFeatureModel(ManageContacts.TAG);
            if(mModel.isRecoveryContactId(contact.getId())) {
                //This is to avoiding to delete User’s Recover Pin contact
                // cannot delete
                TextUtils.disableButton(btnDelete);
            }
        }

        tvTitle.setText(getString(R.string.verify_contact_info));
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnEdit = view.findViewById(R.id.button_edit_contact_info);
        btnEdit.setOnClickListener(this);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        tvSummary = view.findViewById(R.id.textview_verify_contact_info);
        tvTitle = view.findViewById(R.id.textview_title);
        btnDelete = view.findViewById(R.id.button_delete_contact);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        String fragmentName = null;

        if (v == btnNext) {
            // update
            new ContactRepository().update(contact);
            bundle.putBoolean("isEditMode", isEditMode);
            fragmentName = "ContactUpdatedFragment";
        }
        else if (v == btnEdit) {
            bundle.putBoolean("isEditMode", true);
            fragmentName = EditContact.EditContactEnum.ContactName.toString();
        }

        else if (v == btnDelete) {
            //assume we are NOT dealing with User’s Recovery Pin contact here
            fragmentName = (mModel.hasNotifications4Contact(contact.getId())) ?
                            DeleteContact.DeleteContactEnum.CannotDeleteContact.toString():
                            DeleteContact.DeleteContactEnum.ConfirmDeleteContact.toString();

            ((MainActivity)getActivity()).enterFeature(DeleteContact.class.getSimpleName());
            BaseWireframe childModel = ((MainActivity)getActivity()).getFeatureModel();
            childModel.addMapItem("contact", contact);
        }

        if(null!=fragmentName) {
            bundle.putSerializable("contact", contact);
            bundle.putString("fragmentName", fragmentName);
            mListener.onButtonClicked(bundle);
        }
    }
}