package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.BaseWireframe;

public class ContactUpdatedFragment extends BaseHomeFragment {
    public static final String TAG = "ContactUpdatedFragment";
    private TextView tvContactName;
    private Button btnDone;
    private ContactEntity contact;
    private boolean isEditMode;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private boolean isFromChangePIN = false;
    private UserEntity user;

    public ContactUpdatedFragment() {
        // Required empty public constructor
    }

    public static ContactUpdatedFragment newInstance() {
        return new ContactUpdatedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_updated, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        backButton.setVisibility(View.GONE);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isEditMode")) {
                if (bundle.getBoolean("isEditMode")) {
                    isEditMode = true;
                }
            }
            if (bundle.containsKey("isFromAddNewUser")) {
                if (bundle.getBoolean("isFromAddNewUser")) {
                    isFromAddNewUser = true;
                }
            }
            isFromNotifications = bundle.getBoolean("isFromNotifications");
            isFromChangePIN = bundle.getBoolean("isFromChangePIN");
            user = (UserEntity) bundle.getSerializable("user");
            contact = (ContactEntity) bundle.getSerializable("contact");
            tvContactName.setText(contact.getName());
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        tvContactName = view.findViewById(R.id.textview_contact_name);
        btnDone = view.findViewById(R.id.button_ok);
        btnDone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        String fragmentName=null;

        if (v == btnDone) {
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);

            if (isFromAddNewUser && !isFromNotifications || isFromChangePIN) {
                fragmentName = "SelectContactPinRecoveryFragment";
            } else if (isFromNotifications) {
                fragmentName = "NotificationContactsFragment";
            } else {
                fragmentName = ManageUsersFragment.TAG;
            }
        }
        bundle.putString("removeAllFragmentsUpToCurrent", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}
