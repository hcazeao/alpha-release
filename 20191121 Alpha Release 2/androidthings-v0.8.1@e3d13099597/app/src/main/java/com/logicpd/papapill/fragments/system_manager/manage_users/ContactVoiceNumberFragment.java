package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.utils.TextUtils;

public class ContactVoiceNumberFragment extends BaseHomeFragment {

    public static final String TAG = "ContactVoiceNumberFragment";

    private EditText etVoiceNumber;
    private Button btnNext;
    private ContactEntity contact;
    private boolean isEditMode;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private boolean isFromNotificationSettingsAdapter = false;
    private boolean isFromChangePIN = false;
    private boolean isFromSetup = false;
    private String systemKey;
    private UserEntity user;

    public ContactVoiceNumberFragment() {
        // Required empty public constructor
    }

    public static ContactVoiceNumberFragment newInstance() {
        return new ContactVoiceNumberFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_voice_number, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
                systemKey = bundle.getString("system_key");
            }
            if (bundle.containsKey("isEditMode")) {
                if (bundle.getBoolean("isEditMode")) {
                    isEditMode = true;
                }
            }
            user = (UserEntity) bundle.getSerializable("user");
            contact = (ContactEntity) bundle.getSerializable("contact");
            etVoiceNumber.setText(contact.getVoiceNumber());
            isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            isFromNotifications = bundle.getBoolean("isFromNotifications");
            isFromChangePIN = bundle.getBoolean("isFromChangePIN");
            isFromNotificationSettingsAdapter = bundle.getBoolean("isFromNotificationSettingsAdapter");
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display a different progress bar depending on the workflow
        if (isFromAddNewUser && !isFromNotifications) {
            setProgress(PROGRESS_ADD_USER,
                    WorkflowProgress.AddUser.CONTACT_VOICE_NUMBER.value,
                    WorkflowProgress.AddUser.values().length);
        } else if (isFromChangePIN) {
            // TBD
        } else if (isFromNotifications) {
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.CONTACT_VOICE_NUMBER.value,
                    WorkflowProgress.SetNotifications.values().length);
        } else if (isFromSetup) {
            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.CONTACT_VOICE_NUMBER.value,
                    WorkflowProgress.SetupWizard.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        etVoiceNumber = view.findViewById(R.id.edittext_contact_voice_number);
        etVoiceNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etVoiceNumber.getText().length() > 6 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            if (etVoiceNumber.getText().toString().length() <= 6) {
                TextUtils.showToast(getActivity(), "Please enter a valid number");
            } else {

                if (isFromSetup) {
                    bundle.putBoolean("isFromSetup", true);
                    bundle.putString("system_key", systemKey);
                }
                contact.setVoiceNumber(etVoiceNumber.getText().toString());
                bundle.putSerializable("contact", contact);
                bundle.putBoolean("isEditMode", isEditMode);
                bundle.putBoolean("isFromNotifications", isFromNotifications);
                bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
                bundle.putBoolean("isFromChangePIN", isFromChangePIN);
                bundle.putSerializable("user", user);
                if (contact.isEmailSelected()) {
                    bundle.putString("fragmentName", "ContactEmailAddressFragment");
                } else {
                    if (isFromNotificationSettingsAdapter) {
                        new ContactRepository().update(contact);
                        bundle.putString("removeAllFragmentsUpToCurrent", "EditNotificationSettingsFragment");
                    } else if (isEditMode) {
                        bundle.putString("fragmentName", "ContactInfoFragment");
                    } else {
                        bundle.putString("fragmentName", "VerifyContactInfoFragment");
                    }
                }
                mListener.onButtonClicked(bundle);
            }
        }
    }
}
