package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.data.adapters.NotificationSettingsAdapter;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.NotificationSettingRepository;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.DeleteContact;

import java.util.ArrayList;
import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class EditNotificationSettingsFragment extends BaseHomeFragment {

    public static final String TAG = "EditNotificationSettingsFragment";

    private LinearLayout contentLayout;
    private Button btnNext, btnDeselectAll;
    private NotificationSettingsAdapter adapter;
    private RecyclerView recyclerView;
    private List<NotificationSettingEntity> settings;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserEntity user;
    private ContactEntity contact;

    private ProgressBar progressBar;
    private boolean isFromAddNewUser = false;
    private boolean isFromSetup = false;
    private BaseWireframe mModel;

    public EditNotificationSettingsFragment() {
        // Required empty public constructor
    }

    public static EditNotificationSettingsFragment newInstance() {
        return new EditNotificationSettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_edit_notification_settings, container, false);
    }

    private void getFromModel() {
        if(isWireframeDeleteContact()) {
            if (null == contact) {
                contact = mModel.getContact();
            }
            if (null == user) {
                user = mModel.getUser();
            }
        }
    }

    private void getBundle() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            contact = (ContactEntity) bundle.getSerializable("contact");

            if (bundle.containsKey("isFromAddNewUser")) {
                if (bundle.getBoolean("isFromAddNewUser")) {
                    isFromAddNewUser = true;
                }
            }
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBundle();
        getFromModel();
        setupViews(view);

        settings = new ArrayList<>();
        boolean isReadOnly = isWireframeDeleteContact()?true:false;
        adapter = new NotificationSettingsAdapter(getActivity(), settings, mListener, isReadOnly);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new NotificationSettingsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                //TODO?
            }
        });

        new getDataTask().execute();
    }


    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        contentLayout = view.findViewById(R.id.layout_content);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        recyclerView = view.findViewById(R.id.recyclerview_notification_settings_list);

        if(isWireframeDeleteContact()){
            btnDeselectAll = view.findViewById(R.id.button_deselect_all);
            btnDeselectAll.setVisibility(View.VISIBLE);
            btnDeselectAll.setOnClickListener(this);

            // assume existing notification(s)
            TextUtils.disableButton(btnNext);
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.EDIT_NOTIFICATION_SETTINGS.value,
                    WorkflowProgress.SetNotifications.values().length);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            onClickNext();
        }
        else if (v == btnDeselectAll){
            onClickDeselectAll();
        }
    }

    /*
     * are we in a DeleteContact workflow ?
     */
    protected boolean isWireframeDeleteContact() {
        if(null==mModel) {
            mModel = ((MainActivity)getActivity()).getFeatureModel();
        }
        return (mModel.getClass().getSimpleName().equals(DeleteContact.TAG))? true:false;
    }

    /*
     * DeleteContact workflows
     * - available only after 'deselect all' or none selected
     *
     * EditContact workflow
     * - always available
     */
    private void onClickNext() {
        if(isWireframeDeleteContact()) {
            // save current settings to db
            ((DeleteContact)mModel).deleteNotifications4Contact(contact);
            gotoNextFragment();
        }
        else {
            new dbTask().execute();
        }
    }

    /*
     * DeleteContact workflow
     * - MUST delete ALL notifications for contact
     */
    private void onClickDeselectAll() {
        adapter.deselectAll();
        TextUtils.enableButton(btnNext);
    }

    /**
     * Asynchronous task for grabbing settings data from the db or generating new data on the fly
     */
    @SuppressLint("StaticFieldLeak")
    private class getDataTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            // if a list of settings already exists in the db, use that
            // otherwise generate a new list from scratch to be saved to db
            List<NotificationSettingEntity> tempList = new NotificationSettingRepository().readAll(user.getId(), contact.getId());
            if (tempList.size() > 0) {
                settings.clear();
                settings.addAll(tempList);
            } else {
                String[] array = getResources().getStringArray(R.array.notification_settings);
                int i = 0;
                for (String s : array) {
                    NotificationSettingEntity notificationSetting = new NotificationSettingEntity(user.getId(), contact.getId());
                    notificationSetting.setItemId(i);
                    notificationSetting.setSettingName(s);
                    settings.add(notificationSetting);
                    i += 1;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class dbTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            /*
             * Get current list from adapter and add/update to db
             */
            List<NotificationSettingEntity> tempList = adapter.getListFromAdapter();
            new NotificationSettingRepository().deleteByUserIdContactId(user.getId(), contact.getId());
            new NotificationSettingRepository().insertAll((List<IBaseEntity>) (List<?>) tempList);
            contact.setSelected(true);
            new ContactRepository().update(contact);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //TODO - perform a check to see if no settings were made

            progressBar.setVisibility(View.GONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            gotoNextFragment();
        }
    }

    protected void gotoNextFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        bundle.putBoolean("isFromSetup", isFromSetup);
        bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
        bundle.putString("fragmentName", "NotificationSettingsSavedFragment");
        mListener.onButtonClicked(bundle);
    }
}
