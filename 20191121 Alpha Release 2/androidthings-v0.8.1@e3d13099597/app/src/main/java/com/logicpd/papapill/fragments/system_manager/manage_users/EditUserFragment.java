package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;

public class EditUserFragment extends BaseHomeFragment {
    public static final String TAG = "EditUserFragment";

    private TextView tvTitle;
    private Button btnChgName, btnNotifications;
    private UserEntity user;

    public EditUserFragment() {
        // Required empty public constructor
    }

    public static EditUserFragment newInstance() {
        return new EditUserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_edit_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            tvTitle.setText("EDIT USER - " + user.getUserName());
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnChgName = view.findViewById(R.id.button_change_nickname);
        btnChgName.setOnClickListener(this);
        btnNotifications = view.findViewById(R.id.button_notifications);
        btnNotifications.setOnClickListener(this);
        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnChgName) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "EditUserNicknameFragment");
        }
        if (v == btnNotifications) {
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromAddNewUser", false);
            bundle.putString("fragmentName", "NotificationContactsFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
