package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.wireframes.workflows.AddNewUser;
import com.logicpd.papapill.databinding.FragmentManageUsersInvalidNicknameBinding;

public class InvalidNicknameFragment extends BaseHomeFragment {
    public static final String TAG = "InvalidNicknameFragment";
    private FragmentManageUsersInvalidNicknameBinding mBinding;
    private boolean isFromSetup;

    public InvalidNicknameFragment() {}

    public static InvalidNicknameFragment newInstance() {
        return new InvalidNicknameFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_users_invalid_nickname, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }
    }

    public void onClickOK() {
        // must contain userEntity
        Bundle bundle = this.getArguments();
        bundle.putString("fragmentName", AddNewUser.AddNewUserEnum.AddUser.toString());
        bundle.putBoolean("isFromSetup", isFromSetup);
        mListener.onButtonClicked(bundle);
    }
}
