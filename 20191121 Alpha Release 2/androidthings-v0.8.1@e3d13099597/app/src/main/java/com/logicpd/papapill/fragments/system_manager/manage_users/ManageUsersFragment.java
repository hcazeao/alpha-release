package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.App;
import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;
import com.logicpd.papapill.wireframes.workflows.AddNewUser;
import com.logicpd.papapill.wireframes.workflows.DeleteUser;
import com.logicpd.papapill.wireframes.workflows.EditUser;
import com.logicpd.papapill.wireframes.workflows.ManageContacts;

import java.util.List;

public class ManageUsersFragment extends BaseHomeFragment {
    public static final String TAG ="ManageUsersFragment";

    private Button btnAddUser, btnEditUser, btnDeleteUser, btnManageContacts;

    public ManageUsersFragment() {
        // Required empty public constructor
    }

    public static ManageUsersFragment newInstance() {
        return new ManageUsersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        List<UserEntity> userList = model.getUsers();
        if (userList.size() == 0) {
            TextUtils.disableButton(btnEditUser);
            TextUtils.disableButton(btnDeleteUser);
            TextUtils.disableButton(btnManageContacts);
        }
        if (userList.size() == 2) {
            TextUtils.disableButton(btnAddUser);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnAddUser = view.findViewById(R.id.button_add_user);
        btnEditUser = view.findViewById(R.id.button_edit_user);
        btnDeleteUser = view.findViewById(R.id.button_delete_user);
        btnAddUser.setOnClickListener(this);
        btnEditUser.setOnClickListener(this);
        btnDeleteUser.setOnClickListener(this);
        btnManageContacts = view.findViewById(R.id.button_manage_contacts);
        btnManageContacts.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();
        String featureName=null;

        if (v == btnAddUser) {
            featureName = AddNewUser.TAG;
            bundle.putString("authFragment", AddNewUser.AddNewUserEnum.AddUser.toString());
            bundle.putString("fragmentName", AddNewUser.AddNewUserEnum.SystemKey.toString());
        }
        if (v == btnEditUser) {
            featureName = EditUser.TAG;
            bundle.putString("authFragment", EditUser.EditUserEnum.SelectEditUser.toString());
            bundle.putString("fragmentName", EditUser.EditUserEnum.SystemKey.toString());
        }
        if (v == btnDeleteUser) {
            featureName = DeleteUser.TAG;
            bundle.putString("authFragment", DeleteUser.DeleteUserEnum.SelectDeleteUser.toString());
            bundle.putString("fragmentName", DeleteUser.DeleteUserEnum.SystemKey.toString());
        }
        if (v == btnManageContacts) {
            featureName = ManageContacts.TAG;
            bundle.putBoolean("isFromAddNewUser", false);
            bundle.putBoolean("isFromNotifications", false);
            bundle.putString("fragmentName", ManageContacts.ManageContactsEnum.SystemKey.toString());
            bundle.putString("authFragment", ManageContacts.ManageContactsEnum.ContactList.toString());
        }
        ((MainActivity)getActivity()).enterFeature(featureName);
        mListener.onButtonClicked(bundle);
    }
}
