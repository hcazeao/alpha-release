package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

public class NotificationPreferencesFragment extends BaseHomeFragment {

    public static final String TAG = "NotificationPreferencesFragment";

    private TextView tvContactName;
    private Button btnText, btnVoice, btnEmail, btnNext;
    private boolean isTextSelected, isVoiceSelected, isEmailSelected;
    private ContactEntity contact;
    private boolean isEditMode = false;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private boolean isFromChangePIN = false;
    private boolean isFromSetup = false;
    private String systemKey;
    private UserEntity user;

    public NotificationPreferencesFragment() {
        // Required empty public constructor
    }

    public static NotificationPreferencesFragment newInstance() {
        return new NotificationPreferencesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_notification_prefs, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
                systemKey = bundle.getString("system_key");
            }
            if (bundle.containsKey("isEditMode")) {
                if (bundle.getBoolean("isEditMode")) {
                    isEditMode = true;
                }
            }
            user = (UserEntity) bundle.getSerializable("user");
            isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            isFromNotifications = bundle.getBoolean("isFromNotifications");
            isFromChangePIN = bundle.getBoolean("isFromChangePIN");
            contact = (ContactEntity) bundle.getSerializable("contact");

            tvContactName.setText(contact.getName());
            if (contact.isTextSelected()) {
                btnText.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isTextSelected = true;
            } else {
                btnText.setBackgroundResource(R.drawable.button_selector);
                isTextSelected = false;
            }
            if (contact.isVoiceSelected()) {
                btnVoice.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isVoiceSelected = true;
            } else {
                btnVoice.setBackgroundResource(R.drawable.button_selector);
                isVoiceSelected = false;
            }
            if (contact.isEmailSelected()) {
                btnEmail.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isEmailSelected = true;
            } else {
                btnEmail.setBackgroundResource(R.drawable.button_selector);
                isEmailSelected = false;
            }
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display a different progress bar depending on the workflow
        if (isFromAddNewUser && !isFromNotifications) {
            setProgress(PROGRESS_ADD_USER,
                    WorkflowProgress.AddUser.NOTIFICATION_PREFERENCES.value,
                    WorkflowProgress.AddUser.values().length);
        } else if (isFromChangePIN) {
            // TBD
        } else if (isFromNotifications) {
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.NOTIFICATION_PREFERENCES.value,
                    WorkflowProgress.SetNotifications.values().length);
        } else if (isFromSetup) {
            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.NOTIFICATION_PREFERENCES.value,
                    WorkflowProgress.SetupWizard.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvContactName = view.findViewById(R.id.textview_contact_name);
        btnText = view.findViewById(R.id.button_text_message);
        btnText.setOnClickListener(this);
        btnVoice = view.findViewById(R.id.button_voice_message);
        btnVoice.setOnClickListener(this);
        btnEmail = view.findViewById(R.id.button_email_message);
        btnEmail.setOnClickListener(this);

        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnText) {
            if (!isTextSelected) {
                btnText.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isTextSelected = true;
            } else {
                btnText.setBackgroundResource(R.drawable.button_selector);
                isTextSelected = false;
            }

        }
        if (v == btnVoice) {
            if (!isVoiceSelected) {
                btnVoice.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isVoiceSelected = true;
            } else {
                btnVoice.setBackgroundResource(R.drawable.button_selector);
                isVoiceSelected = false;

            }

        }
        if (v == btnEmail) {
            if (!isEmailSelected) {
                btnEmail.setBackgroundResource(R.drawable.rounded_rectangle_pressed);
                isEmailSelected = true;
            } else {
                btnEmail.setBackgroundResource(R.drawable.button_selector);
                isEmailSelected = false;
            }
        }
        if (v == btnNext) {
            if (isFromSetup) {
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("system_key", systemKey);
            }
            contact.setTextSelected(isTextSelected);
            contact.setVoiceSelected(isVoiceSelected);
            contact.setEmailSelected(isEmailSelected);
            bundle.putSerializable("contact", contact);
            bundle.putBoolean("isEditMode", isEditMode);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putBoolean("isFromNotifications", isFromNotifications);
            bundle.putBoolean("isFromChangePIN", isFromChangePIN);
            bundle.putSerializable("user", user);
            if (isTextSelected) {
                bundle.putString("fragmentName", "ContactTextNumberFragment");
                mListener.onButtonClicked(bundle);
            } else if (isVoiceSelected) {
                bundle.putString("fragmentName", "ContactVoiceNumberFragment");
                mListener.onButtonClicked(bundle);
            } else if (isEmailSelected) {
                bundle.putString("fragmentName", "ContactEmailAddressFragment");
                mListener.onButtonClicked(bundle);
            } else {
                TextUtils.showToast(getActivity(), "Please select a method first");
            }
        }
    }
}
