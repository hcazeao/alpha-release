package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class NotificationSettingsMessageFragment extends BaseHomeFragment {

    public static final String TAG = "NotificationSettingsMessageFragment";

    private Button btnNext;
    private UserEntity user;
    private ContactEntity contact;
    private boolean isFromAddNewUser = false;
    private boolean isFromSetup = false;

    public NotificationSettingsMessageFragment() {
        // Required empty public constructor
    }

    public static NotificationSettingsMessageFragment newInstance() {
        return new NotificationSettingsMessageFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_notification_settings_message, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
            contact = (ContactEntity) bundle.getSerializable("contact");
            if (bundle.containsKey("isFromAddNewUser")) {
                if (bundle.getBoolean("isFromAddNewUser")) {
                    isFromAddNewUser = true;
                }
            }
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.NOTIFICATION_SETTINGS_MESSAGE.value,
                    WorkflowProgress.SetNotifications.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putSerializable("contact", contact);
            bundle.putString("fragmentName", "EditNotificationSettingsFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
