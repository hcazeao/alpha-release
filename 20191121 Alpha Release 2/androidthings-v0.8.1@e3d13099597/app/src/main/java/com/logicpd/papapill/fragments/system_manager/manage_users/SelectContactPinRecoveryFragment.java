package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.adapters.ContactsAdapter;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.SimpleDividerItemDecoration;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;

import java.util.List;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class SelectContactPinRecoveryFragment extends BaseHomeFragment {

    public static final String TAG = "SelectContactPinRecoveryFragment";

    private Button btnNewContact, btnEdit;
    private RecyclerView recyclerView;
    private List<ContactEntity> contactList;
    private List<ContactEntity> selectedContactList;

    private UserEntity user;
    private TextView emptyText, tvTitle;
    private ContactsAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private boolean isFromChangePIN = false;
    private boolean isFromSetup = false;

    public SelectContactPinRecoveryFragment() {
        // Required empty public constructor
    }

    public static SelectContactPinRecoveryFragment newInstance() {
        return new SelectContactPinRecoveryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_contact_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }

            user = (UserEntity) bundle.getSerializable("user");
            if (bundle.containsKey("isFromAddNewUser")) {
                isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            }
            if (bundle.containsKey("isFromNotifications")) {
                isFromNotifications = bundle.getBoolean("isFromNotifications");
            }
            if (bundle.containsKey("isFromChangePIN")) {
                isFromChangePIN = bundle.getBoolean("isFromChangePIN");
            }
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display a different progress bar depending on the workflow
        if (isFromAddNewUser && !isFromNotifications) {
            setProgress(PROGRESS_ADD_USER,
                    WorkflowProgress.AddUser.SELECT_CONTACT_PIN_RECOVERY.value,
                    WorkflowProgress.AddUser.values().length);
        } else if (isFromChangePIN) {
            // TBD
        } else if (isFromNotifications) {
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.NOTIFICATION_CONTACTS.value,
                    WorkflowProgress.SetNotifications.values().length);
        }

        // If we got here during the system contact portion of the setup wizard, user will be null.
        // Any other time, the user will be passed in from the previous fragment.
        if (user == null) {
            tvTitle.setText("SELECT A CONTACT FOR KEY RECOVERY");
        } else {
            tvTitle.setText("SELECT A CONTACT FOR PIN RECOVERY");
        }

        contactList = new ContactRepository().getByUserId(user.getId());
        adapter = new ContactsAdapter(getActivity(), contactList, false, false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(adapter);
        if (contactList.size() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText("Press NEW CONTACT to add a contact");
        } else {
            // If a contact was already created, overwrite the progress bar to better
            // indicate the true progress.
            setProgress(PROGRESS_ADD_USER,
                    WorkflowProgress.AddUser.CONTACT_ADDED.value,
                    WorkflowProgress.AddUser.values().length);
            recyclerView.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
        }

        adapter.setOnItemClickListener(new ContactsAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if(position < 0)
                {
                    Log.e(TAG, "invalid position");
                    return;
                }
                //go to VerifyContactInfoFragment
                user.setRecoveryContactId(contactList.get(position).getId());
                Bundle bundle = new Bundle();
                if (isFromChangePIN) {
                    bundle.putBoolean("isFromChangePIN", isFromChangePIN);
                    bundle.putString("fragmentName", "VerifyUserPinFragment");
                } else {
                    bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
                    bundle.putString("fragmentName", "VerifyUserInfoFragment");
                }
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putSerializable("user", user);
                bundle.putSerializable("contact", contactList.get(position));
                mListener.onButtonClicked(bundle);
            }
        });
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnNewContact = view.findViewById(R.id.button_new_contact);
        btnNewContact.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_view_edit);
        btnEdit.setOnClickListener(this);

        // TODO: View/Edit button currently needs to be implemented.
        btnEdit.setEnabled(false);
        btnEdit.setBackgroundResource(R.drawable.rounded_rectangle_pressed);

        recyclerView = view.findViewById(R.id.recyclerview_contact_list);
        emptyText = view.findViewById(R.id.textview_add_contact);

        tvTitle = view.findViewById(R.id.textview_title);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNewContact) {
            bundle.putBoolean("isFromSetup", isFromSetup);
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromChangePIN", isFromChangePIN);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putBoolean("isFromNotifications", isFromNotifications);
            bundle.putString("fragmentName", "ContactNameFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
