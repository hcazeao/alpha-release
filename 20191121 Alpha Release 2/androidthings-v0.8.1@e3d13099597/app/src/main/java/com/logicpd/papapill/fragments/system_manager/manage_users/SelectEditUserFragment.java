package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.List;

public class SelectEditUserFragment extends BaseSelectEditUser {
    public static String TAG = "SelectEditUserFragment";

    public SelectEditUserFragment() {
        // Required empty public constructor
    }

    public static SelectEditUserFragment newInstance() {
        return new SelectEditUserFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_select_edit_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        enableUserSelection();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnUserA) {
            gotoEditUserFragment(0);
        }
        else if (v == btnUserB) {
            gotoEditUserFragment(1);
        }
    }

    protected void gotoEditUserFragment(int index) {
        Bundle bundle = new Bundle();
        Log.d(AppConstants.TAG, "Edit user " + userList.get(index).getUserName() + " selected");
        bundle.putString("fragmentName", "EditUserFragment");
        bundle.putSerializable("user", userList.get(index));
        mListener.onButtonClicked(bundle);
    }
}
