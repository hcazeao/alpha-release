package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.wireframes.BaseWireframe;

public class SelectUserEditNotificationsFragment extends BaseSelectEditUser {
    public static final String TAG = "SelectUserEditNotificationsFragment";

    public SelectUserEditNotificationsFragment() {
    }

    public SelectUserEditNotificationsFragment newInstance() {
        return new SelectUserEditNotificationsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_select_edit_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        enableUserSelection();
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        TextView title = view.findViewById(R.id.txt_title);
        String str = getString(R.string.select_a_user_to_edit_notifications_for);
        title.setText(str);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (v == btnUserA) {
            gotoEditNotificationSettings(0);
        }
        else if (v == btnUserB) {
            gotoEditNotificationSettings(1);
        }
    }

    protected void gotoEditNotificationSettings(int index) {
        Bundle bundle = new Bundle();
        Log.d(AppConstants.TAG, "Edit user " + userList.get(index).getUserName() + " selected");

        /*
         * destination depends of context, need a model here !
         */
        BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
        model.addMapItem("user", userList.get(index));
        String fragmentName = (model.getNextFragmentName(TAG));

        if(null==fragmentName)
            fragmentName = EditUserFragment.class.getSimpleName();

        bundle.putString("fragmentName", fragmentName);
        bundle.putSerializable("user", userList.get(index));
        mListener.onButtonClicked(bundle);
    }
}
