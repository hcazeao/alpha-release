package com.logicpd.papapill.fragments.system_manager.manage_users;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class VerifyContactInfoFragment extends BaseHomeFragment {

    public static final String TAG = "VerifyContactInfoFragment";

    private TextView tvSummary;
    private Button btnEdit, btnNext, btnDelete;
    private ContactEntity contact;
    private boolean isEditMode = false;
    private boolean isFromAddNewUser = false;
    private boolean isFromNotifications = false;
    private boolean isFromChangePIN = false;
    private boolean isFromSetup = false;
    private String systemKey;
    private UserEntity user;

    public VerifyContactInfoFragment() {
        // Required empty public constructor
    }

    public static VerifyContactInfoFragment newInstance() {
        return new VerifyContactInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_users_verify_contact_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
                systemKey = bundle.getString("system_key");
            }
            if (bundle.containsKey("isEditMode")) {
                if (bundle.getBoolean("isEditMode")) {
                    isEditMode = true;
                }
            }
            user = (UserEntity) bundle.getSerializable("user");
            isFromNotifications = bundle.getBoolean("isFromNotifications");
            isFromAddNewUser = bundle.getBoolean("isFromAddNewUser");
            isFromChangePIN = bundle.getBoolean("isFromChangePIN");
            contact = (ContactEntity) bundle.getSerializable("contact");
            StringBuilder sb = new StringBuilder();
            sb.append("NAME: " + contact.getName());
            if (contact.getTextNumber() != null && !contact.getTextNumber().isEmpty()) {
                sb.append("\nTEXT MESSAGE: " + contact.getTextNumber());
            }
            if (contact.getVoiceNumber() != null && !contact.getVoiceNumber().isEmpty()) {
                sb.append("\nVOICE MESSAGE: " + contact.getVoiceNumber());
            }
            if (contact.getEmail() != null && !contact.getEmail().isEmpty()) {
                sb.append("\nEMAIL: " + contact.getEmail());
            }
            tvSummary.setText(sb.toString());
            if (isEditMode) {
                btnDelete.setVisibility(View.VISIBLE);
            } else {
                btnDelete.setVisibility(View.GONE);
            }
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
        }

        // Display a different progress bar depending on the workflow
        if (isFromAddNewUser && !isFromNotifications) {
            setProgress(PROGRESS_ADD_USER,
                    WorkflowProgress.AddUser.VERIFY_CONTACT_INFO.value,
                    WorkflowProgress.AddUser.values().length);
        } else if (isFromChangePIN) {
            // TBD
        } else if (isFromNotifications) {
            setProgress(PROGRESS_SET_NOTIFICATIONS,
                    WorkflowProgress.SetNotifications.VERIFY_CONTACT_INFO.value,
                    WorkflowProgress.SetNotifications.values().length);
        } else if (isFromSetup) {
            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.VERIFY_CONTACT_INFO.value,
                    WorkflowProgress.SetupWizard.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnEdit = view.findViewById(R.id.button_edit_contact_info);
        btnEdit.setOnClickListener(this);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        tvSummary = view.findViewById(R.id.textview_verify_contact_info);
        btnDelete = view.findViewById(R.id.button_delete_contact);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (isFromSetup) {
            bundle.putBoolean("isFromSetup", true);
            bundle.putString("system_key", systemKey);
        }

        if (v == btnNext) {
            // add to db
            new ContactRepository().insert(contact);
            Log.d(TAG, "Inserting new contact into db with user id = " + contact.getUserId());
            bundle.putSerializable("contact", contact);
            bundle.putSerializable("user", user);
            bundle.putBoolean("isFromNotifications", isFromNotifications);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putBoolean("isFromChangePIN", isFromChangePIN);
            bundle.putString("fragmentName", "ContactAddedFragment");
        }
        if (v == btnEdit) {
            bundle.putBoolean("isFromNotifications", isFromNotifications);
            bundle.putBoolean("isFromAddNewUser", isFromAddNewUser);
            bundle.putBoolean("isFromChangePIN", isFromChangePIN);
            bundle.putSerializable("contact", contact);
            bundle.putSerializable("user", user);
            bundle.putBoolean("removeFragment", true);
            bundle.putString("fragmentName", "ContactNameFragment");
        }
        mListener.onButtonClicked(bundle);
    }
}
