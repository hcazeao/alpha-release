package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.databinding.FragmentSystemSettingsAudioVisualSettingsBinding;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.utils.PreferenceUtils;
import com.logicpd.papapill.utils.TextUtils;

/*
 * TODO Refactor to use Android standard controls or Customized or sub-fragment-classes
 */
public class AudioVisualSettingFragment extends BaseHomeFragment  {

    public static final String TAG = "AudioVisualSettingFragment";
    final int STEP = 10;
    final int FLOOR = 0;
    final int CEILING = 100;
    private FragmentSystemSettingsAudioVisualSettingsBinding mBinding;
    private boolean mIsAudioEnabled, mIsVisualEnabled, mIsTouchClicked;
    private LinearLayout mBtnAudio, mBtnVisual, mBtnTouch;
    private LinearLayout mSlideAudio, mSlideBright;
    private int mVolume, mBrigthness;
    public AudioVisualSettingFragment(){}

    public static AudioVisualSettingFragment newInstance() {return new AudioVisualSettingFragment();}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_system_settings_audio_visual_settings, container, false);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        mBinding.invalidateAll();
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        mIsAudioEnabled = PreferenceUtils.getAudioToneIndicators();
        mBtnAudio = view.findViewById(R.id.toggle_audio);
        setToggle(mBtnAudio, mIsAudioEnabled, getString(R.string.audio_tone_indicators));

        mIsVisualEnabled = PreferenceUtils.getVisualIndicatorsEnabled();
        mBtnVisual = view.findViewById(R.id.toggle_visual);
        setToggle(mBtnVisual, mIsVisualEnabled, getString(R.string.visual_indicators));

        mIsTouchClicked = PreferenceUtils.getTouchscreenClicked();
        mBtnTouch = view.findViewById(R.id.toggle_touch);
        setToggle(mBtnTouch, mIsTouchClicked, getString(R.string.touchscreen_clicks));

        mVolume = PreferenceUtils.getAudioVolume();
        mSlideAudio = view.findViewById(R.id.control_audio_volume);
        setSlider(mSlideAudio, mVolume, getString(R.string.audio_volume));

        mBrigthness = PreferenceUtils.getScreenBrightness();
        mSlideBright = view.findViewById(R.id.control_screen_brightness);
        setSlider(mSlideBright, mBrigthness, getString(R.string.screen_brightness));
    }

    /*
     * TODO Should use Android toogleButton instead for correct states [blinks, press, up] for the integral button
     * Or create a separate Fragment class
     */
    private void setToggle(LinearLayout layout,
                           boolean isEnabled,
                           String title){
        Button enabled = (Button)((isEnabled)?
                            layout.findViewById(R.id.btn_enabled):
                            layout.findViewById(R.id.btn_disabled));

        Button disabled = (Button)((isEnabled)?
                            layout.findViewById(R.id.btn_disabled):
                            layout.findViewById(R.id.btn_enabled));

        enabled.setBackgroundColor(Color.parseColor(getString(R.string.enabled_background_color)));
        enabled.setTextColor(Color.parseColor(getString(R.string.enabled_text_color)));
        disabled.setBackgroundColor(Color.parseColor(getString(R.string.disabled_background_color)));
        disabled.setTextColor(Color.parseColor(getString(R.string.disabled_text_color)));

        if(null!=title) {
            TextView text = layout.findViewById(R.id.txt_title);
            text.setText(title);
        }
    }

    /*
     * TODO Should use Android seekbar or slider control
     * Or create a separate Fragment class
     */
    private void setSlider(LinearLayout layout,
                           int percent,
                           String title){
        if(null==layout)
            return;

        for(int i=FLOOR; i<=percent; i+=STEP) {
            fillDot(layout, i, R.string.enabled_text_color);
        }

        for(int i=percent+STEP; i<=CEILING; i+=STEP) {
            fillDot(layout, i, R.string.disabled_text_color);
        }

        if(null!=title) {
            TextView text = layout.findViewById(R.id.txt_title);
            text.setText(title);
        }
    }

    private void fillDot(LinearLayout layout,
                         int percent,
                         int colorId){
        if(percent > FLOOR) {
            String stringId = "btn_percent_" + percent;
            int id = getResources().getIdentifier(stringId, "id", getContext().getPackageName());
            Button btn = layout.findViewById(id);
            btn.setBackgroundColor(Color.parseColor(getString(colorId)));
        }
    }

    public void onClickToggle(View view) {
        View parent = (LinearLayout)view.getParent().getParent();
        int id = parent.getId();
        switch(id) {
            case R.id.toggle_audio:
                onClickAudioEnabled();
                break;

            case R.id.toggle_visual:
                onClickVisualEnabled();
                break;

            case R.id.toggle_touch:
                onClickTouchEnabled();
                break;
        }
    }

    public void onClickMinus(View view) {
        View parent = (LinearLayout) view.getParent();
        int id = parent.getId();
        switch (id) {
            case R.id.control_audio_volume:
                onClickVolumeMinus();
                break;

            case R.id.control_screen_brightness:
                onClickBrightnessMinus();
                break;
        }
    }

    public void onClickPlus(View view) {
        View parent = (LinearLayout) view.getParent();
        int id = parent.getId();
        switch (id) {
            case R.id.control_audio_volume:
                onClickVolumePlus();
                break;

            case R.id.control_screen_brightness:
                onClickBrightnessPlus();
                break;
        }
    }

    /*
     * Audio
     */
    public void onClickAudioEnabled() {
        mIsAudioEnabled = !mIsAudioEnabled;
        PreferenceUtils.setAudioToneIndicators(mIsAudioEnabled);
        setToggle(mBtnAudio, mIsAudioEnabled, null);
    }

    public void onClickVolumeMinus() {
        if(mVolume>FLOOR) {
            mVolume -= STEP;
            PreferenceUtils.setAudioVolume(mVolume);
            setSlider(mSlideAudio, mVolume, null);
            playAudioSample();
        }
    }

    public void onClickVolumePlus() {
        if(mVolume<CEILING) {
            mVolume += STEP;
            PreferenceUtils.setAudioVolume(mVolume);
            setSlider(mSlideAudio, mVolume, null);
            playAudioSample();
        }
    }

    private void playAudioSample() {
        AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mVolume, 0);//TODO setting default volume at 75

            MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.emn_02);
            mp.setVolume(mVolume, mVolume);
            mp.start();
        }
    }

    /*
     * Visual
     */
    public void onClickVisualEnabled() {
        mIsVisualEnabled = !mIsVisualEnabled;
        PreferenceUtils.setVisualIndicatorsEnabled(mIsVisualEnabled);
        setToggle(mBtnVisual, mIsVisualEnabled, null);
    }

    public void onClickBrightnessMinus() {
        if(mBrigthness>0) {
            mBrigthness -= STEP;
            PreferenceUtils.setScreenBrightness(mBrigthness);
            setSlider(mSlideBright, mBrigthness, null);
        }
    }

    public void onClickBrightnessPlus() {
        if(mBrigthness<100) {
            mBrigthness += STEP;
            PreferenceUtils.setScreenBrightness(mBrigthness);
            setSlider(mSlideBright, mBrigthness, null);
        }
    }

    public void onClickTouchEnabled() {
        mIsTouchClicked = !mIsTouchClicked;
        PreferenceUtils.setTouchscreenClicked(mIsTouchClicked);
        setToggle(mBtnTouch, mIsTouchClicked, null);
    }

    public void onClickDone() {
        Bundle bundle = new Bundle();
        bundle.putString("fragmentName", "Home");
        mListener.onButtonClicked(bundle);
    }
}
