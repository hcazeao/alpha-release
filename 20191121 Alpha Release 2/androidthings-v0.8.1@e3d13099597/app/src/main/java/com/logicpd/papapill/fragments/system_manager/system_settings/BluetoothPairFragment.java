package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;

/**
 * Fragment for System Manager...System Settings...Pair Bluetooth Keyboard...Search for Bluetooth Devices
 *
 * @author alankilloren
 */
public class BluetoothPairFragment extends BaseHomeFragment {
    public static final String TAG = "BluetoothPairFragment";
    private Button btnCancel, btnPair;

    public BluetoothPairFragment() {
        // Required empty public constructor
    }

    public static BluetoothPairFragment newInstance() {
        return new BluetoothPairFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_settings_bluetooth_pair, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        /*Bundle bundle = this.getArguments();
        if (bundle != null) {

        }*/

    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);

        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnPair = view.findViewById(R.id.button_pair);
        btnPair.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnPair) {
            //TODO
        }
    }
}
