package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.things.device.TimeManager;
import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;

import java.util.TimeZone;

public class SetSystemDateTimeConfirmationFragment extends BaseHomeFragment {
    public static final String TAG = "SetSystemDateTimeConfirmationFragment";
    private Button btnEdit, btnOK;
    private TextView tvDateTimeInfo;
    private long dateTimeInMillis;
    private TimeZone timeZone;
    private boolean isFromSetup;

    public SetSystemDateTimeConfirmationFragment() {
        // Required empty public constructor
    }

    public static SetSystemDateTimeConfirmationFragment newInstance() {
        return new SetSystemDateTimeConfirmationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_set_system_date_time_confirmation, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            //handle bundle
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
            String dateString = bundle.getString("datestring");
            dateTimeInMillis = bundle.getLong("timeinmillis");
            timeZone = (TimeZone) bundle.getSerializable("timezone");
            tvDateTimeInfo.setText(dateString);
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            //setProgress(PROGRESS_SETUP_WIZARD, 6, MAX_PROGRESS_SETUP_WIZARD);
            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.SET_SYSTEM_DATE_AND_TIME_CONFIRMATION.value,
                    WorkflowProgress.SetupWizard.values().length);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_edit);
        btnEdit.setOnClickListener(this);
        tvDateTimeInfo = view.findViewById(R.id.textview_date_time_info);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOK) {
            TimeManager timeManager = TimeManager.getInstance();
            timeManager.setTimeZone(timeZone.getID());
            timeManager.setTime(dateTimeInMillis);

            if (isFromSetup) {
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("fragmentName", "SetSystemKeyFragment");
            } else {
                homeButton.performClick();
            }
        }
        if (v == btnEdit) {
            backButton.performClick();
        }
        mListener.onButtonClicked(bundle);
    }
}
