package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;

import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * SetSystemDateTimeFragment
 *
 * @author sujithpillai
 */
public class SetSystemDateTimeFragment extends BaseHomeFragment implements View.OnTouchListener {

    public static final String TAG = "SetSystemDateTimeFragment";

    private EditText etMonth, etDay, etYear, etTime, etMinute, etAMPM, etTimeZone;
    private Button btnNext, btnMonthUp, btnMonthDown, btnDayUp, btnDayDown, btnYearUp, btnYearDown,
            btnTimeUp, btnTimeDown, btnAMPMDown, btnAMPMUp, btnTimeZoneUp, btnTimeZoneDown;
    private Calendar calendar;
    private int currentMonth, currentDay, currentYear, currentHour, currentMinute, currentTimeZone;
    private HashMap<Integer, String> months;
    private HashMap<Integer, String> timeZones;
    private boolean timePressHold = false;
    private boolean isFromSetup;

    private final String EST = "EST";
    private final String PST = "PST";
    private final String CST = "CST";
    private final String MST = "MST";

    private final String EASTERN_STANDARD_TIME = "Eastern Standard Time";
    private final String CENTRAL_STANDARD_TIME = "Central Standard Time";
    private final String PACIFIC_STANDARD_TIME = "Pacific Standard Time";
    private final String MOUNTAIN_STANDARD_TIME = "Mountain Standard Time";

    public SetSystemDateTimeFragment() {
        // Required empty public constructor
    }

    public static SetSystemDateTimeFragment newInstance() {
        return new SetSystemDateTimeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_set_system_date_time, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        setDatePicker();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }
        }

        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.SET_SYSTEM_DATE_AND_TIME.value,
                    WorkflowProgress.SetupWizard.values().length);
        }
    }

    private void setDatePicker() {
        calendar = Calendar.getInstance();
        months = new HashMap<>();
        months.put(1, "JAN");
        months.put(2, "FEB");
        months.put(3, "MAR");
        months.put(4, "APR");
        months.put(5, "MAY");
        months.put(6, "JUN");
        months.put(7, "JUL");
        months.put(8, "AUG");
        months.put(9, "SEP");
        months.put(10, "OCT");
        months.put(11, "NOV");
        months.put(12, "DEC");
        currentMonth = calendar.get(Calendar.MONTH) + 1;
        currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        currentYear = calendar.get(Calendar.YEAR);
        etMonth.setText(months.get(currentMonth));
        etDay.setText("" + currentDay);
        etYear.setText("" + currentYear);

        currentHour = calendar.get(Calendar.HOUR);
        currentHour = currentHour == 0 ? 12 : currentHour;
        currentMinute = calendar.get(Calendar.MINUTE);
        etTime.setText("" + currentHour + ":" + (currentMinute > 9 ? currentMinute : "0" + currentMinute));
        if (calendar.get(Calendar.AM_PM) == 0) {
            etAMPM.setText("AM");
        } else {
            etAMPM.setText("PM");
        }

        setupTimeZone();
    }

    private void setupTimeZone() {
        timeZones = new HashMap<>();
        timeZones.put(1, EST);
        timeZones.put(2, CST);
        timeZones.put(3, MST);
        timeZones.put(4, PST);

        String currTimeZone = calendar.getTimeZone().getDisplayName();
        if (currTimeZone.equals(EASTERN_STANDARD_TIME)) {
            currentTimeZone = 1;
        } else if (currTimeZone.equals(CENTRAL_STANDARD_TIME)) {
            currentTimeZone = 2;
        } else if (currTimeZone.equals(MOUNTAIN_STANDARD_TIME)) {
            currentTimeZone = 3;
        } else if (currTimeZone.equals(PACIFIC_STANDARD_TIME)) {
            currentTimeZone = 4;
        } else {
            currentTimeZone = 2;
        }
        etTimeZone.setText(timeZones.get(currentTimeZone));
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnMonthUp = view.findViewById(R.id.button_month_up);
        btnMonthUp.setOnClickListener(this);
        btnMonthDown = view.findViewById(R.id.button_month_down);
        btnMonthDown.setOnClickListener(this);

        btnDayUp = view.findViewById(R.id.button_day_up);
        btnDayUp.setOnClickListener(this);
        btnDayDown = view.findViewById(R.id.button_day_down);
        btnDayDown.setOnClickListener(this);

        btnYearUp = view.findViewById(R.id.button_year_up);
        btnYearUp.setOnClickListener(this);
        btnYearDown = view.findViewById(R.id.button_year_down);
        btnYearDown.setOnClickListener(this);

        btnTimeUp = view.findViewById(R.id.button_time_up);
        btnTimeUp.setOnTouchListener(this);
        btnTimeDown = view.findViewById(R.id.button_time_down);
        btnTimeDown.setOnTouchListener(this);

        btnAMPMUp = view.findViewById(R.id.button_ampm_up);
        btnAMPMUp.setOnClickListener(this);
        btnAMPMDown = view.findViewById(R.id.button_ampm_down);
        btnAMPMDown.setOnClickListener(this);

        btnTimeZoneUp = view.findViewById(R.id.button_timezone_up);
        btnTimeZoneUp.setOnClickListener(this);
        btnTimeZoneDown = view.findViewById(R.id.button_timezone_down);
        btnTimeZoneDown.setOnClickListener(this);

        etMonth = view.findViewById(R.id.edittext_month);
        etMonth.setEnabled(false);
        etDay = view.findViewById(R.id.edittext_day);
        etDay.setEnabled(false);
        etYear = view.findViewById(R.id.edittext_year);
        etYear.setEnabled(false);

        etTime = view.findViewById(R.id.edittext_time);
        etTime.setEnabled(false);
        etAMPM = view.findViewById(R.id.edittext_ampm);
        etAMPM.setEnabled(false);
        etTimeZone = view.findViewById(R.id.edittext_timezone);
        etTimeZone.setEnabled(false);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);

        TextView tvMonth = view.findViewById(R.id.textview_month_label);
        tvMonth.setText("MONTH");
        TextView tvDay = view.findViewById(R.id.textview_day_label);
        tvDay.setText("DAY");
        TextView tvYear = view.findViewById(R.id.textview_year_label);
        tvYear.setText("YEAR");
        TextView tvTime = view.findViewById(R.id.textview_time_label);
        tvTime.setText("TIME");
        TextView tvAmPm = view.findViewById(R.id.textview_am_pm_label);
        tvAmPm.setText("AM/PM");
        TextView tvZone = view.findViewById(R.id.textview_zone_label);
        tvZone.setText("ZONE");
    }

    /**
     * Returns the maximum number of days in a given month/year
     *
     * @param year  Passed in year 4-digits
     * @param month Passed in month
     * @return Max # of days in month
     */
    private int getDaysInMonth(int year, int month) {
        YearMonth yearMonth = YearMonth.of(year, month);
        return yearMonth.lengthOfMonth();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnNext) {
            proceedTimeSaving(bundle);
        }
        if (v == btnYearUp) {
            currentYear += 1;
            etYear.setText("" + currentYear);
        }
        if (v == btnYearDown) {
            currentYear -= 1;
            etYear.setText("" + currentYear);
        }
        if (v == btnMonthUp) {
            monthUp();
        }
        if (v == btnMonthDown) {
            monthDown();
        }
        if (v == btnDayUp) {
            dayUp();
        }
        if (v == btnDayDown) {
            dayDown();
        }
        if (v == btnAMPMUp) {
            etAMPM.setText("PM");
        }
        if (v == btnAMPMDown) {
            etAMPM.setText("AM");
        }
        if (v == btnTimeZoneUp) {
            timeZoneUp();
        }
        if (v == btnTimeZoneDown) {
            timeZoneDown();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        try {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (!timePressHold) {
                    timePressHold = true;
                    if (v == btnTimeUp) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                timeUp();
                            }
                        }).start();
                    }
                    if (v == btnTimeDown) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                timeDown();
                            }
                        }).start();
                    }
                }
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                // button relased.
                if (timePressHold) {
                    timePressHold = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void monthUp() {
        if (currentMonth < 12) {
            currentMonth += 1;
            int max = getDaysInMonth(currentYear, currentMonth);
            if (Integer.parseInt(etDay.getText().toString()) > max) {
                currentDay = max;
                etDay.setText("" + currentDay);
            }
        }
        etMonth.setText(months.get(currentMonth));
    }

    private void monthDown() {
        if (currentMonth > 1) {
            currentMonth -= 1;
            int max = getDaysInMonth(currentYear, currentMonth);
            if (Integer.parseInt(etDay.getText().toString()) > max) {
                currentDay = max;
                etDay.setText("" + currentDay);
            }
        }
        etMonth.setText(months.get(currentMonth));
    }

    private void dayUp() {
        int max = getDaysInMonth(currentYear, currentMonth);
        if (Integer.parseInt(etDay.getText().toString()) > max) {
            currentDay = max;
            etDay.setText("" + currentDay);
        }
        if (currentDay < max) {
            currentDay += 1;
        }
        etDay.setText("" + currentDay);
    }

    private void dayDown() {
        if (currentDay > 1) {
            currentDay -= 1;
        }
        etDay.setText("" + currentDay);
    }

    private synchronized void timeUp() {
        try {
            while (timePressHold) {
                if (currentMinute < 59) {
                    currentMinute++;
                } else {
                    currentMinute = 0;
                    if (currentHour < 12) {
                        currentHour++;
                        changeAMPM(false);
                    } else {
                        currentHour = 1;
                    }
                }
                etTime.setText("" + currentHour + ":" + (currentMinute > 9 ? currentMinute : "0" + currentMinute));
                Thread.sleep(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void timeDown() {
        try {
            while (timePressHold) {
                if (currentMinute > 0) {
                    currentMinute--;
                } else {
                    currentMinute = 59;
                    if (currentHour > 1) {
                        currentHour--;
                        changeAMPM(true);
                    } else {
                        currentHour = 12;
                    }
                }
                etTime.setText("" + currentHour + ":" + (currentMinute > 9 ? currentMinute : "0" + currentMinute));
                Thread.sleep(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeAMPM(boolean isDown) {
        if (currentHour == 12 || (isDown && currentHour == 11)) {
            if (etAMPM.getText().toString().equals("AM")) {
                etAMPM.setText("PM");
            } else {
                etAMPM.setText("AM");
            }
        }
    }

    private void timeZoneUp() {
        if (currentTimeZone < 4) {
            currentTimeZone++;
            etTimeZone.setText(timeZones.get(currentTimeZone));
        }
    }

    private void timeZoneDown() {
        if (currentTimeZone > 1) {
            currentTimeZone--;
            etTimeZone.setText(timeZones.get(currentTimeZone));
        }
    }

    private TimeZone getSelectedTimeZone() {
        for (String id : TimeZone.getAvailableIDs()) {
            TimeZone currTimeZone = TimeZone.getTimeZone(id);
            String timeZoneShort = timeZones.get(currentTimeZone);
            String timeZoneDisplay = currTimeZone.getDisplayName();

            if (timeZoneShort.equals(EST) && timeZoneDisplay.equals(EASTERN_STANDARD_TIME)) {
                return currTimeZone;
            }
            if (timeZoneShort.equals(CST) && timeZoneDisplay.equals(CENTRAL_STANDARD_TIME)) {
                return currTimeZone;
            }
            if (timeZoneShort.equals(PST) && timeZoneDisplay.equals(PACIFIC_STANDARD_TIME)) {
                return currTimeZone;
            }
            if (timeZoneShort.equals(MST) && timeZoneDisplay.equals(MOUNTAIN_STANDARD_TIME)) {
                return currTimeZone;
            }
        }
        return TimeZone.getDefault();
    }

    private void proceedTimeSaving(Bundle bundle) {
        TimeZone timeZone = getSelectedTimeZone();
        calendar.set(Calendar.YEAR, currentYear);
        calendar.set(Calendar.MONTH, currentMonth - 1);
        calendar.set(Calendar.DAY_OF_MONTH, currentDay);
        calendar.set(Calendar.HOUR, currentHour == 12 ? 0 : currentHour);
        calendar.set(Calendar.MINUTE, currentMinute);
        calendar.set(Calendar.AM_PM, etAMPM.getText().toString().equals("AM") ? 0 : 1);
        SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy");
        String dateString = df.format(calendar.getTime()) + "\n" + etTime.getText() + " " + etAMPM.getText() + " " + timeZones.get(currentTimeZone);
        bundle.putBoolean("isFromSetup", isFromSetup);
        bundle.putString("datestring", dateString);
        bundle.putLong("timeinmillis", calendar.getTimeInMillis());
        bundle.putSerializable("timezone", timeZone);
        bundle.putString("fragmentName", "SetSystemDateTimeConfirmationFragment");
        mListener.onButtonClicked(bundle);
    }
}