package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.utils.TextUtils;

/**
 * Fragment for System Manager...System Settings
 *
 * @author alankilloren
 */
public class SystemSettingsFragment extends BaseHomeFragment {
    public static final String TAG = "SystemSettingsFragment";
    private Button btnWireless, btnKey, btnBluetooth, btnDateTime, btnCloud, btnAV;

    public SystemSettingsFragment() {
        // Required empty public constructor
    }

    public static SystemSettingsFragment newInstance() {
        return new SystemSettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        //TODO - disabling these for now until I can get them working
        TextUtils.disableButton(btnBluetooth);
        TextUtils.disableButton(btnCloud);
        /*Bundle bundle = this.getArguments();
        if (bundle != null) {

        }*/
    }

    @Override
    protected void setupViews(View view) {
       super.setupViews(view);

        btnWireless = view.findViewById(R.id.button_wireless);
        btnWireless.setOnClickListener(this);
        btnKey = view.findViewById(R.id.button_system_key);
        btnKey.setOnClickListener(this);
        btnBluetooth = view.findViewById(R.id.button_bluetooth);
        btnBluetooth.setOnClickListener(this);
        btnDateTime = view.findViewById(R.id.button_datetime);
        btnDateTime.setOnClickListener(this);
        btnCloud = view.findViewById(R.id.button_cloud);
        btnCloud.setOnClickListener(this);
        btnAV = view.findViewById(R.id.button_av);
        btnAV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // For the Back or Home buttons.
        if (super.baseHomeOnClick(v)) {
            return;
        }

        Bundle bundle = new Bundle();
        String fragmentName = "Home";

        if (v == btnWireless) {
            fragmentName = "SelectWifiNetworkFragment";
        }
        if (v == btnBluetooth) {
            fragmentName = "BluetoothFragment";
        }
        if (v == btnKey) {
            fragmentName = "ChangeSystemKeyFragment";
        }
        if(v == btnDateTime) {
            fragmentName = "SetSystemDateTimeFragment";
        }
        if(v == btnAV) {
            fragmentName = AudioVisualSettingFragment.class.getSimpleName();
        }
        bundle.putString("fragmentName", "SystemKeyFragment");
        bundle.putString("authFragment", fragmentName);
        mListener.onButtonClicked(bundle);
    }
}
