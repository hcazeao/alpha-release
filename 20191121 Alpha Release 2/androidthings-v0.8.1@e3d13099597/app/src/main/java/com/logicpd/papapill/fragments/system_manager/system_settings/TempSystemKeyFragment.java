package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.utils.TextUtils;
import com.logicpd.papapill.wireframes.BundleFactory;

/**
 * Fragment for entering or changing system keys
 *
 * @author sujith pillai
 */
public class TempSystemKeyFragment extends BaseHomeFragment {

    public static final String TAG = "TempSystemKeyFragment";

    private Button btnOK;
    private EditText etSystemKey;
    private String authFragment;
    private int authAttempts = 1;

    public TempSystemKeyFragment() {
        // Required empty public constructor
    }

    public static TempSystemKeyFragment newInstance() {
        return new TempSystemKeyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_temp_system_key, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("authFragment")) {
                authFragment = bundle.getString("authFragment");
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        TextUtils.disableButton(btnOK);
        etSystemKey = view.findViewById(R.id.edittext_temp_system_key);
        etSystemKey.requestFocus();
        etSystemKey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.d(AppConstants.TAG, "in beforeTextChanged()");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.i(AppConstants.TAG, "in onTextChanged() - " + start);
                if (start > 2) {
                    TextUtils.enableButton(btnOK);
                } else {
                    TextUtils.disableButton(btnOK);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.d(AppConstants.TAG, "in afterTextChanged()");
            }
        });
        etSystemKey.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etSystemKey.getText().length() > 1 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnOK.performClick();
                    handled = true;
                }
                return handled;
            }
        });

        TextUtils.showKeyboard(getActivity(), etSystemKey);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnOK) {
            if (etSystemKey.getText().toString().length() == 4) {
                if (etSystemKey.getText().toString().equals(AppConstants.DEFAULT_SYSTEM_KEY)) {
                    bundle.putBoolean("removeFragment", true);
                    bundle.putString("fragmentName", authFragment);
                    mListener.onButtonClicked(bundle);
                } else {
                    bundle.putInt("authAttempts", authAttempts);
                    if (authAttempts < 3) {
                        authAttempts += 1;
                    }
                    bundle.putString("fragmentName", "IncorrectSystemKeyFragment");
                    mListener.onButtonClicked(bundle);
                }
            }
            etSystemKey.setText("");
        }
    }
}
