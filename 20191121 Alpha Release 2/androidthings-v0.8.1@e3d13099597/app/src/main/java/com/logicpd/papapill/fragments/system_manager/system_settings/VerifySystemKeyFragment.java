package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.utils.PreferenceUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

import static android.view.View.TEXT_ALIGNMENT_TEXT_START;

/**
 * Fragment for System Manager...System Settings...Change System Key...Verify
 *
 * @author alankilloren
 */
public class VerifySystemKeyFragment extends BaseHomeFragment {
    public static final String TAG = "VerifySystemKeyFragment";
    private Button btnOK, btnEdit;
    private TextView tvSystemKey;
    private String systemKey;
    private boolean isFromSetup;
    private ContactEntity contact;
    private PreferenceUtils prefs;

    public VerifySystemKeyFragment() {
        // Required empty public constructor
    }

    public static VerifySystemKeyFragment newInstance() {
        return new VerifySystemKeyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_settings_verify_key, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            systemKey = bundle.getString("system_key");
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
                contact = (ContactEntity) bundle.getSerializable("contact");
            }
        }
        if (isFromSetup) {
            homeButton.setVisibility(View.GONE);
            tvSystemKey.setText("SYSTEM KEY: " + systemKey + "\nCONTACT FOR KEY RECOVERY: " + contact.getName());
            tvSystemKey.setTextSize(20);
            tvSystemKey.setTextAlignment(TEXT_ALIGNMENT_TEXT_START);

            setProgress(PROGRESS_SETUP_WIZARD,
                    WorkflowProgress.SetupWizard.VERIFY_SYSTEM_KEY.value,
                    WorkflowProgress.SetupWizard.values().length);
        } else {
            tvSystemKey.setText(systemKey);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        tvSystemKey = view.findViewById(R.id.textview_verify_key);
        btnOK = view.findViewById(R.id.button_ok);
        btnOK.setOnClickListener(this);
        btnEdit = view.findViewById(R.id.button_edit_system_key);
        btnEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnEdit) {
            if (isFromSetup) {
                // If this is the first time setup wizard workflow, we need to go all the way back
                // to setting the system key.
                bundle.putBoolean("isFromSetup", true);
                bundle.putString("fragmentName", "SetSystemKeyFragment");
                mListener.onButtonClicked(bundle);
            } else {
                backButton.performClick();
            }
        }
        if (v == btnOK) {
            BaseWireframe model = ((MainActivity)getActivity()).getFeatureModel();
            model.updateSystemKey(systemKey);
            if (isFromSetup) {
                //TODO is this a good place to send registration data to the cloud?

                // set preferences to isFirstTimeRun false
                prefs.setFirstTimeRun(false);
                bundle.putBoolean("isFromSetup", true);
                bundle.putString("fragmentName", "NumberOfUsersFragment");
            } else {
                bundle.putString("removeAllFragmentsUpToCurrent", "SystemManagerFragment");
            }
            mListener.onButtonClicked(bundle);
        }
    }
}