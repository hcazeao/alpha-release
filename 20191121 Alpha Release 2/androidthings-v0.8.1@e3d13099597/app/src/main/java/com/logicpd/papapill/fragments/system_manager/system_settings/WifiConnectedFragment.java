package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.enums.WorkflowProgress;
import com.logicpd.papapill.fragments.BaseHomeFragment;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class WifiConnectedFragment extends BaseHomeFragment {

    public static final String TAG = "WifiConnectedFragment";

    private Button btnDone;
    private boolean isFromSetup;
    private TextView tvNetworkName;

    public WifiConnectedFragment() {
        // Required empty public constructor
    }

    public static WifiConnectedFragment newInstance() {
        return new WifiConnectedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_settings_wireless_success, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }

            if (isFromSetup) {
                homeButton.setVisibility(View.GONE);
                setProgress(PROGRESS_SETUP_WIZARD,
                        WorkflowProgress.SetupWizard.WIFI_CONNECTED.value,
                        WorkflowProgress.SetupWizard.values().length);
            }

            String connectedNetwork = bundle.getString("connectedNetwork");
            tvNetworkName.setText(connectedNetwork);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvNetworkName = view.findViewById(R.id.textview_connected_ssid);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            if (isFromSetup) {
                bundle.putBoolean("isFromSetup", isFromSetup);
                bundle.putString("fragmentName", "SetSystemDateTimeFragment");
            } else {
                bundle.putString("removeAllFragmentsUpToCurrent", "SystemSettingsFragment");
            }
        }
        mListener.onButtonClicked(bundle);
    }
}
