package com.logicpd.papapill.fragments.system_manager.system_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;

/**
 * Blank fragment template
 *
 * @author alankilloren
 */
public class WifiConnectionFailedFragment extends BaseHomeFragment {

    public static final String TAG = "WifiConnectionFailedFragment";

    private Button btnDone;
    private boolean isFromSetup;
private TextView tvNetworkName;

    public WifiConnectionFailedFragment() {
        // Required empty public constructor
    }

    public static WifiConnectionFailedFragment newInstance() {
        return new WifiConnectionFailedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_system_settings_wireless_failure, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("isFromSetup")) {
                isFromSetup = bundle.getBoolean("isFromSetup");
            }

            if (isFromSetup) {
                homeButton.setVisibility(View.GONE);
            }

            String connectedNetwork = bundle.getString("connectedNetwork");
            tvNetworkName.setText(connectedNetwork);
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnDone = view.findViewById(R.id.button_done);
        btnDone.setOnClickListener(this);
        tvNetworkName = view.findViewById(R.id.textview_connected_ssid);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnDone) {
            if (isFromSetup) {
                bundle.putString("fragmentName", "SetSystemKeyFragment");
            } else {
                bundle.putString("removeAllFragmentsUpToCurrent", "SystemSettingsFragment");
            }
        }
        mListener.onButtonClicked(bundle);
    }
}
