package com.logicpd.papapill.fragments.user_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * RecoverSystemKeyFragment
 *
 * @author alankilloren
 */
public class RecoverUserPinFragment extends BaseHomeFragment {

    public static final String TAG = "RecoverUserPinFragment";

    private Button btnCancel, btnReset;
    private UserEntity user;

    public RecoverUserPinFragment() {
        // Required empty public constructor
    }

    public static RecoverUserPinFragment newInstance() {
        return new RecoverUserPinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_pin_recover, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("user")) {
                user = (UserEntity) bundle.getSerializable("user");
            }
        }

        Log.d(AppConstants.TAG, TAG + " displayed");
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnCancel = view.findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(this);
        btnReset = view.findViewById(R.id.button_reset);
        btnReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = this.getArguments();

        if (v == btnCancel) {
            backButton.performClick();
        }
        if (v == btnReset) {
            //TODO this needs to go through a cloud-related process to recover the user pin
            bundle.putString("fragmentName", "TempUserPinFragment");
            bundle.putString("authFragment", "ChangeUserPinFragment");
            mListener.onButtonClicked(bundle);
        }
    }
}