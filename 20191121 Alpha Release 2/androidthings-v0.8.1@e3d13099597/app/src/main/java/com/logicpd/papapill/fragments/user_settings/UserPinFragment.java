package com.logicpd.papapill.fragments.user_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logicpd.papapill.R;
import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.utils.TextUtils;

/**
 * Fragment entering for User Pin
 *
 * @author alankilloren
 */
public class UserPinFragment extends BaseHomeFragment {

    public static final String TAG = "UserPinFragment";

    private Button btnNext, btnForgot;
    private EditText etUserPIN;
    private TextView tvTitle;
    private UserEntity user;
    private String authFragment;
    private int authAttempts = 1;
    private boolean isFromMyMeds = false;
    private boolean isFromAlarmReceivedFragment = false;
    private DispenseEventsModel dispenseEventsModel;

    public UserPinFragment() {
        // Required empty public constructor
    }

    public static UserPinFragment newInstance() {
        return new UserPinFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_settings_user_pin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("authFragment")) {
                authFragment = bundle.getString("authFragment");
            }
            if (bundle.containsKey("isFromMyMeds")) {
                isFromMyMeds = bundle.getBoolean("isFromMyMeds");
            }
            if (bundle.containsKey("isFromAlarmReceivedFragment")) {
                isFromAlarmReceivedFragment = bundle.getBoolean("isFromAlarmReceivedFragment");
                if (bundle.containsKey("dispenseEventsModel")) {
                    dispenseEventsModel = (DispenseEventsModel) bundle.getSerializable("dispenseEventsModel");
                }
            }

            // Get user info from either dispense events model (if we got here from the alarm received
            // fragment) or the bundle (anywhere else).
            if (isFromAlarmReceivedFragment) {
                user = dispenseEventsModel.user;
            } else {
                user = (UserEntity) bundle.getSerializable("user");
            }
            if (user != null) {
                String s = "ENTER " + user.getUserName() + "'S USER PIN";
                tvTitle.setText(s);
            }
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnNext = view.findViewById(R.id.button_next);
        btnNext.setOnClickListener(this);
        TextUtils.disableButton(btnNext);
        btnForgot = view.findViewById(R.id.button_forgot_pin);
        btnForgot.setOnClickListener(this);

        etUserPIN = view.findViewById(R.id.edittext_user_pin);
        tvTitle = view.findViewById(R.id.textview_title);
        etUserPIN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start > 2) {
                    TextUtils.enableButton(btnNext);
                } else {
                    TextUtils.disableButton(btnNext);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etUserPIN.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (etUserPIN.getText().length() == 4 && actionId == EditorInfo.IME_ACTION_GO) {
                    btnNext.performClick();
                    handled = true;
                }
                return handled;
            }
        });

        etUserPIN.setFocusableInTouchMode(true);
        etUserPIN.requestFocus();
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = this.getArguments();
        if (v == backButton) {
            if (isFromMyMeds) {
                bundle.putString("fragmentName", "MyMedicationFragment");
            } else if (isFromAlarmReceivedFragment) {
                bundle.putString("fragmentName", "AlarmReceivedFragment");
            } else {
                bundle.putString("removeAllFragmentsUpToCurrent", "UserSettingsFragment");
            }
            mListener.onButtonClicked(bundle);
        }
        if (v == homeButton) {
            bundle.putString("fragmentName", "Home");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnForgot) {
            bundle.putSerializable("user", user);
            bundle.putString("fragmentName", "RecoverUserPinFragment");
            mListener.onButtonClicked(bundle);
        }
        if (v == btnNext) {
            if (etUserPIN.getText().toString().length() == 4 && etUserPIN.getText().toString().equals(user.getPin())) {
                user.setPin(etUserPIN.getText().toString());
                if (isFromAlarmReceivedFragment) {
                    bundle.putSerializable("dispenseEventsModel", dispenseEventsModel);
                } else {
                    bundle.putSerializable("user", user);
                }
                bundle.putString("fragmentName", authFragment);
                bundle.putBoolean("removeFragment", true);
                mListener.onButtonClicked(bundle);
            } else {
                bundle.putInt("authAttempts", authAttempts);
                if (authAttempts < 3) {
                    authAttempts += 1;
                }
                bundle.putSerializable("user", user);
                bundle.putString("fragmentName", "IncorrectUserPinFragment");
                mListener.onButtonClicked(bundle);
            }
            etUserPIN.setText("");
        }
    }
}