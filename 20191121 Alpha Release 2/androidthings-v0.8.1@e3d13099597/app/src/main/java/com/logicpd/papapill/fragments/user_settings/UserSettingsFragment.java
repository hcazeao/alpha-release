package com.logicpd.papapill.fragments.user_settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logicpd.papapill.R;
import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.interfaces.OnButtonClickListener;
import com.logicpd.papapill.room.entities.UserEntity;

/**
 * Fragment for User Settings
 *
 * @author alankilloren
 */
public class UserSettingsFragment extends BaseHomeFragment {

    public static final String TAG = "UserSettingsFragment";

    private Button btnFont, btnVoice, btnColor, btnPin;
    private UserEntity user;

    public UserSettingsFragment() {
        // Required empty public constructor
    }

    public static UserSettingsFragment newInstance() {
        return new UserSettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews(view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            user = (UserEntity) bundle.getSerializable("user");
        }
    }

    @Override
    protected void setupViews(View view) {
        super.setupViews(view);
        btnFont = view.findViewById(R.id.button_font_size);
        btnFont.setOnClickListener(this);
        btnVoice = view.findViewById(R.id.button_select_voice);
        btnVoice.setOnClickListener(this);
        btnColor = view.findViewById(R.id.button_color_theme);
        btnColor.setOnClickListener(this);
        btnPin = view.findViewById(R.id.button_user_pin);
        btnPin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Bundle bundle = new Bundle();

        if (v == btnColor) {
            bundle.putString("authFragment", "ThemeSettingFragment");
            bundle.putString("fragmentName", "SelectUserSettingsFragment");
            bundle.putSerializable("user", user);
        }
        if (v == btnFont) {
            bundle.putString("authFragment", "FontSettingFragment");
            bundle.putString("fragmentName", "SelectUserSettingsFragment");
            bundle.putSerializable("user", user);
        }
        if (v == btnVoice) {
            bundle.putString("authFragment", "VoiceSettingFragment");
            bundle.putString("fragmentName", "SelectUserSettingsFragment");
            bundle.putSerializable("user", user);
        }
        if (v == btnPin) {
            bundle.putString("authFragment", "ChangeUserPinFragment");
            bundle.putString("fragmentName", "SelectUserSettingsFragment");
            bundle.putSerializable("user", user);
        }
        mListener.onButtonClicked(bundle);

    }
}