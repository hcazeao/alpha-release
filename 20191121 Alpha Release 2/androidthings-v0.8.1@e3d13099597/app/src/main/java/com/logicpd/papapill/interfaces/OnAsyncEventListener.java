package com.logicpd.papapill.interfaces;

import javax.xml.transform.Result;

public interface OnAsyncEventListener {
    void OnPreExecute();
    void OnPostExecute(Object result);
}
