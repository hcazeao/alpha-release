package com.logicpd.papapill.interfaces;

public interface OnTimerListener {

    void onTimerFinished();
}
