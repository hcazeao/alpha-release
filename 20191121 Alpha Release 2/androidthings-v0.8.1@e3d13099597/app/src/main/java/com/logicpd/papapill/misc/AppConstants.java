package com.logicpd.papapill.misc;

/**
 * Constant variables used throughout the app
 *
 * @author alankilloren
 */
public class AppConstants {
    public static final String TAG = "Papapill";
    public static final int SPLASH_SCREEN_TIMEOUT = 3000;//amount of time (ms) to display splash screen
    public static String ANDROID_ID;

    // Defines a custom Intent Action for dispense events
    public static final String DISPENSE_EVENT_INTENT = "com.logicpd.papapill.DISPENSE_EVENT";
    // Defines a custom key for schedule items within a dispense event intent
    public static final String DISPENSE_EVENT_SCHEDULE = "com.logicpd.papapill.SCHEDULE";

    // TODO Beta: default system key for first time system setup and reset key. This may need to change in beta to get the key through cloud communication
    public static final String DEFAULT_SYSTEM_KEY = "1234";
    public static final String DISPENSE_EVENT_EARLY_DOSE_MODEL = "com.logicpd.papapill.EARLY_DOSE_MODEL";

    // Constants for interacting with proximity sensors
    public static final int PROXIMITY_READ_TIMER_MS = 1000;
    public static final int PROXIMITY_ERROR = 0;
}
