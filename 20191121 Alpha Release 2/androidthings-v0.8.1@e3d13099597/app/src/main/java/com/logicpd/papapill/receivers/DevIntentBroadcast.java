package com.logicpd.papapill.receivers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.logicpd.papapill.interfaces.IntentMsgInterface;

public class DevIntentBroadcast extends IntentBroadcast {

    private IntentMsgInterface mListener;

    public DevIntentBroadcast(IntentMsgInterface listener)
    {
        super(null);
        mListener = listener;
    }

    @Override
    public void onReceive(Context context,
                          Intent intent) {

        final PendingResult pendingResult = goAsync();
        AsyncTask<Intent, Integer, String> asyncTask = new AsyncTask<Intent, Integer, String>() {
            @Override
            protected String doInBackground(Intent... params) {
                // parse message content
                onParse(params[0]);
                mListener.handleIntent(mCommand, mState, mMsg, mVersion);
                pendingResult.finish();
                return "";
            }
        };
        asyncTask.execute(intent);
    }
}
