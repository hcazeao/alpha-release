package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.logicpd.papapill.room.entities.BinEntity;

import java.util.List;

@Dao
public interface BinDao extends IBaseDao {

    @Query("SELECT * from bins ORDER BY id ASC")
    List<BinEntity> getAll();

    @Query("select * from bins where id = :id")
    BinEntity get(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAll(List<BinEntity> entities);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(BinEntity entity);

    @Update
    int update(BinEntity entity);

    @Query("DELETE FROM bins")
    int deleteAll();

    @Query("DELETE FROM bins WHERE id = :id")
    int delete(int id);
}
