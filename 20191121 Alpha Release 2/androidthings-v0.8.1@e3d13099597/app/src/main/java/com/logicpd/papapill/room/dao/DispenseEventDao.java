package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;

import java.util.List;

@Dao
public interface DispenseEventDao extends IBaseDao {

    @Query("SELECT * FROM dispenseEvents ORDER BY id ASC")
    List<DispenseEventEntity> getAll();

    @Query("SELECT * FROM dispenseEvents WHERE result=0 ORDER BY id ASC")
    List<DispenseEventEntity> getAllActive();

    @Query("SELECT * FROM dispenseEvents WHERE id = :id")
    DispenseEventEntity get(int id);

    @Insert
    Long[] insertAll(List<DispenseEventEntity> entities);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    Long insert(DispenseEventEntity entity);

    @Update
    int update(DispenseEventEntity entity);

    @Query("DELETE FROM dispenseEvents")
    int deleteAll();

    @Query("DELETE FROM dispenseEvents WHERE id = :id")
    int delete(int id);

    // Joins
    @Query("SELECT d.id as [dispenseEventId], s.id as [scheduleId], m.id as [medicationId]" +
            ", d.dispenseType, d.eventReceivedDate, d.scheduledDate, d.dispenseStartDate, d.dispenseStopDate, d.feeling, d.pillsRemaining, d.result" +
            ", s.userId, s.dispenseTimeId, s.dispenseAmount, s.recurrence, s.scheduleDate, s.scheduleDay" +
            ", m.medicationName, m.medicationNickname, m.strengthValue, m.strengthMeasurement, m.dosageInstructions, m.timeBetweenDoses, m.maxUnitsPerDay, m.maxNumberPerDose, m.medicationQuantity, m.useByDate, m.fillDate, m.medicationLocation, m.medicationLocationName, m.isPaused, m.medicationScheduleType, m.patientName, m.medicationLabel " +
            "FROM dispenseEvents d " +
            "JOIN schedules s ON d.scheduleId = s.id " +
            "JOIN medications m ON s.medicationId = m.id ")
    List<JoinDispenseEventScheduleMedication> getJoinAll();

    @Query("SELECT d.id as [dispenseEventId], s.id as [scheduleId], m.id as [medicationId]" +
            ", d.dispenseType, d.eventReceivedDate, d.scheduledDate, d.dispenseStartDate, d.dispenseStopDate, d.feeling, d.pillsRemaining, d.result" +
            ", s.userId, s.dispenseTimeId, s.dispenseAmount, s.recurrence, s.scheduleDate, s.scheduleDay" +
            ", m.medicationName, m.medicationNickname, m.strengthValue, m.strengthMeasurement, m.dosageInstructions, m.timeBetweenDoses, m.maxUnitsPerDay, m.maxNumberPerDose, m.medicationQuantity, m.useByDate, m.fillDate, m.medicationLocation, m.medicationLocationName, m.isPaused, m.medicationScheduleType, m.patientName, m.medicationLabel " +
            "FROM dispenseEvents d " +
            "JOIN schedules s ON d.scheduleId = s.id " +
            "JOIN medications m ON s.medicationId = m.id " +
            "WHERE d.result = 0")
    List<JoinDispenseEventScheduleMedication> getJoinActive();

    @Query("SELECT DISTINCT s.userId " +
            "FROM dispenseEvents d " +
            "JOIN schedules s ON d.scheduleId = s.id " +
            "JOIN medications m ON s.medicationId = m.id " +
            "WHERE d.result = 0")
    List<Integer> getUserIdFromJoinActive();
}
