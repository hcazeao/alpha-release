package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.logicpd.papapill.room.entities.MedicationEntity;

import java.util.List;

@Dao
public interface MedicationDao extends IBaseDao {

    @Query("SELECT * FROM medications ORDER BY id ASC")
    List<MedicationEntity> getAll();

    @Query("SELECT * FROM medications WHERE isPaused=(:isPaused) ORDER BY id ASC")
    List<MedicationEntity> getAll(boolean isPaused);

    @Query("SELECT DISTINCT patientName FROM medications WHERE userId = :userId ORDER BY patientName ASC")
    List<String> getPatientNames(int userId);

    @Query("SELECT COUNT(*) FROM medications WHERE medicationName = :medicationName AND strengthMeasurement = :strengthMeasurement")
    int getCountByMedicationNameStrengthMeasurement(String medicationName, String strengthMeasurement);

    @Query("SELECT * FROM medications WHERE id = :id")
    MedicationEntity get(int id);

    @Query("SELECT * FROM medications ORDER BY medicationLocation ASC")
    List<MedicationEntity> getSortedBin();

    @Query("SELECT * FROM medications WHERE userId = :userId ORDER BY id ASC")
    List<MedicationEntity> getByUserId(int userId);

    /*
     * SCHEDULED or BOTH (NOT AS_NEEDED), IS_PAUSED
     */
    @Query("SELECT * FROM medications WHERE userId = :userId AND medicationScheduleType <> 1 AND isPaused=(:isPaused)  ORDER BY id ASC")
    List<MedicationEntity> getScheduledByUserId(int userId, boolean isPaused);

    /*
     * AS_NEEDED or BOTH (NOT SCHEDULED), IS_PAUSED
     */
    @Query("SELECT * FROM medications WHERE userId = :userId AND medicationScheduleType <> 2 AND isPaused =(:isPaused)  ORDER BY id ASC")
    List<MedicationEntity> getAsNeededByUserId(int userId, boolean isPaused);

    /*
     * Specific schedule type
     */
    @Query("SELECT * FROM medications WHERE userId = :userId AND medicationScheduleType = :scheduleType AND isPaused =(:isPaused) ORDER BY id ASC")
    List<MedicationEntity> getByUserIdScheduleType(int userId, int scheduleType, boolean isPaused);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAll(List<MedicationEntity> entities);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    Long insert(MedicationEntity entity);

    @Update
    int update(MedicationEntity entity);

    @Query("DELETE FROM medications")
    int deleteAll();

    @Query("DELETE FROM medications WHERE id = :id")
    int delete(int id);
}
