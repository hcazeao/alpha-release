package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.logicpd.papapill.room.entities.JoinContactSettingUser;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;
import com.logicpd.papapill.room.entities.NotificationSettingEntity;

import java.util.List;

@Dao
public interface NotificationSettingDao extends IBaseDao {

    @Query("SELECT * FROM settings ORDER BY id ASC")
    List<NotificationSettingEntity> getAll();

    @Query("SELECT * FROM settings WHERE id = :id")
    NotificationSettingEntity get(int id);

    @Query("SELECT * FROM settings WHERE userId = :userId AND contactId = :contactId ORDER BY id ASC")
    List<NotificationSettingEntity> getByUserIdContactId(int userId, int contactId);

    @Query("SELECT * FROM settings WHERE contactId = :contactId ORDER BY id ASC")
    List<NotificationSettingEntity> getByContactId(int contactId);

    @Query("SELECT * FROM settings WHERE userId = :userId ORDER BY id")
    List<NotificationSettingEntity> getByUserId(int userId);

    @Insert
    Long[] insertAll(List<NotificationSettingEntity> entities);

    @Insert
    Long insert(NotificationSettingEntity entity);

    @Update
    int update(NotificationSettingEntity entity);

    @Query("DELETE FROM settings")
    int deleteAll();

    @Query("DELETE FROM settings WHERE id = :id")
    int delete(int id);

    @Query("DELETE FROM settings WHERE contactId = :contactId")
    int deleteByContactId(int contactId);

    @Query("DELETE FROM settings WHERE userId = :userId AND contactId = :contactId")
    int deleteByUserIdContactId(int userId, int contactId);

    // JOINS
    @Query("SELECT contacts.id AS [contactId], contacts.name, contacts.textNumber, contacts.voiceNumber, contacts.email, contacts.category, contacts.isSelected, contacts.relationship, settings.id AS [notificationSettingId], settings.settingName, settings.isTextSelected, settings.isVoiceSelected, settings.isEmailSelected, users.id AS [userId], users.userNumber, users.userName, users.pin, users.patientName, users.audioVolume, users.fontSize, users.screenBrightness, users.voice, users.theme, users.recoveryContactId FROM contacts JOIN settings ON contacts.id = settings.contactId JOIN users ON settings.userId = users.id")
    List<JoinContactSettingUser> getAllJoin();
}
