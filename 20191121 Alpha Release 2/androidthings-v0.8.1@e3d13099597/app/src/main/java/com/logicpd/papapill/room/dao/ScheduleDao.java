package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.utils.TimestampConverter;

import java.util.Date;
import java.util.List;

@Dao
public interface ScheduleDao extends IBaseDao {

    @Query("SELECT * FROM schedules ORDER BY id ASC")
    List<ScheduleEntity> getAll();

    @Query("SELECT * FROM schedules WHERE id = :id")
    ScheduleEntity get(int id);

    @Query("SELECT COUNT(*) FROM schedules WHERE dispenseTimeId = :dispenseTimeId")
    int getCountByDispenseTimeId(int dispenseTimeId);

    @Query("SELECT * FROM schedules WHERE userId = :userId AND medicationId = :medicationId AND dispenseTimeId = :dispenseTimeId")
    ScheduleEntity getByUserIdMedicationIdDispenseTimeId(int userId, int medicationId, int dispenseTimeId);

    @Query("SELECT * FROM schedules WHERE medicationId = :medicationId")
    List<ScheduleEntity> getByMedicationId(int medicationId);

    @Insert
    Long[] insertAll(List<ScheduleEntity> entities);

    @Insert
    Long insert(ScheduleEntity entity);

    @Update
    int update(ScheduleEntity entity);

    @TypeConverters({TimestampConverter.class})
    @Query("UPDATE schedules SET nextProcessDate = dateTime(:nextProcessDate) WHERE id = :id")
    int updateNextProcessDate(int id, Date nextProcessDate);

    @Query("DELETE FROM schedules")
    int deleteAll();

    @Query("DELETE FROM schedules WHERE id = :id")
    int delete(int id);

    @Query("DELETE FROM schedules WHERE recurrence = :recurrence")
    int deleteByRecurrenceType(int recurrence);

    @Query("DELETE FROM schedules WHERE userId = :userId AND medicationId = :medicationId")
    int deleteByUserIdMedicationId(int userId, int medicationId);

    // JOINS

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id WHERE schedules.id = :id")
    JoinScheduleDispense getJoinById(int id);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id ORDER BY schedules.id ASC")
    List<JoinScheduleDispense> getAllJoin();

    @TypeConverters({TimestampConverter.class})
    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id JOIN medications ON schedules.medicationId = medications.id WHERE schedules.nextProcessDate > dateTime(:now) AND schedules.userId = :userId AND NOT(medications.isPaused) ORDER BY schedules.nextProcessDate ASC")
    List<JoinScheduleDispense> getNonPausedJoinNextSorted(int userId, Date now);

    @TypeConverters({TimestampConverter.class})
    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id JOIN medications ON schedules.medicationId = medications.id WHERE schedules.nextProcessDate <= datetime(:processDate) AND NOT(medications.isPaused) AND NOT(schedules.recurrence = 3 AND schedules.timesRecurred > 0) ORDER BY schedules.id ASC")
    List<JoinScheduleDispense> getJoinProcessDateBefore(Date processDate);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id WHERE schedules.userId = :userId AND scheduleDate IS NULL AND scheduleDay IS NULL ORDER BY dispenseTimeId ASC")
    List<JoinScheduleDispense> getJoinByUserIdNullDayDate(int userId);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id JOIN medications ON schedules.medicationId = medications.id WHERE schedules.userId = :userId AND scheduleDate IS NULL AND scheduleDay IS NULL AND NOT(medications.isPaused) ORDER BY dispenseTimeId ASC")
    List<JoinScheduleDispense> getNonPausedJoinByUserIdNullDayDate(int userId);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id WHERE schedules.userId = :userId AND scheduleDay = :scheduleDay ORDER BY dispenseTimeId ASC")
    List<JoinScheduleDispense> getJoinByUserIdScheduleDay(int userId, String scheduleDay);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id JOIN medications ON schedules.medicationId = medications.id WHERE schedules.userId = :userId AND scheduleDay = :scheduleDay AND NOT(medications.isPaused) ORDER BY dispenseTimeId ASC")
    List<JoinScheduleDispense> getNonPausedJoinByUserIdScheduleDay(int userId, String scheduleDay);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id WHERE schedules.userId = :userId AND scheduleDate = :scheduleDate ORDER BY dispenseTimeId ASC")
    List<JoinScheduleDispense> getJoinByUserIdScheduleDate(int userId, String scheduleDate);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id JOIN medications ON schedules.medicationId = medications.id WHERE schedules.userId = :userId AND scheduleDate = :scheduleDate AND NOT(medications.isPaused) ORDER BY dispenseTimeId ASC")
    List<JoinScheduleDispense> getNonPausedJoinByUserIdScheduleDate(int userId, String scheduleDate);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id WHERE dispenseTimeId = :dispenseTimeId")
    JoinScheduleDispense getJoinByDispenseTimeId(int dispenseTimeId);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id WHERE schedules.userId = :userId AND medicationId = :medicationId ORDER BY recurrence, dispenseTimeId ASC")
    List<JoinScheduleDispense> getJoinByUserIdMedicationId(int userId, int medicationId);

    @Query("SELECT schedules.id as [scheduleId], schedules.userId, schedules.medicationId, schedules.dispenseAmount, schedules.recurrence, schedules.timesRecurred, schedules.nextProcessDate, schedules.scheduleDate, schedules.scheduleDay, dispenseTimes.id as [dispenseTimeId], dispenseTimes.dispenseName, dispenseTimes.dispenseTime, dispenseTimes.isActive FROM schedules JOIN dispenseTimes ON schedules.dispenseTimeId = dispenseTimes.id JOIN medications ON schedules.medicationId = medications.id WHERE schedules.userId = :userId AND NOT(medications.isPaused) ORDER BY recurrence, dispenseTimeId ASC")
    List<JoinScheduleDispense> getNonPausedJoinByUserId(int userId);

    // stole this from DispenseEventDao because we need a userId
    @Query("SELECT d.id as [dispenseEventId], s.id as [scheduleId], m.id as [medicationId]" +
            ", d.dispenseType, d.eventReceivedDate, d.scheduledDate, d.dispenseStartDate, d.dispenseStopDate, d.feeling, d.pillsRemaining, d.result" +
            ", s.userId, s.dispenseTimeId, s.dispenseAmount, s.recurrence, s.scheduleDate, s.scheduleDay" +
            ", m.medicationName, m.medicationNickname, m.strengthValue, m.strengthMeasurement, m.dosageInstructions, m.timeBetweenDoses, m.maxUnitsPerDay, m.maxNumberPerDose, m.medicationQuantity, m.useByDate, m.fillDate, m.medicationLocation, m.medicationLocationName, m.isPaused, m.medicationScheduleType, m.patientName " +
            "FROM dispenseEvents d " +
            "JOIN schedules s ON d.scheduleId = s.id " +
            "JOIN medications m ON s.medicationId = m.id " +
            "WHERE s.userId = :userId AND d.result = 0")
    List<JoinDispenseEventScheduleMedication> getJoinActiveByUserId(int userId);

}
