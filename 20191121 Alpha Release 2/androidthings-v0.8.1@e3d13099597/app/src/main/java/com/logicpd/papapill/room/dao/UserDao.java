package com.logicpd.papapill.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.google.android.gms.vision.L;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.UserEntity;
//import androidx.room.OnConflictStrategy;

import java.util.List;

@Dao
public interface UserDao extends IBaseDao
{
    @Query("SELECT * FROM users ORDER BY id ASC")
    List<UserEntity> getAll();

    @Query("SELECT * FROM users WHERE id = :id")
    UserEntity get(int id);

    @Query("SELECT * FROM users WHERE username = :username")
    UserEntity getByUsername(String username);

    @Insert
    Long[] insertAll(List<UserEntity> entities);

    @Insert
    Long insert(UserEntity entity);

    @Update
    int update(UserEntity entity);

    @Query("DELETE FROM users")
    int deleteAll();

    @Query("DELETE FROM users WHERE id = :id")
    int delete(int id);
}
