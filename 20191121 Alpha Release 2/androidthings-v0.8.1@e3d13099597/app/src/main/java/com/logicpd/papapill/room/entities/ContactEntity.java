package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.Serializable;

@Entity(tableName = "contacts")
public class ContactEntity implements IBaseEntity, Serializable {

    @PrimaryKey(autoGenerate=true)
    @NonNull
    private int id;
    private int userId; // remove userId in beta, should retrieve from join of contact - notification setting - user

    @Nullable
    private String name;
    @Nullable
    private String textNumber;
    @Nullable
    private String voiceNumber;
    @Nullable
    private String email;
    @Nullable
    private String category;
    private boolean isSelected;
    private boolean isTextSelected;
    private boolean isVoiceSelected;
    private boolean isEmailSelected;
    private int relationship;
    private boolean isSystemContact;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // remove userId in beta, should retrieve from join of contact - notification setting - user
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextNumber() {
        return textNumber;
    }

    public void setTextNumber(String textNumber) {
        this.textNumber = textNumber;
    }

    public String getVoiceNumber() {
        return voiceNumber;
    }

    public void setVoiceNumber(String voiceNumber) {
        this.voiceNumber = voiceNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isTextSelected() {
        return isTextSelected;
    }

    public void setTextSelected(boolean textSelected) {
        isTextSelected = textSelected;
    }

    public boolean isVoiceSelected() {
        return isVoiceSelected;
    }

    public void setVoiceSelected(boolean voiceSelected) {
        isVoiceSelected = voiceSelected;
    }

    public boolean isEmailSelected() {
        return isEmailSelected;
    }

    public void setEmailSelected(boolean emailSelected) {
        isEmailSelected = emailSelected;
    }

    public int getRelationship() {
        return relationship;
    }

    public void setRelationship(int relationship) {
        this.relationship = relationship;
    }

    public boolean isSystemContact() {
        return isSystemContact;
    }

    public void setSystemContact(boolean systemContact) {
        isSystemContact = systemContact;
    }
}
