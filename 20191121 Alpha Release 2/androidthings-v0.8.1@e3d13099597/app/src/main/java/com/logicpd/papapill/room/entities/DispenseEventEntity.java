package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.room.utils.TimestampConverter;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "dispenseEvents",

        // Should this be a foreign key? If we are keeping history in this table, then we don't want
        // items in this table to be deleted when a schedule or medication is deleted. We would want
        // a snapshot or copy of all the columns at the time of the event.
        foreignKeys = { @ForeignKey(onDelete=CASCADE,
                        entity = ScheduleEntity.class,
                        parentColumns = "id",
                        childColumns = "scheduleId") },

        indices = { @Index("scheduleId") })

public class DispenseEventEntity implements IBaseEntity {

    @PrimaryKey(autoGenerate=true)
    @NonNull
    private int id;

    private int scheduleId;

    private int dispenseType;

    @Nullable
    @TypeConverters({TimestampConverter.class})
    private Date eventReceivedDate;

    @Nullable
    @TypeConverters({TimestampConverter.class})
    private Date scheduledDate;

    @Nullable
    @TypeConverters({TimestampConverter.class})
    private Date dispenseStartDate;

    @Nullable
    @TypeConverters({TimestampConverter.class})
    private Date dispenseStopDate;

    private int feeling;

    private int pillsRemaining;

    private int result;

    public DispenseEventEntity() { }

    public int getId() {
        return id;
    }
    public void setId( int id) {
        this.id = id;
    }

    public int getScheduleId() {
        return scheduleId;
    }
    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public void setDispenseType(int dispenseType) {
        this.dispenseType = dispenseType;
    }

    public int getDispenseType() {
        return this.dispenseType;
    }

    public Date getEventReceivedDate() { return eventReceivedDate; }
    public void setEventReceivedDate(Date eventReceivedDate) { this.eventReceivedDate = eventReceivedDate; }

    public Date getScheduledDate() { return scheduledDate; }
    public void setScheduledDate(Date scheduledDate) { this.scheduledDate = scheduledDate; }

    public Date getDispenseStartDate() { return dispenseStartDate; }
    public void setDispenseStartDate(Date dispenseStartDate) { this.dispenseStartDate = dispenseStartDate; }

    public Date getDispenseStopDate() { return dispenseStopDate; }
    public void setDispenseStopDate(Date dispenseStopDate) { this.dispenseStopDate = dispenseStopDate; }

    public int getFeeling() { return feeling; }
    public void setFeeling(int feeling) { this.feeling = feeling; }

    public int getPillsRemaining() { return pillsRemaining; }
    public void setPillsRemaining(int pillsRemaining) { this.pillsRemaining = pillsRemaining; }

    public int getResult() { return result; }
    public void setResult(int result) { this.result = result; }
}
