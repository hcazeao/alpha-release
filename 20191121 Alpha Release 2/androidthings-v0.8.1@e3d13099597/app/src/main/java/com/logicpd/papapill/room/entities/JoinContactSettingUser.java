package com.logicpd.papapill.room.entities;

import java.io.Serializable;

public class JoinContactSettingUser implements Serializable {
    private int contactId;
    private String name;
    private String textNumber;
    private String voiceNumber;
    private String email;
    private String category;
    private boolean isSelected;
    private int relationship;

    private int notificationSettingId;
    private String settingName;
    private boolean isTextSelected;
    private boolean isVoiceSelected;
    private boolean isEmailSelected;


    private int userId;
    private String userName;
    private int userNumber;
    private String pin;
    private String patientName;
    private int audioVolume;
    private int fontSize;
    private int screenBrightness;
    private int voice;
    private int theme;
    private int recoveryContactId;

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextNumber() {
        return textNumber;
    }

    public void setTextNumber(String textNumber) {
        this.textNumber = textNumber;
    }

    public String getVoiceNumber() {
        return voiceNumber;
    }

    public void setVoiceNumber(String voiceNumber) {
        this.voiceNumber = voiceNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getRelationship() {
        return relationship;
    }

    public void setRelationship(int relationship) {
        this.relationship = relationship;
    }

    public int getNotificationSettingId() {
        return notificationSettingId;
    }

    public void setNotificationSettingId(int notificationSettingId) {
        this.notificationSettingId = notificationSettingId;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public boolean isTextSelected() {
        return isTextSelected;
    }

    public void setTextSelected(boolean textSelected) {
        isTextSelected = textSelected;
    }

    public boolean isVoiceSelected() {
        return isVoiceSelected;
    }

    public void setVoiceSelected(boolean voiceSelected) {
        isVoiceSelected = voiceSelected;
    }

    public boolean isEmailSelected() {
        return isEmailSelected;
    }

    public void setEmailSelected(boolean emailSelected) {
        isEmailSelected = emailSelected;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(int userNumber) {
        this.userNumber = userNumber;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getAudioVolume() {
        return audioVolume;
    }

    public void setAudioVolume(int audioVolume) {
        this.audioVolume = audioVolume;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public int getScreenBrightness() {
        return screenBrightness;
    }

    public void setScreenBrightness(int screenBrightness) {
        this.screenBrightness = screenBrightness;
    }

    public int getVoice() {
        return voice;
    }

    public void setVoice(int voice) {
        this.voice = voice;
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public int getRecoveryContactId() {
        return recoveryContactId;
    }

    public void setRecoveryContactId(int recoveryContactId) {
        this.recoveryContactId = recoveryContactId;
    }
}
