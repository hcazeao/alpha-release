package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.TypeConverters;

import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.room.utils.TimestampConverter;

import java.io.Serializable;
import java.util.Date;

public class JoinDispenseEventScheduleMedication implements Serializable {

    // Dispense event columns
    private int dispenseEventId;
    private int scheduleId;
    private int dispenseType;

    @TypeConverters({TimestampConverter.class})
    private Date eventReceivedDate;
    @TypeConverters({TimestampConverter.class})
    private Date scheduledDate;
    @TypeConverters({TimestampConverter.class})
    private Date dispenseStartDate;
    @TypeConverters({TimestampConverter.class})
    private Date dispenseStopDate;
    private int feeling;
    private int pillsRemaining;
    private int result;

    // Schedule columns
    //private int scheduleId;
    private int userId;
    private int medicationId;
    private int dispenseTimeId;
    private int dispenseAmount;
    private int recurrence;
    private String scheduleDate;
    private String scheduleDay;

    // Medication columns
    //private int medicationId;
    private String medicationName;
    private String medicationNickname;
    //private int userId;
    private int strengthValue;
    private String strengthMeasurement;
    private String dosageInstructions;
    private int timeBetweenDoses;
    private int maxUnitsPerDay;
    private int maxNumberPerDose;
    private int medicationQuantity;
    private String useByDate;
    private String fillDate;
    private int medicationLocation;
    private String medicationLocationName;
    private boolean isPaused;
    private int medicationScheduleType; // MedScheduleTypeEnum -- 0:both, 1:as_needed, 2:scheduled
    private String patientName;
    private String medicationLabel;

    // Dispense Events
    public int getDispenseEventId() { return dispenseEventId; }
    public void setDispenseEventId(int id) { this.dispenseEventId = id; }

    public int getScheduleId() { return scheduleId; }
    public void setScheduleId(int id) {
        this.scheduleId = id;
    }

    public void setDispenseType(int dispenseType) {
        this.dispenseType = dispenseType;
    }

    public int getDispenseType() {
        return this.dispenseType;
    }

    public Date getEventReceivedDate() { return eventReceivedDate; }
    public void setEventReceivedDate(Date eventReceivedDate) { this.eventReceivedDate = eventReceivedDate; }

    public Date getScheduledDate() { return scheduledDate; }
    public void setScheduledDate(Date scheduledDate) { this.scheduledDate = scheduledDate; }

    public Date getDispenseStartDate() { return dispenseStartDate; }
    public void setDispenseStartDate(Date dispenseStartDate) { this.dispenseStartDate = dispenseStartDate; }

    public Date getDispenseStopDate() { return dispenseStopDate; }
    public void setDispenseStopDate(Date dispenseStopDate) { this.dispenseStopDate = dispenseStopDate; }

    public int getFeeling() { return feeling; }
    public void setFeeling(int feeling) { this.feeling = feeling; }

    public int getPillsRemaining() { return pillsRemaining; }
    public void setPillsRemaining(int pillsRemaining) { this.pillsRemaining = pillsRemaining; }

    public int getResult() { return result; }
    public void setResult(int result) { this.result = result; }

    // Schedule
    public int getUserId() { return userId; }
    public void setUserId(int userId) { this.userId = userId; }

    public int getDispenseTimeId() { return dispenseTimeId; }
    public void setDispenseTimeId(int dispenseTimeId) { this.dispenseTimeId = dispenseTimeId; }

    public int getDispenseAmount() { return dispenseAmount; }
    public void setDispenseAmount(int dispenseAmount) { this.dispenseAmount = dispenseAmount; }

    public int getRecurrence() { return recurrence; }
    public void setRecurrence(int scheduleType) { this.recurrence = scheduleType; }

    public String getScheduleDate() { return scheduleDate; }
    public void setScheduleDate(String scheduleDate) { this.scheduleDate = scheduleDate; }

    public String getScheduleDay() { return scheduleDay; }
    public void setScheduleDay(String scheduleDay) { this.scheduleDay = scheduleDay; }

    // Medication
    public int getMedicationId() { return medicationId; }
    public void setMedicationId(int id) { this.medicationId = id; }

    public String getMedicationName() { return medicationName; }
    public void setMedicationName(String medicationName) { this.medicationName = medicationName; }

    public String getMedicationNickname() { return medicationNickname; }
    public void setMedicationNickname(String medicationNickname) { this.medicationNickname = medicationNickname; }

    public int getStrengthValue() { return strengthValue; }
    public void setStrengthValue(int strengthValue) { this.strengthValue = strengthValue; }

    public String getStrengthMeasurement() { return strengthMeasurement; }
    public void setStrengthMeasurement(String strengthMeasurement) { this.strengthMeasurement = strengthMeasurement; }

    public String getDosageInstructions() { return dosageInstructions; }
    public void setDosageInstructions(String dosageInstructions) { this.dosageInstructions = dosageInstructions; }

    public int getTimeBetweenDoses() { return timeBetweenDoses; }
    public void setTimeBetweenDoses(int timeBetweenDoses) { this.timeBetweenDoses = timeBetweenDoses; }

    public int getMaxUnitsPerDay() { return (maxUnitsPerDay <= 0) ? 1: maxUnitsPerDay; }
    public void setMaxUnitsPerDay(int maxUnitsPerDay) {
        int MIN_VALUE = 1;
        this.maxUnitsPerDay = (maxUnitsPerDay < MIN_VALUE)?
                MIN_VALUE:
                maxUnitsPerDay;
    }

    public int getMaxNumberPerDose() { return maxNumberPerDose; }
    public void setMaxNumberPerDose(int maxNumberPerDose) { this.maxNumberPerDose = maxNumberPerDose; }

    public int getMedicationQuantity() { return medicationQuantity; }
    public void setMedicationQuantity(int medicationQuantity) { this.medicationQuantity = medicationQuantity; }

    public String getUseByDate() { return useByDate; }
    public void setUseByDate(String useByDate) { this.useByDate = useByDate; }

    public String getFillDate() { return fillDate; }
    public void setFillDate(String fillDate) { this.fillDate = fillDate; }

    public int getMedicationLocation() { return medicationLocation; }
    public void setMedicationLocation(int medicationLocation) { this.medicationLocation = medicationLocation; }

    public String getMedicationLocationName() { return medicationLocationName; }
    public void setMedicationLocationName(String medicationLocationName) { this.medicationLocationName = medicationLocationName; }

    public boolean isPaused() { return isPaused; }
    public void setPaused(boolean paused) { isPaused = paused; }

    public int getMedicationScheduleType() { return medicationScheduleType; }
    public void setMedicationScheduleType(int medicationScheduleType) { this.medicationScheduleType = medicationScheduleType; }

    public String getPatientName() { return patientName; }
    public void setPatientName(String patientName) { this.patientName = patientName; }

    public String getMedicationLabel() { return medicationLabel; }
    public void setMedicationLabel(String medicationLabel) { this.medicationLabel = medicationLabel; }

    // Decrement Helpers
    public void decrementPillsRemaining() {
        this.pillsRemaining--;
        if (this.pillsRemaining < 0) {
            this.pillsRemaining = 0;
        }
    }
    public void decrementMedicationQuantity() {
        this.medicationQuantity--;
        if (this.medicationQuantity < 0) {
            this.medicationQuantity = 0;
        }
    }

    public DispenseEventEntity getDispenseEventEntity() {
        DispenseEventEntity dispenseEventEntity = new DispenseEventEntity();
        dispenseEventEntity.setId(this.dispenseEventId);
        dispenseEventEntity.setScheduleId(this.scheduleId);
        dispenseEventEntity.setDispenseType(this.dispenseType);
        dispenseEventEntity.setEventReceivedDate(this.eventReceivedDate);
        dispenseEventEntity.setScheduledDate(this.scheduledDate);
        dispenseEventEntity.setDispenseStartDate(this.dispenseStartDate);
        dispenseEventEntity.setDispenseStopDate(this.dispenseStopDate);
        dispenseEventEntity.setFeeling(this.feeling);
        dispenseEventEntity.setPillsRemaining(this.pillsRemaining);
        dispenseEventEntity.setResult(this.result);
        return dispenseEventEntity;
    }

    public ScheduleEntity getScheduleEntity() {
        ScheduleEntity scheduleEntity = new ScheduleEntity(this.userId, this.medicationId, this.dispenseTimeId);
        scheduleEntity.setId(this.scheduleId);
        scheduleEntity.setScheduleDay(this.scheduleDay);
        scheduleEntity.setScheduleDate(this.scheduleDate);
        scheduleEntity.setRecurrence(this.recurrence);
        scheduleEntity.setDispenseAmount(this.dispenseAmount);
        return scheduleEntity;
    }

    public MedicationEntity getMedicationEntity() {
        MedicationEntity medicationEntity = new MedicationEntity(this.userId);
        medicationEntity.setId(this.medicationId);
        medicationEntity.setMedicationName(this.medicationName);
        medicationEntity.setMedicationNickname(this.medicationNickname);
        medicationEntity.setStrengthValue(this.strengthValue);
        medicationEntity.setStrengthMeasurement(this.strengthMeasurement);
        medicationEntity.setDosageInstructions(this.dosageInstructions);
        medicationEntity.setTimeBetweenDoses(this.timeBetweenDoses);
        medicationEntity.setMaxUnitsPerDay(this.maxUnitsPerDay);
        medicationEntity.setMaxNumberPerDose(this.maxNumberPerDose);
        medicationEntity.setMedicationQuantity(this.medicationQuantity);
        medicationEntity.setUseByDate(this.useByDate);
        medicationEntity.setFillDate(this.fillDate);
        medicationEntity.setMedicationLocation(this.medicationLocation);
        medicationEntity.setMedicationLocationName(this.medicationLocationName);
        medicationEntity.setPaused(this.isPaused);
        medicationEntity.setMedicationScheduleType(this.medicationScheduleType);
        medicationEntity.setPatientName(this.patientName);
        medicationEntity.setMedicationLabel(this.medicationLabel);
        return medicationEntity;
    }
}
