package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.logicpd.papapill.enums.MedScheduleTypeEnum;

import java.io.Serializable;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "medications",
        foreignKeys = { @ForeignKey(onDelete=CASCADE,
                entity = UserEntity.class,
                parentColumns = "id",
                childColumns = "userId")},

        indices = { @Index("userId")})
public class MedicationEntity implements IBaseEntity, Serializable {

    @PrimaryKey(autoGenerate=true)
    @NonNull
    private int id;

    @Nullable
    private String medicationName;
    @Nullable
    private String medicationNickname;
    private int userId;
    private int strengthValue;
    @Nullable
    private String strengthMeasurement;
    @Nullable
    private String dosageInstructions;
    private int timeBetweenDoses;
    private int maxUnitsPerDay;
    private int maxNumberPerDose;
    private int medicationQuantity;
    @Nullable
    private String useByDate;
    @Nullable
    private String fillDate;
    private int medicationLocation;
    @Nullable
    private String medicationLocationName;
    private boolean isPaused;
    private int medicationScheduleType; // MedScheduleTypeEnum -- 0:both, 1:as_needed, 2:scheduled
    @Nullable
    private String patientName;
    @Nullable
    private String medicationLabel;

    public MedicationEntity(int userId){
        this.userId = userId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getMedicationNickname() {
        return medicationNickname;
    }

    public void setMedicationNickname(String medicationNickname) {
        this.medicationNickname = medicationNickname;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStrengthValue() {
        return strengthValue;
    }

    public void setStrengthValue(int strengthValue) {
        this.strengthValue = strengthValue;
    }

    public String getStrengthMeasurement() {
        return strengthMeasurement;
    }

    public void setStrengthMeasurement(String strengthMeasurement) {
        this.strengthMeasurement = strengthMeasurement;
    }

    public String getDosageInstructions() {
        return dosageInstructions;
    }

    public void setDosageInstructions(String dosageInstructions) {
        this.dosageInstructions = dosageInstructions;
    }

    public int getTimeBetweenDoses() {
        return timeBetweenDoses;
    }

    public void setTimeBetweenDoses(int timeBetweenDoses) {
        this.timeBetweenDoses = timeBetweenDoses;
    }

    /*
     * MaxUnitsPerDay
     * floor limit of 1
     * no max limit
     */
    public int getMaxUnitsPerDay() {
        return (maxUnitsPerDay<=0)?
                1:
                maxUnitsPerDay;
    }

    /*
     * MaxUnitsPerDay
     * floor limit of 1
     * no max limit
     */
    public void setMaxUnitsPerDay(int maxUnitsPerDay) {
        int MIN_VALUE = 1;

        this.maxUnitsPerDay = (maxUnitsPerDay < MIN_VALUE)?
                                MIN_VALUE:
                                maxUnitsPerDay;
    }

    public int getMaxNumberPerDose() {
        return maxNumberPerDose;
    }

    public void setMaxNumberPerDose(int maxNumberPerDose) {
        this.maxNumberPerDose = maxNumberPerDose;
    }

    public int getMedicationQuantity() {
        return medicationQuantity;
    }

    public void setMedicationQuantity(int medicationQuantity) {
        this.medicationQuantity = medicationQuantity;
    }

    public String getUseByDate() {
        return useByDate;
    }

    public void setUseByDate(String useByDate) {
        this.useByDate = useByDate;
    }

    public String getFillDate() {
        return fillDate;
    }

    public void setFillDate(String fillDate) {
        this.fillDate = fillDate;
    }

    public int getMedicationLocation() {
        return medicationLocation;
    }

    public void setMedicationLocation(int medicationLocation) {
        this.medicationLocation = medicationLocation;
    }

    public String getMedicationLocationName() {
        return medicationLocationName;
    }

    public void setMedicationLocationName(String medicationLocationName) {
        this.medicationLocationName = medicationLocationName;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void setPaused(boolean paused) {
        isPaused = paused;
    }

    public int getMedicationScheduleType() {
        return medicationScheduleType;
    }

    public void setMedicationScheduleType(int medicationScheduleType) {
        this.medicationScheduleType = medicationScheduleType;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getMedicationLabel() { return medicationLabel; }

    public void setMedicationLabel(String label) { this.medicationLabel = label; }
}
