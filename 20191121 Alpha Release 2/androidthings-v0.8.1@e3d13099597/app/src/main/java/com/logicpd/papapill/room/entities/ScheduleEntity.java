package com.logicpd.papapill.room.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.logicpd.papapill.room.utils.TimestampConverter;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "schedules",
        foreignKeys = { @ForeignKey(onDelete=CASCADE,
                        entity = DispenseTimeEntity.class,
                        parentColumns = "id",
                        childColumns = "dispenseTimeId"),

                        @ForeignKey(onDelete=CASCADE,
                        entity = MedicationEntity.class,
                        parentColumns = "id",
                        childColumns = "medicationId"),

                        @ForeignKey(onDelete=CASCADE,
                        entity = UserEntity.class,
                        parentColumns = "id",
                        childColumns = "userId")},

        indices = { @Index("dispenseTimeId"),
                    @Index("medicationId"),
                    @Index("userId")})

public class ScheduleEntity implements IBaseEntity {

    @PrimaryKey(autoGenerate=true)
    @NonNull
    private int id;
    private int userId;
    private int medicationId;
    private int dispenseTimeId;
    private int dispenseAmount;
    private int recurrence; // ScheduleRecurrenceEnum
    private int timesRecurred; // This is the number of times this schedule has recurred (and has generated a dispense event) to date

    @Nullable
    @TypeConverters({TimestampConverter.class})
    private Date nextProcessDate;

    @Nullable
    private String scheduleDate;
    @Nullable
    private String scheduleDay;

    @Ignore
    public ScheduleEntity() { }

    public ScheduleEntity(int userId,
                          int medicationId,
                          int dispenseTimeId)
    {
        this.userId = userId;
        this.medicationId = medicationId;
        this.dispenseTimeId = dispenseTimeId;
    }

    public void setId( int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(int medicationId) {
        this.medicationId = medicationId;
    }

    public int getDispenseTimeId() {
        return dispenseTimeId;
    }

    public void setDispenseTimeId(int dispenseTimeId) {
        this.dispenseTimeId = dispenseTimeId;
    }

    public int getDispenseAmount() {
        return dispenseAmount;
    }

    public void setDispenseAmount(int dispenseAmount) {
        this.dispenseAmount = dispenseAmount;
    }

    public int getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(int scheduleType) {
        this.recurrence = scheduleType;
    }

    public int getTimesRecurred() {
        return timesRecurred;
    }

    public void setTimesRecurred(int timesRecurred) {
        this.timesRecurred = timesRecurred;
    }

    public Date getNextProcessDate() {
        return nextProcessDate;
    }

    public void setNextProcessDate(Date nextProcessDate) {
        this.nextProcessDate = nextProcessDate;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getScheduleDay() {
        return scheduleDay;
    }

    public void setScheduleDay(String scheduleDay) {
        this.scheduleDay = scheduleDay;
    }

}
