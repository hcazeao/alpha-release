package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.dao.DispenseEventDao;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;

import java.util.ArrayList;
import java.util.List;

public class DispenseEventRepository extends BaseRepository{

    public DispenseEventRepository()
    {
        super();
    }

    public List<DispenseEventEntity> getActiveDispenseEvents() {
        DispenseEventEntity entity = new DispenseEventEntity();
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<DispenseEventEntity>)syncOp(CRUDEnum.QUERY_ALL_ACTIVE, list);
    }

    public List<JoinDispenseEventScheduleMedication> getJoinAll() {
        DispenseEventEntity entity = new DispenseEventEntity();
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<JoinDispenseEventScheduleMedication>)syncOp(CRUDEnum.QUERY_JOIN_ALL, list);
    }

    public List<JoinDispenseEventScheduleMedication> getJoinActive() {
        DispenseEventEntity entity = new DispenseEventEntity();
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<JoinDispenseEventScheduleMedication>)syncOp(CRUDEnum.QUERY_JOIN_ACTIVE, list);
    }

    public List<Integer> getUserIdsFromJoinActive() {
        return (List<Integer>)syncOp(CRUDEnum.QUERY_LIST_USER_ID_FROM_JOIN_ACTIVE, null);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<DispenseEventEntity> list = (List<DispenseEventEntity>)(List<?>) entities;
        switch (op)
        {
            case INSERT_ALL:
                return ((DispenseEventDao)dao).insertAll(list);

            case INSERT:
                return ((DispenseEventDao)dao).insert(list.get(0));

            case QUERY_ALL:
                return ((DispenseEventDao)dao).getAll();

            case QUERY_ALL_ACTIVE:
                return ((DispenseEventDao)dao).getAllActive();

            case QUERY_BY_ID:
                return ((DispenseEventDao)dao).get(list.get(0).getId());

            case UPDATE:
                return ((DispenseEventDao)dao).update(list.get(0));

            case DELETE:
                return ((DispenseEventDao)dao).delete(list.get(0).getId());

            case DELETE_ALL:
                return ((DispenseEventDao)dao).deleteAll();

            case QUERY_JOIN_ALL:
                return ((DispenseEventDao)dao).getJoinAll();

            case QUERY_JOIN_ACTIVE:
                return ((DispenseEventDao)dao).getJoinActive();

            case QUERY_LIST_USER_ID_FROM_JOIN_ACTIVE:
                return ((DispenseEventDao)dao).getUserIdFromJoinActive();
        }

        return false;
    }

    public IBaseDao getDAO()
    {
        return db.dispenseEventDao();
    }
}
