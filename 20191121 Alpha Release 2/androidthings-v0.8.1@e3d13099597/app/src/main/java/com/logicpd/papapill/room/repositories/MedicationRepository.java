package com.logicpd.papapill.room.repositories;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.MedScheduleTypeEnum;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.MedicationDao;
import com.logicpd.papapill.room.dao.UserDao;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;


public class MedicationRepository extends BaseRepository{

    public MedicationRepository()
    {
        super();
    }

    /*
     * Query by id -- 
     */
    public MedicationEntity read(int id)
    {
        MedicationEntity medicationEntity = new MedicationEntity(0);
        medicationEntity.setId(id);
        return (MedicationEntity)super.read(medicationEntity);
    }

    public List<MedicationEntity> readAll(MedPausedEnum isPaused)
    {
        MedicationEntity medicationEntity = new MedicationEntity(0);
        medicationEntity.setPaused(isPaused.value);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(medicationEntity);
        return (List<MedicationEntity>)syncOp(CRUDEnum.QUERY_ALL_PAUSED_OPTION, list);
    }

    public List<String> getPatientNames(int userId)
    {
        MedicationEntity medicationEntity = new MedicationEntity(userId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(medicationEntity);
        return (List<String>)syncOp(CRUDEnum.QUERY_PATIENT_NAME_BY_USER_ID, list);
    }

    public boolean isDuplicateMedicationFound(MedicationEntity medicationEntity)
    {
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(medicationEntity);
        return ((int)syncOp(CRUDEnum.QUERY_COUNT_BY_MEDICATION_NAME_STRENGTH_MEASUREMENT, list) > 0) ? true : false;
    }

    public  List<MedicationEntity> getByUserId(int userId)
    {
        MedicationEntity entity = new MedicationEntity(userId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<MedicationEntity>)syncOp(CRUDEnum.QUERY_BY_USER_ID, list);
    }

    public List<MedicationEntity> getScheduledByUserId(int userId,
                                                       MedPausedEnum isPaused)
    {
        MedicationEntity entity = new MedicationEntity(userId);
        entity.setPaused(isPaused.value);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<MedicationEntity>)syncOp(CRUDEnum.QUERY_SCHEDULED_BY_USER_ID, list);
    }

    public List<MedicationEntity> getAsNeededByUserId(int userId,
                                                       MedPausedEnum isPaused)
    {
        MedicationEntity entity = new MedicationEntity(userId);
        entity.setPaused(isPaused.value);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<MedicationEntity>)syncOp(CRUDEnum.QUERY_AS_NEEDED_BY_USER_ID, list);
    }

    public  List<MedicationEntity> getByUserIdScheduleType(int userId,
                                                           MedScheduleTypeEnum scheduleType,
                                                           MedPausedEnum isPaused)
    {
        MedicationEntity entity = new MedicationEntity(userId);
        entity.setMedicationScheduleType(scheduleType.value);
        entity.setPaused(isPaused.value);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (List<MedicationEntity>)syncOp(CRUDEnum.QUERY_BY_USER_ID_SCHEDULE_TYPE, list);
    }

    public  List<MedicationEntity> getByBin()
    {
        return (List<MedicationEntity>)syncOp(CRUDEnum.QUERY_SORT_BIN, null);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<MedicationEntity> list = (List<MedicationEntity>)(List<?>) entities;

        switch (op)
        {
            case INSERT_ALL:
                Long[] ids = ((MedicationDao)dao).insertAll(list);
                int i =0;
                for(MedicationEntity med : list) {
                    med.setId((int)(long)ids[i++]);
                }
                return ids;

            case INSERT:
                int id = (int)(long)((MedicationDao)dao).insert(list.get(0));
                list.get(0).setId(id);
                return id;

            case QUERY_ALL:
                return ((MedicationDao)dao).getAll();

            case QUERY_ALL_PAUSED_OPTION:
                return ((MedicationDao)dao).getAll(list.get(0).isPaused());

            case QUERY_SORT_BIN:
                return ((MedicationDao)dao).getSortedBin();

            case QUERY_COUNT_BY_MEDICATION_NAME_STRENGTH_MEASUREMENT:
                return ((MedicationDao)dao).getCountByMedicationNameStrengthMeasurement(list.get(0).getMedicationName(), list.get(0).getStrengthMeasurement());

            case QUERY_BY_ID:
                return ((MedicationDao)dao).get(list.get(0).getId());

            case QUERY_BY_USER_ID:
                return ((MedicationDao)dao).getByUserId(list.get(0).getUserId());

            case QUERY_PATIENT_NAME_BY_USER_ID:
                return ((MedicationDao)dao).getPatientNames(list.get(0).getUserId());

            case QUERY_SCHEDULED_BY_USER_ID:
                return ((MedicationDao)dao).getScheduledByUserId(list.get(0).getUserId(), list.get(0).isPaused());

            case QUERY_AS_NEEDED_BY_USER_ID:
                return ((MedicationDao)dao).getAsNeededByUserId(list.get(0).getUserId(), list.get(0).isPaused());

            case QUERY_BY_USER_ID_SCHEDULE_TYPE:
                return ((MedicationDao)dao).getByUserIdScheduleType(list.get(0).getUserId(), list.get(0).getMedicationScheduleType(), list.get(0).isPaused());

            case UPDATE:
                return ((MedicationDao)dao).update(list.get(0));

            case DELETE:
                return ((MedicationDao)dao).delete(list.get(0).getId());

            case DELETE_ALL:
                return ((MedicationDao)dao).deleteAll();
        }

        return false;
    }

    public IBaseDao getDAO()
    {
        return db.medicationDao();
    }
}
