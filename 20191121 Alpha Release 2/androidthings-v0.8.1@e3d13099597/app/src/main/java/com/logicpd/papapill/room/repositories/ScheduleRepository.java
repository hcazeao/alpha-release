package com.logicpd.papapill.room.repositories;

import android.graphics.Paint;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.ScheduleDao;
import com.logicpd.papapill.room.dao.UserDao;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ScheduleRepository extends BaseRepository{

    public ScheduleRepository()
    {
        super();
    }

    public boolean isDispenseTimeBeingUsed(int dispenseTimeId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(0, 0,dispenseTimeId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return ((int)syncOp(CRUDEnum.QUERY_COUNT_BY_DISPENSE_TIME_ID, list)>0)?true:false;
    }

    public List<JoinScheduleDispense> getJoin() {
        ScheduleEntity scheduleEntity = new ScheduleEntity();
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_JOIN_SCHEDULE_DISPENSE_TIME, list);
    }

    public List<JoinScheduleDispense> getJoinNextScheduledItems(int userId,
                                                                Date date) {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, 0,0);
        scheduleEntity.setNextProcessDate(date);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_BY_USER_ID_JOIN_SCHEDULE_DISPENSE_TIME_NEXT, list);
    }

    public List<JoinScheduleDispense> getJoinProcessingDateBefore(Date now) {
        ScheduleEntity scheduleEntity = new ScheduleEntity();
        scheduleEntity.setNextProcessDate(now);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_JOIN_SCHEDULE_DISPENSE_TIME_BEFORE, list);
    }

    public List<JoinScheduleDispense> getNonPausedJoinByUserIdDay(int userId,
                                                         String sDay)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, 0,0);
        scheduleEntity.setScheduleDay(sDay);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);

        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_NON_PAUSED_JOIN_BY_USER_ID_SCHEDULE_DAY, list);
    }

    public List<JoinScheduleDispense> getJoinByUserIdMedicationId(int userId,
                                                                  int medicationId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medicationId,0);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_JOIN_BY_USER_ID_MEDICATION_ID, list);
    }

    public JoinScheduleDispense getJoinById(int scheduleId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(0, 0,0);
        scheduleEntity.setId(scheduleId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (JoinScheduleDispense)syncOp(CRUDEnum.QUERY_JOIN_SCHEDULE_DISPENSE_TIME_BY_ID, list);
    }

    public List<JoinScheduleDispense> getNonPausedJoinByUserIdScheduleDate(int userId,
                                                                                String sDate)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, 0,0);
        scheduleEntity.setScheduleDate(sDate);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_NON_PAUSED_JOIN_BY_USER_ID_SCHEDULE_DATE, list);
    }

    public List<JoinScheduleDispense> getNonPausedJoinByUserIdNullDayDate(int userId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, 0,0);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_NON_PAUSED_JOIN_BY_USER_ID_NULL_DAY_DATE, list);
    }

    public List<JoinScheduleDispense> getNonPausedJoinByUserId(int userId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, 0,0);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinScheduleDispense>)syncOp(CRUDEnum.QUERY_NON_PAUSED_JOIN_BY_USER_ID, list);
    }

    public JoinScheduleDispense getJoinByDispenseTimeId(int dispenseTimeId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(0, 0,dispenseTimeId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (JoinScheduleDispense)syncOp(CRUDEnum.QUERY_JOIN_SCHEDULE_DISPENSE_TIME_BY_ID, list);
    }

    public List<ScheduleEntity> getByMedicationId(int medicationId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(0, medicationId,0);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<ScheduleEntity>)syncOp(CRUDEnum.QUERY_BY_MEDICATION_ID, list);
    }

    public ScheduleEntity getByUserIdMedicationIdDispenseTimeId(int userId,
                                                                int medicationId,
                                                                int dispenseId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medicationId,dispenseId);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (ScheduleEntity)syncOp(CRUDEnum.QUERY_BY_USER_ID_MEDICATION_ID_DISPENSE_TIME_ID, list);
    }

    public boolean deleteByUserIdMedicationId(int userId,
                                              int medicationId)
    {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, medicationId, 0);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        asyncOp(CRUDEnum.DELETE_BY_USER_ID_MEDICATION_ID, list);
        return true;
    }

    public int updateNextProcessDate(int scheduleId,
                                     Date nextProcessDate) {

        ScheduleEntity scheduleEntity = new ScheduleEntity(0, 0, 0);
        scheduleEntity.setId(scheduleId);
        scheduleEntity.setNextProcessDate(nextProcessDate);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (int)syncOp(CRUDEnum.UPDATE_NEXT_PROCESS_DATE, list);
    }

    public List<JoinDispenseEventScheduleMedication> getJoinActiveByUserId(int userId) {
        ScheduleEntity scheduleEntity = new ScheduleEntity(userId, 0, 0);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        return (List<JoinDispenseEventScheduleMedication>)syncOp(CRUDEnum.QUERY_JOIN_ACTIVE_BY_USER_ID, list);
    }

    public void deleteByRecurrenceType(ScheduleRecurrenceEnum type) {
        ScheduleEntity scheduleEntity = new ScheduleEntity(0, 0, 0);
        scheduleEntity.setRecurrence(type.value);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(scheduleEntity);
        asyncOp(CRUDEnum.DELETE_BY_RECURRENCE_TYPE, list);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<ScheduleEntity> list = (List<ScheduleEntity>)(List<?>) entities;
        switch (op)
        {
            case INSERT_ALL:
            Long[] ids = ((ScheduleDao)dao).insertAll(list);
            int i =0;
            for(ScheduleEntity item : list) {
                item.setId((int)(long)ids[i++]);
            }
            return ids;

            case INSERT:
            int id = (int)(long)((ScheduleDao)dao).insert(list.get(0));
            list.get(0).setId(id);
            return id;

            case QUERY_ALL:
                return ((ScheduleDao)dao).getAll();

            case QUERY_BY_ID:
                return ((ScheduleDao)dao).get(list.get(0).getId());

            case QUERY_COUNT_BY_DISPENSE_TIME_ID:
                return ((ScheduleDao)dao).getCountByDispenseTimeId(list.get(0).getDispenseTimeId());

            case QUERY_BY_USER_ID_MEDICATION_ID_DISPENSE_TIME_ID:
                return ((ScheduleDao)dao).getByUserIdMedicationIdDispenseTimeId(list.get(0).getUserId(), list.get(0).getMedicationId(), list.get(0).getDispenseTimeId());

            case QUERY_BY_USER_ID_JOIN_SCHEDULE_DISPENSE_TIME_NEXT:
                return ((ScheduleDao)dao).getNonPausedJoinNextSorted(list.get(0).getUserId(), list.get(0).getNextProcessDate());

            case QUERY_JOIN_SCHEDULE_DISPENSE_TIME_BEFORE:
                return ((ScheduleDao)dao).getJoinProcessDateBefore(list.get(0).getNextProcessDate());

            case QUERY_BY_MEDICATION_ID:
                return ((ScheduleDao)dao).getByMedicationId(list.get(0).getMedicationId());

            case UPDATE:
                return ((ScheduleDao)dao).update(list.get(0));

            case UPDATE_NEXT_PROCESS_DATE:
                return ((ScheduleDao)dao).updateNextProcessDate(list.get(0).getId(), list.get(0).getNextProcessDate());

            case DELETE:
                return ((ScheduleDao)dao).delete(list.get(0).getId());

            case DELETE_BY_RECURRENCE_TYPE:
                return ((ScheduleDao)dao).deleteByRecurrenceType(list.get(0).getRecurrence());

            case DELETE_BY_USER_ID_MEDICATION_ID:
                return ((ScheduleDao)dao).deleteByUserIdMedicationId(list.get(0).getUserId(), list.get(0).getMedicationId());

            case DELETE_ALL:
                return ((ScheduleDao)dao).deleteAll();

            // JOINS
            case QUERY_JOIN_SCHEDULE_DISPENSE_TIME_BY_ID:
                return ((ScheduleDao)dao).getJoinById(list.get(0).getId());

            case QUERY_JOIN_SCHEDULE_DISPENSE_TIME:
                return ((ScheduleDao)dao).getAllJoin();

            case QUERY_JOIN_BY_USER_ID_NULL_DAY_DATE:
                return ((ScheduleDao)dao).getJoinByUserIdNullDayDate(list.get(0).getUserId());

            case QUERY_NON_PAUSED_JOIN_BY_USER_ID_NULL_DAY_DATE:
                return ((ScheduleDao)dao).getNonPausedJoinByUserIdNullDayDate(list.get(0).getUserId());

            case QUERY_NON_PAUSED_JOIN_BY_USER_ID_SCHEDULE_DAY:
                return ((ScheduleDao)dao).getNonPausedJoinByUserIdScheduleDay(list.get(0).getUserId(), list.get(0).getScheduleDay());

            case QUERY_JOIN_BY_USER_ID_SCHEDULE_DAY:
                return ((ScheduleDao)dao).getJoinByUserIdScheduleDay(list.get(0).getUserId(), list.get(0).getScheduleDay());

            case QUERY_JOIN_BY_USER_ID_SCHEDULE_DATE:
                return ((ScheduleDao)dao).getJoinByUserIdScheduleDate(list.get(0).getUserId(), list.get(0).getScheduleDate());

            case QUERY_NON_PAUSED_JOIN_BY_USER_ID_SCHEDULE_DATE:
                return ((ScheduleDao)dao).getNonPausedJoinByUserIdScheduleDate(list.get(0).getUserId(), list.get(0).getScheduleDate());

            case QUERY_JOIN_BY_DISPENSE_TIME_ID:
                return ((ScheduleDao)dao).getJoinByDispenseTimeId(list.get(0).getDispenseTimeId());

            case QUERY_JOIN_BY_USER_ID_MEDICATION_ID:
                return ((ScheduleDao)dao).getJoinByUserIdMedicationId(list.get(0).getUserId(), list.get(0).getMedicationId());

            case QUERY_NON_PAUSED_JOIN_BY_USER_ID:
                return ((ScheduleDao)dao).getNonPausedJoinByUserId(list.get(0).getUserId());

            case QUERY_JOIN_ACTIVE_BY_USER_ID:
                return ((ScheduleDao)dao).getJoinActiveByUserId(list.get(0).getUserId());
        }

        return false;
    }

    public IBaseDao getDAO()
    {
        return db.scheduleDao();
    }
}
