package com.logicpd.papapill.room.repositories;

import android.util.Log;

import com.logicpd.papapill.App;
import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.room.AppDatabase;
import com.logicpd.papapill.room.dao.IBaseDao;
import com.logicpd.papapill.room.dao.UserDao;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends BaseRepository {

    public UserRepository()
    {
        super();
        TAG = this.getClass().getSimpleName();
    }

    @Override
    public boolean delete(IBaseEntity user){
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(((IBaseEntity) user));
        int num = (int)syncOp(CRUDEnum.DELETE, list);
        return num>0?true:false;
    }

    public UserEntity readById(int id)
    {
        UserEntity entity = new UserEntity();
        entity.setId(id);
        return (UserEntity)super.read(entity);
    }

    public UserEntity getByUsername(String username)
    {
        UserEntity entity = new UserEntity();
        entity.setUserName(username);
        List<IBaseEntity> list = new ArrayList<IBaseEntity>();
        list.add(entity);
        return (UserEntity) syncOp(CRUDEnum.QUERY_BY_USERNAME, list);
    }

    @Override
    public Object crudOp(IBaseDao dao,
                         CRUDEnum op,
                         List<IBaseEntity> entities)
    {
        if(null==dao)
            return null;

        List<UserEntity> list = (List<UserEntity>)(List<?>) entities;
        switch (op)
        {
            case INSERT_ALL:
                Long[] ids = ((UserDao)dao).insertAll(list);
                int i =0;
                for(UserEntity user : list) {
                    user.setId((int)(long)ids[i++]);
                }
                return ids;

            case INSERT:
                int id = (int)(long)((UserDao)dao).insert(list.get(0));
                list.get(0).setId(id);
                return id;

            case QUERY_ALL:
                return ((UserDao)dao).getAll();

            case QUERY_BY_ID:
                return ((UserDao)dao).get(list.get(0).getId());

            case QUERY_BY_USERNAME:
                return ((UserDao)dao).getByUsername(list.get(0).getUserName());

            case UPDATE:
                return ((UserDao)dao).update(list.get(0));

            case DELETE:
                return ((UserDao)dao).delete(list.get(0).getId());

            case DELETE_ALL:
                return ((UserDao)dao).deleteAll();
        }

        return false;
    }

    public IBaseDao getDAO()
    {
        return db.userDao();
    }
}
