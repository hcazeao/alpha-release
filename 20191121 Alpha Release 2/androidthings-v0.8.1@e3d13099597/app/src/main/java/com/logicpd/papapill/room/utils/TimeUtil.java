package com.logicpd.papapill.room.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtil {
    /*
     * working with date only (MM/DD/YYYY)
     */
    static private DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public static Date filterTime(Date date) {
        String nowString = df.format(date);
        return dateString2Date(nowString);
    }

    public static Boolean isDateToday(Date date) {
        // Restrict comparison to just the day
        return df.format(date).equals(df.format(new Date()));
    }

    public static Boolean isDateTomorrow(Date date) {
        Calendar day = Calendar.getInstance();
        day.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = day.getTime();
        // Restrict comparison to just the day
        return df.format(date).equals(df.format(tomorrow));
    }

    public static Boolean isDateSixDaysOrLessAway(Date date) {
        Calendar day = Calendar.getInstance();
        day.add(Calendar.DAY_OF_YEAR, 6);
        Date sixDaysAway = day.getTime();
        // Restrict comparison to just the day, -1 is before, 0 is equal to six days
        return df.format(date).compareTo(df.format(sixDaysAway)) < 1;
    }

    public static Boolean isDateOneWeekAway(Date date) {
        Calendar day = Calendar.getInstance();
        day.add(Calendar.DAY_OF_YEAR, 7);
        Date oneWeekAway = day.getTime();
        // Restrict comparisons to just days
        return df.format(date).equals(df.format(oneWeekAway));
    }

    public static Boolean isDateLessThanAYearAway(Date date) {
        Calendar day = Calendar.getInstance();
        day.add(Calendar.YEAR, 1);
        Date oneYearAway = day.getTime();
        // Restrict comparison to just the day, -1 is before, 0 is equal to oneYearAway
        return df.format(date).compareTo(df.format(oneYearAway)) < 0;
    }

    public static Date dateString2Date(String value) {

        if (value != null) {
            try {
                return df.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }

    public static String date2TimeString(Date date) {
        return df.format(date);
    }
}
