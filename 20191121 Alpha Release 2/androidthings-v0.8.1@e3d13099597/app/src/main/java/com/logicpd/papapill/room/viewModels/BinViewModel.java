package com.logicpd.papapill.room.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.logicpd.papapill.room.repositories.DataRepository;
import com.logicpd.papapill.room.entities.BinEntity;

//TODO implement view model ?
public class BinViewModel extends AndroidViewModel
{
  // private final LiveData<BinEntity> mObservableBin;
    public ObservableField<BinEntity> user = new ObservableField<>();

    private final int mBinId;

    public BinViewModel(@NonNull Application application,
                         DataRepository repository,
                         final int binId) {
        super(application);
        mBinId = binId;
    //    mObservableBin = repository.loadBin(mBinId);
    }
/*
    public LiveData<BinEntity> getObservableBin() {
        return mObservableBin;
    }

    public void setProduct(BinEntity binEntity) {
        this.user.set(binEntity);
    }*/

}
