package com.logicpd.papapill.services;

import android.app.Fragment;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import com.logicpd.papapill.data.AppConfig;
import com.logicpd.papapill.device.tinyg.TinyGDriver;
import com.logicpd.papapill.device.tinyg.commands.CommandHomeAll;
import com.logicpd.papapill.enums.CarouselPosCmdEnum;
import com.logicpd.papapill.enums.CarouselPosIntentEnum;
import com.logicpd.papapill.enums.IntentParamsEnum;
import com.logicpd.papapill.receivers.IntentBroadcast;
import com.logicpd.papapill.utils.PreferenceUtils;

import java.util.List;

/*
 * Execute CarouselPosService when carousel is expected to stay at a particular location.
 * 0. bin location defined in SharedPreference
 * 1. At idle mode (not dispensing)
 * 2. During LoadMeds; after bin move to door location.
 * 3. During ManualAccess; after bin move to door location.
 */
public class CarouselPosService extends IntentService
{
    private long carouselPosInterval = PreferenceUtils.DEFAULT_INTERVAL_SECOND;
    private String TAG;
    private boolean running = true;

    public CarouselPosService() {
        super("LevelingService");
        carouselPosInterval = PreferenceUtils.getCarouselPosInterval();
        TAG = this.getClass().getSimpleName();
    }

    /*
     * Execute when papapill is on idle (see above criterions)
     */
    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        running = false;
    }

    private CarouselPosCmdEnum command;
    private int binId;
    private TinyGDriver tinyG;

    /*
     * Run this loop forever between start -> EndCommand
     */
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (!AppConfig.getInstance().isTinyGAvailable) {
            // we are done !
            return;
        }
        running = true;
        tinyG = TinyGDriver.getInstance();
        command = (CarouselPosCmdEnum) intent.getSerializableExtra(CarouselPosIntentEnum.command.toString());
        binId = intent.getIntExtra(CarouselPosIntentEnum.binId.toString(), 0);

        while(running) {
            try {
                Log.v(TAG, String.format("onHandleIntent() command: %s binId: %d", command, binId));

                // check if there is a command running
                if(tinyG.isStateMachineIdle()) {
                    invokeMotionCommand();
                }

                // 1. Check carousel location once every second.
                Thread.sleep(carouselPosInterval); // 1 second by default
            }
            catch (Exception ex)
            {
                Log.e(TAG, "onHandleIntent ex:" + ex);
            }
        }
    }

    /*
     * Execute motion command (home or bin-move)
     * if location is not correct
     */
    private void invokeMotionCommand()
    {
        if(null==command ||
                null==tinyG)
        {
            Log.e(TAG, "invokeTinyG() command or tinyG invalid");
            return;
        }

        /*
         * Should I have concern on read delay and values (binId, command) ?
         */
        switch (command) {
            default:
            case home:
                tinyG.doMoveCarouselHome();
                break;

            case bin:
                tinyG.doMoveBin4User(binId);
                break;
        }
    }
}