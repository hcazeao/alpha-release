package com.logicpd.papapill.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.logicpd.papapill.App;
import com.logicpd.papapill.activities.MainActivity;
import com.logicpd.papapill.enums.DispenseEventResultEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.utils.DayOfWeekConverter;
import com.logicpd.papapill.room.utils.SimpleTimeConverter;
import com.logicpd.papapill.utils.CalendarUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MedicationScheduleService extends IntentService {

    private long medicationScheduleInterval = 5000;
    private String TAG;
    private boolean running = true;
    private ScheduleProcessor mScheduleProcessor;

    public MedicationScheduleService() {
        super("MedicationScheduleService");
        TAG = this.getClass().getSimpleName();
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        running = false;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mScheduleProcessor = new ScheduleProcessor();
        running = true;
        BaseWireframe model = new BaseWireframe(null);

        while(running) {
            try {
                Calendar currentDate = Calendar.getInstance();
                currentDate.setTime(new Date());

                Log.v(TAG, String.format("Running Medication Schedule Service... " +
                        "Current Datetime: " + currentDate.getTime()));

                // Query all schedule items with a next process date less than the current date time
                // AND where the medication is not currently paused.
                List<JoinScheduleDispense> schedules = model.getAllScheduledItemsBeforeNow(currentDate.getTime());
                for(JoinScheduleDispense schedule : schedules) {

                    Log.v(TAG, "Processing Schedule Id: " + schedule.getScheduleId() +
                            " - Next Process Date: " + schedule.getNextProcessDate());

                    // Process this schedule and update the db.
                    mScheduleProcessor.processSchedule(schedule, currentDate);
                }

                // Process all dispense events.
                mScheduleProcessor.processDispenseEvents(currentDate);

                Thread.sleep(medicationScheduleInterval);
            }
            catch (Exception ex)
            {
                Log.e(TAG, "onHandleIntent ex:" + ex);
            }
        }
    }
}
