package com.logicpd.papapill.services;

import android.content.Intent;
import android.util.Log;

import com.logicpd.papapill.App;
import com.logicpd.papapill.enums.DispenseEventResultEnum;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.misc.AppConstants;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.JoinDispenseEventScheduleMedication;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.repositories.DispenseEventRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.utils.DayOfWeekConverter;
import com.logicpd.papapill.room.utils.SimpleTimeConverter;
import com.logicpd.papapill.room.utils.TimeUtil;
import com.logicpd.papapill.utils.CalendarUtils;
import com.logicpd.papapill.wireframes.BaseWireframe;

import java.text.ParseException;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ScheduleProcessor {

    protected String TAG;
    public static final int NOTHING_TO_PROCESS = -1;

    public ScheduleProcessor() {
        TAG = this.getClass().getSimpleName();
    }

    /**
     * Process the dispense event and update the database
     * @param currentDate
     */
    public void processDispenseEvents(Calendar currentDate) {

        // Retrieve the list of all dispense events currently pending or "active." (ie. any row with
        // a 0 in the Result column).
        List<JoinDispenseEventScheduleMedication> dispenseEvents = new DispenseEventRepository().getJoinActive();

        // Initialize scheduleId to -1. We'll check this later to see if it has been written with
        // a real schedule Id. If so, we send an intent. If not, we do nothing.
        int scheduleId = NOTHING_TO_PROCESS;

        // Loop through the list of "active" dispense events (joined with schedule and medications)
        // queried from the database.
        for (JoinDispenseEventScheduleMedication dispenseEvent : dispenseEvents) {

            Log.d(TAG, "Processing Dispense Event: " + dispenseEvent.getDispenseEventId() +
                    " for " + dispenseEvent.getMedicationName() + " current state: " + dispenseEvent.getResult());

            // Get the datetime the dispense event was scheduled for.
            Calendar eventReceivedDate = Calendar.getInstance();
            eventReceivedDate.setTime(dispenseEvent.getEventReceivedDate());

            // If the dispense event is still in the future, do not send the intent.
            // If the dispense event is "passed" and not expired, send the intent. If expired,
            // update the db and skip the intent.
            boolean isPassed = false;
            boolean isExpired = false;
            if (eventReceivedDate.compareTo(currentDate) < 0) {
                isPassed = true;
            }
            eventReceivedDate.add(Calendar.MINUTE, 60);
            if (eventReceivedDate.compareTo(currentDate) < 0) {
                isExpired = true;
                // More than 60 minutes has passed, this dispense event has expired (missed dose)
                // Change the result column of this dispense event to EXPIRED.
                DispenseEventEntity dispenseEventEntity = dispenseEvent.getDispenseEventEntity();
                dispenseEventEntity.setResult(DispenseEventResultEnum.EXPIRED.value);

                // Update the dispense event in the database.
                new DispenseEventRepository().update(dispenseEventEntity);

                Log.d(TAG, "60 minutes has passed for this dispense event. " +
                        "Setting result to EXPIRED: " + dispenseEventEntity.getResult());
            }

            if(isPassed && !isExpired) {
                // dispense an intent
                if (dispenseEvent.getResult() == DispenseEventResultEnum.ACTIVE.value)
                    scheduleId = dispenseEvent.getScheduleId();
            }
        }

        /*
         * Send just 1 notification intent for N active dispenseEvents.
         * AlarmReceiveFragment will query and display options.
         * ScheduleId signify the 1st medication.
         */
        if(NOTHING_TO_PROCESS != scheduleId) {
            Log.d(TAG, "Broadcasting a dispense event intent for schedule Id: " + scheduleId);
            sendIntent(scheduleId);
        }

    }

    /**
     * Process the schedule item and update the database
     * @param schedule
     * @param currentDate
     */
    public void processSchedule(JoinScheduleDispense schedule,
                                 Calendar currentDate) throws ParseException {

        // Proceed only if the schedule we are trying to process is a recurring schedule (ie. not
        // an as-needed or a one-time dispense).
        if(schedule.getRecurrence() != ScheduleRecurrenceEnum.NONE.value) {

            // This calls a chain of functions which ultimately calculates the new "next process date"
            // column of the schedule and updates the database.
            Calendar previousNextProcessDate = updateScheduleTable(schedule, DispenseEventTypeEnum.SCHEDULED);

            // Once the schedule table has been updated (with the new next process date), insert a
            // new dispense event into the dispense events table for this schedule.
            createDispenseEvent(schedule.getScheduleId(),
                    schedule.getDispenseAmount(),
                    currentDate,
                    previousNextProcessDate,
                    DispenseEventTypeEnum.SCHEDULED);
        }
    }

    /**
     * Updates and persists new next process date based on recurrence.
     * @param schedule
     * @param dispenseType
     * @return
     */
    public Calendar updateScheduleTable(JoinScheduleDispense schedule, DispenseEventTypeEnum dispenseType) {

        // Before we bump the "next process date" column for this schedule, we save the date and
        // call it the "previous next process date". Then we update the "next process date" to the
        // NEW "next process date" by calling getNextProcessDate(). Yes, the naming is confusing...
        Calendar previousNextProcessDate = Calendar.getInstance();
        previousNextProcessDate.setTime(schedule.getNextProcessDate());

        // Call function to calculate next process date for this schedule.
        Calendar nextProcessDate = getNextProcessDate(schedule, dispenseType);

        // This actually persists the updated next process date to the database.
        schedule.setNextProcessDate(nextProcessDate.getTime());
        // Also increment the number of times this schedule has recurred.
        schedule.incrementTimesRecurred();
        ScheduleEntity scheduleEntity = schedule.getScheduleEntity();
        new ScheduleRepository().update(scheduleEntity);

        Log.d(TAG, "Updated nextProcessDate column from: " + previousNextProcessDate.getTime() +
                " to: " + nextProcessDate.getTime());

        return previousNextProcessDate;
    }

    /*
     * Find the nextProcessDate base on recurrence type
     * WARNING: monthly currently requires definition
     * INPUT: current nextProcessDate, schedule, hours, minutes
     * RETURN: Calendar value
     */
    public Calendar getNextProcessDate(JoinScheduleDispense schedule,
                                       DispenseEventTypeEnum dispenseType) {

        /*
         * TODO should normalize the database so nextProcessDate and dispenseTime don't duplicate
         * 1. nextProcessDate should be yyyy-MM-DD and dispenseTime already hh:mm a
         * 2. write a Query that will join and return the full date + time field.
         * 3. refactor below code: operate with calendar / date
         */
        // Retrieve the dispense time from the schedule in db and convert it to a calendar
        // object to work with.
        Date dispenseTime = SimpleTimeConverter.fromTimestamp(schedule.getDispenseTime());
        Calendar dispenseDate = Calendar.getInstance();
        dispenseDate.setTime(dispenseTime);
        int dispenseDateHrs = dispenseDate.get(Calendar.HOUR_OF_DAY);
        int dispenseDateMin = dispenseDate.get(Calendar.MINUTE);

        // current nextProcessDate, updated with dispense time (in case it is changed)
        Date yyyyMMDD = TimeUtil.filterTime(schedule.getNextProcessDate());
        Calendar nextProcessDate = Calendar.getInstance();
        nextProcessDate.setTime(yyyyMMDD);
        nextProcessDate.set(Calendar.HOUR_OF_DAY, dispenseDateHrs);
        nextProcessDate.set(Calendar.MINUTE, dispenseDateMin);

        return incrementNextProcessDate(nextProcessDate, schedule, dispenseType);
    }

    /**
     * Internal helper to increment the next process date based on recurrence.
     * @param nextProcessDate
     * @param schedule
     * @param dispenseType
     * @return
     */
    private Calendar incrementNextProcessDate(Calendar nextProcessDate,
                                              JoinScheduleDispense schedule,
                                              DispenseEventTypeEnum dispenseType) {

        Date date = nextProcessDate.getTime();
        Date now = new Date();

        // Update the next process date column in the schedule based on the recurrence.
        ScheduleRecurrenceEnum recurrence = ScheduleRecurrenceEnum.values()[schedule.getRecurrence()];

        switch (recurrence) {

            default:
            case DAILY:
                // If the next process date is sometime in the past (for example, resuming a
                // paused med after a while), we should make sure that the new next process date
                // is not in the past as well...
                if (date.compareTo(now) < 0) {
                    // This ensures the next process date is calculated from the current date
                    // plus the recurrence value (thus in the future).
                    CalendarUtils.setCalendarToNextDispenseTime(nextProcessDate,
                            nextProcessDate.get(Calendar.HOUR_OF_DAY),
                            nextProcessDate.get(Calendar.MINUTE));
                } else {
                    // Otherwise, the next process date is already in the future, meaning we are
                    // getting a scheduled med early. If this is the case, simply add 1 day to
                    // push it out even further into the future.
                    nextProcessDate.add(Calendar.DAY_OF_WEEK, 1);
                }
                break;

            case WEEKLY:
                // If the next process date is sometime in the past (for example, resuming a
                // paused med after a while), we should make sure that the new next process date
                // is not in the past as well...
                if (date.compareTo(now) < 0) {
                    // Get the day of the week (1-7).
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(DayOfWeekConverter.fromTimestamp(schedule.getScheduleDay()));
                    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

                    // This ensures the next process date is calculated from the current date
                    // plus the recurrence value (thus in the future).
                    CalendarUtils.setCalendarToNextDispenseTimeOnDayOfWeek(nextProcessDate,
                            nextProcessDate.get(Calendar.HOUR_OF_DAY),
                            nextProcessDate.get(Calendar.MINUTE),
                            dayOfWeek);
                } else {
                    // Otherwise, the next process date is already in the future, meaning we are
                    // getting a scheduled med early. If this is the case, simply add 1 week to
                    // push it out even further into the future.
                    nextProcessDate.add(Calendar.DAY_OF_WEEK, 7);
                }
                break;

            case MONTHLY:
                // do nothing as "monthly" meds are not recurring.
                break;

            case NONE:
                break;
        }
        return nextProcessDate;
    }

    /**
     * Adds a new dispense event to the dispense events table based on the following
     * information provided by caller.
     * @param scheduleId
     * @param dispenseAmount
     * @param currentDate
     * @param previousNextProcessDate
     * @param dispenseType
     * @return
     */
    public boolean createDispenseEvent(int scheduleId,
                                       int dispenseAmount,
                                       Calendar currentDate,
                                       Calendar previousNextProcessDate,
                                       DispenseEventTypeEnum dispenseType) {
        try {
            // Prepare the dispense event to be added.
            DispenseEventEntity dispenseEvent = new DispenseEventEntity();
            dispenseEvent.setScheduleId(scheduleId);
            dispenseEvent.setDispenseType(dispenseType.value);
            dispenseEvent.setEventReceivedDate(currentDate.getTime());
            dispenseEvent.setScheduledDate(previousNextProcessDate.getTime());
            dispenseEvent.setPillsRemaining(dispenseAmount);

            // Do not add a dispense event if the previous nextProcessDate on the schedule was
            // more than 1 hour in the past. The dose is already missed.
            previousNextProcessDate.add(Calendar.MINUTE, 60);
            if (previousNextProcessDate.compareTo(currentDate) < 0) {

                Log.d(TAG, "Previous nextProcessDate is > 1 hour in the past (already missed dose)... " +
                        "setting dispense event to expired");

                dispenseEvent.setResult(DispenseEventResultEnum.EXPIRED.value);
                new DispenseEventRepository().insert(dispenseEvent);
                return false;
            }

            Log.d(TAG, "Inserting new dispense event for schedule: " + scheduleId);

            // Now add the dispense event to the db.
            new DispenseEventRepository().insert(dispenseEvent);
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    /**
     * Send dispense event intent indicating its time to dispense a pill.
     * @param scheduleId
     */
    public void sendIntent(int scheduleId)
    {
        if(ScheduleProcessor.NOTHING_TO_PROCESS != scheduleId) {
            // Send an intent signaling to the system that there is a pending dispense and
            // we should show the alarm received screen.
            Intent broadcastIntent = new Intent(AppConstants.DISPENSE_EVENT_INTENT);
            broadcastIntent.putExtra(AppConstants.DISPENSE_EVENT_SCHEDULE, scheduleId);
            // Broadcast the intent to receiver in the main activity.
            App.getContext().sendBroadcast(broadcastIntent);
        }
    }
}
