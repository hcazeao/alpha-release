package com.logicpd.papapill.utils;

import java.util.Calendar;
import java.util.Date;

public class CalendarUtils {
    /**
     * Set a calendar to the next dispense time.
     * @param c
     * @param hours
     * @param minutes
     */
    public static void setCalendarToNextDispenseTime(Calendar c,
                                                     int hours,
                                                     int minutes) {

        // Right off the bat, we can set the calendar to the current date time since we know
        // the "next" dispense time cannot be in the past.
        Date now = new Date();
        c.setTime(now);

        // Now let us also assume that the next dispense time is today.
        Calendar dispenseTime = Calendar.getInstance();
        dispenseTime.setTime(now);
        dispenseTime.set(Calendar.HOUR_OF_DAY, hours);
        dispenseTime.set(Calendar.MINUTE, minutes);
        dispenseTime.set(Calendar.SECOND, 0);

        // If today's dispense time is less than the current calendar's time, then we were too late.
        // The dispense time has already passed for today, therefore it must occur tomorrow. Simply
        // add 1 day and set the hour and minute. Otherwise, if today's dispense time is greater
        // than the current calendar's time, then the dispense time occurs later on today and we
        // do not need to adjust the calendar.
        if(dispenseTime.compareTo(c) <= 0) {
            c.add(Calendar.DAY_OF_WEEK, 1);
        }
        c.set(Calendar.HOUR_OF_DAY, hours);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.SECOND, 0);
    }

    /**
     * Set a calendar to the next dispense time on a specific day of the week.
     * @param c
     * @param hours
     * @param minutes
     * @param dayOfWeek
     */
    public static void setCalendarToNextDispenseTimeOnDayOfWeek(Calendar c,
                                                                int hours,
                                                                int minutes,
                                                                int dayOfWeek) {

        // Again, we can set the calendar to the current date time since we know that whenever
        // the "next" dispense time is, it cannot be in the past.
        Date now = new Date();
        c.setTime(now);

        // Get the current day of week.
        int currentDayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        // If the current calendar day is equal to the day of week of the next dispense time,
        // we must check the dispense time to see if we missed it or not.
        if (currentDayOfWeek == dayOfWeek) {
            // Calling this function works out here since there are 2 possible outcomes. Either
            // we missed the dispense time for the day and calendar will be incremented by 1 day,
            // or we didn't and the calendar will stay on the current day. In both cases, the
            // calendar will be updated with dispense time's hours and minutes.
            setCalendarToNextDispenseTime(c, hours, minutes);
        }

        // Update the value.
        currentDayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        // If the calendar was incremented, then this conditional will return true. We must adjust
        // the calendar to the following week. Otherwise we are done.
        while(currentDayOfWeek != dayOfWeek) {
            c.add(Calendar.DAY_OF_WEEK, 1);
            currentDayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        }
        c.set(Calendar.HOUR_OF_DAY, hours);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.SECOND, 0);
    }

    /**
     * Converts from Java.Time day of week to Java.Util.Calendar day of week.
     * @param timeDayOfWeek
     * @return
     */
    public static int ConvertDayOfWeekTimeToCalendar(int timeDayOfWeek) {
        int calendarDayOfWeek;
        if (timeDayOfWeek >= 1 && timeDayOfWeek < 7) {
            calendarDayOfWeek = timeDayOfWeek + 1;
        } else {
            calendarDayOfWeek = 1;
        }
        return calendarDayOfWeek;
    }

    /**
     * Coverts from Java.Util.Calendar day of week to Java.Time day of week.
     * @param calendarDayOfWeek
     * @return
     */
    public static int ConvertDayOfWeekCalendarToTime(int calendarDayOfWeek) {
        int timeDayOfWeek;
        if (calendarDayOfWeek > 1 && calendarDayOfWeek <= 7) {
            timeDayOfWeek = calendarDayOfWeek - 1;
        } else {
            timeDayOfWeek = 7;
        }
        return timeDayOfWeek;
    }
}
