package com.logicpd.papapill.utils;

/**
 * Statistics created for use in Reliability testing, but will be useful later on in keeping track
 * of performance.
 */
public class StatsUtil {
    Integer successes;
    Integer failures;
    Boolean isRunningStatus;
    String  ipAddress = null;

    private static class LazyHolder {
        private static final StatsUtil INSTANCE = new StatsUtil();
    }

    public static StatsUtil getInstance() {
        return LazyHolder.INSTANCE;
    }

    public void resetStats() {
        successes = 0;
        failures = 0;
        isRunningStatus = true;
    }

    public void addSuccesses() { this.successes++;}
    public void addFailures() { this.failures++;}
    public Integer getSuccesses() {return this.successes;}
    public Integer getFailures() {return this.failures;}
    public void setIsRunningStatus(Boolean isRunning) {this.isRunningStatus = isRunning;}
    public Boolean getIsRunningStatus() {return this.isRunningStatus;}
    public void setIpAddress(String address) {ipAddress = address;}
    public String getIpAddress() {return ipAddress;}
}
