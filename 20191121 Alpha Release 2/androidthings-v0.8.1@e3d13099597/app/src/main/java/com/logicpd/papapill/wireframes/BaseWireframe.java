package com.logicpd.papapill.wireframes;

import android.app.Application;
import android.os.Bundle;

import com.logicpd.papapill.enums.CRUDEnum;
import com.logicpd.papapill.enums.MedPausedEnum;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.room.entities.ContactEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.entities.SystemEntity;
import com.logicpd.papapill.room.entities.UserEntity;
import com.logicpd.papapill.room.repositories.ContactRepository;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.MedicationRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.repositories.SystemRepository;
import com.logicpd.papapill.room.repositories.UserRepository;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseWireframe implements IWireframe, Serializable {
    protected Map<String, Object> map;
    protected BaseWireframe parent = null;

    public BaseWireframe(BaseWireframe parent)
    {
        map = new HashMap<String, Object>();
        this.parent = parent;
    }

    public Object getMapItem(String key) {
        if(map.containsKey(key))
            return map.get(key);

        return null;
    }

    public void addMapItem(String key,
                           Object item) {
        map.put(key, item);
    }

    public UserEntity getUser() {
        UserEntity user = (UserEntity) this.getMapItem("user");
        return user;
    }

    public int numOfUsers() {
        List<UserEntity> users = getUsers();
        if(null==users )
            return 0;

        return users.size();
    }

    /*
     * Medication
     */

    /*
     * Get current medication in workflow
     */
    public MedicationEntity getMedication() {
        MedicationEntity med = (MedicationEntity)this.getMapItem("medication");
        return med;
    }

    public boolean addMedication(MedicationEntity medication) {
        return new MedicationRepository().insert(medication);
    }

    public boolean updateMedication(MedicationEntity medication) {
        return new MedicationRepository().update(medication);
    }

    public int deleteMedication(MedicationEntity medication) {
        new MedicationRepository().delete(medication);
        return 1;
    }

    public int removeMedication(MedicationEntity medication) {
        return deleteMedication(medication);
    }

    public List<MedicationEntity> getMedications() {
        return (List<MedicationEntity>)(List<?>)new MedicationRepository().readAll();
    }

    public List<MedicationEntity> getMedicationsByBin() {
        return new MedicationRepository().getByBin();
    }

    public List<MedicationEntity> getMedicationsForUser(UserEntity user) {
        return new MedicationRepository().getByUserId(user.getId());
    }

    /*
     * Query medication that is scheduled or both
     */
    public List<MedicationEntity> getScheduledMedicationsForUser(UserEntity user,
                                                                 MedPausedEnum isPaused) {
        return new MedicationRepository().getScheduledByUserId(user.getId(), isPaused);
    }

    /*
     * Query medication that is as_needed or both
     */
    public List<MedicationEntity> getAsNeededMedicationsForUser(UserEntity user,
                                                                MedPausedEnum isPaused) {
        return new MedicationRepository().getAsNeededByUserId(user.getId(), isPaused);
    }

    public MedicationEntity getMedication(int id) {
        return new MedicationRepository().read(id);
    }

    /*
     * User
     */
    public List<UserEntity> getUsers() {
        return (List<UserEntity>)new UserRepository().syncOp(CRUDEnum.QUERY_ALL, null);
    }

    public UserEntity getUserByID(int userId) {
        return (UserEntity) new UserRepository().readById(userId);
    }

    public int updateUser(UserEntity user) {
        new UserRepository().update(user);
        return 1;
    }

    /*
     * Contact
     */

    public ContactEntity getContact() {
        ContactEntity contact = (ContactEntity)this.getMapItem("contact");
        return contact;
    }

    public List<ContactEntity> getContacts() {
        return (List<ContactEntity>)new ContactRepository().syncOp(CRUDEnum.QUERY_ALL, null);
    }

    /*
     * SystemKey
     */
    public String getSystemKey() {
        List<SystemEntity> list = (List<SystemEntity>)(List<?>)new SystemRepository().readAll();
        return (null != list && list.size() > 0) ?
                ((SystemEntity) list.get(0)).getSystemKey():
                null;
    }

    public int updateSystemKey(String systemKey) {
        SystemRepository  systemRepository = new SystemRepository();
        List<SystemEntity> list = (List<SystemEntity>) (List<?>) systemRepository.readAll();
        if (null != list && list.size() > 0) {
            SystemEntity systemEntity = list.get(0);
            systemEntity.systemKey = systemKey;
            systemRepository.update(systemEntity);
            return systemEntity.id;
        }
        return 0;
    }

    public String getSerialNumber() {
        List<SystemEntity> list = (List<SystemEntity>)(List<?>)new SystemRepository().readAll();
        return (null != list && list.size() > 0) ?
                list.get(0).serialNumber:
                null;
    }

    public String getHostName() {
        List<SystemEntity> list = (List<SystemEntity>)(List<?>)new SystemRepository().readAll();
        return (null != list && list.size() > 0) ?
                list.get(0).hostName:
                null;
    }

    public String getSharedAccessKey() {
        List<SystemEntity> list = (List<SystemEntity>)(List<?>)new SystemRepository().readAll();
        return (null != list && list.size() > 0) ?
                list.get(0).sharedAccessKey:
                null;
    }

    /*
     * Dispense Time
     */
    public int addDispenseTime(DispenseTimeEntity dispenseTime) {
        new DispenseTimeRepository().insert(dispenseTime);
        return 1;
    }

    public List<DispenseTimeEntity> getDispenseTimes(boolean isActiveOnly) {
        return (List<DispenseTimeEntity> )(isActiveOnly ?
                new DispenseTimeRepository().getByIsActiveOnly() :
                new DispenseTimeRepository().readAll());
    }

    public int updateDispenseTime(DispenseTimeEntity dispenseTime) {
        new DispenseTimeRepository().update(dispenseTime);
        return 1;
    }

    public int deleteDispenseTime(DispenseTimeEntity dispenseTime) {
        new DispenseTimeRepository().delete(dispenseTime);
        return 1;
    }

    /*
     * Schedule
     */
    public boolean addScheduleList(List<ScheduleEntity> listSchedules) {
        // async
        List<IBaseEntity> list = (List<IBaseEntity>)(List<?>)listSchedules;
        return new ScheduleRepository().insertAll(list);
    }

    public boolean addScheduleItem(ScheduleEntity entity) {
        // asyn
        return new ScheduleRepository().insert(entity);
    }

    public List<ScheduleEntity> getScheduleItemsByMedication(MedicationEntity medication) {
        return new ScheduleRepository().getByMedicationId(medication.getId());
    }

    public JoinScheduleDispense getScheduleItem(int id) {
        return new ScheduleRepository().getJoinById(id);
    }

    public List<JoinScheduleDispense> getAllScheduledItemsBeforeNow(Date now) {
        return new ScheduleRepository().getJoinProcessingDateBefore(now);
    }

    public List<JoinScheduleDispense> getNextScheduledItems(int userId,
                                                            Date date) {
        return new ScheduleRepository().getJoinNextScheduledItems(userId, date);
    }

    public JoinScheduleDispense getScheduleItemByDispenseTimeId(int dispenseTimeId) {
        return new ScheduleRepository().getJoinByDispenseTimeId(dispenseTimeId);
    }

    public List<JoinScheduleDispense> getNonPausedJoinScheduleDispenseByUserId(int userId) {
        return new ScheduleRepository().getNonPausedJoinByUserId(userId);
    }

    public ScheduleEntity getScheduleItem(UserEntity user,
                                          MedicationEntity medication,
                                          int dispenseTimeId) {
        return new ScheduleRepository().getByUserIdMedicationIdDispenseTimeId(user.getId(), medication.getId(), dispenseTimeId);
    }

    public int removeMedicationFromSchedule(UserEntity user, MedicationEntity medication) {
        new ScheduleRepository().deleteByUserIdMedicationId(user.getId(), medication.getId());
        return 1;
    }

    public int deleteScheduleItems(UserEntity user, MedicationEntity medication) {
        return removeMedicationFromSchedule(user, medication);
    }

    /*
     * 1) Override: goto next feature if possible
     * 2) for nested feature, goto parent feature
     * 3) worst case, go home
     */
    public String getNextFragmentName(String currentFragmentName) {
        if(null==parent)
            return parent.getNextFragmentName(currentFragmentName);

        return HomeFragment.class.getSimpleName();
    }

    /*
     * Please override below methods !!!
     */
    public Bundle createBundle() {return null;}

    public Bundle updateBundle(Bundle bundle,
                        String currentFragmentName) {return null;}

    public boolean isEndFragment(String fragmentName) {return false;}

    public void initialize() {

    }

    public void cleanup() {

    }
}
