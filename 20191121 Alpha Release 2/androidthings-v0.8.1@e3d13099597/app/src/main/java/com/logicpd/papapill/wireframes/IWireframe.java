package com.logicpd.papapill.wireframes;

import android.os.Bundle;

public interface IWireframe {

    String getNextFragmentName(String currentFragmentName);

    boolean isEndFragment(String fragmentName);

    void initialize();

    void cleanup();
}
