package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class AddAsNeededMed extends BaseWireframe {
    public static final String TAG = "AddAsNeededMed";

    public AddAsNeededMed(BaseWireframe parentModel) {
        super(parentModel);
    }

}
