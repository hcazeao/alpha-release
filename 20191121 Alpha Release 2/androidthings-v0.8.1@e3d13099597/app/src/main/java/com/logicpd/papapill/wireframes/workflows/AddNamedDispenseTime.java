package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.os.Bundle;
import android.util.Log;

import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.DispenseTimeSummaryFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.EditDispenseTimesFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectEditDispenseTimeFragment;

public class AddNamedDispenseTime extends BaseWireframe {
    public final static String TAG = "AddNamedDispenseTime";

    public enum AddNamedDispenseTimeEnum {
        ManageMeds {
            public String toString() {
                return ManageMedsFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        EditDispenseTimes {
            public String toString() {
                return EditDispenseTimesFragment.class.getSimpleName();
            }
        },
        DispenseTimeName {
            public String toString() {
                return DispenseTimeNameFragment.class.getSimpleName();
            }
        },
        DispenseTime {
            public String toString() {
                return DispenseTimeFragment.class.getSimpleName();
            }
        },
        DispenseTimeSummary {
            public String toString() {
                return DispenseTimeSummaryFragment.class.getSimpleName();
            }
        }
    }

    public AddNamedDispenseTime(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * Update bundle for next fragment
     */
    public String getNextFragmentName(String currentFragmentName) {
        String fragmentName = AddNamedDispenseTimeEnum.ManageMeds.toString();

        try {
            AddNamedDispenseTimeEnum valueEnum = AddNamedDispenseTimeEnum.valueOf(currentFragmentName);
            switch (valueEnum) {
                case ManageMeds:
                    return AddNamedDispenseTimeEnum.SystemKey.toString();

                case SystemKey:
                    return AddNamedDispenseTimeEnum.EditDispenseTimes.toString();

                case EditDispenseTimes:
                    return AddNamedDispenseTimeEnum.DispenseTimeName.toString();

                case DispenseTimeName:
                    return AddNamedDispenseTimeEnum.DispenseTime.toString();

                case DispenseTime:
                    return AddNamedDispenseTimeEnum.DispenseTimeSummary.toString();

                default:
                case DispenseTimeSummary:
                    return AddNamedDispenseTimeEnum.ManageMeds.toString();
            }
        }
        catch (Exception ex)
        {
            Log.e(TAG, "updateBundle() failed"+ ex);
        }
        return null;
    }

    public boolean isEndFragment(String fragmentName)
    {
        return (fragmentName.equals(AddNamedDispenseTimeEnum.DispenseTimeSummary.toString()))?
                true:
                false;
    }
}
