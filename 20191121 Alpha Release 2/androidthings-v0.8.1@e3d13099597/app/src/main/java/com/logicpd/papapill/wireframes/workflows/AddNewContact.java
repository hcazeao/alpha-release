package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.SystemManagerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.AddUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.AddUserPinFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactAddedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactEmailAddressFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactListFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactTextNumberFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactVoiceNumberFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditUserNicknameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.NotificationPreferencesFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.VerifyContactInfoFragment;

public class AddNewContact extends BaseWireframe {
    public static final String TAG = "AddNewContact";

    public AddNewContact(BaseWireframe parentModel) {
        super(parentModel);
    }

    public enum AddNewContactEnum {
        Home {
            public String toString() {
                return HomeFragment.class.getSimpleName();
            }
        },
        SystemManager {
            public String toString() {
                return SystemManagerFragment.class.getSimpleName();
            }
        },
        ManageUsers {
            public String toString() {
                return ManageUsersFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        ContactList {
            public String toString() {
                return ContactListFragment.class.getSimpleName();
            }
        },
        ContactName {
            public String toString() {
                return ContactNameFragment.class.getSimpleName();
            }
        },
        NotificationPreferences {
            public String toString() {
                return NotificationPreferencesFragment.class.getSimpleName();
            }
        },
        ContactTextNumber {
            public String toString() {
                return ContactTextNumberFragment.class.getSimpleName();
            }
        },
        ContactVoiceNumber {
            public String toString() {
                return ContactVoiceNumberFragment.class.getSimpleName();
            }
        },
        ContactEmailAddress  {
            public String toString() {
                return ContactEmailAddressFragment.class.getSimpleName();
            }
        },
        VerifyContactInfo {
            public String toString() {
                return VerifyContactInfoFragment.class.getSimpleName();
            }
        },
        ContactAdded {
            public String toString() {
                return ContactAddedFragment.class.getSimpleName();
            }
        }
    }
}
