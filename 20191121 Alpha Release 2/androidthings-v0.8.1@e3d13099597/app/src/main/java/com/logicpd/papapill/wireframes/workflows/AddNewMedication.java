package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;



public class AddNewMedication extends BaseWireframe {
    public static final String TAG = "AddNewMedication";

    public AddNewMedication(BaseWireframe parentModel) {
        super(parentModel);
    }
}
