package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.SystemManagerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.AddUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.AddUserPinFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditUserNicknameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.InvalidNicknameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.SelectContactPinRecoveryFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.UserAddedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.VerifyUserInfoFragment;

public class AddNewUser extends BaseWireframe {
    public static final String TAG = "AddNewUser";

    public AddNewUser(BaseWireframe parentModel) {
        super(parentModel);
    }

    public enum AddNewUserEnum {
        Home {
            public String toString() {

                return HomeFragment.class.getSimpleName();
            }
        },
        SystemManager {
            public String toString() {

                return SystemManagerFragment.class.getSimpleName();
            }
        },
        ManageUsers {
            public String toString() {

                return ManageUsersFragment.class.getSimpleName();
            }
        },

        SystemKey {
            public String toString() {

                return SystemKeyFragment.class.getSimpleName();
            }
        },
        AddUser {
            public String toString() {

                return AddUserFragment.class.getSimpleName();
            }
        },
        // Adduser -> invalid nickname
        InvalidNickname {
            public String toString() {

                return InvalidNicknameFragment.class.getSimpleName();
            }
        },

        EditUserNickname {
            public String toString() {

                return EditUserNicknameFragment.class.getSimpleName();
            }
        },
        // INVALID NICKNAME missing

        AddUserPin  {
            public String toString() {
                return AddUserPinFragment.class.getSimpleName();
            }
        },
        SelectContactPinRecovery {
            public String toString() {

                return SelectContactPinRecoveryFragment.class.getSimpleName();
            }
        },
        VerifyUserInfo {
            public String toString() {

                return VerifyUserInfoFragment.class.getSimpleName();
            }
        },
        UserAdded {
            public String toString() {

                return UserAddedFragment.class.getSimpleName();
            }
        }
    }
}
