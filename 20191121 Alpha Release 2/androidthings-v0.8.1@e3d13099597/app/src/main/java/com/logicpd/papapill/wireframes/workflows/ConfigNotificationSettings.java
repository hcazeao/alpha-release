package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactCategoryFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditNotificationSettingsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.NewUserSetupCompleteFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.NotificationContactsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.NotificationSettingsMessageFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.NotificationSettingsSavedFragment;

public class ConfigNotificationSettings extends BaseWireframe {
    public static String TAG = "ConfigNotificationSettings";

    public ConfigNotificationSettings(BaseWireframe parentModel) {
        super(parentModel);
    }


    // extends from AddNewUser wireframe
    public enum ConfigNotificationSettingsEnum {
        NotificationContact {
            public String toString() {

                return NotificationContactsFragment.class.getSimpleName();
            }
        },
        ContactCategory {
            public String toString() {

                return ContactCategoryFragment.class.getSimpleName();
            }
        },
        NotificationSettingsMessage {
            public String toString() {

                return NotificationSettingsMessageFragment.class.getSimpleName();
            }
        },
        EditNotificationSettings  {
            public String toString() {

                return EditNotificationSettingsFragment.class.getSimpleName();
            }
        },
        NotificationSettingsSaved  {
            public String toString() {
                return NotificationSettingsSavedFragment.class.getSimpleName();
            }
        },
        NewUserSetupComplete {
            public String toString() {
                return NewUserSetupCompleteFragment.class.getSimpleName();
            }
        }
    }
}
