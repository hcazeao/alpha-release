package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.SystemManagerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ConfirmDeleteMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ConfirmDeleteUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.SelectDeleteUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.UserDeletedFragment;

public class DeleteUser extends BaseWireframe {
    public static final String TAG = "DeleteUser";

    public DeleteUser(BaseWireframe parentModel) {
        super(parentModel);
    }

    public enum DeleteUserEnum {
        Home {
            public String toString() {
                return HomeFragment.class.getSimpleName();
            }
        },
        SystemManager {
            public String toString() {
                return SystemManagerFragment.class.getSimpleName();
            }
        },
        ManageUsers {
            public String toString() {
                return ManageUsersFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        SelectDeleteUser {
            public String toString() {
                return SelectDeleteUserFragment.class.getSimpleName();
            }
        },

        // SelectDeleteUser -> has med
        ConfirmDeleteMedFragment {
            public String toString() {
                return ConfirmDeleteMedsFragment.class.getSimpleName();
            }
        },

        // SelectDeleteUser -> no med
        ConfirmDeleteUser {
            public String toString() {
                return ConfirmDeleteUserFragment.class.getSimpleName();
            }
        },
        UserDeleted {
            public String toString() {
                return UserDeletedFragment.class.getSimpleName();
            }
        }
    }
}
