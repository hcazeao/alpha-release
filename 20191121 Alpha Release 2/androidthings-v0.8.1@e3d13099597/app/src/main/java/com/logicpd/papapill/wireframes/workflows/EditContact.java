package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import com.logicpd.papapill.fragments.BaseHomeFragment;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactAddedFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactEmailAddressFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactNameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactTextNumberFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ContactVoiceNumberFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.NotificationPreferencesFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.VerifyContactInfoFragment;

public class EditContact extends BaseWireframe {
    public static final String TAG = "EditContact";
    public EditContact(BaseWireframe parentModel) {
        super(parentModel);
    }

    // continue from ManageContacts
    public enum EditContactEnum {

        ContactName {
            public String toString() {
                return ContactNameFragment.class.getSimpleName();
            }
        },
        NotificationPreferences {
            public String toString() {
                return NotificationPreferencesFragment.class.getSimpleName();
            }
        },
        ContactTextNumber {
            public String toString() {
                return ContactTextNumberFragment.class.getSimpleName();
            }
        },
        ContactVoiceNumber {
            public String toString() {
                return ContactVoiceNumberFragment.class.getSimpleName();
            }
        },
        ContactEmailAddress {
            public String toString() {
                return ContactEmailAddressFragment.class.getSimpleName();
            }
        },
        VerifyContactInfo {
            public String toString() {
                return VerifyContactInfoFragment.class.getSimpleName();
            }
        },
        ContactAdded {
            public String toString() {
                return ContactAddedFragment.class.getSimpleName();
            }
        }
    }
}
