package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.os.Bundle;

import com.logicpd.papapill.enums.BundleEnums;
import com.logicpd.papapill.fragments.HomeFragment;
import com.logicpd.papapill.fragments.SystemKeyFragment;
import com.logicpd.papapill.fragments.system_manager.SystemManagerFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.ManageMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_medications.SelectUserForMedsFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.EditUserNicknameFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.ManageUsersFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.SelectEditUserFragment;
import com.logicpd.papapill.fragments.system_manager.manage_users.VerifyUserInfoFragment;

public class EditUser extends BaseWireframe {
    public final static String TAG = "EditUser";

    public EditUser(BaseWireframe parentModel) {
        super(parentModel);
    }

    public enum EditUserEnum {
        Home {
            public String toString() {
                return HomeFragment.class.getSimpleName();
            }
        },
        SystemManager {
            public String toString() {
                return SystemManagerFragment.class.getSimpleName();
            }
        },
        ManageUsers {
            public String toString() {
                return ManageUsersFragment.class.getSimpleName();
            }
        },
        SystemKey {
            public String toString() {
                return SystemKeyFragment.class.getSimpleName();
            }
        },
        SelectEditUser {
            public String toString() {
                return SelectEditUserFragment.class.getSimpleName();
            }
        },
        EditUser {
            public String toString() {
                return EditUserFragment.class.getSimpleName();
            }
        },
        EditUserNickname {
            public String toString() {
                return EditUserNicknameFragment.class.getSimpleName();
            }
        },
        VerifyUserInfo {
            public String toString() {
                return VerifyUserInfoFragment.class.getSimpleName();
            }
        }
    }

    /*
     * call this at ManageUsersFragment -- no sooner as the wireframe may suggest.
     */
    public Bundle createBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(BundleEnums.wireframe.toString(), this.getClass().getSimpleName());
        bundle.putString(BundleEnums.fragmentName.toString(), EditUserEnum.SystemKey.toString());
        bundle.putString(BundleEnums.endFragment.toString(), EditUserEnum.VerifyUserInfo.toString());
        bundle.putString(BundleEnums.homeFragment.toString(), EditUserEnum.Home.toString());
        return bundle;
    }

    public Bundle updateBundle(Bundle bundle,
                               String currentFragmentName) {
        String fragmentName = null;
        EditUserEnum valueEnum = EditUserEnum.valueOf(currentFragmentName);
        switch (valueEnum) {
            case Home:
                fragmentName = EditUserEnum.SystemManager.toString();
                break;

            case SystemManager:
                fragmentName = EditUserEnum.ManageUsers.toString();
                break;

            case ManageUsers:
                fragmentName = EditUserEnum.SystemKey.toString();
                break;

            case SystemKey:
                fragmentName = EditUserEnum.SelectEditUser.toString();
                break;

            case SelectEditUser:
                fragmentName = EditUserEnum.EditUser.toString();
                break;

            case EditUser:
                fragmentName = EditUserEnum.EditUserNickname.toString();
                break;

            case EditUserNickname:
                fragmentName = EditUserEnum.VerifyUserInfo.toString();
                break;

            case VerifyUserInfo:
                fragmentName = EditUserEnum.ManageUsers.toString();
                break;
        }
        bundle.putString(BundleEnums.fragmentName.toString(), fragmentName);
        return bundle;
    }

    public boolean isEndFragment(String fragmentName)
    {
        return (fragmentName.equals(EditUserEnum.VerifyUserInfo.toString()))?
                true:
                false;
    }
}
