package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

import android.util.Pair;

import com.logicpd.papapill.data.DispenseEventsModel;
import com.logicpd.papapill.enums.DispenseEventResultEnum;
import com.logicpd.papapill.enums.DispenseEventTypeEnum;
import com.logicpd.papapill.enums.ScheduleRecurrenceEnum;
import com.logicpd.papapill.fragments.my_medication.GetAsNeededDispenseAmountFragment;
import com.logicpd.papapill.fragments.my_medication.GetAsNeededMedicationFragment;
import com.logicpd.papapill.room.entities.DispenseEventEntity;
import com.logicpd.papapill.room.entities.DispenseTimeEntity;
import com.logicpd.papapill.room.entities.IBaseEntity;
import com.logicpd.papapill.room.entities.JoinScheduleDispense;
import com.logicpd.papapill.room.entities.MedicationEntity;
import com.logicpd.papapill.room.entities.ScheduleEntity;
import com.logicpd.papapill.room.repositories.DispenseTimeRepository;
import com.logicpd.papapill.room.repositories.ScheduleRepository;
import com.logicpd.papapill.room.utils.SimpleTimeConverter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GetAsNeededMedication extends BaseWireframe {
    public final static String TAG = "GetAsNeededMedication";
    private DispenseTimeEntity mDispenseTime;
    private ScheduleEntity mSchedule;
    private int mUserId;
    private MedicationEntity mMedication;
    private int mPillsNeeded;
    private DispenseEventEntity mDispenseEvent;
    private DispenseEventsModel mModel;
    public final String AS_NEEDED = "AS_NEEDED";

    public DispenseTimeEntity getDispenseTime() {
        return this.mDispenseTime;
    }

    public ScheduleEntity getSchedule() {
        return this.mSchedule;
    }

    public enum GetAsNeededMedicationEnum {
        GetAsNeededMedication {
            public String toString() {

                return GetAsNeededMedicationFragment.class.getSimpleName();
            }
        },
        GetAsNeededDispenseAmount {
            public String toString() {
                return GetAsNeededDispenseAmountFragment.class.getSimpleName();
            }
        }
    }

    public GetAsNeededMedication(BaseWireframe parentModel) {
        super(parentModel);
    }

    /*
     * clean up any schedule and dispenseTime rows that might have been left from previous
     */
    public void initialize() {
        cleanup();
    }

    /*
     * After dispense, it is necessary to remove schedule and dispenseTime
     */
    @Override
    public void cleanup() {
        // delete schedule with type == none
        new DispenseTimeRepository().deleteByName(AS_NEEDED);
        // delete dispenseTime with name 'now'
        new ScheduleRepository().deleteByRecurrenceType(ScheduleRecurrenceEnum.NONE);
    }

    /*
     * Create a dispense event model
     * - data and state are stored in this model
     */
    public void createScheduleDispenseTime(int userId,
                                           MedicationEntity med,
                                           int pillsNeeded) {
        /*
         * create a disposable dispenseTime, schedule for the purpose of dispense.
         * create a dispenseEvent that persists the history of this dispense.
         */

        mUserId = userId;
        mMedication = med;
        mPillsNeeded = pillsNeeded;

        int dispenseTimeId = createDispenseTime(userId, pillsNeeded);
        int scheduleId = createSchedule(userId, mMedication.getId(), dispenseTimeId);
    }

    /*
     * Create a disposable dispenseTime
     */
    private int createDispenseTime(int userId, int mPillsNeeded) {

        mDispenseTime = new DispenseTimeEntity();
        mDispenseTime.setUserId(userId);
        mDispenseTime.setDispenseName(AS_NEEDED);
        mDispenseTime.setActive(true);
        mDispenseTime.setDispenseAmount(mPillsNeeded);
        String time = SimpleTimeConverter.dateToTimestamp(new Date());
        mDispenseTime.setDispenseTime(time);
        new DispenseTimeRepository().insert(mDispenseTime, true);
        return mDispenseTime.getId();
    }

    private int createSchedule(int userId,
                               int medId,
                               int dispenseTimeId) {

        mSchedule = new ScheduleEntity(userId, medId, dispenseTimeId);
        mSchedule.setRecurrence(ScheduleRecurrenceEnum.NONE.value);
        mSchedule.setNextProcessDate(new Date());
        mSchedule.setDispenseAmount(mPillsNeeded);
        new ScheduleRepository().insert(mSchedule, true);
        return mSchedule.getId();
    }

/*    private int createDispenseEvent(int scheduleId) {
        mDispenseEvent = new DispenseEventEntity();
        mDispenseEvent.setResult(DispenseEventResultEnum.ACTIVE.value);
        mDispenseEvent.setDispenseStartDate(new Date());
        mDispenseEvent.setDispenseStopDate(new Date());
        mDispenseEvent.setDispenseType(DispenseEventTypeEnum.AS_NEEDED.value);
        return mDispenseEvent.getId();
    }*/
}
