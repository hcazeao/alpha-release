package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class GetSingleDoseEarly extends BaseWireframe {
    public static String TAG = "GetSingleDoseEarly";
    public GetSingleDoseEarly(BaseWireframe parentModel) {
        super(parentModel);
    }

}
