package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class GetVacationSupplyEarly extends BaseWireframe {
    public static final String TAG = "GetVacationSupplyEarly";
    public GetVacationSupplyEarly(BaseWireframe parentModel) {
        super(parentModel);
    }

}
