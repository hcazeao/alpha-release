package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class LoadDrawer extends BaseWireframe {
    public static final String TAG = "LoadDrawer";

    public LoadDrawer(BaseWireframe parentModel) {
        super(parentModel);
    }

}
