package com.logicpd.papapill.wireframes.workflows;

import com.logicpd.papapill.wireframes.BaseWireframe;

public class ManageCloudUserAccess extends BaseWireframe {
    public static String TAG = "ManageCloudUserAccess";

    public ManageCloudUserAccess(BaseWireframe parentModel) {
        super(parentModel);
    }

}
