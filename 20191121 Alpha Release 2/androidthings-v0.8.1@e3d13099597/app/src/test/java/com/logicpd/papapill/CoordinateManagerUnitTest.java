package com.logicpd.papapill;

import com.logicpd.papapill.device.tinyg.CoordinateManager;

import org.junit.Test;

import static org.junit.Assert.*;

public class CoordinateManagerUnitTest {
    @Test
    public void getShortestAngle_BoundsTest() {
        for (int targetAngle = 0; targetAngle < 360; targetAngle++) {
            for (int currentAngle = 0; currentAngle < 360; currentAngle++) {
                double result = CoordinateManager.getShortestAngle(currentAngle, targetAngle);
                assertTrue(-180 <= result && result < 180);
            }
        }
    }

    @Test
    public void getShortestAngle_Test() {
        for (int targetAngle = 0; targetAngle < 360; targetAngle++) {
            for (int currentAngle = 0; currentAngle < 360; currentAngle++) {

                int result = currentAngle + (int)CoordinateManager
                        .getShortestAngle(currentAngle, targetAngle);

                // Normalize the result
                while (result < 0) { result += 360; }
                while (result >= 360) { result -= 360; }

                assertEquals(result, targetAngle);
            }
        }
    }
}
