package com.logicpd.papapill;

import com.logicpd.papapill.enums.IntentCommandEnum;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class IntentCommandEnumTest {

    @Test
    public void commandPreDispenseIsSearchable() {
        IntentCommandEnum cmd = IntentCommandEnum.valueOf("CommandPreDispense");
        assertEquals(cmd, IntentCommandEnum.CommandPreDispense);
    }

    @Test
    public void commandHomeAllIsSearchable() {
        IntentCommandEnum cmd = IntentCommandEnum.valueOf("CommandHomeAll");
        assertEquals(cmd, IntentCommandEnum.CommandHomeAll);
    }

    @Test
    public void commandFullDispenseIsSearchable() {
        IntentCommandEnum cmd = IntentCommandEnum.valueOf("CommandFullDispense");
        assertEquals(cmd, IntentCommandEnum.CommandFullDispense);
    }

    @Test
    public void commandRotateCarouselIsSearchable() {
        IntentCommandEnum cmd = IntentCommandEnum.valueOf("CommandRotateCarousel");
        assertEquals(cmd, IntentCommandEnum.CommandRotateCarousel);
    }

    @Test
    public void CommandUnknownIsSearchable() {
        IntentCommandEnum cmd = IntentCommandEnum.valueOf("CommandUnknown");
        assertEquals(cmd, IntentCommandEnum.CommandUnknown);
    }
}