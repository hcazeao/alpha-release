/*
 * relief_valve.c
 *
 * Created: 9/5/2018 8:08:58 AM
 *  Author: patrick.little
 */

 #include "relief_valve.h"

 static uint32_t off_tick;

  void relief_valve_init(void)
 {
    PORTB.DIRSET = PIN1_bm;  //PB1 is output to relief valve
    PORTB.OUTCLR = PIN1_bm;  //valve is off by default
    off_tick = 0xaaaaaaaa;
 }

#define RELIEF_VALVE_ON_TIME    500 // milliseconds
 void pulse_relief_valve(void)
 {
    //our system is running a vacuum pump off the coolant output
    //also need to pulse the relief valve when turning off the vacuum
    PORTB.OUTSET = PIN1_bm;
    //rollover is okay
    off_tick = SysTickTimer_getValue() + RELIEF_VALVE_ON_TIME;
 }

 stat_t relief_valve_release_callback(void)
 {
    if (SysTickTimer_getValue() > off_tick)
    {
        PORTB.OUTCLR = PIN1_bm;     //turn off valve
        off_tick = UINT32_MAX;      //set to max so condition is false
    }
    return STAT_OK;
 }