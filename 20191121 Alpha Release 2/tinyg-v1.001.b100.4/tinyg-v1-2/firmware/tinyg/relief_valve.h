/*
 * relief_valve.h
 *
 * Created: 9/5/2018 8:11:56 AM
 *  Author: patrick.little
 */


#ifndef RELIEF_VALVE_H_
#define RELIEF_VALVE_H_

#include "tinyg.h"  //for stat_t used by config
#include "config.h" //includes config_app.h for cfg global struct
#include "util.h"   //for SysTickTimer_getValue

void relief_valve_init(void);
void pulse_relief_valve(void);
stat_t relief_valve_release_callback(void);

#endif /* RELIEF_VALVE_H_ */